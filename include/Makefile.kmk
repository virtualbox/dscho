# $Id$
## @file
# Some hacks to allow syntax and prerequisite include checking of headers.
# This makefile doesn't and shouldn't build successfully.
#

#
# Copyright (C) 2006-2007 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

DEPTH = ..
include $(KBUILD_PATH)/header.kmk

LIBRARIES = SyntaxVBoxIncludeR3 SyntaxVBoxIncludeR0 SyntaxVBoxIncludeRC

cpp_hdrs := \
	VBox/dbggui.h \
	VBox/settings.h \
	$(wildcard iprt/*_cpp.h iprt/cpp/*.h )

r3_only_hdrs := \
	VBox/vrdpapi.h \
        VBox/vrdpusb.h \
	VBox/VBoxHDD.h \
	VBox/VBoxHDD-Plugin.h \
	VBox/dbus.h \
	VBox/uvm.h \
	iprt/tcp.h \
	iprt/localipc.h \
	iprt/linux/sysfs.h \

# We omit a few headers which have platform specific issues or are templates.
hdrs := $(filter-out \
	VBox/VBoxGuest16.h \
	VBox/VBoxGL2D.h \
	VBox/WinNetConfig.h \
	VBox/dbus-calls.h \
	VBox/usblib-win.h \
	VBox/usblib-solaris.h \
	$(foreach os,$(filter-out $(KBUILD_TARGET),$(KBUILD_OSES)),iprt/$(os)/% VBox/$(os)/%) \
	$(xforeach arch,$(KBUILD_ARCHES),iprt/nocrt/$(arch)/%) \
	, $(wildcard VBox/*.h iprt/*.h iprt/*/*.h))

hdrs.r3 := $(filter-out , $(hdrs))
hdrs.r0 := $(filter-out $(cpp_hdrs) $(r3_only_hdrs), $(hdrs))
hdrs.gc := $(filter-out \
	VBox/VBoxGuestLib.h \
	VBox/gvm.h \
	iprt/thread.h \
	iprt/mem.h \
	iprt/alloc.h \
	iprt/alloca.h \
	$(cpp_hdrs) \
	$(r3_only_hdrs) \
	, $(hdrs))

SyntaxVBoxIncludeR3_TEMPLATE = VBOXMAINEXE
SyntaxVBoxIncludeR3_DEFS = VBOX_WITH_HGCM
SyntaxVBoxIncludeR3_SOURCES := \
	$(addprefix $(PATH_TARGET)/,$(subst .h,-c.c,     $(subst /,_,$(hdrs.r3)))) \
	$(addprefix $(PATH_TARGET)/,$(subst .h,-cpp.cpp, $(subst /,_,$(hdrs.r3))))

SyntaxVBoxIncludeR0_TEMPLATE = VBoxR0
SyntaxVBoxIncludeR0_DEFS = VBOX_WITH_HGCM
SyntaxVBoxIncludeR0_SOURCES := \
	$(addprefix $(PATH_TARGET)/,$(subst .h,-c.c,     $(subst /,_,$(hdrs.r0)))) \
	$(addprefix $(PATH_TARGET)/,$(subst .h,-cpp.cpp, $(subst /,_,$(hdrs.r0))))

SyntaxVBoxIncludeRC_TEMPLATE = VBoxRc
SyntaxVBoxIncludeRC_DEFS = VBOX_WITH_HGCM
SyntaxVBoxIncludeRC_SOURCES := \
	$(addprefix $(PATH_TARGET)/,$(subst .h,-c.c,     $(subst /,_,$(hdrs.gc)))) \
	$(addprefix $(PATH_TARGET)/,$(subst .h,-cpp.cpp, $(subst /,_,$(hdrs.gc))))


# Comment out the next line to simplify header correction.
VBOX_ROOT_INCLUDE_MAKEFILE = $(PATH_ROOT)/include/Makefile.kmk

include $(KBUILD_PATH)/footer.kmk


define def_hdr
$(eval flatname := $(subst /,_,$(basename $(hdr))))
$$(PATH_TARGET)/$(flatname)-cpp.cpp: $$(PATH_TARGET)/
	$(QUIET)$$(APPEND) -t -n $$@ '#include <$(hdr)>' 'int main(int argc, char **argv) {(void)argc; (void)argv; return 0;}'

$$(PATH_TARGET)/$(flatname)-c.c: $(VBOX_ROOT_INCLUDE_MAKEFILE)  | $$(PATH_TARGET)/
ifn1of ($(hdr), $(cpp_hdrs) VBox/intnetinline.h)
	$(QUIET)$$(APPEND) -t -n $$@ '#include <$(hdr)>' 'int main(int argc, char **argv) {(void)argc; (void)argv; return 0;}'
else
	$(QUIET)$$(APPEND) -t -n $$@ 'int main(int argc, char **argv) {(void)argc; (void)argv; return 0;}'
endif

$(subst .h,.o,$(notdir $(hdr)))::
	$$(MAKE) -f $(MAKEFILE) $(flatname)-c.o $(flatname)-cpp.o

endef

$(foreach hdr,$(hdrs), $(eval $(def_hdr)))

