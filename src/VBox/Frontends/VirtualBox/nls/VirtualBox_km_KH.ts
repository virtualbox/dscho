<!DOCTYPE TS>
<TS>
  <context>
    <name>@@@</name>
    <message>
      <source>English</source>
      <comment>Native language name</comment>
      <translation>ខ្មែរ</translation>
    </message>
    <message>
      <source>--</source>
      <comment>Native language country name (empty if this language is for all countries)</comment>
      <translation>--</translation>
    </message>
    <message>
      <source>English</source>
      <comment>Language name, in English</comment>
      <translation>Khmer</translation>
    </message>
    <message>
      <source>--</source>
      <comment>Language country name, in English (empty if native country name is empty)</comment>
      <translation>--</translation>
    </message>
    <message>
      <source>Oracle Corporation</source>
      <comment>Comma-separated list of translators</comment>
      <translation>Khoem Sokhem</translation>
    </message>
  </context>
  <context>
    <name>QApplication</name>
    <message>
      <source>Executable &lt;b>%1&lt;/b> requires Qt %2.x, found Qt %3.</source>
      <translation>អាច​ប្រតិបត្តិបាន &lt;b>%1&lt;/b> ទាមទារ Qt %2.x រក​ឃើញ Qt %3 ។</translation>
    </message>
    <message>
      <source>Incompatible Qt Library Error</source>
      <translation>កំហុស​បណ្ណាល័យ Qt មិន​ឆបគ្នា</translation>
    </message>
    <message>
      <source>VirtualBox - Error In %1</source>
      <translation>VirtualBox - កំហុស​នៅ​ក្នុង %1</translation>
    </message>
    <message>
      <source>&lt;html>&lt;b>%1 (rc=%2)&lt;/b>&lt;br/>&lt;br/></source>
      <translation>&lt;html>&lt;b>%1 (rc=%2)&lt;/b>&lt;br/>&lt;br/></translation>
    </message>
    <message>
      <source>Please try reinstalling VirtualBox.</source>
      <translation>វា​អាច​ជួយ​ឲ្យ​ដំឡើង VirtualBox ឡើង​វិញ ។</translation>
    </message>
    <message>
      <source>This error means that the kernel driver was either not able to allocate enough memory or that some mapping operation failed.</source>
      <translation>កំហុស​នេះ​មាន​ន័យ​ថា កម្មវិធី​បញ្ជា​ខឺណែល​មិនអាច​បម្រុង​ទុក​សតិ​គ្រប់គ្រាន់​ ឬ​បានបរាជ័យ​ក្នុងកា​រ​ផ្គូផ្គង​មួយ​ចំនួន ។</translation>
    </message>
    <message>
      <source>The VirtualBox Linux kernel driver (vboxdrv) is either not loaded or there is a permission problem with /dev/vboxdrv. Please reinstall the kernel module by executing&lt;br/>&lt;br/>  &lt;font color=blue>'/etc/init.d/vboxdrv setup'&lt;/font>&lt;br/>&lt;br/>as root. Users of Ubuntu, Fedora or Mandriva should install the DKMS package first. This package keeps track of Linux kernel changes and recompiles the vboxdrv kernel module if necessary.</source>
      <translation>កម្ម​វិធី​ខឺ​ណែល​លីនុច​ VirtualBox (vboxdrv) មិនត្រូវ​បាន​ផ្ទុក​ ឬ​មាន​បញ្ហា​សិទ្ធិ​ជាមួយ​ /dev/vboxdrv ។ សូម​ដំឡើង​ឡើង​វិញ​ម៉ូ​​ឌុល​ខឺ​ណែល​​​ដោយ​ការ​ប្រតិបត្តិ​​ &lt;br/>&lt;br/>  &lt;font color=blue>'/etc/init.d/vboxdrv ដំឡើ​ង&lt;/font>&lt;br/>&lt;br/>​ដូច Root ។ អ្នក​ប្រើ​យូប៊ុន​ទូ​ Fedora ឬ​ Mandriva គួរ​តែដំឡើង​កញ្ចប់​ដំបូង​របស់​ DKMS ។ កញ្ចប់​នេះ​​តាម​ដាន​​រក្សា​ទុក​​នៃ​ការ​ផ្លាស់​ប្តូរ​ខឺ​ណែល​លីនុច​ និងចង​​ក្រង​ឡើងវិញ​នៃ​​ម៉ូឌុល​ខឺណែល​របស់​ vboxdrv ប្រសិន​បើ​ចាំ​បាច់ ។</translation>
    </message>
    <message>
      <source>The VirtualBox kernel modules do not match this version of VirtualBox. The installation of VirtualBox was apparently not successful. Please try completely uninstalling and reinstalling VirtualBox.</source>
      <translation>ម៉ូ​ឌុល​ខឺ​ណែល​របស់​ VirtualBox មិន​ផ្គូផ្គង​​កំណែ​របស់​ VirtualBox នេះ​ទេ​ ។ ជាក់​ស្តែង​ការ​ដំឡើង VirtualBox ​​​មិន​ទទួល​បាន​ជោគជ័យ​នោះ​ទេ ។ សូម​ព្យាយាម​លុប​​ ហើយ​​ដំឡើង​ VirtualBox ឡើង​វិញ ។</translation>
    </message>
    <message>
      <source>The VirtualBox kernel modules do not match this version of VirtualBox. The installation of VirtualBox was apparently not successful. Executing&lt;br/>&lt;br/>  &lt;font color=blue>'/etc/init.d/vboxdrv setup'&lt;/font>&lt;br/>&lt;br/>may correct this. Make sure that you do not mix the OSE version and the PUEL version of VirtualBox.</source>
      <translation>ម៉ូឌុល​ខឺ​ណែល​របស់​ VirtualBox មិន​ផ្គូផ្គង​កំណែ​របស់​ VirtualBox នេះទេ ។ ជាក់​ស្តែង​ការ​ដំឡើង​របស់​ VirtualBox មិន​ទទួល​បាន​ជោគជ័យ​នោះ​ទេ ។ ការ​ប្រតិបត្តិ​&lt;br/>&lt;br/>  &lt;font color=blue>'/etc/init.d/vboxdrv setup'&lt;/font>&lt;br/>&lt;br/>នេះ​អាច​ត្រឹមត្រូវ​ ។ សូម​ប្រាកដ​ថា​ អ្នក​មិនបញ្ចូល​កំណែ​ OSE និង​​កំណែ​ PUEL របស់​ VirtualBox ចូល​គ្នា​ឡើយ ។</translation>
    </message>
    <message>
      <source>Make sure the kernel module has been loaded successfully.</source>
      <translation>ប្រាកដ​ថា ​ម៉ូឌុល​ខឺណែល​ត្រូវ​បាន​ផ្ទុក​ដោយ​​ជោគ​ជ័យ​ ។</translation>
    </message>
    <message>
      <source>VirtualBox - Runtime Error</source>
      <translation>VirtualBox - កំហុស Runtime</translation>
    </message>
    <message>
      <source>&lt;b>Cannot access the kernel driver!&lt;/b>&lt;br/>&lt;br/></source>
      <translation>&lt;b>មិន​អាច​ដំណើរ​ការ​កម្មវិធី​បញ្ជា​ខឺ​​ណែល​​​បាន​ទេ !&lt;/b>&lt;br/>&lt;br/></translation>
    </message>
    <message>
      <source>Kernel driver not accessible</source>
      <translation>កម្ម​វិធី​បញ្ជា​ខឺ​ណែល​​មិន​អាច​ដំណើរ​ការបាន​​ឡើ​យ​</translation>
    </message>
    <message>
      <source>Unknown error %2 during initialization of the Runtime</source>
      <translation>មិន​ស្គាល់​កំហុស​ %2 អំឡុង​ពេល​ដែលការ​ចាប់​ផ្តើមពេលវេលា​រត់​</translation>
    </message>
  </context>
  <context>
    <name>QIArrowSplitter</name>
    <message>
      <source>&amp;Back</source>
      <translation>ថយ​ក្រោយ​</translation>
    </message>
    <message>
      <source>&amp;Next</source>
      <translation>បន្ទាប់​</translation>
    </message>
  </context>
  <context>
    <name>QIFileDialog</name>
    <message>
      <source>Select a directory</source>
      <translation>ជ្រើស​ថត</translation>
    </message>
    <message>
      <source>Select a file</source>
      <translation>ជ្រើស​ឯកសារ</translation>
    </message>
  </context>
  <context>
    <name>QIHotKeyEdit</name>
    <message>
      <source>Left </source>
      <translation>ឆ្វេង </translation>
    </message>
    <message>
      <source>Right </source>
      <translation>ស្ដាំ </translation>
    </message>
    <message>
      <source>Left Shift</source>
      <translation>ប្ដូរ(Shift) ឆ្វេង</translation>
    </message>
    <message>
      <source>Right Shift</source>
      <translation>ប្ដូរ(Shift) ស្ដាំ</translation>
    </message>
    <message>
      <source>Left Ctrl</source>
      <translation>បញ្ជា (Ctrl) ឆ្វេង</translation>
    </message>
    <message>
      <source>Right Ctrl</source>
      <translation>បញ្ជា(Ctrl) ស្ដាំ</translation>
    </message>
    <message>
      <source>Left Alt</source>
      <translation>ជំនួស​(Alt) ឆ្វេង</translation>
    </message>
    <message>
      <source>Right Alt</source>
      <translation>ជំនួស​(Alt) ស្ដាំ</translation>
    </message>
    <message>
      <source>Left WinKey</source>
      <translation>គ្រាប់ចុច(Win) ឆ្វេង</translation>
    </message>
    <message>
      <source>Right WinKey</source>
      <translation>គ្រាប់ចុច(Win) ស្ដាំ</translation>
    </message>
    <message>
      <source>Menu key</source>
      <translation>គ្រាប់ចុច​ម៉ឺនុយ</translation>
    </message>
    <message>
      <source>Alt Gr</source>
      <translation>Alt Gr</translation>
    </message>
    <message>
      <source>Caps Lock</source>
      <translation>ប្ដូរ​ជាប់(Caps Lock)</translation>
    </message>
    <message>
      <source>Scroll Lock</source>
      <translation>Scroll Lock</translation>
    </message>
    <message>
      <source>&lt;key_%1></source>
      <translation>&lt;key_%1></translation>
    </message>
    <message>
      <source>Pause</source>
      <translation>ផ្អាក</translation>
    </message>
    <message>
      <source>Print Screen</source>
      <translation>បោះពុម្ព​អេក្រង់</translation>
    </message>
    <message>
      <source>F1</source>
      <translation>F1</translation>
    </message>
    <message>
      <source>F2</source>
      <translation>F2</translation>
    </message>
    <message>
      <source>F3</source>
      <translation>F3</translation>
    </message>
    <message>
      <source>F4</source>
      <translation>F4</translation>
    </message>
    <message>
      <source>F5</source>
      <translation>F5</translation>
    </message>
    <message>
      <source>F6</source>
      <translation>F6</translation>
    </message>
    <message>
      <source>F7</source>
      <translation>F7</translation>
    </message>
    <message>
      <source>F8</source>
      <translation>F8</translation>
    </message>
    <message>
      <source>F9</source>
      <translation>F9</translation>
    </message>
    <message>
      <source>F10</source>
      <translation>F10</translation>
    </message>
    <message>
      <source>F11</source>
      <translation>F11</translation>
    </message>
    <message>
      <source>F12</source>
      <translation>F12</translation>
    </message>
    <message>
      <source>F13</source>
      <translation>F13</translation>
    </message>
    <message>
      <source>F14</source>
      <translation>F14</translation>
    </message>
    <message>
      <source>F15</source>
      <translation>F15</translation>
    </message>
    <message>
      <source>F16</source>
      <translation>F16</translation>
    </message>
    <message>
      <source>F17</source>
      <translation>F17</translation>
    </message>
    <message>
      <source>F18</source>
      <translation>F18</translation>
    </message>
    <message>
      <source>F19</source>
      <translation>F19</translation>
    </message>
    <message>
      <source>F20</source>
      <translation>F20</translation>
    </message>
    <message>
      <source>F21</source>
      <translation>F21</translation>
    </message>
    <message>
      <source>F22</source>
      <translation>F22</translation>
    </message>
    <message>
      <source>F23</source>
      <translation>F23</translation>
    </message>
    <message>
      <source>F24</source>
      <translation>F24</translation>
    </message>
    <message>
      <source>Num Lock</source>
      <translation>ប្ដូរជាប់ (Num Lock)</translation>
    </message>
    <message>
      <source>Forward</source>
      <translation>បញ្ជូន​បន្ត</translation>
    </message>
    <message>
      <source>Back</source>
      <translation>ថយក្រោយ</translation>
    </message>
  </context>
  <context>
    <name>QIHttp</name>
    <message>
      <source>Connection timed out</source>
      <translation>អស់ពេល​តភ្ជាប់</translation>
    </message>
    <message>
      <source>Could not locate the file on the server (response: %1)</source>
      <translation>មិនអាច​បម្រុង​ទុក​ឯកសារ​នៅ​លើ​ម៉ាស៊ីន​បម្រើ (ឆ្លើយតប ៖ %1)</translation>
    </message>
  </context>
  <context>
    <name>QILabel</name>
    <message>
      <source>&amp;Copy</source>
      <translation>ចម្លង</translation>
    </message>
  </context>
  <context>
    <name>QIMessageBox</name>
    <message>
      <source>OK</source>
      <translation>យល់ព្រម</translation>
    </message>
    <message>
      <source>Yes</source>
      <translation>បាទ/ចាស</translation>
    </message>
    <message>
      <source>No</source>
      <translation>ទេ</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់</translation>
    </message>
    <message>
      <source>Ignore</source>
      <translation>មិនអើពើ</translation>
    </message>
    <message>
      <source>&amp;Details</source>
      <translation>សេចក្ដី​លម្អិត</translation>
    </message>
    <message>
      <source>&amp;Details (%1 of %2)</source>
      <translation>សេច​ក្តី​លម្អិត​ (%1 នៃ %2)</translation>
    </message>
  </context>
  <context>
    <name>QIWidgetValidator</name>
    <message>
      <source>not complete</source>
      <comment>value state</comment>
      <translation>មិន​បញ្ចប់</translation>
    </message>
    <message>
      <source>invalid</source>
      <comment>value state</comment>
      <translation>មិន​ត្រឹមត្រូវ</translation>
    </message>
    <message>
      <source>&lt;qt>The value of the &lt;b>%1&lt;/b> field on the &lt;b>%2&lt;/b> page is %3.&lt;/qt></source>
      <translation>&lt;qt>តម្លៃ​របស់​វាល &lt;b>%1&lt;/b> នៅ​លើ​ទំព័រ &lt;b>%2&lt;/b> គឺ %3 ។&lt;/qt></translation>
    </message>
    <message>
      <source>&lt;qt>One of the values on the &lt;b>%1&lt;/b> page is %2.&lt;/qt></source>
      <translation>&lt;qt>តម្លៃ​មួយ​ក្នុង​ចំណោមតម្លៃ​នៅ​លើ​ទំព័រ &lt;b>%1&lt;/b> គឺ %2 ។&lt;/qt></translation>
    </message>
  </context>
  <context>
    <name>VBoxAboutDlg</name>
    <message>
      <source>VirtualBox - About</source>
      <translation>VirtualBox - អំពី</translation>
    </message>
    <message>
      <source>VirtualBox Graphical User Interface</source>
      <translation>ចំណុច​ប្រទាក់​អ្នក​ប្រើ​ក្រាហ្វិក​របស់ VirtualBox</translation>
    </message>
    <message>
      <source>Version %1</source>
      <translation>កំណែ %1</translation>
    </message>
  </context>
  <context>
    <name>VBoxAdditionsDownloader</name>
    <message>
      <source>Cancel</source>
      <translation>បោះពុម្ព</translation>
    </message>
    <message>
      <source>Downloading the VirtualBox Guest Additions CD image from &lt;nobr>&lt;b>%1&lt;/b>...&lt;/nobr></source>
      <translation>កំពុង​ទាញយក​រូបភាព​ស៊ីឌី​បន្ថែម​​ភ្ញៀវ​របស់ VirtualBox ពី &lt;nobr>&lt;b>%1&lt;/b>...&lt;/nobr></translation>
    </message>
    <message>
      <source>Cancel the VirtualBox Guest Additions CD image download</source>
      <translation>បោះបង់ការ​ទាញយក​រូបភាព​ស៊ីឌី​បន្ថែម​ភ្ញៀវ VirtualBox</translation>
    </message>
    <message>
      <source>&lt;p>Failed to save the downloaded file as &lt;nobr>&lt;b>%1&lt;/b>.&lt;/nobr>&lt;/p></source>
      <translation>&lt;p>បាន​បរាជ័យ​ក្នុងកា​ររក្សាទុក​ឯកសារ​ដែលបាន​ទាញ​យក​ជា &lt;nobr>&lt;b>%1&lt;/b> ។&lt;/nobr>&lt;/p></translation>
    </message>
    <message>
      <source>Select folder to save Guest Additions image to</source>
      <translation>ជ្រើស​ថត​ដើម្បី​រក្សាទុក​រូបភាព​បន្ថែម​ភ្ញៀវ​ទៅ​កាន់</translation>
    </message>
  </context>
  <context>
    <name>VBoxApplianceEditorWgt</name>
    <message>
      <source>Virtual System %1</source>
      <translation>ប្រ​ព័ន្ធ​​និម្មិត​ %1</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>ឈ្មោះ</translation>
    </message>
    <message>
      <source>Product</source>
      <translation>ផលិត​ផល​</translation>
    </message>
    <message>
      <source>Product-URL</source>
      <translation>ផលិតផល​-URL</translation>
    </message>
    <message>
      <source>Vendor</source>
      <translation>ក្រុម​ហ៊ុន​លក់​</translation>
    </message>
    <message>
      <source>Vendor-URL</source>
      <translation>ក្រុម​ហ៊ុន​លក់​​-URL</translation>
    </message>
    <message>
      <source>Version</source>
      <translation>កំណែ​</translation>
    </message>
    <message>
      <source>Description</source>
      <translation>សេច​ក្តី​ពិណណ៌នា​</translation>
    </message>
    <message>
      <source>License</source>
      <translation>អជ្ញាបណ្ណ</translation>
    </message>
    <message>
      <source>Guest OS Type</source>
      <translation>ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ</translation>
    </message>
    <message>
      <source>CPU</source>
      <translation>ស៊ី​ភី​យូ</translation>
    </message>
    <message>
      <source>RAM</source>
      <translation>សតិ</translation>
    </message>
    <message>
      <source>Hard Disk Controller (IDE)</source>
      <translation>ឧបករណ៍បញ្ជា​​ថាស​រឹង (IDE)</translation>
    </message>
    <message>
      <source>Hard Disk Controller (SATA)</source>
      <translation>ឧបករណ៍បញ្ជា​​ថាស​រឹង (SATA)</translation>
    </message>
    <message>
      <source>Hard Disk Controller (SCSI)</source>
      <translation>ឧបករណ៍​បញ្ជា​ថាស​រឹង (SCSI)</translation>
    </message>
    <message>
      <source>DVD</source>
      <translation>ឌីវីឌី​</translation>
    </message>
    <message>
      <source>Floppy</source>
      <translation>ថាសទន់</translation>
    </message>
    <message>
      <source>Network Adapter</source>
      <translation>អាដាប់​ទ័រ​បណ្តាញ​</translation>
    </message>
    <message>
      <source>USB Controller</source>
      <translation>ឧបករណ៍​បញ្ជា​ USB</translation>
    </message>
    <message>
      <source>Sound Card</source>
      <translation>កាត​សំឡេង​</translation>
    </message>
    <message>
      <source>Virtual Disk Image</source>
      <translation>រូបភាព​ថាស​និម្មិត​</translation>
    </message>
    <message>
      <source>Unknown Hardware Item</source>
      <translation>មិនស្គាល់​ធាតុ​ផ្នែក​រឹង​</translation>
    </message>
    <message>
      <source>MB</source>
      <translation>មេកាបៃ</translation>
    </message>
    <message>
      <source>&lt;b>Original Value:&lt;/b> %1</source>
      <translation>&lt;b>តម្លៃ​​ដើម​ ៖&lt;/b> %1</translation>
    </message>
    <message>
      <source>Configuration</source>
      <translation>ការ​កំណត់​រចនា​សម្ព័ន្ធ</translation>
    </message>
    <message>
      <source>Warnings:</source>
      <translation>ការ​ព្រមាន​ ៖</translation>
    </message>
  </context>
  <context>
    <name>VBoxCloseVMDlg</name>
    <message>
      <source>Close Virtual Machine</source>
      <translation>បិទ​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>You want to:</source>
      <translation>អ្នក​ចង់ ៖</translation>
    </message>
    <message>
      <source>&amp;Save the machine state</source>
      <translation>រក្សាទុក​ស្ថានភាព​ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>&amp;Power off the machine</source>
      <translation>បិទ​ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>S&amp;end the shutdown signal</source>
      <translation>ផ្ញើ​សញ្ញា​បិទ</translation>
    </message>
    <message>
      <source>&lt;p>When checked, the machine will be returned to the state stored in the current snapshot after it is turned off. This is useful if you are sure that you want to discard the results of your last sessions and start again at that snapshot.&lt;/p></source>
      <translation>&lt;p>នៅពេល​គូស​ធីក ស្ថានភាព​ម៉ាស៊ីន​នឹង​ត្រូវ​បាន​ស្ដារ​ពី​ស្ថានភាព​ដែល​បាន​ទុក​នៅ​ក្នុង​រូបថត​បច្ចុប្បន្ន​ខាង​ស្ដាំ​បន្ទាប់​ពី​​បិទ ។ វា​មាន​ប្រយោជន៍​ប្រសិន​បើ​​អ្នក​ប្រាកដ​ថា អ្នក​ចង់​បោះបង់​លទ្ធផល​នៃ​សម័យ​ចុងក្រោយ​របស់​អ្នក ហើយ​ត្រឡប់​ទៅ​រូបថត​បច្ចុប្បន្ន ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Saves the current execution state of the virtual machine to the physical hard disk of the host PC.&lt;/p>&lt;p>Next time this machine is started, it will be restored from the saved state and continue execution from the same place you saved it at, which will let you continue your work immediately.&lt;/p>&lt;p>Note that saving the machine state may take a long time, depending on the guest operating system type and the amount of memory you assigned to the virtual machine.&lt;/p></source>
      <translation>&lt;p>រក្សាទុក​ស្ថានភាព​ប្រតិបត្តិ​បច្ចុប្បន្ន​របស់​ម៉ាស៊ីន​និម្មិត ទៅ​ថាសរឹង​ហ្វីស៊ីខល​របស់​ម៉ាស៊ីន ។&lt;/p>&lt;p>ពេល​​ចាប់ផ្ដើម​ម៉ាស៊ីន​នេះ​លើកក្រោយ វា​នឹង​ត្រូ​វបាន​ស្ដារ​ពី​ស្ថានភាព​ដែល​បាន​រក្សាទុក និង​បន្ត​ប្រតិបត្តិការ​ពី​កន្លែង​ដដែល​ដែល​អ្នក​បាន​រក្សាទុក​វា ដែល​នឹង​អនុញ្ញាត​ឲ្យ​អ្នក​បន្ត​ការងារ​របស់​អ្នក​បាន​ភ្លាមៗ ។&lt;/p>&lt;p>ចំណាំ​ថា ការ​រក្សាទុក​ស្ថានភាព​របស់​ម៉ាស៊ីន​អាច​ចំណាយ​ពេល​យូ ដោយអាស្រ័យ​លើ​ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ និង​ទំហំសតិ​ដែល​អ្នក​បានផ្ដល់​ឲ្យ​ម៉ាស៊ីន​និម្មិត ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Sends the ACPI Power Button press event to the virtual machine.&lt;/p>&lt;p>Normally, the guest operating system running inside the virtual machine will detect this event and perform a clean shutdown procedure. This is a recommended way to turn off the virtual machine because all applications running inside it will get a chance to save their data and state.&lt;/p>&lt;p>If the machine doesn't respond to this action then the guest operating system may be misconfigured or doesn't understand ACPI Power Button events at all. In this case you should select the &lt;b>Power off the machine&lt;/b> action to stop virtual machine execution.&lt;/p></source>
      <translation>&lt;p>ផ្ញើ​ព្រឹត្តិការ​ចុច​ប៊ូតុង​ថាមពល ACPI ទៅកាន់​ម៉ាស៊ីន​និម្មិត ។&lt;/p>&lt;p>ជា​ធម្មតា ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ​រត់​ខាង​ក្នុង​ម៉ាស៊ីន​និម្មិត នឹង​រកឃើញ​ព្រឹត្តិការ​នេះ ហើយ​អនុវត្ត​នីតិវិធី​បិទ​ការ​ជម្រះ ។ នេះ​ជា​មធ្យោបាយ​ដែល​បានផ្ដល់​អនុសាសន៍ ឲ្យ​បិទ​ម៉ាស៊ីន​និម្មិត ពីព្រោះ​កម្មវិធី​ទាំង​អស់​ដែល​កំពុង​រត់​ខាង​ក្នុង​នឹង​មាន​ឱកាស​រក្សាទុក​ស្ថានភាព និង​ទិន្នន័យ​របស់​វា ។&lt;/p>&lt;p>ប្រសិន​បើ​ម៉ាស៊ីន​មិន​ឆ្លើយតប​នឹង​សកម្មភាព​នេះ​ទេ ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ​អាច​ត្រូវ​បាន​កំណត់​រចនាសម្ព័ន្ធ​ខុស ឬ​មិន​យល់​ព្រឹត្តិការណ៍​ប៊ូតុង​ថាមពល ACPI ទាល់តែសោះ ។ ក្នុងករណី​នេះ អ្នក​គួរ​ជ្រើស​សកម្មភាព &lt;b>បិទ​ម៉ាស៊ីន&lt;/b> ដើម្បី​បញ្ឈប់​ការ​ប្រតិបត្តិ​របស់​ម៉ាស៊ីន​និម្មិត ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Turns off the virtual machine.&lt;/p>&lt;p>Note that this action will stop machine execution immediately so that the guest operating system running inside it will not be able to perform a clean shutdown procedure which may result in &lt;i>data loss&lt;/i> inside the virtual machine. Selecting this action is recommended only if the virtual machine does not respond to the &lt;b>Send the shutdown signal&lt;/b> action.&lt;/p></source>
      <translation>&lt;p>បិទ​ម៉ាស៊ីន​និម្មិត ។&lt;/p>&lt;p>ចំណាំ​ថា សកម្មភាព​នេះ​នឹង​បញ្ឈប់​កា​រប្រតិបត្តិ​របស់ម៉ាស៊ីន​ភ្លាមៗ​តែ​ម្ដង ដូច្នេះ​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់ម៉ាស៊ីន​ភ្ញៀវ​កំពុង​រត់​ខាង​​ក្នុង​វា​នឹង​មិនអាច​អនុវត្ត​នីតិវិធី​បិទ​ការ​ជម្រះ​ដែល​អាច​បណ្ដាល​ឲ្យ &lt;i>បាត់បង់​ទិន្នន័យ&lt;/i> ខាង​ក្នុង​ម៉ាស៊ីន​និម្មិត​បានទេ ។ កា​រជ្រើស​សកម្ម​ភាព​នេះ​ត្រូវ​បានផ្ដល់​អនុសាសន៍​តែ​ក្នុងករណី​ដែល​ម៉ាស៊ីន​និមន្មិត​មិន​ឆ្លើយតប​នឹង​សកម្មភាព &lt;b>ផ្ញើ​សញ្ញា​បិទ&lt;/b> តែ​ប៉ុណ្ណោះ ។&lt;/p></translation>
    </message>
    <message>
      <source>Restore the machine state stored in the current snapshot</source>
      <translation>ស្តារ​​ស្ថាន​ភាព​ម៉ាស៊ីន​ដែល​បាន​រក្សា​ទុក​ក្នុង​រូប​ថត​បច្ចុប្បន្ន</translation>
    </message>
    <message>
      <source>&amp;Restore current snapshot '%1'</source>
      <translation>ស្តារ​រូប​ថត​បច្ចុប្បន្ន​ '%1'</translation>
    </message>
  </context>
  <context>
    <name>VBoxConsoleWnd</name>
    <message>
      <source>VirtualBox OSE</source>
      <translation>VirtualBox OSE</translation>
    </message>
    <message>
      <source>&amp;Fullscreen Mode</source>
      <translation>របៀប​ពេញអេក្រង់</translation>
    </message>
    <message>
      <source>Switch to fullscreen mode</source>
      <translation>ប្ដូរ​ទៅ​របៀប​ពេញអេក្រង់</translation>
    </message>
    <message>
      <source>Mouse Integration</source>
      <comment>enable/disable...</comment>
      <translation>ការ​បញ្ចូល​កណ្ដុរ</translation>
    </message>
    <message>
      <source>Auto-resize Guest Display</source>
      <comment>enable/disable...</comment>
      <translation>បង្ហាញ​ភ្ញៀវ​ផ្លាស់ប្ដូរ​ទំហំ​​​ដោយស្វ័យ​ប្រវត្តិ</translation>
    </message>
    <message>
      <source>Auto-resize &amp;Guest Display</source>
      <translation>បង្ហាញ​ភ្ញៀវ​ផ្លាស់ប្ដូរ​ទំហំ​ដោយ​ស្វ័យ​ប្រវត្តិ</translation>
    </message>
    <message>
      <source>Automatically resize the guest display when the window is resized (requires Guest Additions)</source>
      <translation>បង្ហាញ​ភ្ញៀវ​​​ផ្លាស់ប្ដូរ​ទំហំ​ដោយ​ស្វ័យ​ប្រវត្តិ នៅពេល​បង្អួច​ត្រូវ​បាន​ផ្លាស់ប្ដូរ​ (ត្រូវការ​ភ្ញៀវ​បន្ថែម)</translation>
    </message>
    <message>
      <source>&amp;Adjust Window Size</source>
      <translation>លៃតម្រូវ​ទំហំ​បង្អួច</translation>
    </message>
    <message>
      <source>Adjust window size and position to best fit the guest display</source>
      <translation>លៃតម្រូវ​ទំហំ​បង្អួច និងទីតាំង​ទៅ​សម​បំផុត​​នឹង​ការ​បង្ហាញ​ភ្ញៀវ</translation>
    </message>
    <message>
      <source>&amp;Insert Ctrl-Alt-Del</source>
      <translation>បញ្ជាន់ (Insert) បញ្ជា(Ctrl)-ជំនួស(Alt)-លុប(Del)</translation>
    </message>
    <message>
      <source>Send the Ctrl-Alt-Del sequence to the virtual machine</source>
      <translation>ផ្ញើ​លំដាប់​បញ្ជា(Ctrl)-ជំនួស(Alt)-លុប(Del) ទៅ​កាន់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>&amp;Insert Ctrl-Alt-Backspace</source>
      <translation>បញ្ជាន់(Insert) បញ្ជា(Ctrl)-ជំនួស(Alt)-លុបថយក្រោយ(Backspace)</translation>
    </message>
    <message>
      <source>Send the Ctrl-Alt-Backspace sequence to the virtual machine</source>
      <translation>ផ្ញើ​លំដាប់​បញ្ជា(Ctrl)-ជំនួស(Alt)-លុបថយក្រោយ(Backspace) ទៅ​កាន់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>&amp;Reset</source>
      <translation>កំណត់​ឡើង​វិញ</translation>
    </message>
    <message>
      <source>Reset the virtual machine</source>
      <translation>កំណត់​ម៉ាស៊ីន​និម្មិត​ឡើង​វិញ</translation>
    </message>
    <message>
      <source>ACPI S&amp;hutdown</source>
      <translation>បិទ ACPI</translation>
    </message>
    <message>
      <source>Send the ACPI Power Button press event to the virtual machine</source>
      <translation>ផ្ញើព្រឹត្តិការណ៍​ចុច​ប៊ូតុង​ថាមពល ACPI ទៅ​កាន់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>&amp;Close...</source>
      <translation>បិទ...</translation>
    </message>
    <message>
      <source>Close the virtual machine</source>
      <translation>បិទ​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>Take &amp;Snapshot...</source>
      <translation>យក​រូបថត...</translation>
    </message>
    <message>
      <source>Take a snapshot of the virtual machine</source>
      <translation>យក​រូបថត​របស់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>Enable or disable remote desktop (RDP) connections to this machine</source>
      <translation>បើក ឬ​បិទ​ការ​តភ្ជាប់​ផ្ទៃតុ​ពី​ចម្ងាយ (RDP) ទៅ​កាន់​ម៉ាស៊ីន​នេះ</translation>
    </message>
    <message>
      <source>&amp;Shared Folders...</source>
      <translation>ថត​ដែល​បានចែករំលែក...</translation>
    </message>
    <message>
      <source>Create or modify shared folders</source>
      <translation>បើក​ប្រអប់​ដើម្បី​ប្រតិបត្តិ​លើ​ថត​ដែល​បានចែករំលែក</translation>
    </message>
    <message>
      <source>&amp;Install Guest Additions...</source>
      <translation>ដំឡើង​ការ​បន្ថែម​ភ្ញៀវ...</translation>
    </message>
    <message>
      <source>Mount the Guest Additions installation image</source>
      <translation>ម៉ោន​រូបភាព​ដំឡើង​បន្ថែម​ភ្ញៀវ</translation>
    </message>
    <message>
      <source>&amp;USB Devices</source>
      <translation>ឧបករណ៍​ USB</translation>
    </message>
    <message>
      <source>&amp;Devices</source>
      <translation>ឧបករណ៍</translation>
    </message>
    <message>
      <source>De&amp;bug</source>
      <translation>បំបាត់​កំហុស</translation>
    </message>
    <message>
      <source>&amp;Help</source>
      <translation>ជំនួយ</translation>
    </message>
    <message>
      <source>&lt;hr>The VRDP Server is listening on port %1</source>
      <translation>&lt;hr>ម៉ាស៊ីន​បម្រើ VRDP កំពុង​ស្ដាប់​ច្រក %1</translation>
    </message>
    <message>
      <source>&amp;Pause</source>
      <translation>ផ្អាក</translation>
    </message>
    <message>
      <source>Suspend the execution of the virtual machine</source>
      <translation>ផ្អាក​កា​រប្រតិបត្តិ​របស់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>R&amp;esume</source>
      <translation>បន្ត</translation>
    </message>
    <message>
      <source>Resume the execution of the virtual machine</source>
      <translation>បន្ត​ប្រតិបត្តិ​របស់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>Disable &amp;Mouse Integration</source>
      <translation>បិទការ​រួម​បញ្ចូល​កណ្ដុរ</translation>
    </message>
    <message>
      <source>Temporarily disable host mouse pointer integration</source>
      <translation>បិទ​ការ​រួមបញ្ចូល​ព្រួញ​កណ្ដុរ​របស់ម៉ាស៊ីន​ជា​បណ្ដោះអាសន្ន</translation>
    </message>
    <message>
      <source>Enable &amp;Mouse Integration</source>
      <translation>បើក​ការ​រួម​បញ្ចូល​កណ្ដុរ</translation>
    </message>
    <message>
      <source>Enable temporarily disabled host mouse pointer integration</source>
      <translation>បើក​​ការ​រួម​បញ្ចូល​ព្រួញកណ្ដុរ​របស់​ម៉ាស៊ីន​ដែលបាន​បិទ​ជា​បណ្ដោះអាសន្ន</translation>
    </message>
    <message>
      <source>Snapshot %1</source>
      <translation>រូបថត %1</translation>
    </message>
    <message>
      <source>&amp;Machine</source>
      <translation>ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>Seam&amp;less Mode</source>
      <translation>របៀប​គ្មាន​ថ្នេរ</translation>
    </message>
    <message>
      <source>Switch to seamless desktop integration mode</source>
      <translation>ប្ដូរ​ទៅ​របៀប​បញ្ចូល​ផ្ទៃតុ​គ្មាន​ថ្នេរ</translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>No hard disks attached&lt;/b>&lt;/nobr></source>
      <comment>HDD tooltip</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>គ្មាន​ថាស​រឹង​បាន​ភ្ជាប់​ទេ&lt;/b>&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>Adapter %1 (%2)&lt;/b>: cable %3&lt;/nobr></source>
      <comment>Network adapters tooltip</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>អាដាប់ទ័រ %1 (%2)&lt;/b> ៖ ខ្សែ %3&lt;/nobr></translation>
    </message>
    <message>
      <source>connected</source>
      <comment>Network adapters tooltip</comment>
      <translation>បាន​តភ្ជាប់</translation>
    </message>
    <message>
      <source>disconnected</source>
      <comment>Network adapters tooltip</comment>
      <translation>បានផ្ដាច់</translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>All network adapters are disabled&lt;/b>&lt;/nobr></source>
      <comment>Network adapters tooltip</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>អាដាប់ទ័រ​បណ្ដាញ​ទាំង​អស់​ត្រូវ​បានបិទ&lt;/b>&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>No USB devices attached&lt;/b>&lt;/nobr></source>
      <comment>USB device tooltip</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>គ្មាន​ឧបករណ៍ USB បាន​ផ្ដាច់&lt;/b>&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>USB Controller is disabled&lt;/b>&lt;/nobr></source>
      <comment>USB device tooltip</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>វត្ថុ​បញ្ជា USB ត្រូវ​បាន​បិទ&lt;/b>&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>No shared folders&lt;/b>&lt;/nobr></source>
      <comment>Shared folders tooltip</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>គ្មាន​ថត​បាន​ចែករំលែក​ទេ&lt;/b>&lt;/nobr></translation>
    </message>
    <message>
      <source>Session I&amp;nformation Dialog</source>
      <translation>ប្រអប់​ព័ត៌មាន​សម័យ</translation>
    </message>
    <message>
      <source>Show Session Information Dialog</source>
      <translation>បង្ហាញ​ប្រអប់​ព័ត៌មាន​សម័យ</translation>
    </message>
    <message>
      <source>&amp;Statistics...</source>
      <comment>debug action</comment>
      <translation>ស្ថិតិ...</translation>
    </message>
    <message>
      <source>&amp;Command Line...</source>
      <comment>debug action</comment>
      <translation>បន្ទាត់​ពាក្យ​បញ្ជា...</translation>
    </message>
    <message>
      <source>Indicates whether the guest display auto-resize function is On (&lt;img src=:/auto_resize_on_16px.png/>) or Off (&lt;img src=:/auto_resize_off_16px.png/>). Note that this function requires Guest Additions to be installed in the guest OS.</source>
      <translation>បង្ហាញ​ថាតើ​ការបង្ហាញ​ម៉ាស៊ីន​​ភ្ញៀវ​មុខងារ​ផ្លាស់ប្ដូរ​ទំហំ​ដោយ​ស្វ័យ​ប្រវត្តិ​បើក (&lt;img src=:/auto_resize_on_16px.png/>) ឬ​បិទ (&lt;img src=:/auto_resize_off_16px.png/>) ។ ចំណាំ​ថា មុខងារ​នេះ​ត្រូវការ​ការ​បន្ថែម​ភ្ញៀវ​ដើម្បី​ដំឡើង​នៅក្នុង​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់ម៉ាស៊ីន​ភ្ញៀវ ។</translation>
    </message>
    <message>
      <source>Indicates whether the host mouse pointer is captured by the guest OS:&lt;br>&lt;nobr>&lt;img src=:/mouse_disabled_16px.png/>&amp;nbsp;&amp;nbsp;pointer is not captured&lt;/nobr>&lt;br>&lt;nobr>&lt;img src=:/mouse_16px.png/>&amp;nbsp;&amp;nbsp;pointer is captured&lt;/nobr>&lt;br>&lt;nobr>&lt;img src=:/mouse_seamless_16px.png/>&amp;nbsp;&amp;nbsp;mouse integration (MI) is On&lt;/nobr>&lt;br>&lt;nobr>&lt;img src=:/mouse_can_seamless_16px.png/>&amp;nbsp;&amp;nbsp;MI is Off, pointer is captured&lt;/nobr>&lt;br>&lt;nobr>&lt;img src=:/mouse_can_seamless_uncaptured_16px.png/>&amp;nbsp;&amp;nbsp;MI is Off, pointer is not captured&lt;/nobr>&lt;br>Note that the mouse integration feature requires Guest Additions to be installed in the guest OS.</source>
      <translation>បង្ហាញថា​តើ​ទស្សន៍​ទ្រនិច​កណ្ដុរ​របស់​ម៉ាស៊ីន​ត្រូ​វបាន​ចាប់យក​ដោយ​ប្រព័ន្ធ​ប្រតបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​ដែរឬទេ ៖&lt;br>&lt;nobr>&lt;img src=:/mouse_disabled_16px.png/>&amp;nbsp;&amp;nbsp;ទស្សន៍​ទ្រនិច​មិន​ត្រូ​វបាន​ចាប់យក​ទេ&lt;/nobr>&lt;br>&lt;nobr>&lt;img src=:/mouse_16px.png/>&amp;nbsp;&amp;nbsp;ទស្សន៍​ទ្រនិច​ត្រូវ​បាន​ចាប់យក&lt;/nobr>&lt;br>&lt;nobr>&lt;img src=:/mouse_seamless_16px.png/>&amp;nbsp;&amp;nbsp;ការ​រួម​បញ្ចូល​កណ្ដុរ (MI) គឺ​បាន​បើក&lt;/nobr>&lt;br>&lt;nobr>&lt;img src=:/mouse_can_seamless_16px.png/>&amp;nbsp;&amp;nbsp;MI បានបិទ, ទស្សន៍​ទ្រនិច​ត្រូវ​បាន​ចាប់យក&lt;/nobr>&lt;br>&lt;nobr>&lt;img src=:/mouse_can_seamless_uncaptured_16px.png/>&amp;nbsp;&amp;nbsp;MI បាន​បិទ, ទស្សន៍ទ្រនិច​មិន​ត្រូ​វបាន​ចាប់យក​ទេ​&lt;/nobr>&lt;br>ចំណាំ​ថា លក្ខណៈពិសេស​រួម​បញ្ចូល​កណ្ដុរតម្រូវ​ឲ្យ​ដំឡើង​​ផ្នែកបន្ថែម​របស់​ម៉ាស៊ីន​ភ្ញៀវ​នៅ​ក្នុង​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ ។</translation>
    </message>
    <message>
      <source>Indicates whether the keyboard is captured by the guest OS (&lt;img src=:/hostkey_captured_16px.png/>) or not (&lt;img src=:/hostkey_16px.png/>).</source>
      <translation>បង្ហាញ​ថាតើ​ក្ដារចុច​ត្រូវ​បានចាប់យក​ដោយ​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ (&lt;img src=:/hostkey_captured_16px.png/>) ឬ​មិនមែន (&lt;img src=:/hostkey_16px.png/>) ។</translation>
    </message>
    <message>
      <source>Indicates whether the Remote Display (VRDP Server) is enabled (&lt;img src=:/vrdp_16px.png/>) or not (&lt;img src=:/vrdp_disabled_16px.png/>).</source>
      <translation>បង្ហាញ​ថាតើ​ការ​បង្ហាញ​ពី​ចម្ងាយ (ម៉ាស៊ីន​បម្រើ VRDP) ត្រូវ​បានបើក (&lt;img src=:/vrdp_16px.png/>) ឬ​អត់ (&lt;img src=:/vrdp_disabled_16px.png/>) ។</translation>
    </message>
    <message>
      <source>&amp;Logging...</source>
      <comment>debug action</comment>
      <translation>កំពុង​ធ្វើ​កំណត់​ហេតុ...</translation>
    </message>
    <message>
      <source>Shows the currently assigned Host key.&lt;br>This key, when pressed alone, toggles the keyboard and mouse capture state. It can also be used in combination with other keys to quickly perform actions from the main menu.</source>
      <translation>បង្ហាញ​គ្រាប់ចុច​ម៉ាស៊ីន​ដែលបានផ្ដល់​បច្ចុប្បន្ន ។&lt;br>គ្រាប់ចុច​នេះ នៅពេល​ចុច​យូរ បិទ/បើក​ក្ដារចុច ហើយ​ស្ថានភាព​ចាប់យក​កណ្ដុរ ។ វា​អាច​ត្រូវ​បាន​ប្រើ​ក្នុង​បន្សំ​ជា​មួយ​គ្រាប់ចុច​ផ្សេង ដើម្បី​អនុវត្ត​សកម្មភាព​យ៉ាង​លឿន ពី​ម៉ឺនុយ​ចម្បង ។</translation>
    </message>
    <message>
      <source>Sun VirtualBox</source>
      <translation>Sun VirtualBox</translation>
    </message>
    <message>
      <source> EXPERIMENTAL build %1r%2 - %3</source>
      <translation> ស្ថាបនា​ពិសោធ %1r%2 - %3</translation>
    </message>
    <message>
      <source>&amp;CD/DVD Devices</source>
      <translation>ឧបករណ៍​ស៊ី​ឌី​/ឌីវីឌី</translation>
    </message>
    <message>
      <source>&amp;Floppy Devices</source>
      <translation>ឧបករណ៍​ថាសទន់​</translation>
    </message>
    <message>
      <source>&amp;Network Adapters...</source>
      <translation>អាដាប់ទ័រ​​​បណ្តាញ​...</translation>
    </message>
    <message>
      <source>Change the settings of network adapters</source>
      <translation>ផ្លាស់​ប្តូរ​ការ​កំណត់​របស់​​អាដាប់ទ័រ​​បណ្តាញ</translation>
    </message>
    <message>
      <source>&amp;Remote Display</source>
      <translation>ការ​បង្ហាញ​ពី​ចម្ងាយ</translation>
    </message>
    <message>
      <source>Remote Desktop (RDP) Server</source>
      <comment>
enable/disable...</comment>
      <translation>ម៉ាស៊ីន​បម្រើ​ផ្ទៃតុ​ពី​ចម្ងាយ (RDP)​</translation>
    </message>
    <message>
      <source>More CD/DVD Images...</source>
      <translation>រូបភាព​ស៊ីឌី/ឌីវីឌី​ផ្សេង​ទៀត...</translation>
    </message>
    <message>
      <source>Unmount CD/DVD Device</source>
      <translation>អាន់ម៉ោន​ឧបករណ៍​ស៊ីឌី/ឌីវីឌី</translation>
    </message>
    <message>
      <source>More Floppy Images...</source>
      <translation>រូបភាព​ថាសទន់​ផ្សេង​ទៀត...</translation>
    </message>
    <message>
      <source>Unmount Floppy Device</source>
      <translation>អាន់ម៉ោន​ឧបករណ៍​ថាស​ទន់​</translation>
    </message>
    <message>
      <source>No CD/DVD Devices Attached</source>
      <translation>គ្មាន​ឧបករណ៍​ស៊ីឌី/ឌីវីឌី​បាន​ភ្ជាប់​ទេ</translation>
    </message>
    <message>
      <source>No Floppy Devices Attached</source>
      <translation>គ្មាន​ឧបករណ៍​ថាស​ទន់​បាន​ភ្ជាប់​ទេ</translation>
    </message>
    <message>
      <source>&lt;p style='white-space:pre'>&lt;nobr>Indicates the activity of the virtual hard disks:&lt;/nobr>%1&lt;/p></source>
      <comment>
HDD tooltip</comment>
      <translation>&lt;p style='white-space:pre'>&lt;nobr>បង្ហាញ​សកម្មភាព​របស់​ថាសរឹង​និម្មិត ៖&lt;/nobr>%1&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p style='white-space:pre'>&lt;nobr>Indicates the activity of the CD/DVD devices:&lt;/nobr>%1&lt;/p></source>
      <comment>
CD/DVD tooltip</comment>
      <translation>&lt;p style='white-space:pre'>&lt;nobr>បង្ហាញ​​សកម្ម​ភាព​​​របស់​ឧបករណ៍​​ស៊ី​ឌី​​/ឌីវីឌី ៖&lt;/nobr>%1&lt;/p></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>No CD/DVD devices attached&lt;/b>&lt;/nobr></source>
      <comment>
CD/DVD tooltip</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>គ្មាន​ឧបករណ៍​ស៊ីឌី/ឌីវីឌី​បានភ្ជាប់​ទេ&lt;/b>&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;p style='white-space:pre'>&lt;nobr>Indicates the activity of the floppy devices:&lt;/nobr>%1&lt;/p></source>
      <comment>
FD tooltip</comment>
      <translation>&lt;p style='white-space:pre'>&lt;nobr>បង្ហាញ​សកម្មភាព​របស់​ឧបករណ៍​ថាស​ទន់ ៖&lt;/nobr>%1&lt;/p></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>No floppy devices attached&lt;/b>&lt;/nobr></source>
      <comment>
FD tooltip</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>គ្មាន​ឧបករណ៍​ថាស​ទន់​បានភ្ជាប់​ទេ&lt;/b>&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;p style='white-space:pre'>&lt;nobr>Indicates the activity of the network interfaces:&lt;/nobr>%1&lt;/p></source>
      <comment>
Network adapters tooltip</comment>
      <translation>&lt;p style='white-space:pre'>&lt;nobr>បង្ហាញ​សកម្មភាព​របស់​ចំណុច​ប្រទាក់​បណ្ដាញ ៖&lt;/nobr>%1&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p style='white-space:pre'>&lt;nobr>Indicates the activity of the attached USB devices:&lt;/nobr>%1&lt;/p></source>
      <comment>
USB device tooltip</comment>
      <translation>&lt;p style='white-space:pre'>&lt;nobr>បង្ហាញ​សកម្មភាព​របស់​ឧបករណ៍ USB ដែល​បាន​ភ្ជាប់ ៖&lt;/nobr>%1&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p style='white-space:pre'>&lt;nobr>Indicates the activity of the machine's shared folders:&lt;/nobr>%1&lt;/p></source>
      <comment>
Shared folders tooltip</comment>
      <translation>&lt;p style='white-space:pre'>&lt;nobr>បង្ហាញ​សកម្មភាព​របស់​ថត​ដែល​បានចែករំលែក​របស់​ម៉ាស៊ីន ៖&lt;/nobr>%1&lt;/p></translation>
    </message>
    <message>
      <source>Indicates the status of the hardware virtualization features used by this virtual machine:&lt;br>&lt;nobr>&lt;b>%1:&lt;/b>&amp;nbsp;%2&lt;/nobr>&lt;br>&lt;nobr>&lt;b>%3:&lt;/b>&amp;nbsp;%4&lt;/nobr></source>
      <comment>





Virtualization Stuff LED</comment>
      <translation>បង្ហាញថា ​ស្ថាន​ភាព​របស់​លក្ខណៈពិសេស​និម្មិត​ផ្នែក​រឹង​ត្រូ​វបាន​ប្រើ​ដោយ​ម៉ាស៊ីន​និម្មិត​នេះ ៖​&lt;br>&lt;nobr>&lt;b>%1:&lt;/b>&amp;nbsp;%2&lt;/nobr>&lt;br>&lt;nobr>&lt;b>%3:&lt;/b>&amp;nbsp;%4&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>&lt;b>%1:&lt;/b>&amp;nbsp;%2&lt;/nobr></source>
      <comment>
Virtualization Stuff LED</comment>
      <translation>&lt;br>&lt;nobr>&lt;b>%1:&lt;/b>&amp;nbsp;%2&lt;/nobr></translation>
    </message>
  </context>
  <context>
    <name>VBoxDownloaderWgt</name>
    <message>
      <source>The download process has been cancelled by the user.</source>
      <translation>ដំណើរការ​​ទាញយក​ត្រូវ​បានបោះបង់​ដោយ​អ្នកប្រើ​ ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxEmptyFileSelector</name>
    <message>
      <source>&amp;Choose...</source>
      <translation>ជ្រើស​រើស... </translation>
    </message>
  </context>
  <context>
    <name>VBoxExportApplianceWzd</name>
    <message>
      <source>Select a file to export into</source>
      <translation>ជ្រើស​ឯកសាត​ត្រូវ​នាំចេញ</translation>
    </message>
    <message>
      <source>Open Virtualization Format (%1)</source>
      <translation>Open Virtualization Format (%1)</translation>
    </message>
    <message>
      <source>Appliance</source>
      <translation>ឧបករណ៍​</translation>
    </message>
    <message>
      <source>Please choose a filename to export the OVF to.</source>
      <translation>សូម​ជ្រើស​ឈ្មោះ​ឯកសារ​​ ដើម្បី​នាំ​ចូល​ OVF ។</translation>
    </message>
    <message>
      <source>Please complete the additional fields like the username, password and the bucket, and provide a filename for the OVF target.</source>
      <translation>សូម​បំពេញ​​វាល​​ដែល​បន្ថែម ​ដូចជា​​ឈ្មោះ​អ្នក​ប្រើ​ ពាក្យ​សម្ងាត់​ និងសញ្ញា​សម្គាល់​​ ហើយ​​​ផ្តល់​ឈ្មោះ​​ឯកសារ​សម្រាប់​គោលដៅ ​OVF ។</translation>
    </message>
    <message>
      <source>Please complete the additional fields like the username, password, hostname and the bucket, and provide a filename for the OVF target.</source>
      <translation>សូម​បំពេញ​វាល​ដែល​បន្ថែម ​ដូចជា​​ឈ្មោះ​អ្នក​ប្រើ​ ពាក្យ​សម្ងាត់​ ឈ្មោះ​ម៉ាស៊ីន​ និង​សញ្ញា​សម្គាល់​ ហើយ​​​ផ្តល់​ឈ្មោះ​ឯកសារ​សម្រាប់​គោល​ដៅ​ OVF ។</translation>
    </message>
    <message>
      <source>Checking files ...</source>
      <translation>កំពុង​ពិនិត្យ​មើល​​ឯកសារ​...</translation>
    </message>
    <message>
      <source>Removing files ...</source>
      <translation>កំពុង​​យក​ឯកសារ​ចេញ​...</translation>
    </message>
    <message>
      <source>Exporting Appliance ...</source>
      <translation>កំពុង​នាំចេញ​ឧបករណ៍...</translation>
    </message>
    <message>
      <source>Appliance Export Wizard</source>
      <translation>អ្នក​ជំនួយការ​នាំចេញ​ឧបករណ៍</translation>
    </message>
    <message>
      <source>Welcome to the Appliance Export Wizard!</source>
      <translation>ស្វា​គមន៍​មក​​កាន់​អ្នក​​ជំនួយ​ការ​នាំ​ចេញ​ឧបករណ៍​ !</translation>
    </message>
    <message>
      <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;>
&lt;html>&lt;head>&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; />&lt;style type=&quot;text/css&quot;>
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>This wizard will guide you through the process of exporting an appliance. &lt;/p>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Use the &lt;span style=&quot; font-weight:600;&quot;>Next&lt;/span> button to go the next page of the wizard and the &lt;span style=&quot; font-weight:600;&quot;>Back&lt;/span> button to return to the previous page.&lt;/p>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Please select the virtual machines that you wish to the appliance. You can select more than one. Please note that these machines have to be turned off before they can be exported.&lt;/p>&lt;/body>&lt;/html></source>
      <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;>
&lt;html>&lt;head>&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; />&lt;style type=&quot;text/css&quot;>
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>អ្នក​ជំនួយ​កា​រនេះ​នឹង​នាំអ្នក​តាមរយៈ​ដំណើរការ​នាំចេញ​ឧបករណ៍ ។&lt;/p>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>ប្រើ​ប៊ូតុង &lt;span style=&quot; font-weight:600;&quot;>បន្ទាប់&lt;/span> ដើម្បី​ទៅ​កាន់​ទំព័រ​បន្ទាប់​របស់​អ្នក​ជំនួយ​ការ​ ហើយ​ប៊ូតុង &lt;span style=&quot; font-weight:600;&quot;>ថយក្រោយ&lt;/span> ដើម្បី​ត្រឡប់​ទៅ​ទំព័រ​មុន ។&lt;/p>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>សូម​ជ្រើស​ម៉ាស៊ីន​និម្មិត ដែល​អ្នក​ចង់បាន​ឧបករណ៍ ។ អ្នក​អាចជ្រើស​ច្រើនជាង​មួយ​បាន ។ សូម​ចំណាំថា ម៉ាស៊ីន​ទាំង​នេះ​ត្រូវ​តែ​បិទ មុន​នឹង​ពួកវា​អាច​ត្រូវ​បាន​នាំចេញ ។&lt;/p>&lt;/body>&lt;/html></translation>
    </message>
    <message>
      <source>&lt; &amp;Back</source>
      <translation>&lt;ថយ​ក្រោយ​</translation>
    </message>
    <message>
      <source>&amp;Next ></source>
      <translation>បន្ទាប់></translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់​</translation>
    </message>
    <message>
      <source>Appliance Export Settings</source>
      <translation>​កំណត់​ការ​នាំ​ចេញ​ឧបករណ៍​</translation>
    </message>
    <message>
      <source>Here you can change additional configuration values of the selected virtual machines. You can modify most of the properties shown by double-clicking on the items.</source>
      <translation>នៅ​ទី​នេះ អ្នក​​អាច​ប្តូរ​តម្លៃ​​​កំណត់​រចនា​សម្ព័ន្ធ​​​បន្ថែម​របស់​​ម៉ាស៊ីន​និម្មិត​​​ដែល​​បាន​ជ្រើស​ ។ អ្នក​អាច​កែប្រែ​លក្ខណសម្បត្តិ​ភាគច្រើន ដែលបាន​បង្ហាញ​ដោយ​ចុច​ទ្វេរដង​លើ​ធាតុ ។ ។</translation>
    </message>
    <message>
      <source>Restore Defaults</source>
      <translation>ស្តារ​​លំនាំ​ដើម</translation>
    </message>
    <message>
      <source>Please specify the target for the OVF export. You can choose between a local file system export, uploading the OVF to the Sun Cloud service or an S3 storage server.</source>
      <translation>សូម​បញ្ជាក់​គោល​ដៅ​សម្រាប់​នាំ​ចេញ​​ OVF ។ អ្នក​អាច​ជ្រើស​​រវាង​​ការ​នាំ​ចេញ​ប្រព័ន្ធ​ឯកសារ​មូល​ដ្ឋាន​​ ការ​ផ្ទុក​ OVF ឡើង​ទៅ​​កាន់​ Sun អាច​ជា​​សេវា​ ឬម៉ា​ស៊ីន​បម្រើ​ផ្ទុក S3 ។</translation>
    </message>
    <message>
      <source>&amp;Local Filesystem </source>
      <translation>ប្រព័ន្ធ​ឯកសារ​មូលដ្ឋាន ​</translation>
    </message>
    <message>
      <source>Sun &amp;Cloud</source>
      <translation>ពពក​របស់ Sun</translation>
    </message>
    <message>
      <source>&amp;Simple Storage System (S3)</source>
      <translation>ប្រព័ន្ធ​ផ្ទុក​ធម្មតា (S3)</translation>
    </message>
    <message>
      <source>&amp;Username:</source>
      <translation>ឈ្មោះ​អ្នក​ប្រើ​ ៖</translation>
    </message>
    <message>
      <source>&amp;Password:</source>
      <translation>ពាក្យ​សម្ងាត់​ ៖</translation>
    </message>
    <message>
      <source>&amp;File:</source>
      <translation>ឯកសារ​ ៖</translation>
    </message>
    <message>
      <source>&amp;Bucket:</source>
      <translation>ធុង​ ៖</translation>
    </message>
    <message>
      <source>&amp;Hostname:</source>
      <translation>ឈ្មោះ​ម៉ាស៊ីន​ ៖</translation>
    </message>
    <message>
      <source>Write in legacy OVF 0.9 format for compatibility with other virtualization products.</source>
      <translation>សរ​សេរ​ក្នុង​​ទ្រង់​ទ្រាយ​ OVF 0.9 ​ចាស់ៗ​ សម្រាប់​ភាព​ឆប​គ្នា​ជាមួយផលិត​ផលនិម្មិត​ផ្សេង​ទៀត​ ។</translation>
    </message>
    <message>
      <source>&amp;Write legacy OVF 0.9</source>
      <translation>សេរ​សេរ​ OVF 0.9 ចាស់ៗ</translation>
    </message>
    <message>
      <source>&amp;Export ></source>
      <translation>នាំ​ចេញ></translation>
    </message>
  </context>
  <context>
    <name>VBoxFilePathSelectorWidget</name>
    <message>
      <source>&lt;reset to default></source>
      <translation>&lt;កំណត់​ទៅ​លំនាំដើម​ឡើងវិញ></translation>
    </message>
    <message>
      <source>The actual default path value will be displayed after accepting the changes and opening this dialog again.</source>
      <translation>តម្លៃ​ផ្លូវ​លំនាំដើម​ពិតប្រាកដ​នឹង​ត្រូវ​បានបង្ហាញ​បន្ទាប់ពី​ទទួល​ការ​ផ្លាស់ប្ដូរ និង​បើក​ប្រអប់នេះ​ម្ដង​ទៀត ។</translation>
    </message>
    <message>
      <source>&lt;not selected></source>
      <translation>&lt;មិន​បានជ្រើស></translation>
    </message>
    <message>
      <source>Please use the &lt;b>Other...&lt;/b> item from the drop-down list to select a path.</source>
      <translation>សូម​ប្រើធាតុ &lt;b>ផ្សេងៗ...&lt;/b> ពី​បញ្ជី​ទម្លាក់ចុះ​ដើម្បី​ជ្រើស​ផ្លូវ​ដែល​ចង់​បាន ។</translation>
    </message>
    <message>
      <source>Other...</source>
      <translation>ផ្សេងៗ...</translation>
    </message>
    <message>
      <source>Reset</source>
      <translation>កំណត់​ឡើង​វិញ</translation>
    </message>
    <message>
      <source>Opens a dialog to select a different folder.</source>
      <translation>បើក​ប្រអប់ ដើម្បី​ជ្រើស​ថត​ផ្សេង ។</translation>
    </message>
    <message>
      <source>Resets the folder path to the default value.</source>
      <translation>កំណត់​ផ្លូវថត​ទៅ​តម្លៃ​លំនាំដើម​ឡើង​វិញ ។</translation>
    </message>
    <message>
      <source>Opens a dialog to select a different file.</source>
      <translation>បើក​ប្រអប់​ដើម្បី​ជ្រើស​ឯកសារ​ផ្សេង ។</translation>
    </message>
    <message>
      <source>Resets the file path to the default value.</source>
      <translation>កំណត់​ផ្លូវ​ឯកសារ​ទៅ​តម្លៃ​លំនាំដើម​ឡើង​វិញ ។</translation>
    </message>
    <message>
      <source>&amp;Copy</source>
      <translation>ចម្លង</translation>
    </message>
    <message>
      <source>Please type the folder path here.</source>
      <translation>សូម​វាយ​ផ្លូវ​ថត​ដែល​ចង់​បាននៅ​ទីនេះ ។</translation>
    </message>
    <message>
      <source>Please type the file path here.</source>
      <translation>សូម​វាយ​ផ្លូវ​ឯកសារ​ដែល​ចង់​បាន​នៅ​ទីនេះ ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxGLSettingsDlg</name>
    <message>
      <source>General</source>
      <translation>ទូទៅ</translation>
    </message>
    <message>
      <source>Input</source>
      <translation>បញ្ចូល</translation>
    </message>
    <message>
      <source>Update</source>
      <translation>ធ្វើ​ឲ្យ​ទាន់សម័យ</translation>
    </message>
    <message>
      <source>Language</source>
      <translation>ភាសា</translation>
    </message>
    <message>
      <source>USB</source>
      <translation>USB</translation>
    </message>
    <message>
      <source>VirtualBox - %1</source>
      <translation>VirtualBox - %1</translation>
    </message>
    <message>
      <source>Network</source>
      <translation>បណ្ដាញ</translation>
    </message>
  </context>
  <context>
    <name>VBoxGLSettingsGeneral</name>
    <message>
      <source>Displays the path to the default virtual machine folder. This folder is used, if not explicitly specified otherwise, when creating new virtual machines.</source>
      <translation>បង្ហាញ​ផ្លូវ​ទៅ​កាន់​ថត​ម៉ាស៊ីន​និម្មិត​លំនាំដើម ។ ថត​នេះ​ត្រូវ​បាន​ប្រើ ប្រសិនបើ​មិន​បានបញ្ជាក់ជាក់លាក់​ទេ នៅពេល​បង្កើត​ម៉ាស៊ីន​និម្មិត​ថ្មី ។</translation>
    </message>
    <message>
      <source>Displays the path to the library that provides authentication for Remote Display (VRDP) clients.</source>
      <translation>បង្ហាញ​ផ្លូវ​ទៅ​បណ្ណាល័យ ដែល​ផ្ដល់​នូវ​ការ​ផ្ទៀងផ្ទាត់​ភាព​ត្រឹមត្រូវ​សម្រាប់​ម៉ាស៊ីន​ភ្ញៀវ​បង្ហាញ​ពី​ចម្ងាយ (​VRDP) ។</translation>
    </message>
    <message>
      <source>Default &amp;Hard Disk Folder:</source>
      <translation>ថត​ថាសរឹង​លំនាំដើម ៖</translation>
    </message>
    <message>
      <source>Default &amp;Machine Folder:</source>
      <translation>ថត​ម៉ាស៊ីន​លំនាំដើម ៖</translation>
    </message>
    <message>
      <source>V&amp;RDP Authentication Library:</source>
      <translation>បណ្ណាល័យ​ការ​ផ្ទៀងផ្ទាត់​ភាព​ត្រឹមត្រូវ​របស់ VRDP ៖</translation>
    </message>
    <message>
      <source>Displays the path to the default hard disk folder. This folder is used, if not explicitly specified otherwise, when adding existing or creating new virtual hard disks.</source>
      <translation>បង្ហាញ​ផ្លូវ​ទៅ​កាន់​ថត​ថាសរឹង​លំនាំដើម ។ ថត​នេះ​ត្រូ​វបានប្រើ ប្រសិនបើ​គ្មាន​ការ​កំណត់​ជាក់លាក់ នៅពេលបន្ថែម​ថាសរឹង​និម្មិត​ថ្មី​ ឬ​មាន​ស្រាប់ ។</translation>
    </message>
    <message>
      <source>When checked, the application will provide an icon with the context menu in the system tray.</source>
      <translation>នៅពេល​បាន​គូស​ធីក កម្មវិធី​នឹង​ផ្ដល់​នូវ​រូបតំណាង​ដែល​មាន​ម៉ឺនុយ​បរិបទ​នៅ​ក្នុង​ថាស​ប្រព័ន្ធ ។</translation>
    </message>
    <message>
      <source>&amp;Show System Tray Icon</source>
      <translation>បង្ហាញ​រូបតំណាង​ថាស​ប្រព័ន្ធ</translation>
    </message>
    <message>
      <source>When checked, the Dock Icon will reflect the VM window content in realtime.</source>
      <translation>នៅពេលបានគូស​ធីក រូប​តំណាង​ចូលផែ​នឹង​ជះឥទ្ធិពល​លើ​មាតិកា​បង្អួច​ VM នៅ​ក្នុង realtime ។</translation>
    </message>
    <message>
      <source>&amp;Dock Icon Realtime Preview</source>
      <translation>មើល​រូបតំណាង​ចូលផែ Realtime ជា​មុន</translation>
    </message>
    <message>
      <source>&amp;Auto show Dock and Menubar in fullscreen</source>
      <translation>បង្ហាញ​ចត​ដោយ​ស្វ័យ​ប្រវត្តិ​ និងរបារ​ម៉ឺនុយ​​អេក្រង់​ពេញ​</translation>
    </message>
  </context>
  <context>
    <name>VBoxGLSettingsInput</name>
    <message>
      <source>Host &amp;Key:</source>
      <translation>គ្រាប់ចុច​ម៉ាស៊ីន ៖</translation>
    </message>
    <message>
      <source>Displays the key used as a Host Key in the VM window. Activate the entry field and press a new Host Key. Note that alphanumeric, cursor movement and editing keys cannot be used.</source>
      <translation>បង្ហាញគ្រាប់ចុច​ដែល​បាន​ប្រើ​ជា​គ្រាប់​ចុច​ម៉ាស៊ីន​នៅ​ក្នុង​បង្អួច VM ។ ធ្វើ​ឲ្យ​វាល​ធាតុ​សកម្ម ហើយ​ចុច​គ្រាប់ចុច​ម៉ាស៊ីន​ថ្មី ។ ចំណាំថា​លេខ ចលនា​ទស្សន៍ទ្រនិច និង​គ្រាប់ចុចកែសម្រួល​មិនអាច​ត្រូវ​បាន​ប្រើ​ជា​គ្រាប់ចុច​ម៉ាស៊ីន​ទេ ។</translation>
    </message>
    <message>
      <source>When checked, the keyboard is automatically captured every time the VM window is activated. When the keyboard is captured, all keystrokes (including system ones like Alt-Tab) are directed to the VM.</source>
      <translation>នៅពេល​បានគូស​ធីក ក្ដារចុច​ត្រូ​វបាន​ចាប់យក​ដោយ​ស្វ័យ​ប្រវត្តិ​រាល់ពេល​ដែល​បង្អួច VM ត្រូវ​បាន​ធ្វើ​ឲ្យ​សកម្ម ។ នៅពេល​ក្ដារចុច​ត្រូវ​បាន​ចាប់យក  keystrokes (រួម​មាន​ប្រព័ន្ធ​​មួយ ដូចជា ជំនួស(Alt)-ថេប(Tab)) ត្រូវ​បាន​បញ្ជូន​ទៅ​កាន់ VM ។</translation>
    </message>
    <message>
      <source>&amp;Auto Capture Keyboard</source>
      <translation>ចាប់យក​ក្ដារចុច​ដោយ​ស្វ័យ​ប្រវត្តិ</translation>
    </message>
  </context>
  <context>
    <name>VBoxGLSettingsLanguage</name>
    <message>
      <source> (built-in)</source>
      <comment>Language</comment>
      <translation> (ជាប់)</translation>
    </message>
    <message>
      <source>&lt;unavailable></source>
      <comment>Language</comment>
      <translation>&lt;មិនអាច​ប្រើបាន></translation>
    </message>
    <message>
      <source>&lt;unknown></source>
      <comment>Author(s)</comment>
      <translation>&lt;មិនស្គាល់></translation>
    </message>
    <message>
      <source>Default</source>
      <comment>Language</comment>
      <translation>លំនាំដើម</translation>
    </message>
    <message>
      <source>Language:</source>
      <translation>ភាសា ៖</translation>
    </message>
    <message>
      <source>&amp;Interface Language:</source>
      <translation>ភាសា​ចំណុច​ប្រទាក់ ៖</translation>
    </message>
    <message>
      <source>Lists all available user interface languages. The effective language is written in &lt;b>bold&lt;/b>. Select &lt;i>Default&lt;/i> to reset to the system default language.</source>
      <translation>រាយ​ភាសា​ចំណុច​ប្រទាក់​អ្នក​ប្រើ​ដែល​អាច​ប្រើ​បាន ។ ភាសា​ដែល​មានប្រសិទ្ធភាព​ត្រូវ​បាន​សរសេរ​ &lt;b>ដិត&lt;/b> ។ ជ្រើស &lt;i>លំនាំដើម&lt;/i> ដើម្បី​កំណត់​ភាសា​លំនាំដើម​របស់​ប្រព័ន្ធ​ឡើង​វិញ ។</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>ឈ្មោះ</translation>
    </message>
    <message>
      <source>Id</source>
      <translation>លេខសម្គាល់</translation>
    </message>
    <message>
      <source>Language</source>
      <translation>ភាសា</translation>
    </message>
    <message>
      <source>Author</source>
      <translation>អ្នកនិពន្ធ</translation>
    </message>
    <message>
      <source>Author(s):</source>
      <translation>អ្នកនិពន្ធ ៖</translation>
    </message>
  </context>
  <context>
    <name>VBoxGLSettingsNetwork</name>
    <message>
      <source>%1 network</source>
      <comment>


&lt;adapter name> network</comment>
      <translation>%1 បណ្ដាញ</translation>
    </message>
    <message>
      <source>host IPv4 address of &lt;b>%1&lt;/b> is wrong</source>
      <translation>អាសយដ្ឋាន IPv4 ម៉ាស៊ីន​របស់ &lt;b>%1&lt;/b> មិន​ត្រឹមត្រូវ</translation>
    </message>
    <message>
      <source>host IPv4 network mask of &lt;b>%1&lt;/b> is wrong</source>
      <translation>របាំង​បណ្ដាញ​ម៉ាស៊ីន IPv4 របស់ &lt;b>%1&lt;/b> មិន​ត្រឹមត្រូវទេ</translation>
    </message>
    <message>
      <source>host IPv6 address of &lt;b>%1&lt;/b> is wrong</source>
      <translation>អាសយដ្ឋាន​ម៉ាស៊ីន IPv6 របស់ &lt;b>%1&lt;/b> មិន​ត្រឹមត្រូវ​ទេ</translation>
    </message>
    <message>
      <source>DHCP server address of &lt;b>%1&lt;/b> is wrong</source>
      <translation>អាសយដ្ឋាន​ម៉ាស៊ីន​បម្រើ DHCP របស់&lt;b>%1&lt;/b> មិន​ត្រឹមត្រូវ​ទេ</translation>
    </message>
    <message>
      <source>DHCP server network mask of &lt;b>%1&lt;/b> is wrong</source>
      <translation>របាំង​បណ្ដាញ​ម៉ាស៊ីន​បម្រើ DHCP​ របស់&lt;b>%1&lt;/b> មិន​ត្រឹមត្រូវទេ</translation>
    </message>
    <message>
      <source>DHCP lower address bound of &lt;b>%1&lt;/b> is wrong</source>
      <translation>ដែន​អាសយដ្ឋាន​ទាប​របស់ DHCP នៃ&lt;b>%1&lt;/b> មិន​ត្រឹមត្រូវ​ទេ</translation>
    </message>
    <message>
      <source>DHCP upper address bound of &lt;b>%1&lt;/b> is wrong</source>
      <translation>ដែន​អាសយដ្ឋាន​ខ្ពស់​របស់ DHCP នៃ&lt;b>%1&lt;/b> មិន​ត្រឹមត្រូវ​ទេ</translation>
    </message>
    <message>
      <source>Adapter</source>
      <translation>អាដាប់​ទ័រ</translation>
    </message>
    <message>
      <source>Automatically configured</source>
      <comment>



interface</comment>
      <translation>បាន​កំណត់​រចនាសម្ព័ន្ធ​ដោយ​ស្វ័យ​ប្រវត្តិ</translation>
    </message>
    <message>
      <source>Manually configured</source>
      <comment>



interface</comment>
      <translation>បាន​កំណត់​រចនា​សម្ព័ន្ធ​​ដោយ​ដៃ​</translation>
    </message>
    <message>
      <source>IPv4 Address</source>
      <translation>អាសយ​ដ្ឋាន IPv4​</translation>
    </message>
    <message>
      <source>Not set</source>
      <comment>



address</comment>
      <translation>មិន​បាន​​កំណត់​</translation>
    </message>
    <message>
      <source>IPv4 Network Mask</source>
      <translation>របាំង​បណ្តាញ​ IPv4</translation>
    </message>
    <message>
      <source>Not set</source>
      <comment>



mask</comment>
      <translation>មិនបាន​​កំណត់​</translation>
    </message>
    <message>
      <source>IPv6 Address</source>
      <translation>អាសយ​ដ្ឋាន​ IPv6</translation>
    </message>
    <message>
      <source>IPv6 Network Mask Length</source>
      <translation>ប្រវែង​របាំង​បណ្តាញ​ IPv6</translation>
    </message>
    <message>
      <source>Not set</source>
      <comment>



length</comment>
      <translation>មិន​បាន​​កំណត់​</translation>
    </message>
    <message>
      <source>DHCP Server</source>
      <translation>ម៉ាស៊ីន​បម្រើ​ DHCP</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>



server</comment>
      <translation>បាន​បើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>



server</comment>
      <translation>​បាន​បើក</translation>
    </message>
    <message>
      <source>Address</source>
      <translation>អាសយដ្ឋាន​</translation>
    </message>
    <message>
      <source>Network Mask</source>
      <translation>របាំង​បណ្តាញ​</translation>
    </message>
    <message>
      <source>Lower Bound</source>
      <translation>ព្រំ​ដែន​ទាប​​</translation>
    </message>
    <message>
      <source>Not set</source>
      <comment>



bound</comment>
      <translation>មិន​បាន​កំណត់​</translation>
    </message>
    <message>
      <source>Upper Bound</source>
      <translation>ព្រំ​ដែន​ខ្ពស់​</translation>
    </message>
    <message>
      <source>&amp;Add host-only network</source>
      <translation>សម្រាប់​តែ​បណ្ដាញ​ម៉ាស៊ីនប៉ុណ្ណោះ</translation>
    </message>
    <message>
      <source>&amp;Remove host-only network</source>
      <translation>យក​បណ្ដាញ​ម៉ាស៊ីន​ចេញ​ប៉ុណ្ណោះ</translation>
    </message>
    <message>
      <source>&amp;Edit host-only network</source>
      <translation>កែសម្រួល​បណ្ដាញម៉ាស៊ីន​ប៉ុណ្ណោះ​</translation>
    </message>
    <message>
      <source>Performing</source>
      <comment>



creating/removing host-only network</comment>
      <translation>ការ​អនុវត្ត​</translation>
    </message>
    <message>
      <source>&amp;Host-only Networks:</source>
      <translation>បណ្ដាញ​ម៉ាស៊ីន​ប៉ុណ្ណោះ ៖</translation>
    </message>
    <message>
      <source>Lists all available host-only networks.</source>
      <translation>រាយ​បណ្ដាញ​ម៉ាស៊ីន​ដែលអាច​ប្រើបាន ។</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>ឈ្មោះ</translation>
    </message>
  </context>
  <context>
    <name>VBoxGLSettingsNetworkDetails</name>
    <message>
      <source>Host-only Network Details</source>
      <translation>សេច​ក្តី​លម្អិត​បណ្តាញ​​ម៉ាស៊ីន​</translation>
    </message>
    <message>
      <source>&amp;Adapter</source>
      <translation>អាដាប់ទ័រ</translation>
    </message>
    <message>
      <source>Manual &amp;Configuration</source>
      <translation>ការ​កំណត់​រចនា​សម្ព័ន្ធ​ដោយ​ដៃ​</translation>
    </message>
    <message>
      <source>Use manual configuration for this host-only network adapter.</source>
      <translation>ប្រើ​ការ​កំណត់​រចនា​សម្ព័ន្ធ​​ដោយ​ដៃ ​សម្រាប់​​​អាដាប់​ទ័រ​​បណ្តាញ​ម៉ាស៊ីន​​-តែ​​មួយ​នេះ​ ។</translation>
    </message>
    <message>
      <source>&amp;IPv4 Address:</source>
      <translation>អា​សយ​ដ្ឋាន​ IPv4 ៖</translation>
    </message>
    <message>
      <source>Displays the host IPv4 address for this adapter.</source>
      <translation>បង្ហាញ​អា​សយ​ដ្ឋាន​​ម៉ាស៊ីន​ IPv4 សម្រាប់​​អាដាប់​ទ័រ​​នេះ​ ។</translation>
    </message>
    <message>
      <source>IPv4 Network &amp;Mask:</source>
      <translation>របាំង​បណ្តាញ​ IPv4 ៖</translation>
    </message>
    <message>
      <source>Displays the host IPv4 network mask for this adapter.</source>
      <translation>បង្ហាញ​របាំង​បណ្តាញ​ម៉ាស៊ីន​ IPv4 សម្រាប់​អាដាប់​ទ័រ​នេះ​ ។</translation>
    </message>
    <message>
      <source>I&amp;Pv6 Address:</source>
      <translation>អាសយដ្ឋាន​ IPv6 ៖</translation>
    </message>
    <message>
      <source>Displays the host IPv6 address for this adapter if IPv6 is supported.</source>
      <translation>បង្ហាញ​​អា​សយដ្ឋាន​ម៉ាស៊ីន​ IPv6 សម្រាប់​​អាដាប់​ទ័រ​​នេះ ​ប្រ​សិន​បើ​​ IPv6 ត្រូវ​បាន​គាំ​ទ្រ ។</translation>
    </message>
    <message>
      <source>IPv6 Network Mask &amp;Length:</source>
      <translation>ប្រ​វែង​របាំង​បណ្តាញ​ IPv6 ៖</translation>
    </message>
    <message>
      <source>Displays the host IPv6 network mask prefix length for this adapter if IPv6 is supported.</source>
      <translation>បង្ហាញ​ប្រ​វែង​​បុព្វបទ​របាំង​បណ្តាញ​ម៉ាស៊ីន​ IPv6​ សម្រាប់​​អាដាប់​ទ័រ​​នេះ ​ប្រ​សិន​បើ​ IPv6 ត្រូវ​បាន​គាំ​ទ្រ​ ។</translation>
    </message>
    <message>
      <source>&amp;DHCP Server</source>
      <translation>ម៉ាស៊ីន​បម្រើ​ DHCP</translation>
    </message>
    <message>
      <source>&amp;Enable Server</source>
      <translation>បើក​ម៉ាស៊ីន​បម្រើ​</translation>
    </message>
    <message>
      <source>Indicates whether the DHCP Server is enabled on machine startup or not.</source>
      <translation>បង្ហាញ​​ថា​តើ​ម៉ា​ស៊ីន​បម្រើ​​ DHCP ត្រូវ​បាន​បើក​នៅពេល​ចាប់ផ្ដើម​ដែរ​ឬទេ ។</translation>
    </message>
    <message>
      <source>Server Add&amp;ress:</source>
      <translation>អាសយដ្ឋាន​ម៉ាស៊ីន​បម្រើ​ ៖</translation>
    </message>
    <message>
      <source>Displays the address of the DHCP server servicing the network associated with this host-only adapter.</source>
      <translation>បង្ហាញ​អាសយដ្ឋាន​បម្រើ​ម៉ាស៊ីន​​របស់​ DHCP ដែល​ភ្ជាប់​​បណ្តាញ​ជាមួយ​កម្ម​វិធី​សម្រួល​​ម៉ាស៊ីន​​-តែ​មួយនេះ​​ ។</translation>
    </message>
    <message>
      <source>Server &amp;Mask:</source>
      <translation>របាំង​ម៉ា​ស៊ីនបម្រើ​ ៖</translation>
    </message>
    <message>
      <source>Displays the network mask of the DHCP server servicing the network associated with this host-only adapter.</source>
      <translation>បង្ហាញ​របាំង​បណ្តាញ​​សេវា​​បម្រើ​ម៉ា​ស៊ីន​របស់​ DHCP ដែល​ភ្ជាប់​​​ជា​មួយ​កម្ម​វិធី​សម្រួល​ម៉ា​ស៊ីន​​-​តែ​មួយ​នេះ​ ។</translation>
    </message>
    <message>
      <source>&amp;Lower Address Bound:</source>
      <translation>ព្រំ​ដែន​អា​សយ​ដ្ឋាន​ទាប​ ៖</translation>
    </message>
    <message>
      <source>Displays the lower address bound offered by the DHCP server servicing the network associated with this host-only adapter.</source>
      <translation>បង្ហាញ​ព្រំដែន​អាសយដ្ឋានទាប​ដែល​បានផ្ដល់ដោយ​សេវា​ម៉ាស៊ីន​បម្រើ DHCP ដែល​បានភ្ជាប់​ជា​មួយនឹងកម្មវិធី​សម្រួល​ម៉ាស៊ីន​ ។</translation>
    </message>
    <message>
      <source>&amp;Upper Address Bound:</source>
      <translation>ព្រំដែន​អា​សយដ្ឋានខ្ពស់​ ៖</translation>
    </message>
    <message>
      <source>Displays the upper address bound offered by the DHCP server servicing the network associated with this host-only adapter.</source>
      <translation>បង្ហាញ​ព្រំដែន​អាសយដ្ឋាន​ខ្ពស់​ដែលបានផ្ដល់​ដោយ​ម៉ាស៊ីន​បម្រើ DHCP ដោយ​ផ្ដល់​សេវា​បណ្ដាញ​ភ្ជាប់​ជា​មួយ​នឹង​អាដាប់ទ័រ​ម៉ាស៊ីន​ ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxGLSettingsUpdate</name>
    <message>
      <source>When checked, the application will periodically connect to the VirtualBox website and check whether a new VirtualBox version is available.</source>
      <translation>នៅពេល​បាន​គូសធីក កម្មវិធី​នឹង​តភ្ជាប់​ទៅ​កាន់តំបន់បណ្ដាញ VirtualBox តាម​កាល​កំណត់ ហើយ​ពិនិត្យ​មើល​ថាតើ​មានកំណែ VirtualBox ដែរឬទេ ។</translation>
    </message>
    <message>
      <source>&amp;Check for updates</source>
      <translation>ពិនិត្យ​មើល​ភាព​ទាន់សម័យ</translation>
    </message>
    <message>
      <source>&amp;Once per:</source>
      <translation>ម្ដង​ក្នុង ៖</translation>
    </message>
    <message>
      <source>Specifies how often the new version check should be performed. Note that if you want to completely disable this check, just clear the above check box.</source>
      <translation>បញ្ជាក់​វិធី​ដែល​កំណែ​ថ្មី​ពិនិត្យ​មើល​គួរ​ត្រូវ​បាន​អនុវត្ត​ ។ ចំណាំថា ប្រសិនបើ​ង្នក​ចង់​បញ្ចប់ បិទ​ការ​ពិនិត្យនេះ គ្រាន់តែ​ជម្រះ​ប្រអប់​គូស​ធីក​ខាង​លើ ។</translation>
    </message>
    <message>
      <source>Next Check:</source>
      <translation>ការ​ពិនិត្យ​មើល​បន្ទាប់ ៖</translation>
    </message>
    <message>
      <source>Check for:</source>
      <translation>ពិនិត្យ​មើល ៖</translation>
    </message>
    <message>
      <source>&lt;p>Choose this if you only wish to be notified about stable updates to VirtualBox.&lt;/p></source>
      <translation>&lt;p>ជ្រើសវា​ ប្រសិនបើ​អ្នក​ចង់​ឲ្យ​ជូន​ដំណឹង​អំពី​ភាព​ទាន់សម័យ​ស្ថិតស្ថេរ​ដល់ VirtualBox ។&lt;/p></translation>
    </message>
    <message>
      <source>&amp;Stable release versions</source>
      <translation>កំណែ​ចេញ​ផ្សាយ​ស្ថិតស្ថេរ​</translation>
    </message>
    <message>
      <source>&lt;p>Choose this if you wish to be notified about all new VirtualBox releases.&lt;/p></source>
      <translation>&lt;p>ជ្រើសវា​ ប្រសិន​បើ​អ្នក​ចង់​ឲ្យ​ជូន​ដំណឹង​អំពី​ការ​ចេញផ្សាយ​ VirtualBox ថ្មី ។&lt;/p></translation>
    </message>
    <message>
      <source>&amp;All new releases</source>
      <translation>ការ​ចេញផ្សាយ​ថ្មី​ទាំងអស់​</translation>
    </message>
    <message>
      <source>&lt;p>Choose this to be notified about all new VirtualBox releases and pre-release versions of VirtualBox.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​វាដើម្បី​ឲ្យ​ជូន​ដំណឹង​អំពី​​កំណែ​​ចេញផ្សាយ​​​របស់ VirtualBox និង​និងចេញ​ផ្សាយ​មុន​របស់ VirtualBox ។&lt;/p></translation>
    </message>
    <message>
      <source>All new releases and &amp;pre-releases</source>
      <translation>ការ​ចេញ​ផ្សាយ​ថ្មី និង​ចេញ​ផ្សាយ​មុន​ទាំងអស់</translation>
    </message>
  </context>
  <context>
    <name>VBoxGlobal</name>
    <message>
      <source>Unknown device %1:%2</source>
      <comment>USB device details</comment>
      <translation>មិនស្គាល់​ឧបករណ៍ %1 ៖ %2</translation>
    </message>
    <message>
      <source>&lt;nobr>Vendor ID: %1&lt;/nobr>&lt;br>&lt;nobr>Product ID: %2&lt;/nobr>&lt;br>&lt;nobr>Revision: %3&lt;/nobr></source>
      <comment>USB device tooltip</comment>
      <translation>&lt;nobr>លេខសម្គាល់​អ្នកលក់ ៖ %1&lt;/nobr>&lt;br>&lt;nobr>លេខសម្គាល់​អ្នក​ផលិត ៖ %2&lt;/nobr>&lt;br>&lt;nobr>ការ​ពិនិត្យ​ឡើង​វិញ ៖ %3&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>Serial No. %1&lt;/nobr></source>
      <comment>USB device tooltip</comment>
      <translation>&lt;br>&lt;nobr>លេខ​ស៊េរី %1&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;br>&lt;nobr>State: %1&lt;/nobr></source>
      <comment>USB device tooltip</comment>
      <translation>&lt;br>&lt;nobr>ស្ថានភាព ៖ %1&lt;/nobr></translation>
    </message>
    <message>
      <source>Name</source>
      <comment>details report</comment>
      <translation>ឈ្មោះ</translation>
    </message>
    <message>
      <source>OS Type</source>
      <comment>details report</comment>
      <translation>ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ</translation>
    </message>
    <message>
      <source>Base Memory</source>
      <comment>details report</comment>
      <translation>សតិ​មូលដ្ឋាន</translation>
    </message>
    <message>
      <source>General</source>
      <comment>details report</comment>
      <translation>ទូទៅ</translation>
    </message>
    <message>
      <source>Video Memory</source>
      <comment>details report</comment>
      <translation>សតិ​វីដេអូ</translation>
    </message>
    <message>
      <source>Boot Order</source>
      <comment>details report</comment>
      <translation>លំដាប់​ចាប់ផ្ដើម</translation>
    </message>
    <message>
      <source>ACPI</source>
      <comment>details report</comment>
      <translation>ACPI</translation>
    </message>
    <message>
      <source>IO APIC</source>
      <comment>details report</comment>
      <translation>IO APIC</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>details report (ACPI)</comment>
      <translation>បានបើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (ACPI)</comment>
      <translation>បាន​បិទ</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>details report (IO APIC)</comment>
      <translation>បានបើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (IO APIC)</comment>
      <translation>បាន​បិទ</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (audio)</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Audio</source>
      <comment>details report</comment>
      <translation>អូឌីយ៉ូ</translation>
    </message>
    <message>
      <source>Adapter %1</source>
      <comment>details report (network)</comment>
      <translation>អាដាប់ទ័រ %1</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (network)</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Network</source>
      <comment>details report</comment>
      <translation>បណ្ដាញ</translation>
    </message>
    <message>
      <source>Device Filters</source>
      <comment>details report (USB)</comment>
      <translation>តម្រង​ឧបករណ៍</translation>
    </message>
    <message>
      <source>%1 (%2 active)</source>
      <comment>details report (USB)</comment>
      <translation>%1 (%2 សកម្មភាព)</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (USB)</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Powered Off</source>
      <comment>MachineState</comment>
      <translation>បិទ</translation>
    </message>
    <message>
      <source>Saved</source>
      <comment>MachineState</comment>
      <translation>បានរក្សាទុក</translation>
    </message>
    <message>
      <source>Aborted</source>
      <comment>MachineState</comment>
      <translation>បានបោះបង់</translation>
    </message>
    <message>
      <source>Running</source>
      <comment>MachineState</comment>
      <translation>រត់</translation>
    </message>
    <message>
      <source>Paused</source>
      <comment>MachineState</comment>
      <translation>បានផ្អាក</translation>
    </message>
    <message>
      <source>Starting</source>
      <comment>MachineState</comment>
      <translation>ចាប់ផ្ដើម</translation>
    </message>
    <message>
      <source>Stopping</source>
      <comment>MachineState</comment>
      <translation>បញ្ឈប់</translation>
    </message>
    <message>
      <source>Saving</source>
      <comment>MachineState</comment>
      <translation>រក្សាទុក</translation>
    </message>
    <message>
      <source>Restoring</source>
      <comment>MachineState</comment>
      <translation>ស្ដារឡើងវិញ</translation>
    </message>
    <message>
      <source>Closed</source>
      <comment>SessionState</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Open</source>
      <comment>SessionState</comment>
      <translation>បើក</translation>
    </message>
    <message>
      <source>Spawning</source>
      <comment>SessionState</comment>
      <translation>ការ​ព្រមាន​សារ​ឥតបានការ</translation>
    </message>
    <message>
      <source>Closing</source>
      <comment>SessionState</comment>
      <translation>បិទ</translation>
    </message>
    <message>
      <source>None</source>
      <comment>DeviceType</comment>
      <translation>គ្មាន</translation>
    </message>
    <message>
      <source>Floppy</source>
      <comment>DeviceType</comment>
      <translation>ថាសទន់</translation>
    </message>
    <message>
      <source>CD/DVD-ROM</source>
      <comment>DeviceType</comment>
      <translation>ស៊ីឌី/ឌីវីឌីរ៉ូម</translation>
    </message>
    <message>
      <source>Hard Disk</source>
      <comment>DeviceType</comment>
      <translation>ថាសរឹង</translation>
    </message>
    <message>
      <source>Network</source>
      <comment>DeviceType</comment>
      <translation>បណ្ដាញ</translation>
    </message>
    <message>
      <source>Normal</source>
      <comment>DiskType</comment>
      <translation>ធម្មតា</translation>
    </message>
    <message>
      <source>Immutable</source>
      <comment>DiskType</comment>
      <translation>មិនអាច​ផ្លាស់ប្ដូរ​បាន</translation>
    </message>
    <message>
      <source>Writethrough</source>
      <comment>DiskType</comment>
      <translation>សរសរ​កាត់</translation>
    </message>
    <message>
      <source>Null</source>
      <comment>VRDPAuthType</comment>
      <translation>គ្មាន</translation>
    </message>
    <message>
      <source>External</source>
      <comment>VRDPAuthType</comment>
      <translation>ខាងក្រៅ</translation>
    </message>
    <message>
      <source>Guest</source>
      <comment>VRDPAuthType</comment>
      <translation>ម៉ាស៊ីន​ភ្ញៀវ</translation>
    </message>
    <message>
      <source>Ignore</source>
      <comment>USBFilterActionType</comment>
      <translation>មិនអើពើ</translation>
    </message>
    <message>
      <source>Hold</source>
      <comment>USBFilterActionType</comment>
      <translation>កាន់</translation>
    </message>
    <message>
      <source>Null Audio Driver</source>
      <comment>AudioDriverType</comment>
      <translation>គ្មាន​កម្មវិធី​បញ្ជា​អូឌីយ៉ូ​ទេ</translation>
    </message>
    <message>
      <source>Windows Multimedia</source>
      <comment>AudioDriverType</comment>
      <translation>ពហុមេឌៀ​វីនដូ</translation>
    </message>
    <message>
      <source>OSS Audio Driver</source>
      <comment>AudioDriverType</comment>
      <translation>កម្មវិធី​បញ្ជា​អូឌីយ៉ូ OSS</translation>
    </message>
    <message>
      <source>ALSA Audio Driver</source>
      <comment>AudioDriverType</comment>
      <translation>កម្មវិធី​បញ្ជា​អូឌីយ៉ូ ALSA</translation>
    </message>
    <message>
      <source>Windows DirectSound</source>
      <comment>AudioDriverType</comment>
      <translation>DirectSound វីនដូ</translation>
    </message>
    <message>
      <source>CoreAudio</source>
      <comment>AudioDriverType</comment>
      <translation>CoreAudio</translation>
    </message>
    <message>
      <source>Not attached</source>
      <comment>NetworkAttachmentType</comment>
      <translation>មិន​បានភ្ជាប់</translation>
    </message>
    <message>
      <source>NAT</source>
      <comment>NetworkAttachmentType</comment>
      <translation>NAT</translation>
    </message>
    <message>
      <source>Internal Network</source>
      <comment>NetworkAttachmentType</comment>
      <translation>បណ្ដាញ​អ៊ីនធឺណិត</translation>
    </message>
    <message>
      <source>Not supported</source>
      <comment>USBDeviceState</comment>
      <translation>មិនបានគាំទ្រទេ</translation>
    </message>
    <message>
      <source>Unavailable</source>
      <comment>USBDeviceState</comment>
      <translation>មិនអាច​ប្រើបាន</translation>
    </message>
    <message>
      <source>Busy</source>
      <comment>USBDeviceState</comment>
      <translation>រវល់</translation>
    </message>
    <message>
      <source>Available</source>
      <comment>USBDeviceState</comment>
      <translation>អាច​ប្រើបាន</translation>
    </message>
    <message>
      <source>Held</source>
      <comment>USBDeviceState</comment>
      <translation>កាន់</translation>
    </message>
    <message>
      <source>Captured</source>
      <comment>USBDeviceState</comment>
      <translation>បានចាប់យក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>ClipboardType</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Host To Guest</source>
      <comment>ClipboardType</comment>
      <translation>ម៉ាស៊ីន​ទៅ​កាន់​ម៉ាស៊ីន​ភ្ញៀវ</translation>
    </message>
    <message>
      <source>Guest To Host</source>
      <comment>ClipboardType</comment>
      <translation>ម៉ាស៊ីន​ភ្ញៀវ​ទៅកាន់ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>Bidirectional</source>
      <comment>ClipboardType</comment>
      <translation>ទ្វេទិស</translation>
    </message>
    <message>
      <source>Port %1</source>
      <comment>details report (serial ports)</comment>
      <translation>ច្រក %1</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (serial ports)</comment>
      <translation>បាន​បិទ</translation>
    </message>
    <message>
      <source>Serial Ports</source>
      <comment>details report</comment>
      <translation>ច្រក​ស៊េរី</translation>
    </message>
    <message>
      <source>USB</source>
      <comment>details report</comment>
      <translation>USB</translation>
    </message>
    <message>
      <source>Shared Folders</source>
      <comment>details report (shared folders)</comment>
      <translation>ថត​ដែល​បានចែករំលែក</translation>
    </message>
    <message>
      <source>None</source>
      <comment>details report (shared folders)</comment>
      <translation>គ្មាន</translation>
    </message>
    <message>
      <source>Shared Folders</source>
      <comment>details report</comment>
      <translation>ថត​ដែលបានចែករំលែក</translation>
    </message>
    <message>
      <source>Disconnected</source>
      <comment>PortMode</comment>
      <translation>បានផ្ដាច់</translation>
    </message>
    <message>
      <source>Host Pipe</source>
      <comment>PortMode</comment>
      <translation>បំពង់​ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>Host Device</source>
      <comment>PortMode</comment>
      <translation>ឧបករណ៍​ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>User-defined</source>
      <comment>serial port</comment>
      <translation>កំណត់​ដោយ​អ្នកប្រើ</translation>
    </message>
    <message>
      <source>VT-x/AMD-V</source>
      <comment>details report</comment>
      <translation>VT-x/AMD-V</translation>
    </message>
    <message>
      <source>PAE/NX</source>
      <comment>details report</comment>
      <translation>PAE/NX</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>details report (VT-x/AMD-V)</comment>
      <translation>បានបើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (VT-x/AMD-V)</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>details report (PAE/NX)</comment>
      <translation>បានបើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (PAE/NX)</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Host Driver</source>
      <comment>details report (audio)</comment>
      <translation>កម្មវិធី​បញ្ជា​ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>Controller</source>
      <comment>details report (audio)</comment>
      <translation>វត្ថុ​បញ្ជា</translation>
    </message>
    <message>
      <source>Port %1</source>
      <comment>details report (parallel ports)</comment>
      <translation>ច្រក %1</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (parallel ports)</comment>
      <translation>បាន​បិទ</translation>
    </message>
    <message>
      <source>Parallel Ports</source>
      <comment>details report</comment>
      <translation>ច្រក​ប៉ារ៉ាឡែល</translation>
    </message>
    <message>
      <source>USB</source>
      <comment>DeviceType</comment>
      <translation>USB</translation>
    </message>
    <message>
      <source>Shared Folder</source>
      <comment>DeviceType</comment>
      <translation>ថត​ដែល​បាន​ចែករំលែក</translation>
    </message>
    <message>
      <source>IDE</source>
      <comment>StorageBus</comment>
      <translation>IDE</translation>
    </message>
    <message>
      <source>SATA</source>
      <comment>StorageBus</comment>
      <translation>SATA</translation>
    </message>
    <message>
      <source>Primary</source>
      <comment>StorageBusChannel</comment>
      <translation>ចម្បង</translation>
    </message>
    <message>
      <source>Secondary</source>
      <comment>StorageBusChannel</comment>
      <translation>រង</translation>
    </message>
    <message>
      <source>Master</source>
      <comment>StorageBusDevice</comment>
      <translation>មេ</translation>
    </message>
    <message>
      <source>Slave</source>
      <comment>StorageBusDevice</comment>
      <translation>កូនចៅ</translation>
    </message>
    <message>
      <source>Port %1</source>
      <comment>StorageBusChannel</comment>
      <translation>ច្រក %1</translation>
    </message>
    <message>
      <source>Solaris Audio</source>
      <comment>AudioDriverType</comment>
      <translation>អូឌីយ៉ូ Solaris</translation>
    </message>
    <message>
      <source>PulseAudio</source>
      <comment>AudioDriverType</comment>
      <translation>PulseAudio</translation>
    </message>
    <message>
      <source>ICH AC97</source>
      <comment>AudioControllerType</comment>
      <translation>ICH AC97</translation>
    </message>
    <message>
      <source>SoundBlaster 16</source>
      <comment>AudioControllerType</comment>
      <translation>SoundBlaster 16</translation>
    </message>
    <message>
      <source>PCnet-PCI II (Am79C970A)</source>
      <comment>NetworkAdapterType</comment>
      <translation>PCnet-PCI II (Am79C970A)</translation>
    </message>
    <message>
      <source>PCnet-FAST III (Am79C973)</source>
      <comment>NetworkAdapterType</comment>
      <translation>PCnet-FAST III (Am79C973)</translation>
    </message>
    <message>
      <source>Intel PRO/1000 MT Desktop (82540EM)</source>
      <comment>NetworkAdapterType</comment>
      <translation>Intel PRO/1000 MT Desktop (82540EM)</translation>
    </message>
    <message>
      <source>Intel PRO/1000 T Server (82543GC)</source>
      <comment>NetworkAdapterType</comment>
      <translation>Intel PRO/1000 T Server (82543GC)</translation>
    </message>
    <message>
      <source>&lt;nobr>Vendor ID: %1&lt;/nobr></source>
      <comment>USB filter tooltip</comment>
      <translation>&lt;nobr>លេខ​សម្គាល់​អ្នក​លក់ ៖ %1&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Product ID: %2&lt;/nobr></source>
      <comment>USB filter tooltip</comment>
      <translation>&lt;nobr>លេខសម្គាល់​ផលិតផល ៖ %2&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Revision: %3&lt;/nobr></source>
      <comment>USB filter tooltip</comment>
      <translation>&lt;nobr>កា​រពិនិត្យ​ឡើង​វិញ ៖ %3&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Product: %4&lt;/nobr></source>
      <comment>USB filter tooltip</comment>
      <translation>&lt;nobr>ផលិតផល ៖ %4&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Manufacturer: %5&lt;/nobr></source>
      <comment>USB filter tooltip</comment>
      <translation>&lt;nobr>ក្រុមហ៊ុន​ផលិត ៖ %5&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Serial No.: %1&lt;/nobr></source>
      <comment>USB filter tooltip</comment>
      <translation>&lt;nobr>លេខ​ស៊េរី ៖ %1&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Port: %1&lt;/nobr></source>
      <comment>USB filter tooltip</comment>
      <translation>&lt;nobr>ច្រក ៖ %1&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>State: %1&lt;/nobr></source>
      <comment>USB filter tooltip</comment>
      <translation>&lt;nobr>ស្ថានភាព ៖ %1&lt;/nobr></translation>
    </message>
    <message>
      <source>Adapter %1</source>
      <comment>network</comment>
      <translation>អាដាប់ទ័រ %1</translation>
    </message>
    <message>
      <source>Checking...</source>
      <comment>medium</comment>
      <translation>កំពុង​ពិនិត្យ​មើល...</translation>
    </message>
    <message>
      <source>Inaccessible</source>
      <comment>medium</comment>
      <translation>មិនអាច​ចូលដំណើរការ​បាន</translation>
    </message>
    <message>
      <source>3D Acceleration</source>
      <comment>details report</comment>
      <translation>បង្កើន​ល្បឿន​ត្រីមាត្រ</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>details report (3D Acceleration)</comment>
      <translation>បានបើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>details report (3D Acceleration)</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Setting Up</source>
      <comment>MachineState</comment>
      <translation>រៀបចំ</translation>
    </message>
    <message>
      <source>Differencing</source>
      <comment>DiskType</comment>
      <translation>ភាព​ខុសគ្នា</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>



details report (Nested Paging)</comment>
      <translation>បានបើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>



details report (Nested Paging)</comment>
      <translation>​បាន​បើក​</translation>
    </message>
    <message>
      <source>&lt;nobr>%1 MB&lt;/nobr></source>
      <comment>



details report</comment>
      <translation>&lt;nobr>%1 មេកាបៃ​&lt;/nobr></translation>
    </message>
    <message>
      <source>Processor(s)</source>
      <comment>



details report</comment>
      <translation>ប្រព័ន្ធ​ដំណើរ​ការ​</translation>
    </message>
    <message>
      <source>&lt;nobr>%1&lt;/nobr></source>
      <comment>



details report</comment>
      <translation>&lt;nobr>%1&lt;/nobr></translation>
    </message>
    <message>
      <source>Nested Paging</source>
      <comment>



details report</comment>
      <translation>Paging ខាង​ក្នុង</translation>
    </message>
    <message>
      <source>System</source>
      <comment>



details report</comment>
      <translation>ប្រព័ន្ធ</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>



details report (2D Video Acceleration)</comment>
      <translation>បានបើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>



details report (2D Video Acceleration)</comment>
      <translation>បាន​បិទ​</translation>
    </message>
    <message>
      <source>2D Video Acceleration</source>
      <comment>



details report</comment>
      <translation>ការ​បង្កើន​ល្បឿន​វីដេអូ​​ទ្វេ​មាត្រ</translation>
    </message>
    <message>
      <source>Remote Display Server Port</source>
      <comment>



details report (VRDP Server)</comment>
      <translation>ច្រក​ម៉ាស៊ីនបម្រើ​បង្ហាញ​ពី​ចម្ងាយ</translation>
    </message>
    <message>
      <source>Remote Display Server</source>
      <comment>



details report (VRDP Server)</comment>
      <translation>ម៉ា​ស៊ីន​បម្រើ​បង្ហាញ​ពី​ចម្ងាយ​</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>



details report (VRDP Server)</comment>
      <translation>បាន​បិទ</translation>
    </message>
    <message>
      <source>Display</source>
      <comment>



details report</comment>
      <translation>បង្ហាញ​</translation>
    </message>
    <message>
      <source>Not Attached</source>
      <comment>



details report (Storage)</comment>
      <translation>មិន​បានភ្ជាប់</translation>
    </message>
    <message>
      <source>Storage</source>
      <comment>



details report</comment>
      <translation>ផ្ទុក</translation>
    </message>
    <message>
      <source>Bridged adapter, %1</source>
      <comment>



details report (network)</comment>
      <translation>​អាដាប់​ទ័រប្រ៊ីដ្យ​​ %1</translation>
    </message>
    <message>
      <source>Internal network, '%1'</source>
      <comment>



details report (network)</comment>
      <translation>បណ្តាញ​ខាង​ក្នុង '%1'</translation>
    </message>
    <message>
      <source>Host-only adapter, '%1'</source>
      <comment>



details report (network)</comment>
      <translation>​អាដាប់​ទ័រ​​ ម៉ាស៊ីន​​-​តែ​មួយ '%1'​</translation>
    </message>
    <message>
      <source>Teleported</source>
      <comment>



MachineState</comment>
      <translation>បញ្ជួន​</translation>
    </message>
    <message>
      <source>Guru Meditation</source>
      <comment>



MachineState</comment>
      <translation>ការ​វិភាគ​រក​ Guru</translation>
    </message>
    <message>
      <source>Teleporting</source>
      <comment>



MachineState</comment>
      <translation>ការ​បញ្ជួន​</translation>
    </message>
    <message>
      <source>Taking Live Snapshot</source>
      <comment>



MachineState</comment>
      <translation>​ថតរូប​​ផ្ទាល់​</translation>
    </message>
    <message>
      <source>Teleporting Paused VM</source>
      <comment>



MachineState</comment>
      <translation>ការ​បញ្ជូនបានផ្អាក VM</translation>
    </message>
    <message>
      <source>Restoring Snapshot</source>
      <comment>



MachineState</comment>
      <translation>ការ​ស្តារ​រូប​ថត​</translation>
    </message>
    <message>
      <source>Deleting Snapshot</source>
      <comment>



MachineState</comment>
      <translation>ការ​លុប​រូប​ថត​</translation>
    </message>
    <message>
      <source>SCSI</source>
      <comment>



StorageBus</comment>
      <translation>SCSI</translation>
    </message>
    <message>
      <source>Floppy</source>
      <comment>



StorageBus</comment>
      <translation>ថាទន់</translation>
    </message>
    <message>
      <source>Device %1</source>
      <comment>



StorageBusDevice</comment>
      <translation>ឧបករណ៍​​​​ %1</translation>
    </message>
    <message>
      <source>IDE Primary Master</source>
      <comment>



New Storage UI : Slot Name</comment>
      <translation>មេ​ចម្បង​ IDE</translation>
    </message>
    <message>
      <source>IDE Primary Slave</source>
      <comment>



New Storage UI : Slot Name</comment>
      <translation>កូន​ចៅ​ចម្បង​ IDE</translation>
    </message>
    <message>
      <source>IDE Secondary Master</source>
      <comment>



New Storage UI : Slot Name</comment>
      <translation>មេ​រង IDE​</translation>
    </message>
    <message>
      <source>IDE Secondary Slave</source>
      <comment>



New Storage UI : Slot Name</comment>
      <translation>កូន​ចៅ​រង​ IDE</translation>
    </message>
    <message>
      <source>SATA Port %1</source>
      <comment>



New Storage UI : Slot Name</comment>
      <translation>ច្រក SATA​ %1</translation>
    </message>
    <message>
      <source>SCSI Port %1</source>
      <comment>



New Storage UI : Slot Name</comment>
      <translation>ច្រក​ SCSI​ %1</translation>
    </message>
    <message>
      <source>Floppy Device %1</source>
      <comment>



New Storage UI : Slot Name</comment>
      <translation>ឧបករណ៍ថា​​ទន់​​ %1</translation>
    </message>
    <message>
      <source>Raw File</source>
      <comment>



PortMode</comment>
      <translation>ឯកសារ​ដើម​</translation>
    </message>
    <message>
      <source>Intel PRO/1000 MT Server (82545EM)</source>
      <comment>



NetworkAdapterType</comment>
      <translation>ម៉ាស៊ីន​បម្រើ​ Intel PRO/1000 MT ​(82545EM)</translation>
    </message>
    <message>
      <source>Paravirtualized Network (virtio-net)</source>
      <comment>



NetworkAdapterType</comment>
      <translation>បណ្តាញ​ Paravirtualized  (virtio-net)</translation>
    </message>
    <message>
      <source>Bridged Adapter</source>
      <comment>



NetworkAttachmentType</comment>
      <translation>អាដាប់​​ទ័រ​​ប្រ៊ីដ្យ​​</translation>
    </message>
    <message>
      <source>Host-only Adapter</source>
      <comment>



NetworkAttachmentType</comment>
      <translation>អាដាប់​ទ័រ​​ម៉ាស៊ីន​​-​តែ​មួយ​</translation>
    </message>
    <message>
      <source>PIIX3</source>
      <comment>



StorageControllerType</comment>
      <translation>PIIX3</translation>
    </message>
    <message>
      <source>PIIX4</source>
      <comment>



StorageControllerType</comment>
      <translation>PIIX4</translation>
    </message>
    <message>
      <source>ICH6</source>
      <comment>



StorageControllerType</comment>
      <translation>ICH6</translation>
    </message>
    <message>
      <source>AHCI</source>
      <comment>



StorageControllerType</comment>
      <translation>AHCI</translation>
    </message>
    <message>
      <source>Lsilogic</source>
      <comment>



StorageControllerType</comment>
      <translation>Lsilogic</translation>
    </message>
    <message>
      <source>BusLogic</source>
      <comment>



StorageControllerType</comment>
      <translation>BusLogic</translation>
    </message>
    <message>
      <source>I82078</source>
      <comment>



StorageControllerType</comment>
      <translation>I82078</translation>
    </message>
    <message>
      <source>Empty</source>
      <comment>



medium</comment>
      <translation>ទទេ</translation>
    </message>
    <message>
      <source>Host Drive '%1'</source>
      <comment>



medium</comment>
      <translation>ដ្រាយ​ម៉ាស៊ីន​​ '%1'</translation>
    </message>
    <message>
      <source>Host Drive %1 (%2)</source>
      <comment>



medium</comment>
      <translation>ដ្រាយ​ម៉ាស៊ីន​​ %1 (%2)</translation>
    </message>
    <message>
      <source>&lt;p style=white-space:pre>Type (Format):  %1 (%2)&lt;/p></source>
      <comment>



medium</comment>
      <translation>&lt;p style=white-space:pre>ប្រភេទ​(​ទ្រង់​ទ្រាយ​) ៖ %1 (%2)&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Attached to:  %1&lt;/p></source>
      <comment>



image</comment>
      <translation>&lt;p>បាន​​ភ្ជាប់​ទៅ​កាន់ ៖ %1&lt;/p></translation>
    </message>
    <message>
      <source>&lt;i>Not Attached&lt;/i></source>
      <comment>



image</comment>
      <translation>&lt;i>មិន​បានភ្ជាប់&lt;/i></translation>
    </message>
    <message>
      <source>&lt;i>Checking accessibility...&lt;/i></source>
      <comment>



medium</comment>
      <translation>&lt;i>កំពុង​ពិនិត្យមើល​​​មធ្យោបាយ​ងាយ​ស្រួល​​...&lt;/i></translation>
    </message>
    <message>
      <source>Failed to check media accessibility.</source>
      <comment>



medium</comment>
      <translation>បានបរាជ័យ​ក្នុងកា​រ​ពិនិត្យ​មើល​មធ្យោ​បាយ​ងាយ​ស្រួល​​របស់​មេឌៀ ។​</translation>
    </message>
    <message>
      <source>&lt;b>No medium selected&lt;/b></source>
      <comment>



medium</comment>
      <translation>&lt;b>គ្មាន​ឧបករណ៍​ត្រូវ​បាន​ជ្រើស​ទេ&lt;/b></translation>
    </message>
    <message>
      <source>You can also change this while the machine is running.</source>
      <translation>អ្នក​ក៏អាច​ផ្លាស់ប្ដូរ​វា ខណៈពេល​ដែល​ម៉ាស៊ីន​កំពុង​រត់ ។</translation>
    </message>
    <message>
      <source>&lt;b>No media available&lt;/b></source>
      <comment>



medium</comment>
      <translation>&lt;b>មិនមាន​មេឌៀ​ទេ&lt;/b></translation>
    </message>
    <message>
      <source>You can create media images using the virtual media manager.</source>
      <translation>អ្នក​អាច​បង្កើត​រូបភាព​មេឌៀ ដោយ​ប្រើ​កម្មវិធី​គ្រប់គ្រង​មេឌៀ​និម្មិត ។</translation>
    </message>
    <message>
      <source>Attaching this hard disk will be performed indirectly using a newly created differencing hard disk.</source>
      <comment>




medium</comment>
      <translation>ការ​ភ្ជាប់​ជា​មួយ​នឹង​ថាស​រឹង​នេះ នឹង​ត្រូវ​បាន​អនុវត្ត​ដោយ​ប្រយោល​ ដោយ​ប្រើ​ថាសរឹង​ផ្សេង​ដែលបានបង្កើត​ថ្មីៗ ។</translation>
    </message>
    <message>
      <source>Some of the media in this hard disk chain are inaccessible. Please use the Virtual Media Manager in &lt;b>Show Differencing Hard Disks&lt;/b> mode to inspect these media.</source>
      <comment>





medium</comment>
      <translation>មេឌៀ​មួយ​ចំនួន​នៅ​ក្នុង​ស្រឡាយ​ផ្នែក​រឹង​នេះ​មិនអាច​ចូល​ដំណើរការ​បាន​ទេ ។ សូម​ប្រើ​កម្មវិធី​គ្រប់គ្រង​មេឌៀ​និម្មិត​នៅ​ក្នុង​របៀប &lt;b>បង្ហាញ​ថាសរឹង​ផ្សេងៗ​គ្នា&lt;/b> ដើម្បី​ពិនិត្យមើល​មេឌៀ​ទាំង​នេះ ។</translation>
    </message>
    <message>
      <source>This base hard disk is indirectly attached using the following differencing hard disk:</source>
      <comment>




medium</comment>
      <translation>ថាសរឹង​មូលដ្ឋាន​នេះ​ត្រូវ​បាន​ភ្ជាប់​ដោយ​ប្រយោល​ ដោយ​ប្រើ​ថាសរឹង​ផ្សេង ៖</translation>
    </message>
    <message>
      <source>%n year(s)</source>
      <translation>%n ឆ្នាំ</translation>
    </message>
    <message>
      <source>%n month(s)</source>
      <translation>%n ខែ</translation>
    </message>
    <message>
      <source>%n day(s)</source>
      <translation>%n ថ្ងៃ</translation>
    </message>
    <message>
      <source>%n hour(s)</source>
      <translation>%n ម៉ោង</translation>
    </message>
    <message>
      <source>%n minute(s)</source>
      <translation>%n នាទី​</translation>
    </message>
    <message>
      <source>%n second(s)</source>
      <translation>%n វិនាទី</translation>
    </message>
    <message>
      <source>(CD/DVD)</source>
      <translation>(ស៊ីឌី​/​ឌី​វីឌី​)</translation>
    </message>
  </context>
  <context>
    <name>VBoxGlobalSettings</name>
    <message>
      <source>'%1 (0x%2)' is an invalid host key code.</source>
      <translation>'%1 (0x%2)' គឺជា​កូដ​គ្រាប់ចុច​ម៉ាស៊ីន​មិនត្រឹមត្រូវ ។</translation>
    </message>
    <message>
      <source>The value '%1' of the key '%2' doesn't match the regexp constraint '%3'.</source>
      <translation>តម្លៃ '%1' នៃ​សោ '%2' មិនផ្គូផ្គង​កម្រិត​ regexp '%3' ។</translation>
    </message>
    <message>
      <source>Cannot delete the key '%1'.</source>
      <translation>មិន​អាច​លុប​គ្រាប់ចុច '%1' បានទេ ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxHelpButton</name>
    <message>
      <source>&amp;Help</source>
      <translation>ជំនួយ</translation>
    </message>
  </context>
  <context>
    <name>VBoxImportApplianceWgt</name>
    <message>
      <source>Reading Appliance ...</source>
      <translation>កំពុង​អាន​ឧបករណ៍​...</translation>
    </message>
    <message>
      <source>Importing Appliance ...</source>
      <translation>កំពុង​នាំ​ចូល​ឧបករណ៍​...</translation>
    </message>
  </context>
  <context>
    <name>VBoxImportApplianceWzd</name>
    <message>
      <source>Select an appliance to import</source>
      <translation>ជ្រើស​ឧបករណ៍​​ដើម្បី​នាំ​ចូល​</translation>
    </message>
    <message>
      <source>Open Virtualization Format (%1)</source>
      <translation>Open Virtualization Format (%1)</translation>
    </message>
    <message>
      <source>Appliance Import Wizard</source>
      <translation>អ្នក​ជំនួយ​ការ​នាំ​ចូល​ឧបករណ៍​</translation>
    </message>
    <message>
      <source>Welcome to the Appliance Import Wizard!</source>
      <translation>សូម​ស្វាគមន៍​មក​កាន់​អ្នក​ជំនួយកា​រនាំចូល​ឧបករណ៍ !</translation>
    </message>
    <message>
      <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;>
&lt;html>&lt;head>&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; />&lt;style type=&quot;text/css&quot;>
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>This wizard will guide you through importing an appliance. &lt;/p>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>Use the &lt;span style=&quot; font-weight:600;&quot;>Next&lt;/span> button to go the next page of the wizard and the &lt;span style=&quot; font-weight:600;&quot;>Back&lt;/span> button to return to the previous page.&lt;/p>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>VirtualBox currently supports importing appliances saved in the Open Virtualization Format (OVF). To continue, select the file to import below:&lt;/p>&lt;/body>&lt;/html></source>
      <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;>
&lt;html>&lt;head>&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; />&lt;style type=&quot;text/css&quot;>
p, li { white-space: pre-wrap; }
&lt;/style>&lt;/head>&lt;body>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>អ្នក​ជំនួយ​ការ​នេះ​នឹង​នាំយក​​តាម​រយៈ​ការ​នាំចូល​ឧបករណ៍ ។&lt;/p>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>ប្រើ​ប៊ូតុង &lt;span style=&quot; font-weight:600;&quot;>បន្ទាប់&lt;/span> ទៅ​ទំព័រ​បន្ទាប់​របស់​អ្នក​ជំនួយ​ការ និងប៊ូតុង​ &lt;span style=&quot; font-weight:600;&quot;>ថយក្រោយ&lt;/span> ដើម្បី​ត្រឡប់​ទៅ​ទំព័រ​មុន ។&lt;/p>
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;>បច្ចុប្បន្ន VirtualBox គាំទ្រ​ការ​នាំចូល​ឧបករណ៍ ដែលបាន​រក្សាទុក​នៅ​ក្នុង​ទ្រង់ទ្រាយ Open Virtualization Format (OVF) ។ ដើម្បី​បន្ត ជ្រើស​ឯកសារ​​ត្រូវ​នាំ​ចូល​ខាង​ក្រោម ៖&lt;/p>&lt;/body>&lt;/html></translation>
    </message>
    <message>
      <source>&lt; &amp;Back</source>
      <translation>&lt;ថយ​ក្រោយ​</translation>
    </message>
    <message>
      <source>&amp;Next ></source>
      <translation>បន្ទាប់></translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់​</translation>
    </message>
    <message>
      <source>Appliance Import Settings</source>
      <translation>​កំណត់​ការ​​នាំ​ចូល​ឧបករណ៍​</translation>
    </message>
    <message>
      <source>These are the virtual machines contained in the appliance and the suggested settings of the imported VirtualBox machines. You can change many of the properties shown by double-clicking on the items and disable others using the check boxes below.</source>
      <translation>មាន​ម៉ាស៊ីន​និម្មិត​ដែល​មាន​នៅ​ក្នុង​ឧបករណ៍​ និង​ការ​កំណត់​ដែលបាន​ស្នើ​របស់ម៉ាស៊ីន VirtualBox ដែល​បាននាំចូល ។ អ្នក​អាច​ផ្លាស់ប្ដូរ​លក្ខណសម្បត្តិ​ដែលបានបង្ហាញ​ដោយ​ចុច​ទ្វេដង​លើ​ធាតុ និងបិទ​​លក្ខណសម្បត្តិ​ផ្សេងៗ​ទៀត ដោយ​ប្រើ​ប្រ​អប់ធីក​ខាង​ក្រោម ។</translation>
    </message>
    <message>
      <source>Restore Defaults</source>
      <translation>ស្តារ​លំនាំដើម</translation>
    </message>
    <message>
      <source>&amp;Import ></source>
      <translation>នាំ​ចូល​​></translation>
    </message>
  </context>
  <context>
    <name>VBoxImportLicenseViewer</name>
    <message>
      <source>&lt;b>The virtual system &quot;%1&quot; requires that you agree to the terms and conditions of the software license agreement shown below.&lt;/b>&lt;br />&lt;br />Click &lt;b>Agree&lt;/b> to continue or click &lt;b>Disagree&lt;/b> to cancel the import.</source>
      <translation>&lt;b>ប្រ​ព័ន្ធ​ជាក់​ស្តែង​របស់​​ &quot;%1&quot; តម្រូវ​ការ​ដែល​អ្នក​យល់​ព្រមទៅ​កាន់ពេល​កំណត់​ និ​ងលក្ខ​ខ័ណ្នៃអាជ្ញាបណ្ណ​​ផ្នែកទន់​កិច្ច​ព្រម​ព្រៀង​​បង្ហាញ​ខាង​ក្រោម​ ។&lt;/b>&lt;br />&lt;br />ចុច​​ &lt;b>យល់​ព្រម​​&lt;/b> ទៅ​បន្ត​ ឬ​&lt;b>មិន​យល់​ព្រម​​&lt;/b> ទៅ​​បោះ​បង់​កា​រនាំចូល​ ។</translation>
    </message>
    <message>
      <source>Software License Agreement</source>
      <translation>កិច្ច​ព្រមព្រៀង​អាជ្ញាបណ្ណ​​កម្មវិធី​</translation>
    </message>
    <message>
      <source>&amp;Disagree</source>
      <translation>មិន​យល់ព្រម​</translation>
    </message>
    <message>
      <source>&amp;Agree</source>
      <translation>យល់ព្រម​</translation>
    </message>
    <message>
      <source>&amp;Print...</source>
      <translation>បោះពុម្ព...</translation>
    </message>
    <message>
      <source>&amp;Save...</source>
      <translation>រក្សាទុក...</translation>
    </message>
    <message>
      <source>Text (*.txt)</source>
      <translation>អត្ថបទ​​ (*.txt)</translation>
    </message>
    <message>
      <source>Save license to file...</source>
      <translation>រក្សាទុក​អាជ្ញា​បណ្ណទៅ​ឯកសារ...</translation>
    </message>
  </context>
  <context>
    <name>VBoxLicenseViewer</name>
    <message>
      <source>VirtualBox License</source>
      <translation>អាជ្ញាបណ្ណ VirtualBox</translation>
    </message>
    <message>
      <source>I &amp;Agree</source>
      <translation>ខ្ញុំ​យល់ព្រម</translation>
    </message>
    <message>
      <source>I &amp;Disagree</source>
      <translation>ខ្ញុំ​មិន​យល់ព្រម​ទេ</translation>
    </message>
  </context>
  <context>
    <name>VBoxLineTextEdit</name>
    <message>
      <source>&amp;Edit</source>
      <translation>កែសម្រួល​</translation>
    </message>
  </context>
  <context>
    <name>VBoxLogSearchPanel</name>
    <message>
      <source>Close the search panel</source>
      <translation>បិទ​បន្ទះ​ស្វែងរក</translation>
    </message>
    <message>
      <source>Find </source>
      <translation>រក</translation>
    </message>
    <message>
      <source>Enter a search string here</source>
      <translation>បញ្ចូល​ខ្សែអក្សរ​ស្វែងរក​នៅ​ទីនេះ</translation>
    </message>
    <message>
      <source>&amp;Previous</source>
      <translation>មុន</translation>
    </message>
    <message>
      <source>Search for the previous occurrence of the string</source>
      <translation>ស្វែងរក​ការ​កើតឡើង​មុន​របស់​ខ្សែអក្សរ</translation>
    </message>
    <message>
      <source>&amp;Next</source>
      <translation>បន្ទាប់</translation>
    </message>
    <message>
      <source>Search for the next occurrence of the string</source>
      <translation>ស្វែងរក​ការ​កើតឡើង​បន្ទាប់​របស់​ខ្សែអក្សរ</translation>
    </message>
    <message>
      <source>C&amp;ase Sensitive</source>
      <translation>ប្រកាន់​អក្សរ​តូចធំ</translation>
    </message>
    <message>
      <source>Perform case sensitive search (when checked)</source>
      <translation>អនុវត្ត​ការ​ស្វែងរក​ប្រកាន់​អក្សរតូចធំ (នៅពេល​គូស​ធីក)</translation>
    </message>
    <message>
      <source>String not found</source>
      <translation>រក​មិនឃើញ​ខ្សែអក្សរ</translation>
    </message>
  </context>
  <context>
    <name>VBoxMediaManagerDlg</name>
    <message>
      <source>&amp;Actions</source>
      <translation>សកម្មភាព</translation>
    </message>
    <message>
      <source>&amp;New...</source>
      <translation>ថ្មី...</translation>
    </message>
    <message>
      <source>&amp;Add...</source>
      <translation>បន្ថែម...</translation>
    </message>
    <message>
      <source>R&amp;emove</source>
      <translation>យកចេញ</translation>
    </message>
    <message>
      <source>Re&amp;lease</source>
      <translation>ចេញផ្សាយ</translation>
    </message>
    <message>
      <source>Re&amp;fresh</source>
      <translation>ធ្វើ​ឲ្យ​ស្រស់</translation>
    </message>
    <message>
      <source>Create a new virtual hard disk</source>
      <translation>បង្កើត​ថាសរឹង​និម្មិត​ថ្មី</translation>
    </message>
    <message>
      <source>Add an existing medium</source>
      <translation>បន្ថែម​ឧបករណ៍​ដែល​មាន​ស្រាប់</translation>
    </message>
    <message>
      <source>Remove the selected medium</source>
      <translation>យក​ឧបករណ៍​ផ្ទុក​ដែល​បាន​ជ្រើស</translation>
    </message>
    <message>
      <source>Release the selected medium by detaching it from the machines</source>
      <translation>ចេញផ្សាយ​ឧបករណ៍​ផ្ទុក​ដែល​បានជ្រើស ដោយ​ផ្ដាច់​វា​ពី​ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>Refresh the media list</source>
      <translation>ធ្វើ​ឲ្យ​បញ្ជី​មេឌៀ​ស្រស់</translation>
    </message>
    <message>
      <source>Location</source>
      <translation>ទីតាំង</translation>
    </message>
    <message>
      <source>Type (Format)</source>
      <translation>ប្រភេទ (ទ្រង់ទ្រាយ)</translation>
    </message>
    <message>
      <source>Attached to</source>
      <translation>បាន​ភ្ជាប់​ទៅកាន់</translation>
    </message>
    <message>
      <source>Checking accessibility</source>
      <translation>ពិនិត្យ​មើល​ភាព​អាច​ដំណើរការ​បាន​ទេ</translation>
    </message>
    <message>
      <source>&amp;Select</source>
      <translation>ជ្រើស</translation>
    </message>
    <message>
      <source>All hard disk images (%1)</source>
      <translation>រូបភាព​ថាសរឹង​ទាំងអស់ (%1)</translation>
    </message>
    <message>
      <source>All files (*)</source>
      <translation>ឯកសារ​ទាំង​អស់ (*)</translation>
    </message>
    <message>
      <source>Select a hard disk image file</source>
      <translation>ជ្រើស​ឯកសារ​រូបភាព​ថាសរឹង</translation>
    </message>
    <message>
      <source>CD/DVD-ROM images (*.iso);;All files (*)</source>
      <translation>រូបភាព​ស៊ីឌី/ឌីវីឌី​រ៉ូម (*.iso);; ឯកសារ​ទាំងអស់ (*)</translation>
    </message>
    <message>
      <source>Select a CD/DVD-ROM disk image file</source>
      <translation>ជ្រើស​ឯកសារ​រូបភាព​ថាស​ស៊ីឌី/ឌីវីឌីរ៉ូម</translation>
    </message>
    <message>
      <source>Floppy images (*.img);;All files (*)</source>
      <translation>រូបភាព​ថាស​ទន់ (*.img);; ឯកសារ​ទាំង​អស់ (*)</translation>
    </message>
    <message>
      <source>Select a floppy disk image file</source>
      <translation>ជ្រើស​ឯកសារ​រូបភាព​ថាស​ទន់</translation>
    </message>
    <message>
      <source>&lt;i>Not&amp;nbsp;Attached&lt;/i></source>
      <translation>&lt;i>មិនបាន​ភ្ជាប់&lt;/i></translation>
    </message>
    <message>
      <source>--</source>
      <comment>no info</comment>
      <translation>--</translation>
    </message>
    <message>
      <source>Virtual Media Manager</source>
      <translation>កម្មវិធី​គ្រប់គ្រង​មេឌៀ​និម្មិត</translation>
    </message>
    <message>
      <source>Hard &amp;Disks</source>
      <translation>ថាសរឹង</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>ឈ្មោះ</translation>
    </message>
    <message>
      <source>Virtual Size</source>
      <translation>ទំហំ​និម្មិត</translation>
    </message>
    <message>
      <source>Actual Size</source>
      <translation>ទំហំ​ពិតប្រាកដ</translation>
    </message>
    <message>
      <source>&amp;CD/DVD Images</source>
      <translation>រូបភាព​ស៊ីឌី/ឌីវីឌី</translation>
    </message>
    <message>
      <source>Size</source>
      <translation>ទំហំ</translation>
    </message>
    <message>
      <source>&amp;Floppy Images</source>
      <translation>រូបភាព​ថាសទន់</translation>
    </message>
    <message>
      <source>Attached to</source>
      <comment>



VMM: Virtual Disk</comment>
      <translation>បាន​ភ្ជាប់​ទៅកាន់</translation>
    </message>
    <message>
      <source>Attached to</source>
      <comment>



VMM: CD/DVD Image</comment>
      <translation>បានភ្ជាប់​ទៅ​កាន់</translation>
    </message>
    <message>
      <source>Attached to</source>
      <comment>



VMM: Floppy Image</comment>
      <translation>បាន​ភ្ជាប់​ទៅកាន់</translation>
    </message>
  </context>
  <context>
    <name>VBoxMiniToolBar</name>
    <message>
      <source>Always show the toolbar</source>
      <translation>​បង្ហាញ​របារ​ឧបករណ៍​ជានិច្ច​</translation>
    </message>
    <message>
      <source>Exit Full Screen or Seamless Mode</source>
      <translation>ចេញពី​របៀប​ពេញ​​អេ​ក្រង់​​ ឬពង្រីក​</translation>
    </message>
    <message>
      <source>Close VM</source>
      <translation>បិទ VM</translation>
    </message>
  </context>
  <context>
    <name>VBoxNetworkDialog</name>
    <message>
      <source>Network Adapters</source>
      <translation>អាដាប់​ទ័រ​បណ្តាញ</translation>
    </message>
  </context>
  <context>
    <name>VBoxNewHDWzd</name>
    <message>
      <source>Create New Virtual Disk</source>
      <translation>បង្កើត​ថាស​និម្មិត​ថ្មី</translation>
    </message>
    <message>
      <source>Welcome to the Create New Virtual Disk Wizard!</source>
      <translation>សូម​ស្វាគមន៍​មក​កាន់​​អ្នក​ជំនួយការ​បង្កើត​ថាស​និម្មិ​តថ្មី !</translation>
    </message>
    <message>
      <source>Virtual Disk Location and Size</source>
      <translation>ទីតាំង និង​ទំហំ​ថាស​និម្មិត</translation>
    </message>
    <message>
      <source>Summary</source>
      <translation>សេចក្ដី​សង្ខេប</translation>
    </message>
    <message>
      <source>&lt;nobr>%1 Bytes&lt;/nobr></source>
      <translation>&lt;nobr>%1 បៃ&lt;/nobr></translation>
    </message>
    <message>
      <source>Hard disk images (*.vdi)</source>
      <translation>រូបភាព​ថាសរឹង (*.vdi)</translation>
    </message>
    <message>
      <source>Select a file for the new hard disk image file</source>
      <translation>ជ្រើស​ឯកសារ​សម្រាប់​ឯកសារ​រូបភាព​ថាស​រឹង</translation>
    </message>
    <message>
      <source>&lt; &amp;Back</source>
      <translation>&lt; ថយក្រោយ</translation>
    </message>
    <message>
      <source>&amp;Next ></source>
      <translation>បន្ទាប់ ></translation>
    </message>
    <message>
      <source>&amp;Finish</source>
      <translation>បញ្ចប់</translation>
    </message>
    <message>
      <source>Type</source>
      <comment>summary</comment>
      <translation>ប្រភេទ</translation>
    </message>
    <message>
      <source>Location</source>
      <comment>summary</comment>
      <translation>ទីតាំង</translation>
    </message>
    <message>
      <source>Size</source>
      <comment>summary</comment>
      <translation>ទំហំ</translation>
    </message>
    <message>
      <source>Bytes</source>
      <comment>summary</comment>
      <translation>បៃ</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់</translation>
    </message>
    <message>
      <source>&lt;p>This wizard will help you to create a new virtual hard disk for your virtual machine.&lt;/p>&lt;p>Use the &lt;b>Next&lt;/b> button to go to the next page of the wizard and the &lt;b>Back&lt;/b> button to return to the previous page.&lt;/p></source>
      <translation>&lt;p>អ្នក​ជំនួយការ​នេះ​ជួយ​អ្នក​ក្នុងកា​របង្កើត​ថាសរឹង​និម្មិត​សម្រាប់​ម៉ាស៊ីន​និម្មិត​របស់​អ្នក ។&lt;/p>&lt;p>ប្រើប៊ូតុង &lt;b>បន្ទាប់&lt;/b> ដើម្បី​ទៅកាន់​ទំព័រ​បន្ទាប់​របស់​អ្នក​ជំនួយការ ប៊ូតុង&lt;b>ថយក្រោយ&lt;/b> ដើម្បី​ត្រឡប់​ទៅ​ទំព័រ​មុន ។&lt;/p></translation>
    </message>
    <message>
      <source>Hard Disk Storage Type</source>
      <translation>ប្រភេទ​ផ្ទុក​របស់​ថាសរឹង</translation>
    </message>
    <message>
      <source>&lt;p>Select the type of virtual hard disk you want to create.&lt;/p>&lt;p>A &lt;b>dynamically expanding storage&lt;/b> initially occupies a very small amount of space on your physical hard disk. It will grow dynamically (up to the size specified) as the Guest OS claims disk space.&lt;/p>&lt;p>A &lt;b>fixed-size storage&lt;/b> does not grow. It is stored in a file of approximately the same size as the size of the virtual hard disk. The creation of a fixed-size storage may take a long time depending on the storage size and the write performance of your harddisk.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​ប្រភេទ​ថាសរឹង​និម្មិត​ដែល​អ្នក​ចង់​បង្កើត ។&lt;/p>&lt;p> &lt;b>កា​រផ្ទុក​ពង្រីក​ជា​ថាមវន្ត&lt;/b> ដំបូង​គ្រប់គ្រង​រាល់​ទំហំ​តូចៗ​របស់ចន្លោះ​នៅ​ក្នុង​ថាសរឹង​ហ្វីស៊ីខល​របស់​អ្នក ។ វា​នឹង​ពង្រីក​ដោយ​ថាមវន្ត (ដល់​ទំហំ​ដែល​បាន​បញ្ជាក់) ជា​ការ​អះអាង​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ ។&lt;/p>&lt;p> &lt;b>កា​រផ្ទុក​ទំហំថេរ&lt;/b> មិន​រីកចំរើន​ទេ ។ វា​ត្រូ​វបាន​ផ្ទុក​នៅ​ក្នុង​ឯកសារ​របស់​ទំហំ​​ប្រហែល​ ជា​ទំហំ​របស់​ថាសរឹង​និម្មិត ។ ការ​បង្កើត​ការ​ផ្ទុក​ទំហំថេរ អាច​ចំណាយ​ពេល​យូរ​ ដោយអាស្រ័យ​លើ​ទំហំ​ផ្ទុក និង​សរសេរ​ការ​អនុវត្ត​នៃ​ថាសរឹង​របស់​អ្នក ។&lt;/p></translation>
    </message>
    <message>
      <source>Storage Type</source>
      <translation>ប្រភេទ​ផ្ទុក</translation>
    </message>
    <message>
      <source>&amp;Dynamically expanding storage</source>
      <translation>ពង្រីក​ការ​ផ្ទុក​ថាមវន្ត</translation>
    </message>
    <message>
      <source>&amp;Fixed-size storage</source>
      <translation>កា​រ​ផ្ទុក​ទំហំថេរ</translation>
    </message>
    <message>
      <source>&lt;p>Press the &lt;b>Select&lt;/b> button to select the location of a file to store the hard disk data or type a file name in the entry field.&lt;/p></source>
      <translation>&lt;p>ចុច​ប៊ូតុង &lt;b>ជ្រើស&lt;/b> ដើម្បី​ជ្រើស​ទីតាំង​របស់​ឯកសារ​ដើម្បី​ផ្ទុក​ថាសរឹង ឬ​ប្រភេទ​ឈ្មោះ​ឯកសារ​នៅ​ក្នុង​វាល​ធាតុ ។&lt;/p></translation>
    </message>
    <message>
      <source>&amp;Location</source>
      <translation>ទីតាំង</translation>
    </message>
    <message>
      <source>&lt;p>Select the size of the virtual hard disk in megabytes. This size will be reported to the Guest OS as the maximum size of this hard disk.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​ទំហំ​ថាសរឹង​និម្មិត​គិត​ជាមេកាបៃ ។ ទំហំ​នេះ​នឹង​ត្រូវ​បាន​រាយការណ៍​ទៅ​កាន់​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​តាម​ទំហំ​អតិបរមា​របស់​ថាសរឹង​នេះ ។&lt;/p></translation>
    </message>
    <message>
      <source>&amp;Size</source>
      <translation>ទំហំ</translation>
    </message>
    <message>
      <source>You are going to create a new virtual hard disk with the following parameters:</source>
      <translation>អ្នកនឹង​បង្កើត​ថាសរឹង​និម្មិត​ថ្មី​ដោយ​មាន​ប៉ារ៉ាម៉ែត្រ​ដូច​ខាងក្រោម ៖</translation>
    </message>
    <message>
      <source>If the above settings are correct, press the &lt;b>Finish&lt;/b> button. Once you press it, a new hard disk will be created.</source>
      <translation>ប្រសិនបើ​កា​រកំណត់​ខាង​លើ​ត្រឹមត្រូវ ចុច​ប៊ូតុង &lt;b>បញ្ចប់&lt;/b> ។ នៅពេល​អ្នក​ចុចវា ថាសរឹង​ថ្មី​នឹង​ត្រូវ​បានបង្កើត​ឡើង ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxNewVMWzd</name>
    <message>
      <source>Create New Virtual Machine</source>
      <translation>បង្កើត​ម៉ាស៊ីន​និម្មិត​ថ្មី</translation>
    </message>
    <message>
      <source>Welcome to the New Virtual Machine Wizard!</source>
      <translation>សូម​ស្វាគមន៍​មក​កាន់​អ្នក​ជំនួយការ​ម៉ាស៊ីន​និម្មិត​ថ្មី !</translation>
    </message>
    <message>
      <source>N&amp;ame</source>
      <translation>ឈ្មោះ</translation>
    </message>
    <message>
      <source>OS &amp;Type</source>
      <translation>ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ</translation>
    </message>
    <message>
      <source>VM Name and OS Type</source>
      <translation>ឈ្មោះ​របស់ VM និង​ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ</translation>
    </message>
    <message>
      <source>&lt;p>Select the amount of base memory (RAM) in megabytes to be allocated to the virtual machine.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​ចំនួន​សតិ (RAM) ​មូលដ្ឋាន​គិត​ជាមេកាបៃ ដែល​​ត្រូវ​បម្រុង​ទុក​សម្រាប់​ម៉ាស៊ីន​និម្មិត ។&lt;/p></translation>
    </message>
    <message>
      <source>Base &amp;Memory Size</source>
      <translation>ទំហំ​សតិ​មូលដ្ឋាន</translation>
    </message>
    <message>
      <source>MB</source>
      <translation>មេកាបៃ</translation>
    </message>
    <message>
      <source>Memory</source>
      <translation>សតិ</translation>
    </message>
    <message>
      <source>Virtual Hard Disk</source>
      <translation>ថាសរឹង​និម្មិត</translation>
    </message>
    <message>
      <source>Summary</source>
      <translation>សេចក្ដី​សង្ខេប</translation>
    </message>
    <message>
      <source>The recommended base memory size is &lt;b>%1&lt;/b> MB.</source>
      <translation>ទំហំ​សតិ​មូលដ្ឋាន​ដែលបានផ្ដល់​អនុសាសន៍​គឺ &lt;b>%1&lt;/b> មេកាបៃ ។</translation>
    </message>
    <message>
      <source>The recommended size of the boot hard disk is &lt;b>%1&lt;/b> MB.</source>
      <translation>ទំហំ​ដែល​បានផ្ដល់​អនុសាសន៍​របស់​ថាសរឹង​ចាប់ផ្ដើម​គឺ &lt;b>%1&lt;/b> មេកាបៃ ។</translation>
    </message>
    <message>
      <source>&lt;p>This wizard will guide you through the steps that are necessary to create a new virtual machine for VirtualBox.&lt;/p>&lt;p>Use the &lt;b>Next&lt;/b> button to go the next page of the wizard and the &lt;b>Back&lt;/b> button to return to the previous page.&lt;/p></source>
      <translation>&lt;p>អ្នក​ជំនួយ​កា​រនេះ​នឹង​នាំអ្នក​តាម​ជំហាន​ដែល​ចាំបាច់​ដើម្បីបង្កើត​ម៉ាស៊ីន​និម្មិ​តថ្មី​សម្រាប់ VirtualBox ។&lt;/p>&lt;p>ប្រើ​ប៊ូតុង &lt;b>បន្ទាប់&lt;/b> ដើម្បី​ទៅកាន់​ទំព័រ​បន្ទាប់​របស់​អ្នក​ជំនួយការ ហើយ​ប៊ូតុង &lt;b>ថយក្រោយ&lt;/b> ដើម្បី​ត្រឡប់​ទៅ​ទំព័រ​មុន ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt; &amp;Back</source>
      <translation>&lt;ថយក្រោយ</translation>
    </message>
    <message>
      <source>&amp;Next ></source>
      <translation>បន្ទាប់ ></translation>
    </message>
    <message>
      <source>&lt;p>Enter a name for the new virtual machine and select the type of the guest operating system you plan to install onto the virtual machine.&lt;/p>&lt;p>The name of the virtual machine usually indicates its software and hardware configuration. It will be used by all VirtualBox components to identify your virtual machine.&lt;/p></source>
      <translation>&lt;p>បញ្ចូល​ឈ្មោះ​ឲ្យ​ម៉ាស៊ីន​និម្មតិ​ថ្មី ហើយ​ជ្រើស​ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ ដែល​អ្នក​ចង់​ដំឡើង​ក្នុង​ម៉ាស៊ីន​និម្មិត ។ &lt;/p>&lt;p>ឈ្មោះ​របស់​ម៉ាស៊ីន​និម្មិត​តាមធម្មតា​បង្ហាញ​ការ​កំណត់​រចនាសម្ព័ន្ធ​ផ្នែក​រឹង និង​ផ្នែក​ទន់​របស់​វា ។ វា​នឹង​ត្រូវ​បាន​ប្រើ​ដោយ​សមាសធាតុ​ VirtualBox ដើម្បី​បញ្ជាក់​អត្តសញ្ញាណ​ម៉ាស៊ីន​និម្មិត​របស់​អ្នក ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>You are going to create a new virtual machine with the following parameters:&lt;/p></source>
      <translation>&lt;p>អ្នករៀបនឹង​បង្កើត​ម៉ាស៊ីន​និម្មិត​ថ្មី​ដែល​មាន​ប៉ារ៉ាម៉ែត្រ​ដូច​ខាងក្រោម ៖&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>If the above is correct press the &lt;b>Finish&lt;/b> button. Once you press it, a new virtual machine will be created. &lt;/p>&lt;p>Note that you can alter these and all other setting of the created virtual machine at any time using the &lt;b>Settings&lt;/b> dialog accessible through the menu of the main window.&lt;/p></source>
      <translation>&lt;p>ប្រសិនបើ​ខា​ងលើ​ត្រឹមត្រូវ​​ហើយ ចុច​ប៊ូតុង &lt;b>បញ្ចប់&lt;/b> ។ នៅពេល​អ្នក​ចុច​វា ម៉ាស៊ីន​និម្មិត​ថ្មី​នឹង​ត្រូវ​បាន​បង្កើត​ឡើង ។ &lt;/p>&lt;p>ចំណាំ​ថា អ្នក​អាច​ត្រង​ការ​កំណត់​ទាំង​នេះ និង​ការ​កំណត់​ផ្សេងៗ​របស់​ម៉ាស៊ីន​និម្មិត​ដែល​បានបង្កើត​​នៅពេល​ដែល​ប្រើ​ប្រអប់ &lt;b>ការ​កំណត់&lt;/b> ដែល​អាច​ចូល​ដំណើរ​ការ​បាន​តាមរយៈ​ម៉ឺនុយ​របស់​បង្អួច​មេ ។&lt;/p></translation>
    </message>
    <message>
      <source>&amp;Finish</source>
      <translation>បញ្ចប់</translation>
    </message>
    <message>
      <source>MB</source>
      <comment>megabytes</comment>
      <translation>មេកាបៃ</translation>
    </message>
    <message>
      <source>Name</source>
      <comment>summary</comment>
      <translation>ឈ្មោះ</translation>
    </message>
    <message>
      <source>OS Type</source>
      <comment>summary</comment>
      <translation>ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ</translation>
    </message>
    <message>
      <source>Base Memory</source>
      <comment>summary</comment>
      <translation>សតិមូលដ្ឋាន</translation>
    </message>
    <message>
      <source>Boot Hard Disk</source>
      <comment>summary</comment>
      <translation>ថាសរឹង​ចាប់ផ្ដើម</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់</translation>
    </message>
    <message>
      <source>&lt;p>Select a hard disk image to be used as the boot hard disk of the virtual machine. You can either create a new hard disk using the &lt;b>New&lt;/b> button or select an existing hard disk image from the drop-down list or by pressing the &lt;b>Existing&lt;/b> button (to invoke the Virtual Media Manager dialog).&lt;/p>&lt;p>If you need a more complicated hard disk setup, you can also skip this step and attach hard disks later using the VM Settings dialog.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​រូបភាព​ថាសរឹង​ត្រូវ​ប្រើ​ជា​ថាស​រឹង​ចាប់ផ្ដើម​របស់ម៉ាស៊ីន​និម្មិត ។ អ្នកអាច​បង្កើត​ថាសរឹង​ថ្មី​ដោយ​ប្រើ​ប៊ូតុង &lt;b>ថ្មី&lt;/b> ឬ​ជ្រើស​រូបភាព​ថាសរឹង​ដែលមាន​ស្រាប់​ពី​បញ្ជី​ទម្លាក់ចុះ ឬ​ដោយ​ចុច​ប៊ូតុង &lt;b>មាន​ស្រាប់&lt;/b> (ដើម្បីដកហូត​ប្រអប់​កម្មវិធី​គ្រប់គ្រង​មេឌៀ​និម្មិត) ។&lt;/p>&lt;p>ប្រសិនបើ​អ្នក​ត្រូវការ​រៀបចំ​ថាសរឹង​ដែល​ស្មុគស្មាញ អ្នក​ក៏​អាច​រំលង​ជំហាន​នេះ ហើយ​ភ្ជាប់​ថាសរឹង​ពេលក្រោយ​ដោយ​ប្រើ​ប្រអប់​កំណត់របស់ VM ។&lt;/p></translation>
    </message>
    <message>
      <source>Boot Hard &amp;Disk (Primary Master)</source>
      <translation>ថាសរឹង​ចាប់ផ្ដើម (Primary Master)</translation>
    </message>
    <message>
      <source>&amp;Create new hard disk</source>
      <translation>បង្កើត​ថាស​រឹង​ថ្មី</translation>
    </message>
    <message>
      <source>&amp;Use existing hard disk</source>
      <translation>ប្រើ​ថាស​រឹង​ដែល​មាន​ស្រាប់​</translation>
    </message>
  </context>
  <context>
    <name>VBoxOSTypeSelectorWidget</name>
    <message>
      <source>Operating &amp;System:</source>
      <translation>ប្រព័ន្ធ​ប្រតិបត្តិការ ៖</translation>
    </message>
    <message>
      <source>Displays the operating system family that you plan to install into this virtual machine.</source>
      <translation>បង្ហាញ​ក្រុមគ្រួសារ​ប្រព័ន្ធ​ប្រតិបត្តិការ​ដែល​អ្នក​មានគម្រោង​ដំឡើង​ក្នុង​ម៉ាស៊ីន​និម្មិត​នេះ ។</translation>
    </message>
    <message>
      <source>Displays the operating system type that you plan to install into this virtual machine (called a guest operating system).</source>
      <translation>បង្ហាញ​ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ​ដែល​អ្នក​​មាន​​គម្រោង​ដំឡើង​ម៉ាស៊ីន​និម្មិត​នេះ (បាន​ហៅ​ប្រព័ន្ធ​ប្រតិបត្តិ​របស់​ម៉ាស៊ីន​ភ្ញៀវ) ។</translation>
    </message>
    <message>
      <source>&amp;Version:</source>
      <translation>កំណែ ៖</translation>
    </message>
  </context>
  <context>
    <name>VBoxProblemReporter</name>
    <message>
      <source>VirtualBox - Information</source>
      <comment>msg box title</comment>
      <translation>VirtualBox - ព័ត៌មាន</translation>
    </message>
    <message>
      <source>VirtualBox - Question</source>
      <comment>msg box title</comment>
      <translation>VirtualBox - សំណួរ</translation>
    </message>
    <message>
      <source>VirtualBox - Warning</source>
      <comment>msg box title</comment>
      <translation>VirtualBox - កា​រព្រមាន</translation>
    </message>
    <message>
      <source>VirtualBox - Error</source>
      <comment>msg box title</comment>
      <translation>VirtualBox - កំហុស</translation>
    </message>
    <message>
      <source>VirtualBox - Critical Error</source>
      <comment>msg box title</comment>
      <translation>VirtualBox - កំហុស​ធ្ងន់ធ្ងរ</translation>
    </message>
    <message>
      <source>Do not show this message again</source>
      <comment>msg box flag</comment>
      <translation>កុំ​បង្ហាញ​សារ​នេះ​ម្ដង​ទៀត</translation>
    </message>
    <message>
      <source>Failed to open &lt;tt>%1&lt;/tt>. Make sure your desktop environment can properly handle URLs of this type.</source>
      <translation>បានបរាជ័យ​ក្នុងការ​បើក &lt;tt>%1&lt;/tt> ។ ប្រាកដ​ថា​បរិស្ថាន​ផ្ទៃតុ​របស់​អ្នក​អាច​គ្រប់គ្រង URLs នៃ​ប្រភេទ​នេះ​យ៉ាង​ត្រឹមត្រូវ ។</translation>
    </message>
    <message>
      <source>&lt;p>Failed to initialize COM or to find the VirtualBox COM server. Most likely, the VirtualBox server is not running or failed to start.&lt;/p>&lt;p>The application will now terminate.&lt;/p></source>
      <translation>&lt;p>បានបរាជ័យ​ក្នុងកា​រចាប់ផ្ដើម COM ឬ​រកម៉ាស៊ីនបម្រើ VirtualBox COM ។ ភាគ​ច្រើន​ម៉ាស៊ីនបម្រើ VirtualBox មិន​រត់​ ឬ​បរាជ័យ​ក្នុងកា​រចាប់ផ្ដើម ។&lt;/p>&lt;p>កម្មវិធី​នឹង​បិទ​ឥឡូវ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Failed to create the VirtualBox COM object.&lt;/p>&lt;p>The application will now terminate.&lt;/p></source>
      <translation>&lt;p>បានបរាជ័យ​ក្នុងកា​របង្កើត​វត្ថុ VirtualBox COM ។&lt;/p>&lt;p>កម្មវិធី​នឹង​បិទ​ឥឡូវ​នេះ ។&lt;/p></translation>
    </message>
    <message>
      <source>Failed to set global VirtualBox properties.</source>
      <translation>បាន​បរាជ័យ​ក្នុង​ការ​កំណត់លក្ខណៈសម្បត្តិរបស់ VirtualBox ។</translation>
    </message>
    <message>
      <source>Failed to access the USB subsystem.</source>
      <translation>បាន​បរាជ័យ​ក្នុង​ការ​​ចូលដំណើរការ​ប្រព័ន្ធ​រង​របស់ USB ។</translation>
    </message>
    <message>
      <source>Failed to create a new virtual machine.</source>
      <translation>បាន​បរាជ័យ​ក្នុង​ការ​បង្កើត​ម៉ាស៊ីន​និម្មិត​ថ្មី ។</translation>
    </message>
    <message>
      <source>Failed to create a new virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បានបរាជ័យ​ក្នុងការ​បង្កើត​ម៉ាស៊ីន​និម្មិត​ថ្មី &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to apply the settings to the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បានបរាជ័យ​ក្នុងការ​អនុវត្ត​ការ​កំណត់ទៅ​កាន់​ម៉ាស៊ីន​និម្មិត​ថ្មី &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to start the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​ចាប់ផ្ដើម​ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to pause the execution of the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុង​កា​រផ្អាក​ប្រតិបត្តិការ​របស់ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to resume the execution of the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បានបរាជ័យ​ក្នុងការ​បន្ត​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to save the state of the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​ររក្សាទុក​ស្ថានភាព​របស់​ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to create a snapshot of the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បានបរាជ័យ​ក្នុងការ​បង្កើត​រូបថត​អេក្រង់​របស់ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to stop the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​បញ្ឈប់​ម៉ាស៊ីននិម្មិត &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to remove the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​រយក​ម៉ាស៊ីន​និម្មិត​ចេញ &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to discard the saved state of the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​បោះបង់​ស្ថានភាព​របស់​ម៉ាស៊ីន​និម្មិត​ដែលបាន​រក្សាទុក &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>There is no virtual machine named &lt;b>%1&lt;/b>.</source>
      <translation>គ្មាន​ម៉ាស៊ីន​និម្មិត​ដែល​មាន​ឈ្មោះ &lt;b>%1&lt;/b> នោះ​ទេ ។</translation>
    </message>
    <message>
      <source>&lt;p>Are you sure you want to permanently delete the virtual machine &lt;b>%1&lt;/b>?&lt;/p>&lt;p>This operation cannot be undone.&lt;/p></source>
      <translation>&lt;p>តើអ្នក​ប្រាកដ​ជា​ចង់​លុប​ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ជា​អចិន្ត្រៃយ៍​ឬ ?&lt;/p>&lt;p>ប្រតិបត្តិការ​នេះ​មិនអាច​ត្រូវ​បាន​ធ្វើ​វិញ​ទេ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Are you sure you want to unregister the inaccessible virtual machine &lt;b>%1&lt;/b>?&lt;/p>&lt;p>You will not be able to register it again from GUI.&lt;/p></source>
      <translation>&lt;p>តើ​អ្នក​ប្រាកដ​ជា​ចង់​លុប​ឈ្មោះ​ម៉ាស៊ីននិម្មិត &lt;b>%1&lt;/b>​ដែល​មិនអាច​ចូលដំណើរការ​បាន​​ដែរឬទេ?&lt;/p>&lt;p>អ្នក​នឹង​មិនអាច​ចុះឈ្មោះ​វា​បានទៀតទេ​ពី GUI ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Are you sure you want to discard the saved state of the virtual machine &lt;b>%1&lt;/b>?&lt;/p>&lt;p>This operation is equivalent to resetting or powering off the machine without doing a proper shutdown of the guest OS.&lt;/p></source>
      <translation>&lt;p>តើ​អ្នក​ប្រាកដ​ជា​ចង់​បោះបង់​ស្ថានភាព​ដែលបានរក្សាទុក​របស់ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b>ដែរឬទេ ?&lt;/p>&lt;p>ប្រតិបត្តិការ​នេះ​មិន​ស្មើ​នឹង​កា​រកំណត់​ឡើង​វិញ ឬ​បិទ​ម៉ាស៊ីន​​ដោ​​យ​មិន​បានចុច​ប៊ូតុង​ដោយ​សមរម្យ​តាម​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ ។&lt;/p></translation>
    </message>
    <message>
      <source>Failed to create a new session.</source>
      <translation>បានបរាជ័យ​ក្នុងការ​បង្កើត​សម័យ​ថ្មី ។</translation>
    </message>
    <message>
      <source>Failed to open a session for the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បានបរាជ័យ​ក្នុងកា​របើក​សម័យ​សម្រាប់​ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to remove the host network interface &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​យក​ចំណុច​ប្រទាក់​បណ្ដាញ​ម៉ាស៊ីន​ &lt;b>%1&lt;/b> ចេញ ។</translation>
    </message>
    <message>
      <source>Failed to attach the USB device &lt;b>%1&lt;/b> to the virtual machine &lt;b>%2&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​រភ្ជាប់​ឧបករណ៍ USB &lt;b>%1&lt;/b> ទៅ​ម៉ាស៊ីន​និម្មិត &lt;b>%2&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to detach the USB device &lt;b>%1&lt;/b> from the virtual machine &lt;b>%2&lt;/b>.</source>
      <translation>បាន​ប​រាជ័យ​ក្នុងការ​ផ្ដាច់​ឧបករណ៍ USB &lt;b>%1&lt;/b> ពី​ម៉ាស៊ីន​និម្មិត &lt;b>%2&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to create the shared folder &lt;b>%1&lt;/b> (pointing to &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>) for the virtual machine &lt;b>%3&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​របង្កើត​ថត &lt;b>%1&lt;/b> (ចង្អុល​ទៅ​ &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>) សម្រាប់​ម៉ាស៊ីន​និម្មិត &lt;b>%3&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to remove the shared folder &lt;b>%1&lt;/b> (pointing to &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>) from the virtual machine &lt;b>%3&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​រយក​ថត​ដែល​បាន​ចែករំលែក​ចេញ &lt;b>%1&lt;/b> (ចង្អុល​ទៅ &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>) ពី​ម៉ាស៊ីន​និម្មិត &lt;b>%3&lt;/b> ។</translation>
    </message>
    <message>
      <source>&lt;p>The Virtual Machine reports that the guest OS does not support &lt;b>mouse pointer integration&lt;/b> in the current video mode. You need to capture the mouse (by clicking over the VM display or pressing the host key) in order to use the mouse inside the guest OS.&lt;/p></source>
      <translation>&lt;p>ម៉ាស៊ីន​និម្មិត​រាយការណ៍​ថា​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​មិន​គាំទ្រទេ ។ &lt;b>ការ​រួម​បញ្ចូល​ទ្រនិច​កណ្ដុរ&lt;/b> នៅ​ក្នុង​របៀប​វីដេអូ​បច្ចុប្បន្ន ។ អ្នក​ត្រូវតែ​ចាប់យក​កណ្ដុរ (ដោយ​ចុច​លើ​ការ​បង្ហាញ VM ឬ​ចុច​គ្រាប់ចុច​លើ​ម៉ាស៊ីន) ដើម្បី​ប្រើ​កណ្ដុរ​ខាង​ក្នុង​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The Virtual Machine is currently in the &lt;b>Paused&lt;/b> state and not able to see any keyboard or mouse input. If you want to continue to work inside the VM, you need to resume it by selecting the corresponding action from the menu bar.&lt;/p></source>
      <translation>&lt;p>ម៉ាស៊ីន​និម្មិត​បច្ចុប្បន្ន​ស្ថិត​ក្នុង​ស្ថានភាព &lt;b>បាន​ផ្អាក&lt;/b> ហើយ​ដូច្នេះ​មិន​ទទួល​​ក្ដារចុច​ ឬ​កណ្ដុរ​ណា​មួយ ។ ប្រសិន​បើ​អ្នក​ចង់​បន្ត​ដំណើរការ​ខាង​ក្នុង VM អ្នក​ត្រូវ​តែ​បន្តវា ដោយ​ជ្រើស​សកម្មភាព​ដែល​ទាក់ទង​ពីរបារ​ម៉ឺនុយ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Cannot run VirtualBox in &lt;i>VM Selector&lt;/i> mode due to local restrictions.&lt;/p>&lt;p>The application will now terminate.&lt;/p></source>
      <translation>&lt;p>មិន​អាច​រត់ VirtualBox នៅ​ក្នុងរបៀប &lt;i>កម្មវិធី​ជ្រើស VM​&lt;/i> ដោយសារ​តែ​ការ​ដាក់កម្រិត​មូលដ្ឋាន ។&lt;/p>&lt;p>កម្មវិធី​នឹង​បញ្ចប់​ឥឡូវ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;nobr>Fatal Error&lt;/nobr></source>
      <comment>runtime error info</comment>
      <translation>&lt;nobr>កំហុស​ធ្ងន់ធ្ងរ&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Non-Fatal Error&lt;/nobr></source>
      <comment>runtime error info</comment>
      <translation>&lt;nobr>មិនមែន​កំហុស​ធ្ងន់ធ្ងរ&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Warning&lt;/nobr></source>
      <comment>runtime error info</comment>
      <translation>&lt;nobr>កា​រព្រមាន&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Error ID: &lt;/nobr></source>
      <comment>runtime error info</comment>
      <translation>&lt;nobr>លេខសម្គាល់​កំហុស ៖&lt;/nobr></translation>
    </message>
    <message>
      <source>Severity: </source>
      <comment>runtime error info</comment>
      <translation>ភាព​ម៉ឺងម៉ាត់ ៖</translation>
    </message>
    <message>
      <source>&lt;p>A fatal error has occurred during virtual machine execution! The virtual machine will be powered off. Please copy the following error message using the clipboard to help diagnose the problem:&lt;/p></source>
      <translation>&lt;p>កំហុស​ធ្ងន់ធ្ងរ​មានកើត​ឡើង​កំឡុង​ពេល​ប្រតិបត្តិ​ម៉ាស៊ីន​និម្មិត ! ម៉ាស៊ីន​និម្មិត​នឹង​ត្រូវ​បាន​បិទ ។ វា​ត្រូ​វបាន​ស្នើ​ឲ្យ​ប្រើ​ក្ដារតម្បៀតខ្ទាស់​ដើម្បី​ចម្លង​សារ​កំហុស​ដើម្បីពិនិត្យ​មើល​បន្ថែម​ទេ ៖&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>An error has occurred during virtual machine execution! The error details are shown below. You may try to correct the error and resume the virtual machine execution.&lt;/p></source>
      <translation>&lt;p>កំហុស​បានកើត​ឡើង​កំឡុង​ពេល​ប្រតិបត្តិ​ម៉ាស៊ីន​និម្មិត ! សេចក្ដី​លម្អិត​កំហុស​ត្រូវ​បានបង្ហាញ​ខាង​ក្រោម ។ អ្នកអាច​ព្យាយាម​កែ​កំហុស​ដែល​ពិពណ៌នា ហើយ​បន្ត​ប្រតិបត្តិ​ម៉ាស៊ីន​និម្មិត ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The virtual machine execution may run into an error condition as described below. We suggest that you take an appropriate action to avert the error.&lt;/p></source>
      <translation>&lt;p>ការ​ប្រតិបត្តិ​ម៉ាស៊ីន​និម្មិត​អាច​រត់​ក្នុង​លក្ខខណ្ឌ​មាន​កំហុស​ដូច​បាន​ពិពណ៌នា​ខាង​ក្រោម ។ អ្នក​អាច​មិនអើពើ​សារ​នេះ​បាន ប៉ុន្តែ​វា​ត្រូវ​បាន​ស្នើ​ឲ្យ​អនុវត្ត​សកម្មភាព​ដែល​សមរម្យ ដើម្បីប្រាកដ​ថា​សារ​ដែលបានពិពណ៌នា​មិន​កើតឡើង​ទេ ។&lt;/p></translation>
    </message>
    <message>
      <source>Result&amp;nbsp;Code: </source>
      <comment>error info</comment>
      <translation>កូដ​&amp;nbsp;លទ្ធផល ៖</translation>
    </message>
    <message>
      <source>Component: </source>
      <comment>error info</comment>
      <translation>សមាសភាគ ៖</translation>
    </message>
    <message>
      <source>Interface: </source>
      <comment>error info</comment>
      <translation>ចំណុច​ប្រទាក់ ៖</translation>
    </message>
    <message>
      <source>Callee: </source>
      <comment>error info</comment>
      <translation>Callee ៖ </translation>
    </message>
    <message>
      <source>Callee&amp;nbsp;RC: </source>
      <comment>error info</comment>
      <translation>Callee&amp;nbsp;RC ៖ </translation>
    </message>
    <message>
      <source>&lt;p>Could not find a language file for the language &lt;b>%1&lt;/b> in the directory &lt;b>&lt;nobr>%2&lt;/nobr>&lt;/b>.&lt;/p>&lt;p>The language will be temporarily reset to the system default language. Please go to the &lt;b>Preferences&lt;/b> dialog which you can open from the &lt;b>File&lt;/b> menu of the main VirtualBox window, and select one of the existing languages on the &lt;b>Language&lt;/b> page.&lt;/p></source>
      <translation>&lt;p>រក​មិន​ឃើញ​​ឯកសារ​ភាសា​សម្រាប់​ភាសា &lt;b>%1&lt;/b> នៅ​ក្នុង &lt;b>&lt;nobr>%2&lt;/nobr>&lt;/b>ទេ ។&lt;/p>&lt;p>ភាសា​នឹង​ត្រូវ​បានកំណត់​ឡើង​វិញជា​បណ្ដោះអាសន្ន​ទៅ​ភាសា​លំនាំដើម​របស់​ប្រព័ន្ធ ។ សូម​ចូល​ទៅ​ប្រអប់ &lt;b>ចំណូលចិត្ត&lt;/b> ដែល​អ្នក​អាច​បើក​ពី​ម៉ឺនុយ &lt;b>ឯកសារ&lt;/b> នៃ​បង្អួច​ VirtualBox មេ ហើយ​ជ្រើស​ភាសា​មួយ​ក្នុង​ចំណោម​ភាសា​ដែល​មាន​នៅ​លើ​ទំព័រ &lt;b>ភាសា&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Could not load the language file &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b>. &lt;p>The language will be temporarily reset to English (built-in). Please go to the &lt;b>Preferences&lt;/b> dialog which you can open from the &lt;b>File&lt;/b> menu of the main VirtualBox window, and select one of the existing languages on the &lt;b>Language&lt;/b> page.&lt;/p></source>
      <translation>&lt;p>មិនអាច​ផ្ទុក​ឯកសារ​ភាសា &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b> បានទេ ។ &lt;p>ភាសា​នឹង​ត្រូវ​បានកំណត់​ទៅ​ជា​ភាសា​អង់គ្លេស​ជា​បណ្ដោះអាសន្ន ។ សូម​ចូល​ទៅ​ប្រអប់ &lt;b>ចំណូល​ចិត្ត&lt;/b> ដែល​អ្នក​អាច​បើក​ពី​ម៉ឺនុយ​ &lt;b>ឯកសារ&lt;/b> នៃ​បង្អួច VirtualBox មេ ហើយ​ជ្រើស​ភាសា​មួយ​ក្នុងចំណោម​ភាសា​ដែល​មាន​ស្រាប់​នៅ​លើ​ទំព័រ &lt;b>ភាសា&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The VirtualBox Guest Additions installed in the Guest OS are too old: the installed version is %1, the expected version is %2. Some features that require Guest Additions (mouse integration, guest display auto-resize) will most likely stop working properly.&lt;/p>&lt;p>Please update the Guest Additions to the current version by choosing &lt;b>Install Guest Additions&lt;/b> from the &lt;b>Devices&lt;/b> menu.&lt;/p></source>
      <translation>&lt;p>ផ្នែក​បន្ថែម​ភាសា​ភ្ញៀវ​របស់ VirtualBox បាន​ដំឡើង​នៅ​ក្នុង​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​ចាស់​ពេក ៖ កំណែ​ដែល​បាន​ដំឡើង​គឺ %1 កំណែ​ដែល​រំពឹង​ទុកគឺ %2 ។ លក្ខណៈពិសេស​មួយ​ចំនួន​ត្រូវការ​ផ្នែកបន្ថែម​របស់​ម៉ាស៊ីន​ភ្ញៀវ (ការ​រួម​បញ្ចូល​កណ្ដុរ បង្ហាញ​ការ​ផ្លាស់ប្ដូរ​ទំហំ​ដោយ​ស្វ័យ​ប្រវត្តិ​របស់​ម៉ាស៊ីន​ភ្ញៀវ) នឹង​បញ្ឈប់​ដំណើរការ​យ៉ាង​ត្រឹមត្រូវ ។&lt;/p>&lt;p>សូម​ធ្វើ​ឲ្យ​ផ្នែក​បន្ថែម​របស់ម៉ាស៊ីន​ភ្ញៀវ​ទាន់សម័យ​ទៅ​កំណែ​បច្ចុប្បន្ន​ ដោយ​ជ្រើស &lt;b>ដំឡើង​ផ្នែក​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ&lt;/b> ពី​ម៉ឺនុយ &lt;b>ឧបករណ៍&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The VirtualBox Guest Additions installed in the Guest OS are outdated: the installed version is %1, the expected version is %2. Some features that require Guest Additions (mouse integration, guest display auto-resize) may not work as expected.&lt;/p>&lt;p>It is recommended to update the Guest Additions to the current version  by choosing &lt;b>Install Guest Additions&lt;/b> from the &lt;b>Devices&lt;/b> menu.&lt;/p></source>
      <translation>&lt;p>ផ្នែក​បន្ថែម​របស់ម៉ាស៊ីន​ភ្ញៀវ VirtualBox បានដំឡើង​នៅ​ក្នុង​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ​គឺ​ហួស​សម័យ​ហើយ ៖ កំណែ​ដែលបានដំឡើង​គឺ %1 កំណែ​ដែល​រំពឹង​ទុក​គឺ %2 ។ លក្ខណៈ​ពិសេស​មួយ​ចំនួន​ដែល​ត្រូវការ​ផ្នែក​បន្ថែម​របស់​ម៉ាស៊ីន​ភ្ញៀវ (ការ​រួម​បញ្ចូល​កណ្ដុរ បង្ហាញ​កា​រផ្លាស់ប្ដូរ​ទំហំ​ដោយ​ស្វ័យ​ប្រវត្តិ​របស់ម៉ាស៊ីន​ភ្ញៀវ) មិនអាច​ដំណើរការ​ដូច​ដែល​បាន​រំពឹង​ទុក​ទេ ។&lt;/p>&lt;p>វា​ត្រូ​វបានផ្ដល់​អនុសាសន៍​ឲ្យ​ធ្វើ​ឲ្យ​ផ្នែកបន្ថែម​របស់​ម៉ាស៊ីន​ភ្ញៀវ​ទាន់សម័យ​ទៅ​កណែ​បច្ចុប្បន្ន ដោយ​ជ្រើស​ &lt;b>ដំឡើង​ផ្នែក​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ&lt;/b> ពី​ម៉ឺនុយ &lt;b>ឧបករណ៍&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The VirtualBox Guest Additions installed in the Guest OS are too recent for this version of VirtualBox: the installed version is %1, the expected version is %2.&lt;/p>&lt;p>Using a newer version of Additions with an older version of VirtualBox is not supported. Please install the current version of the Guest Additions by choosing &lt;b>Install Guest Additions&lt;/b> from the &lt;b>Devices&lt;/b> menu.&lt;/p></source>
      <translation>&lt;p>ផ្នែក​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ​របស់ VirtualBox នៅ​ក្នុង​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​ថ្មីពេក​សម្រាប់​កំណែ​របស់ VirtualBox ៖ កំណែ​ដែល​បាន​ដំឡើង​គឺ %1 កំណែ​ដែល​រំពឹង​ទុក​គឺ %2 ។&lt;/p>&lt;p>កា​រប្រើកំណែ​ថ្មី​របស់​បន្ថែម​ផ្នែកបន្ថែម​ជាង​កំណែ​ចាស់​របស់ VirtualBox មិន​ត្រូវ​បានគាំទ្រទេ ។ សូម​ដំឡើង​កំណែ​បច្ចុប្បន្ន​របស់​ផ្នែកបន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ ដោយ​ជ្រើស​&lt;b>ដំឡើង​ផ្នែក​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ&lt;/b> ពី​ម៉ឺនុយ &lt;b>ឧបករណ៍&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>Failed to change the snapshot folder path of the virtual machine &lt;b>%1&lt;b> to &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​ផ្លាស់ប្ដូរ​ផ្លូវ​ថត​រូបថត​របស់​ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;b> ទៅ &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> ។</translation>
    </message>
    <message>
      <source>&lt;p>Failed to remove the shared folder &lt;b>%1&lt;/b> (pointing to &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>) from the virtual machine &lt;b>%3&lt;/b>.&lt;/p>&lt;p>Please close all programs in the guest OS that may be using this shared folder and try again.&lt;/p></source>
      <translation>&lt;p>បានបរាជ័យ​ក្នុងកា​រយក​ថត​ដែល​បានចែករំលែក &lt;b>%1&lt;/b> ចេញ (ចង្អុល​ទៅ &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>) ពី​ម៉ាស៊ីន​និម្មិត &lt;b>%3&lt;/b> ។&lt;/p>&lt;p>សូម​បិទ​កម្មវិធី​ទាំង​អស់​នៅ​ក្នុង​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់ម៉ាស៊ីន​ភ្ញៀវ​ដែល​អាច​កំពុង​ប្រើ​ថត​ដែលបានចែករំលែក​នេះ ហើយ​ព្យាយាម​ម្ដង​ទៀត ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Could not find the VirtualBox Guest Additions CD image file &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr> or &lt;nobr>&lt;b>%2&lt;/b>.&lt;/nobr>&lt;/p>&lt;p>Do you wish to download this CD image from the Internet?&lt;/p></source>
      <translation>&lt;p>មិនអាច​រក​​ឯកសារ​រូបភាព​ស៊ីឌី​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ​របស់ VirtualBox &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr> ឬ &lt;nobr>&lt;b>%2&lt;/b> ។&lt;/nobr>&lt;/p>&lt;p>តើអ្នក​ចង់​ទាញ​យក​រូបភាព​ស៊ីឌី​ពី​អ៊ីនធឺណិត​ដែរឬទេ ?&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Failed to download the VirtualBox Guest Additions CD image from &lt;nobr>&lt;a href=&quot;%1&quot;>%2&lt;/a>.&lt;/nobr>&lt;/p>&lt;p>%3&lt;/p></source>
      <translation>&lt;p>បាន​បរាជ័យ​ក្នុងកា​រទាញ​​​យក​រូបភាព​ស៊ីឌី​បន្ថែម​របស់ម៉ាស៊ីន​ភ្ញៀវ​របស់ VirtualBox ពី &lt;nobr>&lt;a href=&quot;%1&quot;>%2&lt;/a> ។&lt;/nobr>&lt;/p>&lt;p>%3&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Are you sure you want to download the VirtualBox Guest Additions CD image from &lt;nobr>&lt;a href=&quot;%1&quot;>%2&lt;/a>&lt;/nobr> (size %3 bytes)?&lt;/p></source>
      <translation>&lt;p>តើ​អ្នក​ប្រាក​ដ​ជា​ចង់​ទាញ​យក​រូបភាព​ស៊ីឌី​បន្ថែម​របស់​ម៉ាស៊ីន​ភ្ញៀវ​របស់ VirtualBox ពី &lt;nobr>&lt;a href=&quot;%1&quot;>%2&lt;/a>&lt;/nobr> (ទំហំ %3 បៃ) ដែរឬទេ?&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The VirtualBox Guest Additions CD image has been successfully downloaded from &lt;nobr>&lt;a href=&quot;%1&quot;>%2&lt;/a>&lt;/nobr> and saved locally as &lt;nobr>&lt;b>%3&lt;/b>.&lt;/nobr>&lt;/p>&lt;p>Do you wish to register this CD image and mount it on the virtual CD/DVD drive?&lt;/p></source>
      <translation>&lt;p>រូបភាពស៊ីឌីផ្នែក​នែក​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ VirtualBត្រូ​វបាន​ទាញយក​ដោយ​ជោគជ័យ​ពីr&lt;nobr>&lt;a href=&quot;%1&quot;>​%2&lt;/a>&lt;/nobr>bហើយ​បាន​រក្សាទុក​ក្នុង​មូលដ្ឋាន​ជា as &lt;nobr>&lt;b>%3&lt;/b> ។&lt;/nobr>&lt;/p>&lt;p>តើ​អ្នក​ចង់​ចុះឈ្មោះ​រូបភាព​ស៊ីឌី​នេះ ហើយ​ម៉ោន​វា​នៅ​លើ​ដ្រាយ​ស៊ីឌី/ឌីវីឌី​និម្មិតដែរឬទេ ?ve?&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The virtual machine window is optimized to work in &lt;b>%1&amp;nbsp;bit&lt;/b> color mode but the virtual display is currently set to &lt;b>%2&amp;nbsp;bit&lt;/b>.&lt;/p>&lt;p>Please open the display properties dialog of the guest OS and select a &lt;b>%3&amp;nbsp;bit&lt;/b> color mode, if it is available, for best possible performance of the virtual video subsystem.&lt;/p>&lt;p>&lt;b>Note&lt;/b>. Some operating systems, like OS/2, may actually work in 32&amp;nbsp;bit mode but report it as 24&amp;nbsp;bit (16 million colors). You may try to select a different color mode to see if this message disappears or you can simply disable the message now if you are sure the required color mode (%4&amp;nbsp;bit) is not available in the guest OS.&lt;/p></source>
      <translation>&lt;p>បង្អួច​ម៉ាស៊ីន​និម្មិត​ត្រូវ​បានធ្វើ​ឲ្យ​ប្រសើរ​ដើម្បី​ធ្វើការ​ក្នុងរបៀប​ពណ៌ &lt;b>%1&amp;nbsp;ប៊ីត&lt;/b> ប៉ុន្តែ​គុណភាពពណ៌​​នៃ​ការ​បង្ហាញ​និម្មិត​បច្ចុប្បន្ន​ត្រូវ​បានកំណត់​ទៅ &lt;b>%2&amp;nbsp;ប៊ីត&lt;/b> ។&lt;/p>&lt;p>សូម​បើក​ប្រអប់លក្ខណៈសម្បត្តិ​បង្ហាញ​របស់​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ ហើយ​ជ្រើសរបៀប​ពណ៌ &lt;b>%3&amp;nbsp;ប៊ីត&lt;/b> ប្រសិនបើ​មាន សម្រាប់​ការអនុវត្ត​ដ៏​ល្អ​បំផុត​របស់​ប្រព័ន្ធ​រង​វីដែអូ​និម្មិត ។&lt;/p>&lt;p>&lt;b>ចំណាំ&lt;/b> ។ ប្រព័ន្ធ​ប្រតិបត្តិការ​មួយ​ចំនួន ដូចជា OS/2 អាច​ដំណើរការ​យ៉ាង​ពិតប្រាកដ​នៅ​ក្នុង​របៀប ៣២&amp;nbsp;ប៊ីត ប៉ុន្តែ​រាយការណ៍​ជា ២៤&amp;nbsp;ប៊ីត (១៦​ លាន​ពណ៌) ។ អ្នក​អាច​ព្យាយាម​ជ្រើស​គុណភាព​ពណ៌​ផ្សេង​ដើម្បីមើល​ថាតើ​សារ​នេះ​មិនបង្ហាញ​ ឬ​អ្នក​អាច​បិទ​សារ​យ៉ាង​សាមញ្ញ ឥឡូវ​ប្រសិនបើ​អ្នក​ប្រាកដ​គុណភាព​ពណ៌​ដែល​ត្រូវការ (%4&amp;nbsp;ប៊ីត) មិនអាច​ប្រើ​បាននៅ​ក្នុង​ប្រព័ន្ធ ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ​ដែល​បានផ្ដល់​ឲ្យ​ទេ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>You didn't attach a hard disk to the new virtual machine. The machine will not be able to boot unless you attach a hard disk with a guest operating system or some other bootable media to it later using the machine settings dialog or the First Run Wizard.&lt;/p>&lt;p>Do you wish to continue?&lt;/p></source>
      <translation>&lt;p>អ្នកមិនបានភ្ជាប់​ថាសរឹង​ទៅកាន់​ម៉ាស៊ីន​និម្មិត​ថ្មីទេ ។ ម៉ាស៊ីន​នឹង​មិនអាច​ចាប់ផ្ដើម​បានទេ លុះត្រា​តែ​អ្នក​ភ្ជាប់​ថាសរឹង​ជា​មួយ​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ ឬ​មេឌៀ​ចាប់ផ្ដើម​ផ្សេងៗ​មួយ​ចំនួន​ទៀត​ទៅវា​ ពេលក្រោយ​ប្រើ​ប្រអប់​កំណត់​ម៉ាស៊ីន​ ឬ​រត់​អ្នក​ជំនួយការ​សិន ។&lt;/p>&lt;p>តើអ្នក​ចង់​បន្ត​ដែរឬទេ ?&lt;/p></translation>
    </message>
    <message>
      <source>Failed to find license files in &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr>.</source>
      <translation>បាន​បរាជ័យ​យក្នុងកា​ររក​ឯកសារ​អាជ្ញាបណ្ណ​នៅ​ក្នុង &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr> ។</translation>
    </message>
    <message>
      <source>Failed to open the license file &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr>. Check file permissions.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​របើក​ឯកសារ​អាជ្ញាប័ណ្ណ &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr> ។ ពិនិត្យ​មើល​សិទ្ធិ​របស់​ឯកសារ ។</translation>
    </message>
    <message>
      <source>Failed to send the ACPI Power Button press event to the virtual machine &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​ផ្ញើ​កា​រចុច​ប៊ូតុងថាមពល ACPI ទៅ​កាន់​ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>&lt;p>Congratulations! You have been successfully registered as a user of VirtualBox.&lt;/p>&lt;p>Thank you for finding time to fill out the registration form!&lt;/p></source>
      <translation>&lt;p>សូមអបអរសាទរ ! អ្នក​បានចុះឈ្មោះ​ជា​អ្នក​ប្រើ VirtualBox ដោយ​ជោគជ័យ​ហើយ ។ &lt;/p>&lt;p>សូម​អរគុណ​ចំពោះ​ការ​ចំណាយ​ពេលវេលា​បំពេញ​សំណុំបែបបទ​ចុះឈ្មោះ !&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Failed to save the global VirtualBox settings to &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b>.&lt;/p></source>
      <translation>&lt;p>បានបរាជ័យ​ក្នុងកា​ររក្សាទុក​ការ​កំណត់ VirtualBox សកល​ទៅ​កាន់ &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Failed to load the global GUI configuration from &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b>.&lt;/p>&lt;p>The application will now terminate.&lt;/p></source>
      <translation>&lt;p>បានបរាជ័យ​ក្នុងការ​ផ្ទុក​ការ​កំណត់​រចនាសម្ព័ន្ធ​ចំណុច​ប្រទាក់​អ្នក​ប្រើ​សកល​ពី &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b> ។&lt;/p>&lt;p>កម្មវិធី​នឹង​បិទ​ឥឡូវ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Failed to save the global GUI configuration to &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b>.&lt;/p>&lt;p>The application will now terminate.&lt;/p></source>
      <translation>&lt;p>បានបរាជ័យ​ក្នុងកា​ររក្សាទុក​ការកំណត់​រចនាសម្ព័ន្ធ​ចំណុច​ប្រទាក់​អ្នក​ប្រើសកល​ទៅ​&lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b> ។&lt;/p>&lt;p>កម្មវិធី​នឹង​បិទ​ឥឡូវ ។&lt;/p></translation>
    </message>
    <message>
      <source>Failed to save the settings of the virtual machine &lt;b>%1&lt;/b> to &lt;b>&lt;nobr>%2&lt;/nobr>&lt;/b>.</source>
      <translation>បានបរាជ័យ​ក្នុង​ការ​រក្សាទុក​ការ​កំណត់​​របស់​ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ទៅ&lt;b>&lt;nobr>%2&lt;/nobr>&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to load the settings of the virtual machine &lt;b>%1&lt;/b> from &lt;b>&lt;nobr>%2&lt;/nobr>&lt;/b>.</source>
      <translation>បានបរាជ័យ​ក្នុងកា​រផ្ទុក​កា​រកំណត់​របស់ម៉ាស៊ីន​និម្មិត &lt;b>%1&lt;/b> ពី &lt;b>&lt;nobr>%2&lt;/nobr>&lt;/b> ។</translation>
    </message>
    <message>
      <source>Delete</source>
      <comment>machine</comment>
      <translation>លុប</translation>
    </message>
    <message>
      <source>Unregister</source>
      <comment>machine</comment>
      <translation>មិន​ចុះឈ្មោះ</translation>
    </message>
    <message>
      <source>Discard</source>
      <comment>saved state</comment>
      <translation>បោះបង់</translation>
    </message>
    <message>
      <source>Disable</source>
      <comment>hard disk</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Download</source>
      <comment>additions</comment>
      <translation>ទាញយក</translation>
    </message>
    <message>
      <source>Mount</source>
      <comment>additions</comment>
      <translation>ម៉ោន</translation>
    </message>
    <message>
      <source>&lt;p>The host key is currently defined as &lt;b>%1&lt;/b>.&lt;/p></source>
      <comment>additional message box paragraph</comment>
      <translation>&lt;p>គ្រាប់ចុច​ម៉ាស៊ីន​បច្ចុប្បន្ន​ត្រូវ​បាន​កំណត់​ជា &lt;b>%1&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>Capture</source>
      <comment>do input capture</comment>
      <translation>ចាប់យក</translation>
    </message>
    <message>
      <source>Check</source>
      <comment>inaccessible media message box</comment>
      <translation>ពិនិត្យ​មើល</translation>
    </message>
    <message>
      <source>Switch</source>
      <comment>fullscreen</comment>
      <translation>ប្ដូរ</translation>
    </message>
    <message>
      <source>Switch</source>
      <comment>seamless</comment>
      <translation>ប្ដូរ</translation>
    </message>
    <message>
      <source>&lt;p>Do you really want to reset the virtual machine?&lt;/p>&lt;p>This will cause any unsaved data in applications running inside it to be lost.&lt;/p></source>
      <translation>&lt;p>តើ​អ្នក​ពិត​ជា​ចង់​កំណត់​ម៉ាស៊ីននិម្មិត​ឡើង​វិញ​ដែរឬទេ ?&lt;/p>&lt;p>នៅ​ពេល​ម៉ាស៊ីន​ត្រូវ​បាន​កំណត់​ឡើង​វិញ ទិន្នន័យ​របស់​កម្មវិធី​ដែល​មិនបានរក្សាទុកទាំង​អស់​ដែល​កំពុង​រត់​វា​នឹង​ត្រូវ​បានបាត់បង់ ។&lt;/p></translation>
    </message>
    <message>
      <source>Reset</source>
      <comment>machine</comment>
      <translation>កំណត់​ឡើង​វិញ</translation>
    </message>
    <message>
      <source>Continue</source>
      <comment>no hard disk attached</comment>
      <translation>បន្ត</translation>
    </message>
    <message>
      <source>Go Back</source>
      <comment>no hard disk attached</comment>
      <translation>ទៅ​ក្រោយ</translation>
    </message>
    <message>
      <source>Failed to copy file &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b> to &lt;b>&lt;nobr>%2&lt;/nobr>&lt;/b> (%3).</source>
      <translation>បានបរាជ័យ​ក្នុងកា​រចម្លង​ឯកសារ &lt;b>&lt;nobr>%1&lt;/nobr>&lt;/b> ទៅ &lt;b>&lt;nobr>%2&lt;/nobr>&lt;/b> (%3) ។</translation>
    </message>
    <message>
      <source>&lt;p>Could not enter seamless mode due to insufficient guest video memory.&lt;/p>&lt;p>You should configure the virtual machine to have at least &lt;b>%1&lt;/b> of video memory.&lt;/p></source>
      <translation>&lt;p>មិនអាច​ចូល​របៀប​គ្មាន​ថេរ ដោយសារ​តែ​សតិ​វីដេអូ​ម៉ាស៊ីន​ភ្ញៀវ​មិន​គ្រប់គ្រាន់ ។&lt;/p>&lt;p>អ្នក​គួរ​កំណត់​រចនាសម្ព័ន្ធ​ម៉ាស៊ីន​និម្មិត​យ៉ាង​ហោចណាស់​មានសតិ​វីដេអូ &lt;b>%1&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Could not switch the guest display to fullscreen mode due to insufficient guest video memory.&lt;/p>&lt;p>You should configure the virtual machine to have at least &lt;b>%1&lt;/b> of video memory.&lt;/p>&lt;p>Press &lt;b>Ignore&lt;/b> to switch to fullscreen mode anyway or press &lt;b>Cancel&lt;/b> to cancel the operation.&lt;/p></source>
      <translation>&lt;p>មិនអាច​ប្ដូរ​ការ​បង្ហាញ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​ទៅ​របៀប​ពេញ​អេក្រង់​បានទេ ដោយសារ​តែ​សតិ​វីដេអូ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​មិន​គ្រប់គ្រាន់ ។&lt;/p>&lt;p>អ្នក​គួរ​កំណត់​រចនាសម្ព័ន្ធ​ម៉ាស៊ីន​និម្មិត​ឲ្យ​មានសតិ​​យ៉ាង​ហោចណាស់​ &lt;b>%1&lt;/b> នៃ​សតិ​វីដេអូ ។&lt;/p>&lt;p>ចុច​ &lt;b>មិនអើពើ&lt;/b> ដើម្បី​ប្ដូរ​ទៅ​របៀប​ពេញអេក្រង់ ឬ​បើ​មិន​ដូច្នេះ​ទេ​ចុច &lt;b>បោះបង់&lt;/b> ដើម្បី​បោះបង់​ប្រតិបត្តិការ ។&lt;/p></translation>
    </message>
    <message>
      <source>You are already running the most recent version of VirtualBox.</source>
      <translation>អ្នក​បាន​ដំឡើង​កំណែ VirtualBox ចុងក្រោយ​បំផុត​រួ​ចហើយ ។ សូម​​ពិនិត្យ​មើល​កំណែ​ពេលក្រោយ ។</translation>
    </message>
    <message>
      <source>&lt;p>You have &lt;b>clicked the mouse&lt;/b> inside the Virtual Machine display or pressed the &lt;b>host key&lt;/b>. This will cause the Virtual Machine to &lt;b>capture&lt;/b> the host mouse pointer (only if the mouse pointer integration is not currently supported by the guest OS) and the keyboard, which will make them unavailable to other applications running on your host machine.&lt;/p>&lt;p>You can press the &lt;b>host key&lt;/b> at any time to &lt;b>uncapture&lt;/b> the keyboard and mouse (if it is captured) and return them to normal operation. The currently assigned host key is shown on the status bar at the bottom of the Virtual Machine window, next to the&amp;nbsp;&lt;img src=:/hostkey_16px.png/>&amp;nbsp;icon. This icon, together with the mouse icon placed nearby, indicate the current keyboard and mouse capture state.&lt;/p></source>
      <translation>&lt;p>អ្នក​បាន &lt;b>ចុចកណ្ដុរ&lt;/b> ខាង​ក្នុង​ការ​បង្ហាញ​របស់​ម៉ាស៊ីន​និម្មិត ឬ​បាន​ចុច​ &lt;b>គ្រាប់ចុច host&lt;/b> ។ វា​នឹងធ្វើ​​ឲ្យ​ម៉ាស៊ីន​និម្មិត &lt;b>ចាប់យក&lt;/b> ទស្សន៍ទ្រនិច​កណ្ដុរ​របស់​ម៉ាស៊ីន (តែ​ក្នុងករណី​ដែល​ការ​រួម​បញ្ចូល​ទស្សន៍ទ្រនិច​បច្ចុប្បន្ន​មិន​ត្រូ​វបានគាំទ្រ​ដោយ​ប្រព័ន្ធប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​ទេ ។) ហើយ​នឹង​ក្ដារចុច ដែល​នឹង​ធ្វើ​ឲ្យ​កម្មវិធី​ផ្សេងៗ​ដែល​កំពុង​រត់​ក្នុង​ម៉ាស៊ីន​របស់​​​មិនអាច​ប្រើបាន​ទេ ។&lt;/p>&lt;p>អ្នក​អាចចុច​ &lt;b>គ្រាប់ចុច host&lt;/b> នៅពេល​ណាមួយ​ដើម្បី​ &lt;b>មិន​ចាប់យក&lt;/b> ក្ដារចុច និងកណ្ដុរ (ប្រសិន​បើ​​ត្រូវ​បានចាប់​យក) ហើយ​ត្រឡប់​ទៅ​ប្រតិបត្តិការ​ធម្មតា​វិញ ។ បច្ចុប្បន្ន​បានផ្ដល់​គ្រាប់ចុច host ត្រូវ​បានបង្ហាញ​នៅ​ក្នុង​របារ​ស្ថានភាពនៅ​ផ្នែក​ខាងក្រោម​របស់​បង្អួច​ម៉ាស៊ីន​និម្មិត នៅជាប់​នឹងរូបតំណាង&amp;nbsp;&lt;img src=:/hostkey_16px.png/>&amp;nbsp; ។ រូបតំណាង​នេះ រួម​ជា​មួយ​នឹង​រូបតំណាង​កណ្ដុរ​ដែល​នៅ​ជាប់នោះ បង្ហាញ​នូវ​ក្ដារចុច និង​ស្ថានភាព​ចាប់យក​កណ្ដុរ​បច្ចុប្បន្ន ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>You have the &lt;b>Auto capture keyboard&lt;/b> option turned on. This will cause the Virtual Machine to automatically &lt;b>capture&lt;/b> the keyboard every time the VM window is activated and make it unavailable to other applications running on your host machine: when the keyboard is captured, all keystrokes (including system ones like Alt-Tab) will be directed to the VM.&lt;/p>&lt;p>You can press the &lt;b>host key&lt;/b> at any time to &lt;b>uncapture&lt;/b> the keyboard and mouse (if it is captured) and return them to normal operation. The currently assigned host key is shown on the status bar at the bottom of the Virtual Machine window, next to the&amp;nbsp;&lt;img src=:/hostkey_16px.png/>&amp;nbsp;icon. This icon, together with the mouse icon placed nearby, indicate the current keyboard and mouse capture state.&lt;/p></source>
      <translation>&lt;p>អ្នក​បានបើក​ជម្រើស &lt;b>ចាប់យក​ក្ដារចុច​ដោយ​ស្វ័យ​ប្រវត្តិ&lt;/b> ។ វា​នឹង​​ធ្វើ​ឲ្យ​ម៉ាស៊ីន​និម្មិត​ &lt;b>ចាប់យក&lt;/b> ក្ដារចុច​ដោយ​ស្វ័យ​ប្រវត្តិ​រាល់ពេល​ដែល​បង្អួច VM ត្រូវ​បាន​ធ្វើ​ឲ្យ​សកម្ម​ ហើយ​ធ្វើ​ឲ្យ​វា​មិនអាច​ប្រើបាន​ចំពោះ​កម្មវិធី​ផ្សេងៗ​ដែល​កំពុង​រត់​ក្នុង​ម៉ាស៊ីន​របស់​អ្នក ៖ នៅពេល​ក្ដារចុច​ត្រូវ​បាន​ចាប់យក  keystrokes ទាំង អស់ (រួម​មាន​ប្រព័ន្ធ​មួយ​ដូចជា ជំនួស(Alt)-ថេប(Tab)) នឹង​ត្រូវ​បញ្ជូន​បន្ត​ទៅ​ VM ។&lt;/p>&lt;p>អ្នក​អាច​ចុច &lt;b>គ្រាប់ចុច host&lt;/b> នៅពេល​ណាមួយ​ទៅ &lt;b>មិន​ចាប់យក&lt;/b> ​ក្ដារចុច​ និង​កណ្ដុរ (ប្រសិនបើ​ត្រូវ​បាន​ចាប់យក) និង​​ត្រឡប់​ពួកវា​ទៅ​ប្រតិបត្តិការ​ធម្មតា​វិញ ។ បច្ចុប្បន្នបានផ្ដល់​គ្រាប់ចុច host ត្រូវ​បានបង្ហាញ​នៅ​លើ​របារ​ស្ថានភាព​នៅផ្នែក​ខាងក្រោម​របស់​បង្អួច​និម្មិត នៅជាប់នឹងរូបតំណាង&amp;nbsp;&lt;img src=:/hostkey_16px.png/>&amp;nbsp; ។ រូបតំណាង​នេះ​ រួម​ជា​មួយ​នឹង​រូបតំណាង​កណ្ដុរ​នៅជាប់​គ្នា​នោះ បង្ហាញក្ដារចុច និង​ស្ថានភាព​ចាប់យក​កណ្ដុរ​បច្ចុប្បន្ន ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The Virtual Machine reports that the guest OS supports &lt;b>mouse pointer integration&lt;/b>. This means that you do not need to &lt;i>capture&lt;/i> the mouse pointer to be able to use it in your guest OS -- all mouse actions you perform when the mouse pointer is over the Virtual Machine's display are directly sent to the guest OS. If the mouse is currently captured, it will be automatically uncaptured.&lt;/p>&lt;p>The mouse icon on the status bar will look like&amp;nbsp;&lt;img src=:/mouse_seamless_16px.png/>&amp;nbsp;to inform you that mouse pointer integration is supported by the guest OS and is currently turned on.&lt;/p>&lt;p>&lt;b>Note&lt;/b>: Some applications may behave incorrectly in mouse pointer integration mode. You can always disable it for the current session (and enable it again) by selecting the corresponding action from the menu bar.&lt;/p></source>
      <translation>&lt;p>ម៉ាស៊ីន​និម្មិត​រាយការណ៍​ថា​ប្រព័ន្ធ​ប្រតិបត្តិការ​គាំទ្រ &lt;b>ការ​រួម​បញ្ចូល​ទស្សន៍​ទ្រនិច​កណ្ដុរ&lt;/b> ។ នេះ​មានន័យ​ថា អ្នក​មិន​ចាំបាច់ &lt;i>ចាប់យក&lt;/i> ទស្សន៍ទ្រនិ​ច​កណ្ដុរ ដើម្បី​អាច​ប្រើ​វា​នៅ​ក្នុង​ប្រព័ន្ធប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ​បានទេ -- សកម្មភាព​កណ្ដុរ​ទាំង​អស់​ដែល​អ្នក​អនុវត្ត​នៅពេល​ទស្សន៍ទ្រនិចកណ្ដុរ​នៅ​លើ​កា​របង្ហាញ​របស់​ម៉ាស៊ីន​និម្មិត ត្រូវ​បានបញ្ជូន​ដោយ​ផ្ទាល់​ទៅ​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ ។ ប្រសិនបើកណ្ដុរបច្ចុប្បន្ន​ត្រូវ​បាន​ចាប់យក វា​នឹង​​មិន​ត្រូ​វបាន​ចាប់យក​ដោយ​ស្វ័យ​ប្រវត្តិ​ទេ ។&lt;/p>&lt;p>រូបតំណាង​កណ្ដុរ​នៅ​លើ​របារ​ស្ថានភាព​នឹង​មាន​រូបរាង​ដូចជា&amp;nbsp;&lt;img src=:/mouse_seamless_16px.png/>&amp;nbsp;ដើម្បី​ប្រាប់​អ្នក​ថា ការ​រួមបញ្ចូល​ទស្សន៍​ទ្រនិច​កណ្ដុរ​ត្រូវ​បានគាំទ្រ​ដោយ​ប្រព័ន្ធប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ និង​បច្ចុប្បន្ន​ត្រូវ​បានបើក ។&lt;/p>&lt;p>&lt;b>ចំណាំ&lt;/b> ៖ កម្មវិធី​មួយ​ចំនួន​អាច​មាន​ឥរិយាបទ​មិន​ត្រឹមត្រូវ​នៅ​ក្នុង​របៀប​រួម​បញ្ចូល​ទស្សន៍​ទ្រនិច​កណ្ដុរ ។ អ្នក​អាច​បិទ​វា​ជា​និច្ច​សម្រាប់​សម័យ​បច្ចុប្បន្ន (ហើយ​បើក​វា​ម្ដង​ទៀត) ដោយជ្រើសសកម្មភាព​ដែល​ទាក់ទង​ពី​របារ​ម៉ឺនុយ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The virtual machine window will be now switched to &lt;b>fullscreen&lt;/b> mode. You can go back to windowed mode at any time by pressing &lt;b>%1&lt;/b>. Note that the &lt;i>Host&lt;/i> key is currently defined as &lt;b>%2&lt;/b>.&lt;/p>&lt;p>Note that the main menu bar is hidden in fullscreen mode. You can access it by pressing &lt;b>Host+Home&lt;/b>.&lt;/p></source>
      <translation>&lt;p>បង្អួច​ម៉ាស៊ីន​និម្មិត​នឹង​ត្រូ​វបានប្ដូរ​ទៅ​របៀប &lt;b>ពេញអេក្រង់&lt;/b> ឥឡូវ ។ អ្នក​អាច​ត្រឡប់ក្រោយ​​ទៅ​របៀប​បង្អួច​ នៅពេល​ណាមួយ ដោយ​ចុច &lt;b>%1&lt;/b> ។ ចំណាំ​ថា គ្រាប់ចុច &lt;i>ម៉ាស៊ីន&lt;/i> បច្ចុប្បន្ន​ត្រូវ​បាន​កំណត់​ជា &lt;b>%2&lt;/b> ។&lt;/p>&lt;p>ចំណាំ​ថា របារ​ម៉ឺនុយ​មេ​​ត្រូវ​បានលាក់​ក្នុង​របៀប​ពេញអេក្រង់ ។​ អ្នក​អាច​ចូលដំណើរការ​វា​ដោយ​ចុច &lt;b>ម៉ាស៊ីន(Host)+ដើម(Home)&lt;/b>.&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>The virtual machine window will be now switched to &lt;b>Seamless&lt;/b> mode. You can go back to windowed mode at any time by pressing &lt;b>%1&lt;/b>. Note that the &lt;i>Host&lt;/i> key is currently defined as &lt;b>%2&lt;/b>.&lt;/p>&lt;p>Note that the main menu bar is hidden in seamless mode. You can access it by pressing &lt;b>Host+Home&lt;/b>.&lt;/p></source>
      <translation>&lt;p>បង្អួច​ម៉ាស៊ីន​និម្មិត​​នឹង​ត្រូ​វបានប្ដូរ​ទៅ​របៀប &lt;b>គ្មាន​ថ្នេរ&lt;/b> ឥឡូវ ។ អ្នកអាច​ត្រឡប់​ទៅ​​របៀប​បង្អួច​នៅ​ពេល​ណាមួយ​ដោយចុច &lt;b>%1&lt;/b> ។ ចំណាំ​ថា គ្រាប់ចុច &lt;i>Host&lt;/i> បច្ចុប្បន្ន​ត្រូវ​បានកំណត់​ជា &lt;b>%2&lt;/b> ។&lt;/p>&lt;p>ចំណាំថា របារ​ម៉ឺនុយមេ​ត្រូវ​បានលាក់​នៅ​ក្នុង​របៀប​គ្មាន​ថ្នេរ ។ អ្នកអាច​ចូលដំណើរការ​វា ដោយ​ចុច​ &lt;b>Host+ដើម(Home)&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>&amp;Contents...</source>
      <translation>មាតិកា</translation>
    </message>
    <message>
      <source>Show the online help contents</source>
      <translation>បង្ហាញ​មាតិកា​ជំនួយលើ​បណ្ដាញ</translation>
    </message>
    <message>
      <source>&amp;VirtualBox Web Site...</source>
      <translation>តំបន់បណ្ដាញ VirtualBox​...</translation>
    </message>
    <message>
      <source>Open the browser and go to the VirtualBox product web site</source>
      <translation>បើក​កម្មវិធី​រុករក ហើយ​ទៅ​កាន់​តំបន់បណ្ដាញ​ផលិតផល​របស់ VirtualBox</translation>
    </message>
    <message>
      <source>&amp;Reset All Warnings</source>
      <translation>កំណត់​ការ​ព្រមាន​ទាំងអស់</translation>
    </message>
    <message>
      <source>Go back to showing all suppressed warnings and messages</source>
      <translation>ធ្វើ​ឲ្យ​ការ​ព្រមាន​ទាំងអស់​ដែល​បានបង្ហាប់ ហើយសារ​ត្រូវ​បានបង្ហាញ​ម្ដង​ទៀត</translation>
    </message>
    <message>
      <source>R&amp;egister VirtualBox...</source>
      <translation>ចុះឈ្មោះ VirtualBox...</translation>
    </message>
    <message>
      <source>Open VirtualBox registration form</source>
      <translation>បើក​សំណុំ​បែបបទ​ចុះឈ្មោះ​របស់ VirtualBox</translation>
    </message>
    <message>
      <source>C&amp;heck for Updates...</source>
      <translation>ពិនិត្យ​មើល​ភាព​ទាន់សម័យ...</translation>
    </message>
    <message>
      <source>Check for a new VirtualBox version</source>
      <translation>ពិនិត្យ​មើល​កំណែ​របស់​ VirtualBox ថ្មី</translation>
    </message>
    <message>
      <source>&amp;About VirtualBox...</source>
      <translation>អំពី VirtualBox...</translation>
    </message>
    <message>
      <source>Show a dialog with product information</source>
      <translation>បង្ហាញ​ប្រអប់​ដែល​មាន​ព័ត៌មាន​ផលិតផល</translation>
    </message>
    <message>
      <source>&lt;p>A new version of VirtualBox has been released! Version &lt;b>%1&lt;/b> is available at &lt;a href=&quot;http://www.virtualbox.org/&quot;>virtualbox.org&lt;/a>.&lt;/p>&lt;p>You can download this version using the link:&lt;/p>&lt;p>&lt;a href=%2>%3&lt;/a>&lt;/p></source>
      <translation>&lt;p>កំណែ​ថ្មី​របស់ VirtualBox ត្រូវ​បាន​ចេញ​ផ្សាយ ! គឺ​កំណែ &lt;b>%1&lt;/b> ដែលអាច​រក​បាន​នៅ &lt;a href=&quot;http://www.virtualbox.org/&quot;>virtualbox.org&lt;/a> ។&lt;/p>&lt;p>អ្នក​អាច​ទាញ​យក​កំណែ​នេះ​ពី​តំណ​នេះ​ដោយ​ផ្ទាល់ ៖&lt;/p>&lt;p>&lt;a href=%2>%3&lt;/a>&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Are you sure you want to release the %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>?&lt;/p>&lt;p>This will detach it from the following virtual machine(s): &lt;b>%3&lt;/b>.&lt;/p></source>
      <translation>&lt;p>តើ​អ្នក​ប្រាកដ​ជា​ចង់​ចេញផ្សាយ %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>?&lt;/p>&lt;p>វា​នឹង​ផ្ដាច់​វា​ពី​ម៉ាស៊ីន​និម្មិត​ដូច​ខាងក្រោម ៖ &lt;b>%3&lt;/b> ។&lt;/p></translation>
    </message>
    <message>
      <source>Release</source>
      <comment>detach medium</comment>
      <translation>ចេញផ្សាយ</translation>
    </message>
    <message>
      <source>&lt;p>Are you sure you want to remove the %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> from the list of known media?&lt;/p></source>
      <translation>&lt;p>តើអ្នក​ប្រាកដ​ជា​ចង់​យក %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> ចេញ​ពី​មេឌៀ​ដែលបាន​ស្គាល់​ដែរឬទេ ?&lt;/p></translation>
    </message>
    <message>
      <source>Note that as this hard disk is inaccessible its storage unit cannot be deleted right now.</source>
      <translation>ចំណាំថា ថាសរឹង​នេះ​មិនអាច​ចូលដំណើរការ​បានទេ ដូច្នេះ​ឯកតា​ផ្ទុក​របស់វា​មិនអាច​ត្រូវ​បានលុប​ឥឡូវ​ទេ ។</translation>
    </message>
    <message>
      <source>The next dialog will let you choose whether you also want to delete the storage unit of this hard disk or keep it for later usage.</source>
      <translation>ប្រអប់​បន្ទាប់​នឹង​អនុញ្ញាត​ឲ្យ​អ្នក​ជ្រើស​ថាតើ អ្នក​ចង់​លុប​ឯកតា​ផ្ទុក​របស់​ថាសរឹង​នេះ ឬ​ទុក​វា​សម្រាប់​ប្រើ​ពេលក្រោយ ។</translation>
    </message>
    <message>
      <source>&lt;p>Note that the storage unit of this medium will not be deleted and that it will be possible to add it to the list later again.&lt;/p></source>
      <translation>&lt;p>ចំណាំ​ថា ឯកតា​ផ្ទុក​របស់​ឧបករណ៍​ផ្ទុក​នេះ​នឹង​មិន​ត្រូវ​បានលុប​ទេ ហើយ​ដូច្នេះ​វា​នឹង​អាច​បន្ថែម​ទៅ​កាន់​បញ្ជី​ពេល​ក្រោយ​ម្ដង​ទៀត ។&lt;/p></translation>
    </message>
    <message>
      <source>Remove</source>
      <comment>medium</comment>
      <translation>យកចេញ</translation>
    </message>
    <message>
      <source>&lt;p>The hard disk storage unit at location &lt;b>%1&lt;/b> already exists. You cannot create a new virtual hard disk that uses this location because it can be already used by another virtual hard disk.&lt;/p>&lt;p>Please specify a different location.&lt;/p></source>
      <translation>&lt;p>មាន​ឯកតា​ផ្ទុក​​របស់​ថាសរឹង​នៅ​ទីតាំង &lt;b>%1&lt;/b> រួច​ហើយ ។ អ្នក​មិនអាច​បង្កើត​ថាសរឹង​និម្មិត​ថ្មី​បាន​ទេ ដែល​ប្រើ​ទីតាំង​នេះ ពីព្រោះ​វា​អាច​ត្រូវ​បានប្រើ​ដោយ​ថាសរឹង​និម្មិត​ផ្សេង ។&lt;/p>&lt;p>សូម​បញ្ជាក់​ទីតាំង​ផ្សេង ​។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Do you want to delete the storage unit of the hard disk &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr>?&lt;/p>&lt;p>If you select &lt;b>Delete&lt;/b> then the specified storage unit will be permanently deleted. This operation &lt;b>cannot be undone&lt;/b>.&lt;/p>&lt;p>If you select &lt;b>Keep&lt;/b> then the hard disk will be only removed from the list of known hard disks, but the storage unit will be left untouched which makes it possible to add this hard disk to the list later again.&lt;/p></source>
      <translation>&lt;p>តើ​អ្នក​ចង់​លុប​ឯកតា​ផ្ទុក​របស់​ថាសរឹង​ &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr>ដែរឬទេ?&lt;/p>&lt;p>ប្រសិនបើ​អ្នក​ជ្រើស &lt;b>លុប&lt;/b> បន្ទាប់​មក​ឯកតា​ផ្ទុក​ដែល​បានបញ្ជាក់​នឹង​ត្រូ​វបាន​លុប​ជា​អចិន្ត្រៃយ៍ ។ ប្រតិបត្តិការ​នេះ &lt;b>មិនអាច​ត្រូ​វបាន​ធ្វើ​វិញ​បានទេ&lt;/b> ។&lt;/p>&lt;p>ប្រសិនបើ​អ្នក​ជ្រើស &lt;b>រក្សាទុក&lt;/b> បន្ទាប់​មក​ថាសរឹង​នឹង​ត្រូវ​បានយកចេញ​តែ​ពី​បញ្ជី​របស់​ថាសរឹង​ដែល​ស្គាល់​ប៉ុណ្ណោះ ប៉ុន្តែ​ឯកតា​ផ្ទុក​នឹង​មិន​ត្រូ​វបាន​ប៉ះពាល់​ទេ ដែល​ធ្វើ​ឲ្យ​វា​អាច​បន្ថែម​ថាសរឹង​នេះ​ទៅកាន់​បញ្ជី​ពេលក្រោយ​ម្ដង​ទៀត​បាន ។&lt;/p></translation>
    </message>
    <message>
      <source>Delete</source>
      <comment>hard disk storage</comment>
      <translation>លុប</translation>
    </message>
    <message>
      <source>Keep</source>
      <comment>hard disk storage</comment>
      <translation>រក្សាទុក</translation>
    </message>
    <message>
      <source>Failed to delete the storage unit of the hard disk &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​លុប​ការ​ផ្ទុក​របស់​ថាសរឹង &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to create the hard disk storage &lt;nobr>&lt;b>%1&lt;/b>.&lt;/nobr></source>
      <translation>បានបរាជ័យ​ក្នុង​​ការ​បង្កើត​ការ​ផ្ទុក​ថាសរឹង &lt;nobr>&lt;b>%1&lt;/b> ។&lt;/nobr></translation>
    </message>
    <message>
      <source>Failed to open the %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​បើក %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> ។</translation>
    </message>
    <message>
      <source>Failed to close the %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr>.</source>
      <translation>បានបរាជ័យ​ក្នុងកា​របិទ %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> ។</translation>
    </message>
    <message>
      <source>Failed to determine the accessibility state of the medium &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr>.</source>
      <translation>បានបរាជ័យ​ក្នុងកា​រ​ចូលដំណើរការ​ស្ថានភាព​របស់​ឧបករណ៍​ផ្ទុក &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr> ។</translation>
    </message>
    <message>
      <source>&lt;p>Failed to connect to the VirtualBox online registration service due to the following error:&lt;/p>&lt;p>&lt;b>%1&lt;/b>&lt;/p></source>
      <translation>&lt;p>បានបរាជ័យ​ក្នុងកា​រតភ្ជាប់​ទៅ​កាន់​សេវា​ចុះឈ្មោះ​លើ​បណ្ដាញ​របស់ VirtualBox ដោយ​សារ​តែ​កំហុស​ដូច​ខាងក្រោម ៖&lt;/p>&lt;p>&lt;b>%1&lt;/b>&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Unable to obtain the new version information due to the following error:&lt;/p>&lt;p>&lt;b>%1&lt;/b>&lt;/p></source>
      <translation>&lt;p>មិនអាច​ទទួល​យក​ព័ត៌មានកំណែ​ថ្មី​បាន​ទេ ដោយ​សារ​តែ​កំហុស​ដូច​ខាងក្រោម ៖&lt;/p>&lt;p>&lt;b>%1&lt;/b>&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>One or more virtual hard disks, CD/DVD or floppy media are not currently accessible. As a result, you will not be able to operate virtual machines that use these media until they become accessible later.&lt;/p>&lt;p>Press &lt;b>Check&lt;/b> to open the Virtual Media Manager window and see what media are inaccessible, or press &lt;b>Ignore&lt;/b> to ignore this message.&lt;/p></source>
      <translation>&lt;p>ថាសរឹង​និម្មិត​មួយ ឬ​ច្រើន ស៊ីឌី/ឌីវីឌី ឬ​មេឌៀ​ថាសទន់​បច្ចុប្បន្ន​មិនអាច​ចូលដំណើរការ​បានទេ ។ ជា​លទ្ធផល អ្នកនឹង​មិនអាច​ប្រតិបត្តិ​ម៉ាស៊ីន​និម្មិត​ ដែល​ប្រើ​មេឌៀ​នេះ​បាន​ទេ រហូត​ដល់​ពួកវា​អាច​ចូលដំណើរការ​បាននៅពេល​ក្រោយ ។&lt;/p>&lt;p>ចុច &lt;b>ពិនិត្យ​មើល&lt;/b> ដើម្បី​ប្រើ​បង្អួច​កម្មវិធី​គ្រប់គ្រង​មេឌៀ​និម្មិត ហើយ​មើល​អ្វី​ដែល​មេឌៀ​មិនអាច​ចូលដំណើរការ​បាន ឬ​ចុច &lt;b>មិនអើពើ&lt;/b> ដើម្បី​មិនអើពើ​នឹង​សារ​នេះ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>A critical error has occurred while running the virtual machine and the machine execution has been stopped.&lt;/p>&lt;p>For help, please see the Community section on &lt;a href=http://www.virtualbox.org>http://www.virtualbox.org&lt;/a> or your support contract. Please provide the contents of the log file &lt;tt>VBox.log&lt;/tt> and the image file &lt;tt>VBox.png&lt;/tt>, which you can find in the &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr> directory, as well as a description of what you were doing when this error happened. Note that you can also access the above files by selecting &lt;b>Show Log&lt;/b> from the &lt;b>Machine&lt;/b> menu of the main VirtualBox window.&lt;/p>&lt;p>Press &lt;b>OK&lt;/b> if you want to power off the machine or press &lt;b>Ignore&lt;/b> if you want to leave it as is for debugging. Please note that debugging requires special knowledge and tools, so it is recommended to press &lt;b>OK&lt;/b> now.&lt;/p></source>
      <translation>&lt;p>កំហុស​ធ្ងន់ធ្ងរ​បាន​កើត​ឡើង​ខណៈពេល​កំពុង​រត់​ម៉ាស៊ីននិម្មិត ហើយ​ការ​ប្រតិបត្តិ​ម៉ាស៊ីន​ត្រូ​វបានបញ្ឈប់ ។&lt;/p>&lt;p>ចំពោះ​ជំនួយ សូមមើល​ផ្នែក​សហគមន៍​នៅ​លើ &lt;a href=http://www.virtualbox.org>http://www.virtualbox.org&lt;/a> ឬ​កិច្ចសន្យា​គាំទ្រ​របស់​អ្នក ។ សូម​ផ្ដល់​នូវ​ខ្លឹមសារ​របស់​ឯកសារ​កំណត់ហេតុ &lt;tt>VBox.log&lt;/tt> ហើយ​នឹង​ឯកសារ​រូបភាព &lt;tt>VBox.png&lt;/tt> ដែល​អ្នក​អាច​រក​បាន​នៅ​ក្នុង​ថត &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr> ព្រមទាំង​សេចក្ដីពិពណ៌នា​នូវ​អ្វីដែល​អ្នក​កំពុង​ធ្វើ នៅពេល​កំហុស​កើត​ឡើង ។ ចំណាំ​ថា អ្នក​ក៏អាច​ចូលដំណើរការ​ឯកសារ​ខាង​លើ ដោយ​ជ្រើស &lt;b>បង្ហាញ​កំណត់​ហេតុ&lt;/b> ពី​ម៉ឺនុយ &lt;b>ម៉ាស៊ីន&lt;/b> របស់​បង្អួច VirtualBox មេ ។&lt;/p>&lt;p>ចុច &lt;b>យល់ព្រម&lt;/b> ប្រសិនបើ​​អ្នក​ចង់​បិទ​ម៉ាស៊ីន ឬ​ចុច &lt;b>មិនអើពើ&lt;/b> ប្រសិន​បើ​អ្នក​ទុក​វា​សម្រាប់បំបាត់​កំហុស ។ សូម​ចំណាំថា​កា​របំបាត់​កំហុស​ទាមទារចំណេះដឹង និង​កម្មវិធី​ពិសេស ដូច្នេះ​ត្រូវ​បានផ្ដល់​អនុសាសន៍​ឲ្យ​ចុច &lt;b>យល់ព្រម&lt;/b> ឥឡូវ ។&lt;/p></translation>
    </message>
    <message>
      <source>A file named &lt;b>%1&lt;/b> already exists. Are you sure you want to replace it?&lt;br />&lt;br />Replacing it will overwrite its contents.</source>
      <translation>​មាន​ឈ្មោះ​ឯកសាររបស់​&lt;b>%1&lt;/b>រួច​ហើយ​ ។ តើ​អ្នក​ពិត​ជាចង់​ជំនួស​វា​​ឬ ?&lt;br />&lt;br />​ការ​ជំនួស​វា​នឹង​សរសេរ​ជាន់​លើ​
មាតិកា​​របស់​វា​ ។</translation>
    </message>
    <message>
      <source>The following files already exist:&lt;br />&lt;br />%1&lt;br />&lt;br />Are you sure you want to replace them? Replacing them will overwrite their contents.</source>
      <translation>មាន​ឯកសារ​​ជាបន្ត​បន្ទាប់​រួច​ហើយ​ ៖&lt;br />&lt;br />%1&lt;br />&lt;br />តើ​​អ្នក​ពិត​ជាចង់​ជំនួស​​​ពួកវា​ឬ ? ការ​ជំនួស​វា​នឹង​សរសេរ​ជាប់​លើ​មាតិកា​របស់វា ។</translation>
    </message>
    <message>
      <source>Failed to remove the file &lt;b>%1&lt;/b>.&lt;br />&lt;br />Please try to remove the file yourself and try again.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​រយក​ឯកសារ &lt;b>%1&lt;/b>ចេញ ។&lt;br />&lt;br />សូម​ព្យាយាម​យក​ឯកសារ​ចេញ​ដោយ​ខ្លួន​ឯង ហើយ​ព្យាយាម​ម្ដង​ទៀត ។</translation>
    </message>
    <message>
      <source>You seem to have the USBFS filesystem mounted at /sys/bus/usb/drivers. We strongly recommend that you change this, as it is a severe mis-configuration of your system which could cause USB devices to fail in unexpected ways.</source>
      <translation>អ្នក​ហាក់​បី​ដូច​ជា​មាន​ប្រព័ន្ធ​ឯកសារ USBFS បាន​ម៉ោន​នៅ /sys/bus/usb/drivers ។ យើ​ង​ផ្ដល់​អនុសាសន៍​ឲ្យ​ផ្លាស់ប្ដូរ​វា ព្រោះ​ថា​វា​ជា​កា​រកំណត់​រចនាសម្ព័ន្ធ​មិន​ត្រឹមត្រូវ​នៃ​ប្រព័ន្ធ​របស់​អ្នក ដែល​អាច​ធ្វើ​ឲ្យ​ឧបករណ៍ USB បរាជ័យ​​តាម​វិធី​ដែល​មិនបាន​រំពឹង​ទុក ។</translation>
    </message>
    <message>
      <source>You are running a prerelease version of VirtualBox. This version is not suitable for production use.</source>
      <translation>អ្នក​កំពុង​ដំណើរការ​កំណែ​ចេញ​ផ្សាយ​មុន​របស់ VirtualBox ។ កំណែ​នេះ​មិន​សម​សម្រាប់​ការ​ប្រើផលិតផល​នេះ​ទេ ។</translation>
    </message>
    <message>
      <source>You are running an EXPERIMENTAL build of VirtualBox. This version is not suitable for production use.</source>
      <translation>អ្នកកំពុង​ដំណើរការ​ការ​ស្ថាបនា​ពិសោធន៍របស់ VirtualBox ។ កំណែ​នេះ​មិន​សម​សម្រាប់ការ​ប្រើ​ផលិតផល​នេះ​ទេ ។</translation>
    </message>
    <message>
      <source>Could not access USB on the host system, because neither the USB file system (usbfs) nor the DBus and hal services are currently available. If you wish to use host USB devices inside guest systems, you must correct this and restart VirtualBox.</source>
      <translation>មិនអាច​ដំណើរការ​ USB ក្នុង​ប្រព័ន្ធ​របស់​ម៉ាស៊ីនបាន​ទេ ពីព្រោះប្រព័ន្ធ​ឯកសារ USB (usbfs) ឬ DBus និង​សេវា hal បច្ចុប្បន្ននេះអាច​ប្រើ​បាន ។ ប្រសិនបើ​អ្នក​ចង់​ប្រើ​ឧបករណ៍​ USB របស់​ម៉ាស៊ីន អ្នក​ត្រូវ​តែ​កែវា ហើយ​ចាប់ផ្ដើម VirtualBox ឡើង​វិញ ។</translation>
    </message>
    <message>
      <source>You are trying to shut down the guest with the ACPI power button. This is currently not possible because the guest does not support software shutdown.</source>
      <translation>អ្នក​កំពុង​ព្យាយាប​បិទ​ម៉ាស៊ីន​ដោយ​ប្រើប៊ូតុង​ថាមពល ACPI  ហើយ ។ បច្ចុប្បន្ន​នេះ​មិនអាច​ប្រើបាន​ទេ ពីព្រោះ​ម៉ាស៊ីន​​មិន​គាំទ្រ​​កម្មវិធី​បិទ​​ទេ ។</translation>
    </message>
    <message>
      <source>&lt;p>VT-x/AMD-V hardware acceleration has been enabled, but is not operational. Your 64-bit guest will fail to detect a 64-bit CPU and will not be able to boot.&lt;/p>&lt;p>Please ensure that you have enabled VT-x/AMD-V properly in the BIOS of your host computer.&lt;/p></source>
      <translation>&lt;p>ការ​បង្កើន​ល្បឿន​ផ្នែក​រឹង VT-x/AMD-V ត្រូវ​បានបើក ប៉ុន្តែ​មិន​ដំណើរការ​ទេ ។ ម៉ាស៊ីន​ ៦៤ ប៊ីត​របស់​អ្នក​នឹង​បរាជ័យ​ក្នុង​រក​ស៊ីភីយូ ៦៤ ប៊ីត ហើយ​មិន​អាច​ចាប់ផ្ដើម​បាន​ទេ ។&lt;/p>&lt;p>សូម​ប្រាកដ​ថា អ្នក​បានបើក VT-x/AMD-V យ៉ាង​ត្រាមត្រូវ​នៅ​ក្នុង BIOS នៃ​កុំព្យូទ័រ​របស់​អ្នក ។&lt;/p></translation>
    </message>
    <message>
      <source>Close VM</source>
      <translation>បិទ VM</translation>
    </message>
    <message>
      <source>Continue</source>
      <translation>បន្ត</translation>
    </message>
    <message>
      <source>&lt;p>VT-x/AMD-V hardware acceleration has been enabled, but is not operational. Certain guests (e.g. OS/2 and QNX) require this feature.&lt;/p>&lt;p>Please ensure that you have enabled VT-x/AMD-V properly in the BIOS of your host computer.&lt;/p></source>
      <translation>&lt;p>ការ​បង្កើន​ល្បឿន​ផ្នែក​រឹង VT-x/AMD-V ត្រូវ​បានបើក ប៉ុន្តែ​មិន​អាច​ដំណើរការ​បាន​ទេ ។ ម៉ាស៊ីន​ជាក់លាក់ (ឧ. OS/2 និង QNX) ទាមទារ​លក្ខណៈពិសេស​នេះ ។&lt;/p>&lt;p>សូម​ប្រាកដ​ថា អ្នក​បានបើក​ VT-x/AMD-V យ៉ាង​ត្រឹមត្រូវ​នៅ​ក្នុង BIOS នៃ​​កុំព្យូទ័រ​របស់​អ្នក ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Are you sure you want to restore snapshot &lt;b>%1&lt;/b>? This will cause you to lose your current machine state, which cannot be recovered.&lt;/p></source>
      <translation>&lt;p>តើ​អ្នក​ប្រាក​ដ​ជា​ចង់​ស្ដារ​រូបថត​ &lt;b>%1&lt;/b>ឬ ? វា​នឹង​ធ្វើ​ឲ្យ​អ្នក​បាត់បង់​ស្ថានភាព​ម៉ាស៊ីន​បច្ចុប្បន្ន​របស់​អ្នក ដែល​មិនអាច​សង្គ្រោះ​បាន​ទេ ។&lt;/p></translation>
    </message>
    <message>
      <source>Restore</source>
      <translation>ស្តារ​</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់​</translation>
    </message>
    <message>
      <source>&lt;p>Deleting the snapshot will cause the state information saved in it to be lost, and disk data spread over several image files that VirtualBox has created together with the snapshot will be merged into one file. This can be a lengthy process, and the information in the snapshot cannot be recovered.&lt;/p>&lt;/p>Are you sure you want to delete the selected snapshot &lt;b>%1&lt;/b>?&lt;/p></source>
      <translation>&lt;p>ការ​លុប​រូប​ថត​នឹង​បណ្តាល​ឲ្យព័ត៌​មានចេញ​​ដែល​បាន​រ​ក្សា​ទុក​ត្រូវ​បាន​បាត់​បង់​ និងការ​ផ្សាយ​​ទិន្នន័យ​ថាស​​​លើ​ឯកសារ​រូប​ភាពបី​បួន​​ដែល​ VirtualBox ត្រូវ​បានបង្កើត​ជាមួយ​គ្នា ​រូប​ថត​នឹង​ត្រូវ​បានបញ្ចូល​ចូល​​គ្នា​​ទៅ​ក្នុង​ឯកសារ​តែ​មួយ​ ។​ វាអាច​ដំណើរ​ការ​យូរ​ និង​ព័ត៌​មាន​ក្នុង​រូប​ថត​មិន​អាច​ត្រូវ​បានយកបាន​​ឡើយ ។&lt;/p>&lt;/p>តើ​អ្នក​ប្រាកដ​ជា​ចង់​លុប​រូប​ថត​ដែលបាន​​ជ្រើស​&lt;b>%1&lt;/b> ឬ ?&lt;/p></translation>
    </message>
    <message>
      <source>Delete</source>
      <translation>លុប</translation>
    </message>
    <message>
      <source>Failed to restore the snapshot &lt;b>%1&lt;/b> of the virtual machine &lt;b>%2&lt;/b>.</source>
      <translation>បាន​បរាជ័យក្នុងការ​​​ស្តារ​រូបថត​​ &lt;b>%1&lt;/b> របស់​ម៉ាស៊ីននិម្មិ​ត&lt;b>%2&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to delete the snapshot &lt;b>%1&lt;/b> of the virtual machine &lt;b>%2&lt;/b>.</source>
      <translation>បានបរាជ័យក្នុងការ​​​លុប​រូបថត​​&lt;b>%1&lt;/b> របស់​​ម៉ាស៊ីន​​និម្មិត​​&lt;b>%2&lt;/b> ។</translation>
    </message>
    <message>
      <source>&lt;p>There are hard disks attached to ports of the additional controller. If you disable the additional controller, all these hard disks will be automatically detached.&lt;/p>&lt;p>Are you sure you want to disable the additional controller?&lt;/p></source>
      <translation>&lt;p>មានថាស​រឹង​​ដែ​ល​ភ្ជាប់​ជា​មួយច្រក​របស់​ឧបករណ៍​បញ្ជា​បន្ថែម​ ។ ប្រ​សិន​បើ​អ្នកបិទ​​ឧបករណ៍​បញ្ជា​បន្ថែម​ គ្រប់​ថាស​រឹង​ទាំង​នេះ​នឹង​ត្រូវ​បាន​ផ្តាច់​ដោយ​ស្វ័យ​ប្រ​វត្តិ​ ។&lt;/p>&lt;p>​តើ​​អ្នក​ប្រាកដ​​ជា​​ចង់បិទ​​ឧបករណ៍​បញ្ជា​បន្ថែម​ ឬ ?&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>There are hard disks attached to ports of the additional controller. If you change the additional controller, all these hard disks will be automatically detached.&lt;/p>&lt;p>Are you sure you want to change the additional controller?&lt;/p></source>
      <translation>&lt;p>មាន​ថាសរឹង​ដែល​ភ្ជាប់​ជាមួយ​ច្រក​របស់​ឧបករណ៍​បញ្ជា​បន្ថែម​ ។ ប្រ​សិន​បើ​អ្នក​​ផ្លាស់​ប្តូរ​ឧបរណ៍​បញ្ញា​បន្ថែម​​ គ្រប់​ថាស​រឹង​ទាំង​នេះ​នឹង​ត្រូវ​បាន​ផ្តាច់​ដោយ​ស្វ័យ​ប្រវត្តិ​ ។​&lt;/p>&lt;p>​តើ​អ្នក​ប្រាកដជា​​ចង់​ផ្លាស់​ប្តូរ​​ឧបករណ៍​បញ្ជា​បន្ថែម​ឬ ?&lt;/p></translation>
    </message>
    <message>
      <source>Change</source>
      <comment>



hard disk</comment>
      <translation>ផ្លាស់ប្តូរ​</translation>
    </message>
    <message>
      <source>&lt;p>There are no unused media available for the newly created attachment.&lt;/p>&lt;p>Press the &lt;b>Create&lt;/b> button to start the &lt;i>New Virtual Disk&lt;/i> wizard and create a new medium, or press the &lt;b>Select&lt;/b> if you wish to open the &lt;i>Virtual Media Manager&lt;/i>.&lt;/p></source>
      <translation>&lt;p>មិនមាន​មេឌៀ​ដែល​មិនបានប្រើ​​សម្រាប់​​​ឯក​សារ​ភ្ជាប់​ដែល​បង្កើតថ្មី​ ។​&lt;/p>&lt;p>ចុច​ &lt;b>បង្កើត​​&lt;/b> ប៊ូតុង​ទៅ​ចាប់​ផ្តើម &lt;i>​ថាស​និម្មិត​ថ្មី&lt;/i> អ្នក​ជំនួយ​ការ​ និង​បង្កើតឧបករណ៍​ផ្ទុកថ្មី​ ឬ​ចុច​​ &lt;b>ជ្រើស​​&lt;/b> ប្រ​សិន​បើ​​អ្នក​មាន​បំណង​ទៅ​បើក​ &lt;i>កម្ម​វិធី​គ្រប់​គ្រង​មេឌៀ​និម្មិត​​&lt;/i>.&lt;/p></translation>
    </message>
    <message>
      <source>&amp;Create</source>
      <comment>



medium</comment>
      <translation>បង្កើត</translation>
    </message>
    <message>
      <source>&amp;Select</source>
      <comment>



medium</comment>
      <translation>ជ្រើស</translation>
    </message>
    <message>
      <source>&lt;p>There are no unused media available for the newly created attachment.&lt;/p>&lt;p>Press the &lt;b>Select&lt;/b> if you wish to open the &lt;i>Virtual Media Manager&lt;/i>.&lt;/p></source>
      <translation>&lt;p>មិនមាន​មេឌៀ​មិន​បាន​ប្រើទេ សម្រាប់​ឯកសារ​ភ្ជាប់​ដែលបានបង្កើត​ថ្មី ។&lt;/p>&lt;p>ចុច &lt;b>ជ្រើស&lt;/b> ប្រសិន​បើ​អ្នក​ចង់​បើក &lt;i>កម្មវិធី​គ្រប់គ្រង​មេឌៀ​និម្មិត&lt;/i> ។&lt;/p></translation>
    </message>
    <message>
      <source>Failed to attach the %1 to slot &lt;i>%2&lt;/i> of the machine &lt;b>%3&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​ភ្ជាប់ %1 ទៅ​រន្ធ &lt;i>%2&lt;/i> របស់​ម៉ាស៊ីន&lt;b>%3&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to detach the %1 from slot &lt;i>%2&lt;/i> of the machine &lt;b>%3&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុង​ការ​ផ្ដាច់​ %1 ពី​រន្ធ &lt;i>%2&lt;/i> របស់​ម៉ាស៊ីន&lt;b>%3&lt;/b> ។</translation>
    </message>
    <message>
      <source>Unable to mount the %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> on the machine &lt;b>%3&lt;/b>.</source>
      <translation>មិនអាច​ម៉ោន %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> នៅ​លើ​ម៉ាស៊ីន &lt;b>%3&lt;/b>បាន​ទេ ។</translation>
    </message>
    <message>
      <source> Would you like to force mounting of this medium?</source>
      <translation> តើ​អ្នក​ចង់​បង្ខំ​ម៉ោន​ឧបករណ៍​ផ្ទុក​នេះ​ដែរឬទេ ?</translation>
    </message>
    <message>
      <source>Unable to unmount the %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> from the machine &lt;b>%3&lt;/b>.</source>
      <translation>មិនអាច​អាន់ម៉ោន​ %1 &lt;nobr>&lt;b>%2&lt;/b>&lt;/nobr> ពី​ម៉ាស៊ីន &lt;b>%3&lt;/b> ។</translation>
    </message>
    <message>
      <source> Would you like to force unmounting of this medium?</source>
      <translation> តើ​អ្នក​ចង់​បង្ខំ​អាន់ម៉ោន​ឧបករណ៍​ផ្ទុក​នេះ​ដែរឬទេ ?</translation>
    </message>
    <message>
      <source>Force Unmount</source>
      <translation>បង្ខំ​អាន់ម៉ោន​</translation>
    </message>
    <message>
      <source>Failed to eject the disk from the virtual drive. The drive may be locked by the guest operating system. Please check this and try again.</source>
      <translation>បានបរាជ័យ​​ក្នុង​កា​រច្រាន​ថាស​ចេញ​ពី​ដ្រាយ​និម្មិត ។ ដ្រាយ​អាច​ត្រូវ​បានចាក់​សោ​ដោយ​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ ។ សូម​ពិនិត្យ​មើល​វា ហើយ​ព្យាយាម​ម្ដង​ទៀត ។</translation>
    </message>
    <message>
      <source>&lt;p>Deleting this host-only network will remove the host-only interface this network is based on. Do you want to remove the (host-only network) interface &lt;nobr>&lt;b>%1&lt;/b>?&lt;/nobr>&lt;/p>&lt;p>&lt;b>Note:&lt;/b> this interface may be in use by one or more virtual network adapters belonging to one of your VMs. After it is removed, these adapters will no longer be usable until you correct their settings by either choosing a different interface name or a different adapter attachment type.&lt;/p></source>
      <translation>&lt;p>ការ​លុប​បណ្តាញ​ ម៉ា​ស៊ីន​​-តែ​មួយ​​ នេះ​នឹង​យក​ចេញ​ សំអាង​ទៅ​លើ​បណ្តាញ​ចំនុច​ប្រទាក់​ ម៉ាស៊ីន​​-តែ​មួយ​ នេះ ។ ​តើ​អ្នកពិត​ជា​ចង់​យក​ចេញ​​ (បណ្តាញ ម៉ាស៊ីន​​-​តែ​មួយ​​) ចំណុច​ប្រទាក់​​&lt;nobr>&lt;b>%1&lt;/b>?&lt;/nobr>&lt;/p>&lt;p>&lt;b>សម្គាល់​ ៖&lt;/b> ចំណុច​ប្រ​ទាក់​នេះ​អាចប្រើ​​មួយ​ ឬ​​ច្រើន​​អាដាប់​ទ័រ​បណ្តាញនិម្មិត​​​ជាកម្ម​សិទ្ធ​មួយ​នៃ​​​ VMs របស់​អ្នក​ ។ ក្រោយ​ពេល​វា​ត្រូវ​បាន​យក​ចេញ​ អាដាប់​ទ័រ​ទាំង​នេះ​នឹង​មិន​អាច​ប្រើ​បាន​យូរ​ឡើយ​​ រហូត​ដល់​អ្នក​​កែ​ការ​កំណត់​​របស់​វា​មួយ​ណា​ក៏មាន​​​ឈ្មោះ​ខុស​គ្នា​​ចំណុច​ប្រទាក់​ ឬ​ប្រ​ភេទ​ខុស​គ្នា​នៃ​​​ឯកសារ​​ភ្ជាប់​អាដាប់​ទ័រ​ ។&lt;/p></translation>
    </message>
    <message>
      <source>Failed to create the host-only network interface.</source>
      <translation>បាន​បរាជ័យ​ក្នុងការ​​​បង្កើត​ចំណុច​ប្រ​ទាក់​បណ្តាញ​ម៉ាស៊ីន​​-តែ​មួយ​</translation>
    </message>
    <message>
      <source>&lt;p>Could not insert the VirtualBox Guest Additions installer CD image into the virtual machine &lt;b>%1&lt;/b>, as the machine has no CD/DVD-ROM drives. Please add a drive using the storage page of the virtual machine settings dialog.&lt;/p></source>
      <translation>&lt;p>មិនអាច​បញ្ចូល​រូបភាព​ស៊ីឌី​កម្មវិធី​ដំឡើង​បន្ថែម​របស់​ម៉ាស៊ីន VirtualBox ​ទៅ​ក្នុង​ម៉ាស៊ីន​និម្មិត​&lt;b>%1&lt;/b>បាន​ទេ ព្រោះ​ថា​ម៉ាស៊ីន​មាន​ដ្រាយ​ស៊ីឌី/ឌីវីឌី​រ៉ូម ។ សូម​បន្ថែម​ដ្រាយ ដោយ​ប្រើ​ទំព័រ​ផ្ទុក​នៃ​ប្រអប់​កំណត់​ម៉ាស៊ីន​និម្មិត ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Invalid e-mail address or password specified.&lt;/p></source>
      <translation>&lt;p>អាសយដ្ឋាន​អ៊ីមែល ឬ​ពាក្យ​សម្ងាត់​ដែលបាន​បញ្ជាក់មិន​ត្រឹមត្រូវ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Failed to register the VirtualBox product.&lt;/p>&lt;p>%1&lt;/p></source>
      <translation>&lt;p>បាន​បរាជ័យ​ក្នុងកា​រចុះឈ្មោះផលិតផល VirtualBox ។&lt;/p>&lt;p>%1&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Your existing VirtualBox settings files will be automatically converted from the old format to a new format required by the new version of VirtualBox.&lt;/p>&lt;p>Press &lt;b>OK&lt;/b> to start VirtualBox now or press &lt;b>Exit&lt;/b> if you want to terminate the VirtualBox application without any further actions.&lt;/p></source>
      <translation>&lt;p>ឯកសារ​កំណត់​ VirtualBox មាន​ស្រាប់​​របស់​អ្នក នឹង​ត្រូវ​បានបម្លែង​ដោយ​ស្វ័យ​ប្រវត្តិ​ពី​ទ្រង់ទ្រាយ​ចាស់ ទៅ​ទ្រង់ទ្រាយ​ថ្មី​ដែលបានទាមទារ​ដោយ​កំណែ​ថ្មី​របស់ VirtualBox ​។&lt;/p>&lt;p>ចុច &lt;b>យល់&lt;/b> ដើម្បី​ចាប់ផ្ដើម VirtualBox ឥឡូវ ឬ​ចុច &lt;b>ចាកចេញ&lt;/b> ប្រសិនបើ​អ្នក​ចង់​បញ្ចប់​កម្មវិធី​របស់ VirtualBox ដោយ​គ្មាន​សកម្មភាព​បន្ថែម​ផ្សេង​ទៀត ។&lt;/p></translation>
    </message>
    <message>
      <source>E&amp;xit</source>
      <comment>



warnAboutSettingsAutoConversion message box</comment>
      <translation>ចេញ</translation>
    </message>
    <message>
      <source>&lt;p>The following VirtualBox settings files will be automatically converted from the old format to a new format required by the new version of VirtualBox.&lt;/p>&lt;p>Press &lt;b>OK&lt;/b> to start VirtualBox now or press &lt;b>Exit&lt;/b> if you want to terminate the VirtualBox application without any further actions.&lt;/p></source>
      <translation>&lt;p>ឯកសារ​កំណត់ VirtualBox ដូច​ខាងក្រោម​នឹង​ត្រូវ​បានបម្លែង​ដោយ​ស្វ័យ​ប្រវត្តិ​ពី​ទ្រង់ទ្រាយ​ចាស់​ទៅ​ទ្រង់ទ្រាយ​ថ្មី​របស់ VirtualBox ។&lt;/p>&lt;p>ចុច &lt;b>យល់&lt;/b> ដើម្បី​ចាប់ផ្ដើម VirtualBox ឥឡូវ ឬ​ចុច &lt;b>ចាកចេញ&lt;/b> ប្រសិនបើ​អ្នក​ចង់​បញ្ចប់​កម្មវិធី VirtualBox ដោយ​គ្មាន​សកម្មភាព​បន្ថែម​ទៀត ។&lt;/p></translation>
    </message>
    <message>
      <source>Failed to open appliance.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​របើក​ឧបករណ៍ ។</translation>
    </message>
    <message>
      <source>Failed to open/interpret appliance &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​របើក/បកប្រែ​ឧបករណ៍&lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to import appliance &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​រនាំចូល​ឧបករណ៍&lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to check files.</source>
      <translation>បាន​បរាជ័យ​ទៅ​ពិនិត្យ​មើល​ឯកសារ​ ។</translation>
    </message>
    <message>
      <source>Failed to remove file.</source>
      <translation>បាន​បរាជ័យ​ក្នុងកា​រយក​ឯកសារ​ចេញ ។</translation>
    </message>
    <message>
      <source>Failed to create appliance.</source>
      <translation>បាន​បរាជ័យ​ទៅ​បង្កើតឧបករណ៍ ។</translation>
    </message>
    <message>
      <source>Failed to prepare the export of the appliance &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ទៅ​រៀប​ចំ​នាំ​ចូលឧបករណ៍​ &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>Failed to create an appliance.</source>
      <translation>បាន​បរាជ័យ​ទៅ​បង្កើតឧបករណ៍​ ។</translation>
    </message>
    <message>
      <source>Failed to export appliance &lt;b>%1&lt;/b>.</source>
      <translation>បាន​បរាជ័យ​ទៅ​នាំ​ចេញឧបករណ៍​​ &lt;b>%1&lt;/b> ។</translation>
    </message>
    <message>
      <source>hard disk</source>
      <comment>




failed to mount ...</comment>
      <translation>ថាសរឹង</translation>
    </message>
    <message>
      <source>CD/DVD</source>
      <comment>




failed to mount ... host-drive</comment>
      <translation>ស៊ីឌី​​​/​ឌីវីឌី​​</translation>
    </message>
    <message>
      <source>CD/DVD image</source>
      <comment>




failed to mount ...</comment>
      <translation>រូបភាព​ស៊ីឌី/ឌីវីឌី</translation>
    </message>
    <message>
      <source>floppy</source>
      <comment>




failed to mount ... host-drive</comment>
      <translation>ថាសទន់​</translation>
    </message>
    <message>
      <source>floppy image</source>
      <comment>




failed to mount ...</comment>
      <translation>រូបភាព​ថាសទន់</translation>
    </message>
    <message>
      <source>hard disk</source>
      <comment>




failed to attach ...</comment>
      <translation>ថាសរឹង</translation>
    </message>
    <message>
      <source>CD/DVD device</source>
      <comment>




failed to attach ...</comment>
      <translation>ឧបករណ៍​ស៊ីឌី​​/​ឌីវីឌី​</translation>
    </message>
    <message>
      <source>floppy device</source>
      <comment>




failed to close ...</comment>
      <translation>ឧបករណ៍​ថាសទន់​</translation>
    </message>
    <message>
      <source>&lt;p>Are you sure you want to delete the CD/DVD-ROM device?&lt;/p>&lt;p>You will not be able to mount any CDs or ISO images or install the Guest Additions without it!&lt;/p></source>
      <translation>&lt;p>តើ​​​អ្នក​ប្រាកដ​ជា​ចង់​លុប​ឧបករណ៍​​ស៊ី​ឌី​​/​ឌី​វីឌី​-រ៉ូម​ឬ​​?&lt;/p>&lt;p>អ្នក​នឹង​​មិន​អាច​ម៉ោន​ស៊ី​ឌី​ ឬ​រូប​ភាព ISO ឬ​ដំឡើង​​ការ​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ​​ដោយ​គ្មាន​វា​បាន​​ទេ ​​!&lt;/p></translation>
    </message>
    <message>
      <source>&amp;Remove</source>
      <comment>




medium</comment>
      <translation>យកចេញ​</translation>
    </message>
  </context>
  <context>
    <name>VBoxProgressDialog</name>
    <message>
      <source>A few seconds remaining</source>
      <translation>នៅ​សល់​ពីរ​ ​បី​​វិនាទី​</translation>
    </message>
    <message>
      <source>Canceling...</source>
      <translation>កំពុង​បោះបង់...</translation>
    </message>
    <message>
      <source>&amp;Cancel</source>
      <translation>បោះបង់​</translation>
    </message>
    <message>
      <source>Cancel the current operation</source>
      <translation>បោះ​បង់​ប្រ​តិបត្តិការ​បច្ចុប្បន្ន</translation>
    </message>
    <message>
      <source>%1, %2 remaining</source>
      <comment>




You may wish to translate this more like &quot;Time remaining: %1, %2&quot;</comment>
      <translation>នៅ​សល់​ %1, %2</translation>
    </message>
    <message>
      <source>%1 remaining</source>
      <comment>




You may wish to translate this more like &quot;Time remaining: %1&quot;</comment>
      <translation>នៅ​សល់​ %1</translation>
    </message>
  </context>
  <context>
    <name>VBoxRegistrationDlg</name>
    <message>
      <source>VirtualBox Registration Dialog</source>
      <translation>ប្រអប់​ចុះឈ្មោះ VirtualBox</translation>
    </message>
    <message>
      <source>Enter your full name using Latin characters.</source>
      <translation>បញ្ចូល​ឈ្មោះ​ពេញ​ ដោយ​ប្រើ​តួអក្សរ​ឡាតាំង ។</translation>
    </message>
    <message>
      <source>Enter your e-mail address. Please use a valid address here.</source>
      <translation>បញ្ចូល​អាសយដ្ឋាន​អ៊ីមែល​ដែល​ត្រឹមត្រូវ​របស់អ្នក ។</translation>
    </message>
    <message>
      <source>Welcome to the VirtualBox Registration Form!</source>
      <translation>សូម​ស្វាគមន៍​មក​កាន់​សំណុំ​បែបបទ​ចុះឈ្មោះ​របស់ VirtualBox !</translation>
    </message>
    <message>
      <source>Could not perform connection handshake.</source>
      <translation>មិនអាច​អនុវត្ត​ការ​តភ្ជាប់​ចាប់ដៃ​បានទេ ។</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់</translation>
    </message>
    <message>
      <source>Select Country/Territory</source>
      <translation>ជ្រើស​​​ដែន​ដី​​/​ប្រ​ទេស​</translation>
    </message>
    <message>
      <source>&lt;p>Please fill out this registration form to let us know that you use VirtualBox and, optionally, to keep you informed about VirtualBox news and updates.&lt;/p>&lt;p>Please use Latin characters only to fill in  the fields below. Sun Microsystems will use this information only to gather product usage statistics and to send you VirtualBox newsletters. In particular, Sun Microsystems will never pass your data to third parties. Detailed information about how we use your personal data can be found in the &lt;b>Privacy Policy&lt;/b> section of the VirtualBox Manual or on the &lt;a href=http://www.virtualbox.org/wiki/PrivacyPolicy>Privacy Policy&lt;/a> page of the VirtualBox web-site.&lt;/p></source>
      <translation>&lt;p>សូម​បំ​ពេញ​សំណុំ​​បែប​បទ​ការ​ចុះ​ឈ្មោះ​នេះ​​ដើម្បី​អនុ​ញ្ញាត​ពួក​យើង​ដឹង​ថា​អ្នក​ប្រើ​ VirtualBox និង ដោយ​ជម្រើស​ ​ដើម្បី​រក្សា​ទុកដែល​អ្នក​បាន​អង្កេត​អំពី​​ព័ត៌​មាន និងភាព​ទាន់​សម័យ​របស់​​​ VirtualBox ។&lt;/p>&lt;p>សូម​ប្រើ​តូ​អក្សរ​ឡាតាំង​តែ​មួយ​​ដើម្បី​បំ​ពេញ​ក្នុង​វាល​ខាង​ក្រោម​ ។ ប្រ​ព័ន្ធ​ម៉ៃ​​​ក្រូ​ Sun នឹង​ប្រើ​ព័ត៌​មានស្ថិតិ​​ប្រើ​ផលិតផល​ជាមួយ​គ្នា​ និង​​​ផ្ញើ​​សំបុត្រ​ព័ត៌​មានឲ្យ​អ្នក​​របស់​​ VirtualBox ។ ពិសេស​​ ប្រព័ន្ធ​ម៉ៃ​ក្រូ​ Sun ​នឹង​មិន​ហុច​ទិន្ន​ន័យ​ភាគី​ទី​បី​ ។ បាន​រៀប​រាប់​ព័ត៌​មាន​អំពី​ យើង​ប្រើ​យ៉ាង​ដូចម្តេច​នៃ​ទិន្ន​ន័យ​ផ្ទាល់​ដែល​ត្រូវ​បាន​ស្វែង​រក​ក្នុង​ &lt;b>នយោ​បាយ​ផ្ទាស់​ខ្លួន​​&lt;/b> ភាគ​របស់​ VirtualBox សៀវ​ភៅ​ដៃ​ ឬ​លើ​​ &lt;a href=http://www.virtualbox.org/wiki/PrivacyPolicy>នយោ​បាយ​ឯក​ជន​​&lt;/a> ទំព័រ​​ បណ្តាញ​-​តំបន់​បណ្តាញ​របស់​ VirtualBox ។&lt;/p></translation>
    </message>
    <message>
      <source>I &amp;already have a Sun Online account:</source>
      <translation>ខ្ញុំ​មាន​គណនី​លើបណ្តាញ​របស់​​ Sun រួច​ហើយ ៖</translation>
    </message>
    <message>
      <source>&amp;E-mail:</source>
      <translation>អ៊ី​មែល​ ៖</translation>
    </message>
    <message>
      <source>&amp;Password:</source>
      <translation>​ពាក្យ​សម្ងាត់​ ៖</translation>
    </message>
    <message>
      <source>I &amp;would like to create a new Sun Online account:</source>
      <translation>ខ្ញុំ​ចង់​បង្កើត​​គណនី​លើ​បណ្តាញ​របស់​ Sun ថ្មី ៖</translation>
    </message>
    <message>
      <source>&amp;First Name:</source>
      <translation>នាម​ខ្លួន​ ៖</translation>
    </message>
    <message>
      <source>&amp;Last Name:</source>
      <translation>នាម​ត្រ​កូល​ ៖</translation>
    </message>
    <message>
      <source>&amp;Company:</source>
      <translation>ក្រុម​ហ៊ុន​​ ៖</translation>
    </message>
    <message>
      <source>Co&amp;untry:</source>
      <translation>ប្រ​ទេស​ ៖</translation>
    </message>
    <message>
      <source>E-&amp;mail:</source>
      <translation>អ៊ី​មែល​ ៖</translation>
    </message>
    <message>
      <source>P&amp;assword:</source>
      <translation>ពាក្យសម្ងាត់​ ៖</translation>
    </message>
    <message>
      <source>Co&amp;nfirm Password:</source>
      <translation>បញ្ជាក់​ពាក្យ​សម្ងាត់ ៖</translation>
    </message>
    <message>
      <source>&amp;Register</source>
      <translation>ចុះ​ឈ្មោះ​</translation>
    </message>
  </context>
  <context>
    <name>VBoxSFDialog</name>
    <message>
      <source>Shared Folders</source>
      <translation>ថត​ដែល​បាន​ចែករំលែក</translation>
    </message>
  </context>
  <context>
    <name>VBoxScreenshotViewer</name>
    <message>
      <source>Screenshot of %1 (%2)</source>
      <translation>រូប​ថត​អេ​ក្រង់​របស់​ %1 (%2)</translation>
    </message>
    <message>
      <source>Click to view non-scaled screenshot.</source>
      <translation>ចុច​ ដើម្បី​មើល​រូបថត​អេក្រង់​គ្មានមាត្រដ្ឋាន ។</translation>
    </message>
    <message>
      <source>Click to view scaled screenshot.</source>
      <translation>ចុច ដើម្បី​​មើល​រូប​ថត​អេក្រង់​ដែល​មានមាត្រ​ដ្ឋាន​ ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxSelectorWnd</name>
    <message>
      <source>VirtualBox OSE</source>
      <translation>VirtualBox OSE</translation>
    </message>
    <message>
      <source>&amp;Details</source>
      <translation>សេចក្ដី​លម្អិត</translation>
    </message>
    <message>
      <source>&amp;Preferences...</source>
      <comment>global settings</comment>
      <translation>ចំណូល​ចិត្ត...</translation>
    </message>
    <message>
      <source>Display the global settings dialog</source>
      <translation>បង្ហាញ​ប្រអប់​កំណត់​សកល</translation>
    </message>
    <message>
      <source>E&amp;xit</source>
      <translation>ចេញ</translation>
    </message>
    <message>
      <source>Close application</source>
      <translation>បិទ​កម្មវិធី</translation>
    </message>
    <message>
      <source>&amp;New...</source>
      <translation>ថ្មី...</translation>
    </message>
    <message>
      <source>Create a new virtual machine</source>
      <translation>បង្កើត​ម៉ាស៊ីន​និម្មិត​ថ្មី</translation>
    </message>
    <message>
      <source>&amp;Settings...</source>
      <translation>ការ​កំណត់...</translation>
    </message>
    <message>
      <source>Configure the selected virtual machine</source>
      <translation>កំណត់​រចនាសម្ព័ន្ធ​ម៉ាស៊ីន​និម្មិត​ដែល​បានជ្រើស</translation>
    </message>
    <message>
      <source>&amp;Delete</source>
      <translation>លុប</translation>
    </message>
    <message>
      <source>Delete the selected virtual machine</source>
      <translation>លុប​ម៉ាស៊ីននិម្មិត​ដែលបាន​ជ្រើស</translation>
    </message>
    <message>
      <source>D&amp;iscard</source>
      <translation>បោះបង់</translation>
    </message>
    <message>
      <source>Discard the saved state of the selected virtual machine</source>
      <translation>បោះបង់​ស្ថានភាព​របស់​ម៉ាស៊ីន​និម្មិត​ដែល​ជ្រើស​ដែលបាន​រក្សាទុក</translation>
    </message>
    <message>
      <source>Refresh the accessibility state of the selected virtual machine</source>
      <translation>ធ្វើ​ឲ្យ​ស្ថានភាព​ចូលដំណើរការ​ស្រស់​របស់​ម៉ាស៊ីន​និម្មិត​ដែលបានជ្រើស</translation>
    </message>
    <message>
      <source>&amp;File</source>
      <translation>ឯកសារ</translation>
    </message>
    <message>
      <source>&amp;Help</source>
      <translation>ជំនួយ</translation>
    </message>
    <message>
      <source>&amp;Snapshots</source>
      <translation>រូបថត</translation>
    </message>
    <message>
      <source>D&amp;escription</source>
      <translation>សេចក្ដី​ពិពណ៌នា</translation>
    </message>
    <message>
      <source>D&amp;escription *</source>
      <translation>សេចក្ដី​ពិពណ៌នា *</translation>
    </message>
    <message>
      <source>S&amp;how</source>
      <translation>បង្ហាញ</translation>
    </message>
    <message>
      <source>Switch to the window of the selected virtual machine</source>
      <translation>ប្ដូរ​ទៅបង្អួច​របស់​ម៉ាស៊ីននិម្មិត​ដែលបានជ្រើស</translation>
    </message>
    <message>
      <source>S&amp;tart</source>
      <translation>ចាប់ផ្ដើម</translation>
    </message>
    <message>
      <source>Start the selected virtual machine</source>
      <translation>ចាប់ផ្ដើម​ម៉ាស៊ីន​និម្មិត​ដែល​បានជ្រើស</translation>
    </message>
    <message>
      <source>&amp;Machine</source>
      <translation>ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source>Show &amp;Log...</source>
      <translation>បង្ហាញ​កំណត់​ហេតុ...</translation>
    </message>
    <message>
      <source>Show the log files of the selected virtual machine</source>
      <translation>បង្ហាញ​​ឯកសារ​កំណត់ហេតុ​របស់​ម៉ាស៊ីន​និម្មិត​ដែល​បានជ្រើស</translation>
    </message>
    <message>
      <source>R&amp;esume</source>
      <translation>បន្ត</translation>
    </message>
    <message>
      <source>Resume the execution of the virtual machine</source>
      <translation>បន្ត​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>&amp;Pause</source>
      <translation>ផ្អាក</translation>
    </message>
    <message>
      <source>Suspend the execution of the virtual machine</source>
      <translation>ផ្អាក​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីននិម្មិត</translation>
    </message>
    <message>
      <source>&amp;Virtual Media Manager...</source>
      <translation>កម្មវិធី​គ្រប់គ្រង​ម៉ាស៊ីន​និម្មិត...</translation>
    </message>
    <message>
      <source>Display the Virtual Media Manager dialog</source>
      <translation>បង្ហាញ​ប្រអប់​កម្មវិធី​គ្រប់គ្រង​​​មេឌៀ​និម្មិត</translation>
    </message>
    <message>
      <source>Log</source>
      <comment>icon text</comment>
      <translation>កំណត់​ហេតុ</translation>
    </message>
    <message>
      <source>Sun VirtualBox</source>
      <translation>Sun VirtualBox</translation>
    </message>
    <message>
      <source>&amp;Import Appliance...</source>
      <translation>នាំ​ចូល​ឧបករណ៍​...</translation>
    </message>
    <message>
      <source>Import an appliance into VirtualBox</source>
      <translation>នាំ​ចូលឧបករណ៍​ទៅ​ក្នុង​ VirtualBox</translation>
    </message>
    <message>
      <source>&amp;Export Appliance...</source>
      <translation>នាំចេញឧបករណ៍...</translation>
    </message>
    <message>
      <source>Export one or more VirtualBox virtual machines as an appliance</source>
      <translation>នាំ​ចេញ​​ម៉ាស៊ីន​និម្មិត​មួយ​ ឬ​ច្រើន​របស់​ VirtualBox ជាឧបករណ៍​</translation>
    </message>
    <message>
      <source>Re&amp;fresh</source>
      <translation>ធ្វើ​ឲ្យ​ស្រស់</translation>
    </message>
    <message>
      <source>&lt;h3>Welcome to VirtualBox!&lt;/h3>&lt;p>The left part of this window is  a list of all virtual machines on your computer. The list is empty now because you haven't created any virtual machines yet.&lt;img src=:/welcome.png align=right/>&lt;/p>&lt;p>In order to create a new virtual machine, press the &lt;b>New&lt;/b> button in the main tool bar located at the top of the window.&lt;/p>&lt;p>You can press the &lt;b>%1&lt;/b> key to get instant help, or visit &lt;a href=http://www.virtualbox.org>www.virtualbox.org&lt;/a> for the latest information and news.&lt;/p></source>
      <translation>&lt;h3>សូមស្វាគមន៍​​មក​កាន់ VirtualBox !&lt;/h3>&lt;p>ផ្នែកដែល​នៅ​សល់​របស់​បង្អួច​នេះ គឺជា​បញ្ជី​ម៉ាស៊ីន​និម្មិត​នៅ​ក្នុង​កុំព្យូទ័រ​របស់​អ្នក ។ បញ្ជី​ទទេ​ឥឡូវ​នេះ ពីព្រោះ​អ្នកមិនបានបង្កើត​ម៉ាស៊ីននិម្មិត​នៅ​ឡើយ​ទេ ។&lt;img src=:/welcome.png align=right/>&lt;/p>&lt;p>ដើម្បី​បង្កើត​ម៉ាស៊ីន​និម្មិត ចុច​ប៊ូតុង &lt;b>ថ្មី&lt;/b> ក្នុង​របារ​ឧបករណ៍​ដែល​មាននៅ​​ផ្នែក​ខាង​លើ​នៃ​បង្អួច ។&lt;/p>&lt;p>អ្នក​អាច​ចុច​គ្រាប់ចុច &lt;b>%1&lt;/b> ដើម្បីទទួល​បាន​ជំនួយ​ភ្លាមៗ ឬ​ចំពោះ​ព័ត៌មាន​បន្ថែម ​សូម​មើល &lt;a href=http://www.virtualbox.org>www.virtualbox.org&lt;/a> ។&lt;/p></translation>
    </message>
  </context>
  <context>
    <name>VBoxSettingsDialog</name>
    <message>
      <source>&lt;i>Select a settings category from the list on the left-hand side and move the mouse over a settings item to get more information&lt;/i>.</source>
      <translation>&lt;i>ជ្រើស​ប្រភេទ​ការ​កំណត់​ពី​បញ្ជីនៅ​ខាង​ផ្នែកខាង​ឆ្វេង​ ហើយ​ផ្លាស់ទី​កណ្ដុរ​លើ​ធាតុ​កំណត់​ដើម្បី​ទទួល​យក​ព័ត៌មានបន្ថែម&lt;/i> ។</translation>
    </message>
    <message>
      <source>Invalid settings detected</source>
      <translation>បានរក​ឃើញ​ការ​កំណត់​ដែល​មិន​ត្រឹមត្រូវ</translation>
    </message>
    <message>
      <source>Settings</source>
      <translation>កំណត់</translation>
    </message>
    <message>
      <source>Non-optimal settings detected</source>
      <translation>បា​ន​រកឃើញ​ការ​កំណត់​ដែល​មិន​ប្រសើរ</translation>
    </message>
    <message>
      <source>On the &lt;b>%1&lt;/b> page, %2</source>
      <translation>នៅ​លើទំព័រ &lt;b>%1&lt;/b> %2</translation>
    </message>
  </context>
  <context>
    <name>VBoxSnapshotDetailsDlg</name>
    <message>
      <source>Details of %1 (%2)</source>
      <translation>សេចក្ដី​លម្អិត​របស់ %1 (%2)</translation>
    </message>
    <message>
      <source>Click to enlarge the screenshot.</source>
      <translation>ចុច ដើម្បី​ពង្រីក​រូប​ថត​អេ​ក្រង់​ ។</translation>
    </message>
    <message>
      <source>&amp;Name:</source>
      <translation>ឈ្មោះ ៖</translation>
    </message>
    <message>
      <source>Taken:</source>
      <translation>បាន​ថត ៖</translation>
    </message>
    <message>
      <source>&amp;Description:</source>
      <translation>សេចក្តី​ពិពណ៌នា​ ៖</translation>
    </message>
    <message>
      <source>D&amp;etails:</source>
      <translation>សេច​ក្តី​លម្អិត​ ៖</translation>
    </message>
  </context>
  <context>
    <name>VBoxSnapshotsWgt</name>
    <message>
      <source>VBoxSnapshotsWgt</source>
      <translation>VBoxSnapshotsWgt</translation>
    </message>
    <message>
      <source>Current State (changed)</source>
      <comment>Current State (Modified)</comment>
      <translation>ស្ថានភាព​បច្ចុប្បន្ន (បាន​ផ្លាស់ប្ដូរ)</translation>
    </message>
    <message>
      <source>Current State</source>
      <comment>Current State (Unmodified)</comment>
      <translation>ស្ថានភាព​បច្ចុប្បន្ន</translation>
    </message>
    <message>
      <source>The current state differs from the state stored in the current snapshot</source>
      <translation>ស្ថានភាព​បច្ចុប្បន្ន​ខុសពី​ស្ថានភាព​ដែល​បាន​ទុក​នៅក្នុង​រូបថត​បច្ចុប្បន្ន</translation>
    </message>
    <message>
      <source>The current state is identical to the state stored in the current snapshot</source>
      <translation>ស្ថានភាព​បច្ចុប្បន្ន​គឺ​ដូច​គ្នា​នឹង​ស្ថានភាព​ដែល​បាន​ទុក​នៅ​ក្នុង​រូបថត​បច្ចុប្បន្ន</translation>
    </message>
    <message>
      <source> (current, </source>
      <comment>Snapshot details</comment>
      <translation> (បច្ចុប្បន្ន </translation>
    </message>
    <message>
      <source>online)</source>
      <comment>Snapshot details</comment>
      <translation>លើ​បណ្ដាញ)</translation>
    </message>
    <message>
      <source>offline)</source>
      <comment>Snapshot details</comment>
      <translation>ក្រៅបណ្ដាញ)</translation>
    </message>
    <message>
      <source>Taken at %1</source>
      <comment>Snapshot (time)</comment>
      <translation>បាន​ថត​នៅ %1</translation>
    </message>
    <message>
      <source>Taken on %1</source>
      <comment>Snapshot (date + time)</comment>
      <translation>បានថត​នៅ​ថ្ងៃទី %1</translation>
    </message>
    <message>
      <source>%1 since %2</source>
      <comment>Current State (time or date + time)</comment>
      <translation>%1 តាំង​ពី %2</translation>
    </message>
    <message>
      <source>Snapshot %1</source>
      <translation>រូបថត %1</translation>
    </message>
    <message>
      <source>Take &amp;Snapshot</source>
      <translation>យក​រូបថត</translation>
    </message>
    <message>
      <source>S&amp;how Details</source>
      <translation>បង្ហាញ​សេចក្ដី​លម្អិត</translation>
    </message>
    <message>
      <source>Take a snapshot of the current virtual machine state</source>
      <translation>យក​រូបថត​របស់​ស្ថានភាព​ម៉ាស៊ីន​និម្មិត​បច្ចុប្បន្ន</translation>
    </message>
    <message>
      <source>Show the details of the selected snapshot</source>
      <translation>បង្ហាញ​សេចក្ដី​លម្អិត​របស់​រូបថត​ដែល​បានជ្រើស</translation>
    </message>
    <message>
      <source> (%1)</source>
      <translation> (%1)</translation>
    </message>
    <message>
      <source>&amp;Restore Snapshot</source>
      <translation>ស្តារ​រូប​ថត​</translation>
    </message>
    <message>
      <source>&amp;Delete Snapshot</source>
      <translation>លុប​រូបថត​</translation>
    </message>
    <message>
      <source>Restore the selected snapshot of the virtual machine</source>
      <translation>ស្តារ​រូប​ថត​ដែល​បាន​ជ្រើស​របស់​​ម៉ាស៊ីន​និម្មិត​</translation>
    </message>
    <message>
      <source>Delete the selected snapshot of the virtual machine</source>
      <translation>លុប​រូប​ថត​ដែល​បាន​ជ្រើស​របស់​​ម៉ាស៊ីន​និម្មិត​</translation>
    </message>
    <message>
      <source> (%1 ago)</source>
      <translation> (%1 កន្លង​ទៅ)</translation>
    </message>
  </context>
  <context>
    <name>VBoxSwitchMenu</name>
    <message>
      <source>Disable</source>
      <translation>បិទ</translation>
    </message>
    <message>
      <source>Enable</source>
      <translation>បើក</translation>
    </message>
  </context>
  <context>
    <name>VBoxTakeSnapshotDlg</name>
    <message>
      <source>Take Snapshot of Virtual Machine</source>
      <translation>យក​រូបថត​របស់ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>Snapshot &amp;Name</source>
      <translation>ឈ្មោះ​រូបថត</translation>
    </message>
    <message>
      <source>Snapshot &amp;Description</source>
      <translation>សេចក្ដី​ពិពណ៌នា​រូបថត</translation>
    </message>
    <message>
      <source>Warning: You are taking a snapshot of a running machine which has %n immutable image(s) attached to it. As long as you are working from this snapshot the immutable image(s) will not be reset to avoid loss of data.</source>
      <translation>ការ​ព្រមាន ៖ អ្នកកំពុង​ថត​រូប​របស់​ម៉ាស៊ីន​ដែល​កំពុង​រត់ ដែលមាន​រូបភាព​មិនប្រែប្រួល %n បានភ្ជាប់​ទៅកាន់​វា ។ ដ៏រាប​ណា​អ្នកកំពុង​ធ្វើការ​​ពី​រូបភាព​ដែល​មិនប្រែប្រួល​នេះ នឹង​មិន​ត្រូ​វបានកំណត់​ឡើងវិញ​ ដើម្បី​ចៀសវាង​ពី​ការ​បាត់បង់​ទិន្នន័យ​ទេ ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxTextEditor</name>
    <message>
      <source>Edit text</source>
      <translation>កែសម្រួល​អត្ថបទ​</translation>
    </message>
    <message>
      <source>&amp;Replace...</source>
      <translation>ជំនួស​...</translation>
    </message>
    <message>
      <source>Replaces the current text with the content of a file.</source>
      <translation>ជំនួស​អត្ថបទ​បច្ចុប្បន្ន​ដោយ​មាតិកា​របស់​ឯកសារ​មួយ ។</translation>
    </message>
    <message>
      <source>Text (*.txt);;All (*.*)</source>
      <translation>អត្ថបទ (*.txt);;All (*.*)</translation>
    </message>
    <message>
      <source>Select a file to open...</source>
      <translation>ជ្រើស​ឯកសារត្រូវ​បើក...</translation>
    </message>
  </context>
  <context>
    <name>VBoxTrayIcon</name>
    <message>
      <source>Show Selector Window</source>
      <translation>បង្ហាញ​បង្អួច​កម្មវិធី​​ជ្រើស</translation>
    </message>
    <message>
      <source>Show the selector window assigned to this menu</source>
      <translation>បង្ហាញ​បង្អួច​កម្មវិធី​ជ្រើស​ដែលបានផ្ដល់​ទៅ​ម៉ឺនុយ​នេះ</translation>
    </message>
    <message>
      <source>Hide Tray Icon</source>
      <translation>លាក់​រូបតំណាង​ថាស​ប្រព័ន្ធ</translation>
    </message>
    <message>
      <source>Remove this icon from the system tray</source>
      <translation>យក​រូបតំណាង​នេះ​ចេញ​ពី​ថាស​ប្រព័ន្ធ</translation>
    </message>
    <message>
      <source>&amp;Other Machines...</source>
      <comment>tray menu</comment>
      <translation>ម៉ាស៊ីន​ផ្សេងៗ...</translation>
    </message>
  </context>
  <context>
    <name>VBoxUSBMenu</name>
    <message>
      <source>&lt;no devices available></source>
      <comment>USB devices</comment>
      <translation>&lt;មិនមាន​ឧបករណ៍​ទេ></translation>
    </message>
    <message>
      <source>No supported devices connected to the host PC</source>
      <comment>USB device tooltip</comment>
      <translation>គ្មាន​ឧបករណ៍​ដែលបានគាំទ្រ​ទេ​ បានភ្ជាប់​ទៅកាន់​ម៉ាស៊ីន​ទេ</translation>
    </message>
  </context>
  <context>
    <name>VBoxUpdateDlg</name>
    <message>
      <source>1 day</source>
      <translation>១ ថ្ងៃ</translation>
    </message>
    <message>
      <source>2 days</source>
      <translation>២ ថ្ងៃ</translation>
    </message>
    <message>
      <source>3 days</source>
      <translation>៣ ថ្ងៃ</translation>
    </message>
    <message>
      <source>4 days</source>
      <translation>៤ ថ្ងៃ</translation>
    </message>
    <message>
      <source>5 days</source>
      <translation>៥ ថ្ងៃ</translation>
    </message>
    <message>
      <source>6 days</source>
      <translation>៦ ថ្ងៃ</translation>
    </message>
    <message>
      <source>1 week</source>
      <translation>១ សប្ដាហ៍</translation>
    </message>
    <message>
      <source>2 weeks</source>
      <translation>២ សប្ដាហ៍</translation>
    </message>
    <message>
      <source>3 weeks</source>
      <translation>៣ សប្ដាហ៍</translation>
    </message>
    <message>
      <source>1 month</source>
      <translation>១ ខែ</translation>
    </message>
    <message>
      <source>Never</source>
      <translation>កុំ</translation>
    </message>
    <message>
      <source>Chec&amp;k</source>
      <translation>ពិនិត្យមើល</translation>
    </message>
    <message>
      <source>&amp;Close</source>
      <translation>បិទ</translation>
    </message>
    <message>
      <source>VirtualBox Update Wizard</source>
      <translation>អ្នកជំនួយការ​ធ្វើ​ឲ្យ​ទាន់សម័យ​របស់ VirtualBox</translation>
    </message>
    <message>
      <source>Check for Updates</source>
      <translation>ពិនិត្យ​មើល​ភាព​ទាន់សម័យ</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់</translation>
    </message>
    <message>
      <source>Summary</source>
      <translation>សេចក្ដី​សង្ខេប</translation>
    </message>
    <message>
      <source>&lt;p>A new version of VirtualBox has been released! Version &lt;b>%1&lt;/b> is available at &lt;a href=&quot;http://www.virtualbox.org/&quot;>virtualbox.org&lt;/a>.&lt;/p>&lt;p>You can download this version using the link:&lt;/p>&lt;p>&lt;a href=%2>%3&lt;/a>&lt;/p></source>
      <translation>&lt;p>កំណែ​ថ្មី​របស់ VirtualBox ត្រូ​វបាន​ចេញ​ផ្សាយ​ហើយ ! គឺ​កំណែ &lt;b>%1&lt;/b> ដែល​អាច​រក​បាននៅ &lt;a href=&quot;http://www.virtualbox.org/&quot;>virtualbox.org&lt;/a>។&lt;/p>&lt;p>អ្នកអាច​ទាញ​យក​កំណែ​នេះ​​ដោយ​ផ្ទាល់ពី​តំណ ៖&lt;/p>&lt;p>&lt;a href=%2>%3&lt;/a>&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Unable to obtain the new version information due to the following network error:&lt;/p>&lt;p>&lt;b>%1&lt;/b>&lt;/p></source>
      <translation>&lt;p>មិនអាច​យក​ព័ត៌មានកំណែ​ថ្មី​បានទេ ដោយសារ​តែ​កំហុស​បណ្ដាញ​ដូច​ខាងក្រោម ៖&lt;/p>&lt;p>&lt;b>%1&lt;/b>&lt;/p></translation>
    </message>
    <message>
      <source>You are already running the most recent version of VirtualBox.</source>
      <translation>អ្នក​បាន​ដំឡើង​កំណែ​របស់ VirtualBox ចុងក្រោយ​រួច​ហើយ ។ សូម​ធ្វើ​ការ​ពិនិត្យ​ពេលក្រោយ​ម្ដង​ទៀត ។</translation>
    </message>
    <message>
      <source>&lt;p>This wizard will connect to the VirtualBox web-site and check if a newer version of VirtualBox is available.&lt;/p>&lt;p>Use the &lt;b>Check&lt;/b> button to check for a new version now or the &lt;b>Cancel&lt;/b> button if you do not want to perform this check.&lt;/p>&lt;p>You can run this wizard at any time by choosing &lt;b>Check for Updates...&lt;/b> from the &lt;b>Help&lt;/b> menu.&lt;/p></source>
      <translation>&lt;p>អ្នក​ជំនួយ​ការ​នេះ​នឹង​តភ្ជាប់​ទៅ​កាន់​តំបន់​បណ្ដាញ​របស់ VirtualBox ហើយ​ពិនិត្យ​មើល​ប្រសិនបើ​មានកំណែ​ថ្មី​របស់​ VirtualBox ។&lt;/p>&lt;p>ប្រើ​ប៊ូតុង &lt;b>ពិនិត្យ​មើល&lt;/b> ដើម្បី​ពិនិត្យ​មើល​កំណែ​ថ្មី​ឥឡូវ ឬ​ប៊ូតុង &lt;b>បោះបង់&lt;/b> ប្រសិនបើ​អ្នក​មិន​ចង់​អនុវត្ត​ការ​ត្រួតពិនិត្យ​នេះ​ទេ ។&lt;/p>&lt;p>អ្នក​អាច​រត់​អ្នក​ជំនួយការ​នេះ​នៅពេល​ណាមួយ ដោយ​​ជ្រើស &lt;b>ពិនិត្យ​រកមើល​ភាព​ទាន់សម័យ...&lt;/b> ពី​ម៉ឺនុយ &lt;b>ជំនួយ&lt;/b> ។&lt;/p></translation>
    </message>
  </context>
  <context>
    <name>VBoxVMDescriptionPage</name>
    <message>
      <source>No description. Press the Edit button below to add it.</source>
      <translation>គ្មាន​សេចក្ដី​ពិពណ៌នា​ទេ ។ ចុច​ប៊ុតុង កែសម្រួល​ខា​ងក្រោម​ដើម្បី​បន្ថែម​វា ។</translation>
    </message>
    <message>
      <source>Edit</source>
      <translation>កែសម្រួល</translation>
    </message>
    <message>
      <source>Edit (Ctrl+E)</source>
      <translation>កែសម្រួល (បញ្ជា(Ctrl)+E)</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMDetailsView</name>
    <message>
      <source>The selected virtual machine is &lt;i>inaccessible&lt;/i>. Please inspect the error message shown below and press the &lt;b>Refresh&lt;/b> button if you want to repeat the accessibility check:</source>
      <translation>ម៉ាស៊ីន​និម្មិត​ដែល​បាន​ជ្រើស​ &lt;i>មិន​អាច​ចូលដំណើរការ​បានទេ&lt;/i> ។ សូម​ពិនិត្យ​មើល​សារ​កំហុស​ដែល​បានបង្ហាញ​ខាង​ក្រោម ហើយ​ចុច​ប៊ូតុង &lt;b>ធ្វើ​ឲ្យ​ស្រស់&lt;/b> ប្រសិន​បើ​អ្នក​ចង់​ធ្វើការ​ពិនិត្យ​ការ​ចូលដំណើរការ​​ឡើង​វិញ ៖</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMFirstRunWzd</name>
    <message>
      <source>First Run Wizard</source>
      <translation>អ្នក​ជំនួយកា​ររត់​ដំបូង</translation>
    </message>
    <message>
      <source>&lt;p>You have started a newly created virtual machine for the first time. This wizard will help you to perform the steps necessary for installing an operating system of your choice onto this virtual machine.&lt;/p>&lt;p>Use the &lt;b>Next&lt;/b> button to go to the next page of the wizard and the &lt;b>Back&lt;/b> button to return to the previous page. You can also press &lt;b>Cancel&lt;/b> if you want to cancel the execution of this wizard.&lt;/p></source>
      <translation>&lt;p>អ្នក​បាន​ចាប់ផ្ដើម​ម៉ាស៊ីន​និម្មិត​ដែលបាន​បង្កើត​ថ្មីៗ​ពេល​ដំបូង​បាន​ ។ អ្នក​ជំនួយការ​នេះ​នឹង​ជួយ​អ្នក​ឲ្យ​អនុវត្ត​ជំហាន​ដែល​ចាំបាច់​សម្រាប់ដំឡើង​​ប្រព័ន្ធ​ប្រតិបត្តិការ​នៃ​ជម្រើស​របស់អ្នក​ក្នុង​​ម៉ាស៊ីន​និម្មិត​នេះ ។&lt;/p>&lt;p>ប្រើ​ប៊ូតុង &lt;b>បន្ទាប់&lt;/b> ដើម្បី​ទៅ​កាន់​ទំព័រ​បន្ទាប់​របស់​អ្នក​ជំនួយការ ហើយ​ប៊ូតុង​ &lt;b>ថយក្រោយ&lt;/b> ដើម្បីត្រឡប់​ទៅ​ទំព័រ​មុន ។ អ្នកក៏​អាច​ចុច &lt;b>បោះបង់&lt;/b> ប្រសិន​បើ​អ្នក​ចង់​បោះបង់​ប្រតិបត្តិការ​របស់​អ្នក​ជំនួយកា​រនេះ ។&lt;/p></translation>
    </message>
    <message>
      <source>Welcome to the First Run Wizard!</source>
      <translation>សូម​ស្វាគមន៍​មកកាន់​អ្នក​ជំនួយកា​រដំបូង !</translation>
    </message>
    <message>
      <source>&lt;p>Select the type of media you would like to use for installation.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​ប្រភេទ​មេឌៀ​ដែល​អ្នក​ចង់​ប្រើ​សម្រាប់​ដំឡើងខាង​ក្រោម ។&lt;/p></translation>
    </message>
    <message>
      <source>Media Type</source>
      <translation>ប្រភេទ​មេឌៀ</translation>
    </message>
    <message>
      <source>&amp;CD/DVD-ROM Device</source>
      <translation>ឧបករណ៍​ស៊ីឌី/ឌីវីឌី​រ៉ូម</translation>
    </message>
    <message>
      <source>&amp;Floppy Device</source>
      <translation>ឧបករណ៍​ថាស​ទន់</translation>
    </message>
    <message>
      <source>&lt;p>Select the media which contains the setup program of the operating system you want to install. This media must be bootable, otherwise the setup program will not be able to start.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​មេឌៀ​ដែល​មាន​កម្មវិធីរៀបចំ​របស់​ប្រព័ន្ធ​ប្រតិបត្តិការ​ដែល​អ្នក​ចង់​ដំឡើង ។ មេឌៀ​នេះ​ត្រូវ​តែ​អាច​ចាប់ផ្ដើម​បាន បើ​មិនដូច្នេះ​ទេ កម្មវិធី​រៀបចំ​នឹង​មិនអាច​ចាប់ផ្ដើម​បានទេ ។&lt;/p></translation>
    </message>
    <message>
      <source>Media Source</source>
      <translation>ប្រភព​មេឌៀ</translation>
    </message>
    <message>
      <source>Select Installation Media</source>
      <translation>ជ្រើស​មេឌៀ​ដំឡើង</translation>
    </message>
    <message>
      <source>&lt;p>You have selected the following media to boot from:&lt;/p></source>
      <translation>&lt;p>អ្នក​បាន​ជ្រើស​មេឌៀ​ដូចខាងក្រោម​ដើម្បី​ចាប់ផ្ដើម ៖&lt;/p></translation>
    </message>
    <message>
      <source>Summary</source>
      <translation>សេចក្ដីសង្ខេប</translation>
    </message>
    <message>
      <source>CD/DVD-ROM Device</source>
      <translation>ឧបករណ៍​ស៊ីឌី/ឌីវីឌីរ៉ូម</translation>
    </message>
    <message>
      <source>Floppy Device</source>
      <translation>ឧបករណ៍​ថាស​ទន់</translation>
    </message>
    <message>
      <source>&lt;p>You have started a newly created virtual machine for the first time. This wizard will help you to perform the steps necessary for booting an operating system of your choice on the virtual machine.&lt;/p>&lt;p>Note that you will not be able to install an operating system into this virtual machine right now because you did not attach any hard disk to it. If this is not what you want, you can cancel the execution of this wizard, select &lt;b>Settings&lt;/b> from the &lt;b>Machine&lt;/b> menu of the main VirtualBox window to access the settings dialog of this machine and change the hard disk configuration.&lt;/p>&lt;p>Use the &lt;b>Next&lt;/b> button to go to the next page of the wizard and the &lt;b>Back&lt;/b> button to return to the previous page. You can also press &lt;b>Cancel&lt;/b> if you want to cancel the execution of this wizard.&lt;/p></source>
      <translation>&lt;p>អ្នក​បាន​ចាប់ផ្ដើម​ម៉ាស៊ីន​និម្មិត​ដែល​ទើប​បានបង្កើត​ថ្មី​ជា​លើក​ដំបូង ។ អ្នក​ជំនួយ​ការ​នេះ​នឹង​ជួយ​អ្នក​ក្នុងការ​អនុវត្ត​ជំហាន​ដែល​ចាំបាច់​សម្រាប់​ចាប់ផ្ដើម​ប្រព័ន្ធ​ប្រតិបត្តិការ​ជា​ជម្រើស​របស់​អ្នក​នៅ​លើ​ម៉ាស៊ីន​និម្មិត ។&lt;/p>&lt;p>ចំណាំ​ថា អ្នក​នឹង​មិន​អាច​ដំឡើង​ប្រព័ន្ធ​ប្រតិបត្តិការ​ក្នុង​ម៉ាស៊ីន​និម្មិត​ឥឡូវ​បានទេ ពីព្រោះ​អ្នក​មិន​បាន​ភ្ជាប់​ថាសរឹង​ ។ ប្រសិន​បើ​នេះ​មិនមែន​ជា​អ្វី​ដែល​អ្នក​ចង់​បានទេ អ្នក​អាច​បោះបង់​ប្រតិបត្តិការ​នៃ​អ្នក​ជំនួយ​ការ​នេះ​បាន ដោយ​ជ្រើស &lt;b>ការ​កំណត់&lt;/b> ពី​ម៉ឺនុយ​ &lt;b>ម៉ាស៊ីន&lt;/b> របស់​បង្អួច​ VirtualBox មេ ដើម្បី​ចូលដំណើរការ​ប្រអប់​កំណត់​របស់​ម៉ាស៊ីន​នេះ ហើយ​ផ្លាស់ប្ដូរ​កា​រកំណត់​រចនាសម្ព័ន្ធ​ថាសរឹង ។&lt;/p>&lt;p>ប្រើ​ប៊ូតុង &lt;b>បន្ទាប់&lt;/b> ដើម្បី​ទៅ​ទំព័រ​បន្ទាប់​របស់​អ្នក​ជំនួយការ ហើយ​ប៊ូតុង &lt;b>ថយក្រោយ&lt;/b> ដើម្បីត្រឡប់​ទៅ​ទំព័រ​មុន ។ អ្នក​ក៏អា​ច​ចុច &lt;b>បោះបង់&lt;/b> ប្រសិន​បើ​អ្នក​បោះបង់​ប្រត្តិការ​អ្នក​ជំនួយការ​នេះ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Select the type of media you would like to use for booting an operating system.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​ប្រភេទ​មេឌៀ​ដែល​អ្នកចង់​ប្រើ​ដើម្បី​ចាប់ផ្ដើម​ប្រព័ន្ធ​ប្រតិបត្តិការ​ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>Select the media that contains the operating system you want to work with. This media must be bootable, otherwise the operating system will not be able to start.&lt;/p></source>
      <translation>&lt;p>ជ្រើស​មេឌៀ​ដែល​មាន​ប្រព័ន្ធ​ប្រតិបត្តិការ​ដែល​អ្នក​ចង់​ធ្វើការ​ជា​មួយ ។ មេឌៀ​នេះ​ត្រូវទេ​អាច​ចាប់ផ្ដើម​បាន បើ​មិន​ដូច្នេះ​ទេ ប្រព័ន្ធ​ប្រតិបត្តិការ​មិនអាច​ចាប់ផ្ដើម​បានទេ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>You have selected the following media to boot an operating system from:&lt;/p></source>
      <translation>&lt;p>អ្នក​បាន​ជ្រើស​មេឌៀ​ដូចខាង​ក្រោម​ដើម្បី​ចាប់ផ្ដើម​ប្រព័ន្ធ​ប្រតិបត្តិការ ៖&lt;/p></translation>
    </message>
    <message>
      <source>&lt;p>If the above is correct, press the &lt;b>Finish&lt;/b> button. Once you press it, the selected media will be mounted on the virtual machine and the machine will start execution.&lt;/p></source>
      <translation>&lt;p>ប្រសិន​បើ​ខា​ងលើ​ត្រឹមត្រូវ​ហើយ ចុច​ប៊ូតុង &lt;b>បញ្ចប់&lt;/b> ។ នៅពេល​អ្នក​ចុច​វា មេឌៀ​ដែល​បាន​ជ្រើស​នឹង​ត្រូវ​បានម៉ោន​​លើ​ម៉ាស៊ីន​និម្មិត ហើយ​ម៉ាស៊ីន​នឹង​ចាប់ផ្ដើម​ប្រតិបត្តិ ។&lt;/p></translation>
    </message>
    <message>
      <source>&lt; &amp;Back</source>
      <translation>&lt; ថយក្រោយ</translation>
    </message>
    <message>
      <source>&amp;Next ></source>
      <translation>បន្ទាប់ ></translation>
    </message>
    <message>
      <source>&amp;Finish</source>
      <translation>បញ្ចប់</translation>
    </message>
    <message>
      <source>Type</source>
      <comment>summary</comment>
      <translation>ប្រភេទ</translation>
    </message>
    <message>
      <source>Source</source>
      <comment>summary</comment>
      <translation>ប្រភព</translation>
    </message>
    <message>
      <source>Cancel</source>
      <translation>បោះបង់</translation>
    </message>
    <message>
      <source>&lt;p>If the above is correct, press the &lt;b>Finish&lt;/b> button. Once you press it, the selected media will be temporarily mounted on the virtual machine and the machine will start execution.&lt;/p>&lt;p>Please note that when you close the virtual machine, the specified media will be automatically unmounted and the boot device will be set back to the first hard disk.&lt;/p>&lt;p>Depending on the type of the setup program, you may need to manually unmount (eject) the media after the setup program reboots the virtual machine, to prevent the installation process from starting again. You can do this by selecting the corresponding &lt;b>Unmount...&lt;/b> action in the &lt;b>Devices&lt;/b> menu.&lt;/p></source>
      <translation>&lt;p>​​ចុច​ ប្រ​សិន​បើ​ខាងលើ​​ត្រឹម​ត្រូវ​ &lt;b>​បញ្ចប់​​&lt;/b> ប៊ូតុង​ ។​ លើ​ទី​មួយ​ដែល​អ្នក​ចុច​វា​Once you press it, the selected media will be temporarily mounted on the virtual machine and the machine will start execution.&lt;/p>&lt;p>Please note that when you close the virtual machine, the specified media will be automatically unmounted and the boot device will be set back to the first hard disk.&lt;/p>&lt;p>Depending on the type of the setup program, you may need to manually unmount (eject) the media after the setup program reboots the virtual machine, to prevent the installation process from starting again. You can do this by selecting the corresponding &lt;b>Unmount...&lt;/b> action in the &lt;b>Devices&lt;/b> menu.&lt;/p></translation>
    </message>
  </context>
  <context>
    <name>VBoxVMInformationDlg</name>
    <message>
      <source>%1 - Session Information</source>
      <translation>%1 - ព័ត៌មាន​សម័យ</translation>
    </message>
    <message>
      <source>&amp;Details</source>
      <translation>សេចក្ដី​លម្អិត</translation>
    </message>
    <message>
      <source>&amp;Runtime</source>
      <translation>ពេលវេលា​រត់</translation>
    </message>
    <message>
      <source>DMA Transfers</source>
      <translation>ការ​ផ្ទេរ DMA</translation>
    </message>
    <message>
      <source>PIO Transfers</source>
      <translation>ការ​ផ្ទេរ PIO</translation>
    </message>
    <message>
      <source>Data Read</source>
      <translation>អាន​ទិន្នន័យ</translation>
    </message>
    <message>
      <source>Data Written</source>
      <translation>សរសេរ​ទិន្នន័យ</translation>
    </message>
    <message>
      <source>Data Transmitted</source>
      <translation>បានផ្ទេរ​ទិន្នន័យ</translation>
    </message>
    <message>
      <source>Data Received</source>
      <translation>បាន​ទទួល​ទិន្នន័យ</translation>
    </message>
    <message>
      <source>Runtime Attributes</source>
      <translation>គុណលក្ខណៈ​ពេលវេលា​រត់</translation>
    </message>
    <message>
      <source>Screen Resolution</source>
      <translation>គុណភាព​បង្ហាញ​របស់​អេក្រង់</translation>
    </message>
    <message>
      <source>Version %1.%2</source>
      <comment>guest additions</comment>
      <translation>កំណែ %1.%2</translation>
    </message>
    <message>
      <source>Not Detected</source>
      <comment>guest additions</comment>
      <translation>រក​មិនឃើញ</translation>
    </message>
    <message>
      <source>Not Detected</source>
      <comment>guest os type</comment>
      <translation>រក​មិន​ឃើញ</translation>
    </message>
    <message>
      <source>Guest Additions</source>
      <translation>ការ​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ</translation>
    </message>
    <message>
      <source>Guest OS Type</source>
      <translation>ប្រភេទ​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ</translation>
    </message>
    <message>
      <source>No Network Adapters</source>
      <translation>គ្មានអាដាប់ទ័រ​បណ្ដាញ​ទេ</translation>
    </message>
    <message>
      <source>Enabled</source>
      <comment>nested paging</comment>
      <translation>បានបើក</translation>
    </message>
    <message>
      <source>Disabled</source>
      <comment>nested paging</comment>
      <translation>បានបិទ</translation>
    </message>
    <message>
      <source>Nested Paging</source>
      <translation>ភេយីង​ខាង​ក្នុង</translation>
    </message>
    <message>
      <source>VBoxVMInformationDlg</source>
      <translation>VBoxVMInformationDlg</translation>
    </message>
    <message>
      <source>Not Available</source>
      <comment>


details report (VRDP server port)</comment>
      <translation>មិនអាច​ប្រើបាន​</translation>
    </message>
    <message>
      <source>Storage Statistics</source>
      <translation>ស្ថិតិ​ផ្ទុក​</translation>
    </message>
    <message>
      <source>No Storage Devices</source>
      <translation>គ្មាន​ឧបករណ៍​ផ្ទុក​ទេ​</translation>
    </message>
    <message>
      <source>Network Statistics</source>
      <translation>ស្ថិតិ​បណ្ដាញ</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMListView</name>
    <message>
      <source>Inaccessible</source>
      <translation>មិនអាច​ចូលដំណើរការ​បាន</translation>
    </message>
    <message>
      <source>&lt;nobr>%1&lt;br>&lt;/nobr>&lt;nobr>%2 since %3&lt;/nobr>&lt;br>&lt;nobr>Session %4&lt;/nobr></source>
      <comment>VM tooltip (name, last state change, session state)</comment>
      <translation>&lt;nobr>%1&lt;br>&lt;/nobr>&lt;nobr>%2 តាំងពី %3&lt;/nobr>&lt;br>&lt;nobr>សម័យ %4&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>&lt;b>%1&lt;/b>&lt;br>&lt;/nobr>&lt;nobr>Inaccessible since %2&lt;/nobr></source>
      <comment>Inaccessible VM tooltip (name, last state change)</comment>
      <translation>&lt;nobr>&lt;b>%1&lt;/b>&lt;br>&lt;/nobr>&lt;nobr>មិនអាច​ចូលដំណើរការ​បានតាំងពីរ %2&lt;/nobr></translation>
    </message>
    <message>
      <source>S&amp;how</source>
      <translation>បង្ហាញ</translation>
    </message>
    <message>
      <source>Switch to the window of the selected virtual machine</source>
      <translation>ប្ដូរ​ទៅ​បង្អួច​របស់​ម៉ាស៊ីន​និម្មិត​ដែល​បានជ្រើស</translation>
    </message>
    <message>
      <source>S&amp;tart</source>
      <translation>ចាប់ផ្ដើម</translation>
    </message>
    <message>
      <source>Start the selected virtual machine</source>
      <translation>ចាប់ផ្ដើម​ម៉ាស៊ីន​និម្មិត​ដែល​បានជ្រើស</translation>
    </message>
    <message>
      <source>R&amp;esume</source>
      <translation>បន្ត</translation>
    </message>
    <message>
      <source>Resume the execution of the virtual machine</source>
      <translation>បន្ត​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
    <message>
      <source>&amp;Pause</source>
      <translation>ផ្អាក</translation>
    </message>
    <message>
      <source>Suspend the execution of the virtual machine</source>
      <translation>ផ្អាក​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​និម្មិត</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMLogViewer</name>
    <message>
      <source>Log Viewer</source>
      <translation>កម្មវិធី​មើល​កំណត់​ហេតុ</translation>
    </message>
    <message>
      <source>&amp;Save</source>
      <translation>រក្សាទុក</translation>
    </message>
    <message>
      <source>&amp;Refresh</source>
      <translation>ធ្វើ​ឲ្យ​ស្រស់</translation>
    </message>
    <message>
      <source>%1 - VirtualBox Log Viewer</source>
      <translation>%1 - កម្មវិធីមើល​កំណត់​ហេតុ​របស់​ VirtualBox</translation>
    </message>
    <message>
      <source>&lt;p>No log files found. Press the &lt;b>Refresh&lt;/b> button to rescan the log folder &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr>.&lt;/p></source>
      <translation>&lt;p>រក​មិនឃើញ​កំណត់ហេតុទេ ។ ចុច​ប៊ូតុង &lt;b>ធ្វើ​ឲ្យ​ស្រស់&lt;/b> ដើម្បី​វិភាគ​ថត​កំណត់ហេតុ​ម្ដង​ទៀត &lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr> ។&lt;/p></translation>
    </message>
    <message>
      <source>Save VirtualBox Log As</source>
      <translation>រក្សាទុក​កំណត់ហេតុ VirtualBox ជា</translation>
    </message>
    <message>
      <source>&amp;Find</source>
      <translation>រក</translation>
    </message>
    <message>
      <source>Close</source>
      <translation>បិទ</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsAudio</name>
    <message>
      <source>When checked, a virtual PCI audio card will be plugged into the virtual machine and will communicate with the host audio system using the specified driver.</source>
      <translation>នៅពេល​បាន​គូស​ធីក កាត​អូឌីយ៉ូ PCI និម្មិត​នៅ​ក្នុង​ម៉ាស៊ីន​និម្មិត ​ដែល​ប្រើ​កម្មវិធី​បញ្ជា​ដែល​បានបញ្ជាក់​ទាក់ទង​ទៅ​កាត​អូឌីយ៉ូ​របស់​ម៉ាស៊ីន ។</translation>
    </message>
    <message>
      <source>Enable &amp;Audio</source>
      <translation>បើក​អូឌីយ៉ូ</translation>
    </message>
    <message>
      <source>Host Audio &amp;Driver:</source>
      <translation>កម្មវិធី​បញ្ជា​អូឌីយ៉ូ​របស់​ម៉ាស៊ីន ៖</translation>
    </message>
    <message>
      <source>Controls the audio output driver. The &lt;b>Null Audio Driver&lt;/b> makes the guest see an audio card, however every access to it will be ignored.</source>
      <translation>ត្រួតពិនិត្យ​កម្មវិធី​បញ្ជា​លទ្ធផល​អូឌីយ៉ូ ។ &lt;b>គ្មាន​កម្មវិធី​បញ្ជា​អូឌីយ៉ូ&lt;/b> ធ្វើ​ឲ្យ​ម៉ាស៊ីន​ភ្ញៀវ​ឃើញ​កាត​អូឌីយ៉ូ ទោះ​ជា​យ៉ាង​ណា​រាល់​ពេល​ចូលដំណើរការ​វា​នឹង​ត្រូ​វបាន​មិនអើពើ ។</translation>
    </message>
    <message>
      <source>Audio &amp;Controller:</source>
      <translation>វត្ថុ​បញ្ជា​អូឌីយ៉ូ ៖</translation>
    </message>
    <message>
      <source>Selects the type of the virtual sound card. Depending on this value, VirtualBox will provide different audio hardware to the virtual machine.</source>
      <translation>ជ្រើស​ប្រភេទ​កាត​សំឡេង​និម្មិត ។ ដោយ​អាស្រ័យ​លើ​តម្លៃនេះ VirtualBox នឹ​ងផ្ដល់​នូវ​ផ្នែក​រឹង​អូឌីយ៉ូ​ផ្សេងៗ​គ្នា​ដល់​ម៉ាស៊ីន​និម្មិត ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsDisplay</name>
    <message>
      <source>you have assigned less than &lt;b>%1&lt;/b> of video memory which is the minimum amount required to switch the virtual machine to fullscreen or seamless mode.</source>
      <translation>អ្នកបានផ្ដល់​សតិវីដេអូតិច​ជាង &lt;b>%1&lt;/b> ដែល​ជា​ចំនួន​អប្បបរមាដែលបានទាមទារ​ដើម្បី​ប្ដូរ​ម៉ាស៊ីន​និម្មិត​ទៅរបៀប​​ពេញអេក្រង់ ឬ​ពង្រីក ។</translation>
    </message>
    <message>
      <source>you have assigned less than &lt;b>%1&lt;/b> of video memory which is the minimum amount required for HD Video to be played efficiently.</source>
      <translation>អ្នក​បានផ្ដល់​សតិ​វីដេអូតិច​ជាង &lt;b>%1&lt;/b> ដែលជា​ចំនួន​អប្បបរមា​សម្រាប់​វីដេអូ HD ដើម្បីចាក់​ឲ្យ​មាន​ប្រសិទ្ធភាព ។</translation>
    </message>
    <message>
      <source>&lt;qt>%1&amp;nbsp;MB&lt;/qt></source>
      <translation>&lt;qt>%1&amp;nbsp;មេកាបៃ&lt;/qt></translation>
    </message>
    <message>
      <source>&amp;Video</source>
      <translation>វីដេអូ​</translation>
    </message>
    <message>
      <source>Video &amp;Memory:</source>
      <translation>សតិ​វីដេអូ​ ៖</translation>
    </message>
    <message>
      <source>Controls the amount of video memory provided to the virtual machine.</source>
      <translation>ត្រួតពិនិត្យ​ចំនួន​សតិ​វីដេ​អូ​​ដែល​បានផ្ដល់​ឲ្យ​​ម៉ាស៊ីន​និម្មិត ។</translation>
    </message>
    <message>
      <source>MB</source>
      <translation>មេកាបៃ</translation>
    </message>
    <message>
      <source>Extended Features:</source>
      <translation>លក្ខណសម្បត្តិ​ដែល​បានពង្រីក ៖</translation>
    </message>
    <message>
      <source>When checked, the virtual machine will be given access to the 3D graphics capabilities available on the host.</source>
      <translation>នៅពេល​បាន​គូស​ធីក ម៉ាស៊ីន​និម្មិត​នឹង​ចូលដំណើរការ​សមត្ថភាព​ក្រាហ្វិក​ត្រីមាត្រ​ដែល​​អាច​រក​បាន​​នៅ​លើ​ម៉ាស៊ីន ។</translation>
    </message>
    <message>
      <source>Enable &amp;3D Acceleration</source>
      <translation>បើក​ការបង្កើន​ល្បឿន​ត្រីមាត្រ​</translation>
    </message>
    <message>
      <source>When checked, the virtual machine will be given access to the Video Acceleration capabilities available on the host.</source>
      <translation>នៅ​ពេល​ដែល​បាន​គូស​ធីក​ ម៉ាស៊ីន​​និម្មិត​​នឹង​ចូល​ដំណើរ​ការ​សមត្ថ​ភាព​​បង្កើនល្បឿន​វីដេអូ​ដែល​អាច​ប្រើ​​បាន​​លើម៉ាស៊ីន​ ។</translation>
    </message>
    <message>
      <source>Enable &amp;2D Video Acceleration</source>
      <translation>បើក​ការ​បង្កើន​ល្បឿន​ទ្វេមាត្រ</translation>
    </message>
    <message>
      <source>&amp;Remote Display</source>
      <translation>កា​របង្ហាញ​ពី​ចម្ងាយ​</translation>
    </message>
    <message>
      <source>When checked, the VM will act as a Remote Desktop Protocol (RDP) server, allowing remote clients to connect and operate the VM (when it is running) using a standard RDP client.</source>
      <translation>នៅពេល​បាន​គូស​ធីក VM នឹង​ដើរតួ​ជា​ម៉ាស៊ីន​បម្រើ​ពិធីការ​ផ្ទៃតុ​ពី​ចម្ងាយ (RDP) ដោយ​អនុញ្ញាត​ឲ្យ​ម៉ាស៊ីនភ្ញៀវ​ពី​ចម្ងាយ​តភ្ជាប់​ និង​ប្រតិបត្តិ​ VM (នៅ​ពេល​កំពុង​រត់) ដោយ​ប្រើ​ម៉ាស៊ីន​ភ្ញៀវ RDP ស្តង់ដារ ។</translation>
    </message>
    <message>
      <source>&amp;Enable Server</source>
      <translation>បើក​ម៉ាស៊ីនបម្រើ​</translation>
    </message>
    <message>
      <source>Server &amp;Port:</source>
      <translation>ច្រក​ម៉ាស៊ីនបម្រើ ៖</translation>
    </message>
    <message>
      <source>The VRDP Server port number. You may specify &lt;tt>0&lt;/tt> (zero), to select port 3389, the standard port for RDP.</source>
      <translation>លេខ​​ច្រក​ម៉ាស៊ីន​​បម្រើ​ VRDP ។ អ្នក​អាច​បញ្ជាក់​​ &lt;tt>0&lt;/tt> (សូន្យ​) ដើម្បី​ជ្រើស​ច្រក​ ៣៣៨៩ ច្រក​ស្តង់​ដារ​សម្រាប់​ RDP ។</translation>
    </message>
    <message>
      <source>Authentication &amp;Method:</source>
      <translation>វិធីសាស្ត្រ​ផ្ទៀងផ្ទាត់​ភាព​ត្រឹមត្រូវ ៖</translation>
    </message>
    <message>
      <source>Defines the VRDP authentication method.</source>
      <translation>កំណត់​វិធីសាស្ត្រ​ផ្ទៀងផ្ទាត់​ភាព​ត្រឹមត្រូវ​របស់ VRDP ។</translation>
    </message>
    <message>
      <source>Authentication &amp;Timeout:</source>
      <translation>អស់ពេល​ក្នុងការ​​​ផ្ទៀងផ្ទាត់​ភាព​ត្រឹមត្រូវ ៖</translation>
    </message>
    <message>
      <source>Specifies the timeout for guest authentication, in milliseconds.</source>
      <translation>បញ្ជាក់​ការ​អស់ពេល ​សម្រាប់​ការ​ផ្ទៀងផ្ទាត់​ភាព​ត្រឹមត្រូវ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​ គិត​​ជា​មិល្លី​វិនាទី ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsDlg</name>
    <message>
      <source>General</source>
      <translation>ទូទៅ</translation>
    </message>
    <message>
      <source>Storage</source>
      <translation>កា​រផ្ទុក</translation>
    </message>
    <message>
      <source>Hard Disks</source>
      <translation>ថាសរឹង</translation>
    </message>
    <message>
      <source>CD/DVD-ROM</source>
      <translation>ស៊ីឌី/ឌីវីឌី​រ៉ូម</translation>
    </message>
    <message>
      <source>Floppy</source>
      <translation>ថាសទន់</translation>
    </message>
    <message>
      <source>Audio</source>
      <translation>អូឌីយ៉ូ</translation>
    </message>
    <message>
      <source>Network</source>
      <translation>បណ្ដាញ</translation>
    </message>
    <message>
      <source>Ports</source>
      <translation>ច្រក</translation>
    </message>
    <message>
      <source>Serial Ports</source>
      <translation>ច្រកស៊េរៀល</translation>
    </message>
    <message>
      <source>Parallel Ports</source>
      <translation>ច្រក​ស្រប</translation>
    </message>
    <message>
      <source>USB</source>
      <translation>USB</translation>
    </message>
    <message>
      <source>Shared Folders</source>
      <translation>ថត​ដែល​បានចែករំលែក</translation>
    </message>
    <message>
      <source>%1 - %2</source>
      <translation>%1 - %2</translation>
    </message>
    <message>
      <source>System</source>
      <translation>ប្រព័ន្ធ</translation>
    </message>
    <message>
      <source>Display</source>
      <translation>បង្ហាញ​</translation>
    </message>
    <message>
      <source>you have selected a 64-bit guest OS type for this VM. As such guests require hardware virtualization (VT-x/AMD-V), this feature will be enabled automatically.</source>
      <translation>អ្នក​បាន​​ជ្រើស​ប្រ​ភេទ​ប្រ​ព័ន្ធ​ប្រ​តិបត្តិ​​ការ​ម៉ាស៊ីន​ភ្ញៀវ​ ៦៤​-​ប៊ីត​ សម្រាប់​​ VM នេះ​ ។ ជាក់​ស្តែង​ ដូច​ម៉ាស៊ីន​ភ្ញៀវ​ទាមទារ​ថាសរឹង​ (VT-x/AMD-V) លក្ខណៈ​ពិសេស​នេះ​នឹង​ត្រូវ​បាន​បើក​ដោយ​ស្វ័យ​ប្រ​វត្តិ​ ។</translation>
    </message>
    <message>
      <source>you have selected a 64-bit guest OS type for this VM. VirtualBox does not currently support more than one virtual CPU for 64-bit guests executed on 32-bit hosts.</source>
      <translation>អ្នក​​បាន​ជ្រើស​​ប្រ​ភេទ​ប្រ​ព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ​ ៦៤​-​ប៊ីត​ សម្រាប់​​ VM នេះ​ ។ បច្ចុប្បន្ន​ VirtualBox មិន​គាំ​ទ្រ​​ស៊ី​ភី​យូ​និម្មិត​ច្រើន​ជាង​មួយសម្រាប់​​ម៉ាស៊ីន​ភ្ញៀវ​ ៦៤​-​ប៊ីត​​ដែល​បាន​ប្រតិបត្តិ​ការ​លើ​ម៉ាស៊ីន​​ ៣២​-​ប៊ីត​ ។</translation>
    </message>
    <message>
      <source>you have 2D Video Acceleration enabled. As 2D Video Acceleration is supported for Windows guests only, this feature will be disabled.</source>
      <translation>អ្នកបាន​បើក​បង្កើនល្បឿន​​វី​ដេអូ​​ទ្វេ​​មាត្រ​ ។ បង្កើនល្បឿន​វីដេអូ​​ទ្វេ​មាត្រ​​ត្រូវ​បាន​គាំ​ទ្រ​សម្រាប់​​​ម៉ាស៊ីន​វីនដូ​តែ​ប៉ុ​ណ្ណោះ​ លក្ខណៈ​ពិសេសនេះ​​នឹង​​មិន​ត្រូវ​បាន​អនុញ្ញាត​ ។​</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsGeneral</name>
    <message>
      <source>Displays the path where snapshots of this virtual machine will be stored. Be aware that snapshots can take quite a lot of disk space.</source>
      <translation>បង្ហាញ​ផ្លូវ​ដែល​រូបថត​របស់ម៉ាស៊ីន​និម្មិត​នេះ​នឹង​ត្រូ​វបានផ្ទុក ។ ចំណាំ​ថា រូបថត​អាចចាប់យក​​ទំហំ​ថាស​ជា​ច្រើន​​យ៉ាង​ស្ងៀវស្ងាត់​ ។</translation>
    </message>
    <message>
      <source>&amp;Basic</source>
      <translation>មូលដ្ឋាន</translation>
    </message>
    <message>
      <source>&amp;Name:</source>
      <translation>ឈ្មោះ ៖</translation>
    </message>
    <message>
      <source>Displays the name of the virtual machine.</source>
      <translation>បង្ហាញ​ឈ្មោះ​របស់​ម៉ាស៊ីន​និម្មិត ។</translation>
    </message>
    <message>
      <source>&amp;Advanced</source>
      <translation>កម្រិតខ្ពស់</translation>
    </message>
    <message>
      <source>&amp;Shared Clipboard:</source>
      <translation>ក្ដារតម្បៀតខ្ទាស់​ដែល​បាន​ចែករំលែក ៖</translation>
    </message>
    <message>
      <source>Selects which clipboard data will be copied between the guest and the host OS. This feature requires Guest Additions to be installed in the guest OS.</source>
      <translation>កំណត់​របៀប​ចែករំលែក​ក្ដារតម្បៀតខ្ទាស់​រវាង​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ និង​របស់ម៉ាស៊ីន ។ ចំណាំ​ថា លក្ខណៈពិសេស​នេះ​តម្រូវ​ឲ្យ​ដំឡើង​ផ្នែក​បន្ថែម​ម៉ាស៊ីន​ភ្ញៀវ​ក្នុង​ប្រព័ន្ធប្រតិបត្តិការ​របស់​ម៉ាស៊ីន ។</translation>
    </message>
    <message>
      <source>S&amp;napshot Folder:</source>
      <translation>ថត​រូបថត ៖</translation>
    </message>
    <message>
      <source>&amp;Description</source>
      <translation>សេចក្ដី​ពិពណ៌នា</translation>
    </message>
    <message>
      <source>Displays the description of the virtual machine. The description field is useful for commenting on configuration details of the installed guest OS.</source>
      <translation>បង្ហាញ​សេចក្ដីពិពណ៌នា​របស់ម៉ាស៊ីន​និម្មិត ។ វាល​សេចក្ដី​ពិពន៌នា​មានប្រយោជន៍​សម្រាប់​ផ្ដល់​យោបល់​អំពី​សេចក្ដី​លម្អិត​កំណត់​រចនាសម្ព័ន្ធ​របស់​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ​ដែល​បាន​ដំឡើង ។</translation>
    </message>
    <message>
      <source>If checked, any change to mounted CD/DVD or Floppy media performed during machine execution will be saved in the settings file in order to preserve the configuration of mounted media between runs.</source>
      <translation>ប្រសិនបើបាន​គូស​ធីក ការ​ផ្លាស់ប្ដូរ​ទៅ​មេឌៀ​ថាសទន់ ឬ​ស៊ីឌី/ឌីវីឌី​ដែល​បានម៉ោន​ បាន អនុវត្ត​កំឡុង​ពេល​​ប្រតិបត្តិការ​ម៉ាស៊ីន នឹង​ត្រូវ​បាន​រក្សាទុក​ក្នុង​ឯកសារ​កំណត់​ ដើម្បី​បម្រុង​ទុក​នូវ​ការ​កំណត់​រចនាសម្ព័ន្ធ​របស់​មេឌៀ​ដែល​បានម៉ោន​​ចន្លោះ​ពេល​រត់ ។</translation>
    </message>
    <message>
      <source>Removable Media:</source>
      <translation>មេឌៀ​ចល័ត ៖</translation>
    </message>
    <message>
      <source>&amp;Remember Runtime Changes</source>
      <translation>ចង់​ចាំ​ការផ្លាស់​ប្តូរ​ពេល​ដំណើរ​ការ​</translation>
    </message>
    <message>
      <source>Mini ToolBar:</source>
      <translation>របារ​ឧបករណ៍​តូច​ ៖</translation>
    </message>
    <message>
      <source>If checked, show the Mini ToolBar in Fullscreen and Seamless modes.</source>
      <translation>ប្រ​សិន​បើ​ធីក​ បង្ហាញ​របារ​ឧបករណ៍​តូច​ក្នុងរបៀប​​អេ​​ក្រង់​ពេញ​ និង​​​ព​ង្រីក​ ។</translation>
    </message>
    <message>
      <source>Show In &amp;Fullscreen/Seamless</source>
      <translation>​បង្ហាញ​អេ​ក្រង់​ពេញ​​/​ពង្រីក​</translation>
    </message>
    <message>
      <source>If checked, show the Mini ToolBar at the top of the screen, rather than in its default position at the bottom of the screen.</source>
      <translation>ប្រ​សិន​បើ​ធីក​ បង្ហាញ​របារឧបករណ៍​តូច​​នៅ​ផ្នែកខាង​លើ​​អេក្រង់​ ជា​ជាង​ក្នុង​ទី​តាំង​លំនាំ​ដើម​នៃ​ប៊ូតុង​របស់​អេក្រង់​ ។</translation>
    </message>
    <message>
      <source>Show At &amp;Top Of Screen</source>
      <translation>បង្ហាញ​កំពូល​​អេក្រង់​</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsHD</name>
    <message>
      <source>If checked, shows the differencing hard disks that are attached to slots rather than their base hard disks (shown for indirect attachments) and allows explicit attaching of differencing hard disks. Check this only if you need a complex hard disk setup.</source>
      <translation>ប្រសិនបើ​បានគូសធីគ វា​នឹង​បង្ហាញ​ថាសរឹង​ផ្សេងៗ​ដែល​បានភ្ជាប់​ទៅ​រន្ធ​ជំនួស​ឲ្យ​ថាសរឹង​មូលដ្ឋាន​​របស់​ពួកវា (បង្ហាញតែ​ក្នុងករណី​ដែល​មានការ​ភ្ជាប់​ដោយ​ប្រយោល​ប៉ុណ្ណោះ) ហើយ​នឹង​អនុញ្ញាត​ឲ្យ​ភ្ជាប់​ថាសរឹង​ផ្សេងៗ​ផងដែរ ។ គូស​ធីក​តែ​ក្នុងករណី​ដែល​អ្នក​ត្រូវការ​ការ​រៀបចំ​ថាសរឹង​ស្មុគស្មាញ​ប៉ុណ្ណោះ ។</translation>
    </message>
    <message>
      <source>&lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr>&lt;br>&lt;nobr>Bus:&amp;nbsp;&amp;nbsp;%2&lt;/nobr>&lt;br>&lt;nobr>Type:&amp;nbsp;&amp;nbsp;%3&lt;/nobr></source>
      <translation>&lt;nobr>&lt;b>%1&lt;/b>&lt;/nobr>&lt;br>&lt;nobr>​ខ្សែ​បញ្ជួន​ ៖ &amp;nbsp;&amp;nbsp;%2&lt;/nobr>&lt;br>&lt;nobr>ប្រភេទ​ ៖&amp;nbsp;&amp;nbsp;%3&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Expand/Collapse&amp;nbsp;Item&lt;/nobr></source>
      <translation>&lt;nobr>ពង្រីក​​/​វេញ&amp;nbsp; ធាតុ​&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Add&amp;nbsp;Hard&amp;nbsp;Disk&lt;/nobr></source>
      <translation>&lt;nobr>បន្ថែម&amp;nbsp;ថាសរឹង&amp;nbsp;&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Add&amp;nbsp;CD/DVD&amp;nbsp;Device&lt;/nobr></source>
      <translation>&lt;nobr>បន្ថែម​ឧបករណ៍&amp;nbsp;ស៊ីឌី/ឌីវឌី&amp;nbsp;&lt;/nobr></translation>
    </message>
    <message>
      <source>&lt;nobr>Add&amp;nbsp;Floppy&amp;nbsp;Device&lt;/nobr></source>
      <translation>&lt;nobr>បន្ថែម​ឧបករណ៍&amp;nbsp;ថាសទន់&amp;nbsp;&lt;/nobr></translation>
    </message>
    <message>
      <source>No hard disk is selected for &lt;i>%1&lt;/i>.</source>
      <translation>មិន​មាន​ថាស​រឹង​​ត្រូវ​បាន​ជ្រើស​​សម្រាប់&lt;i>%1&lt;/i> នោះ​ទេ ។</translation>
    </message>
    <message>
      <source>&lt;i>%1&lt;/i> uses a medium that is already attached to &lt;i>%2&lt;/i>.</source>
      <translation>&lt;i>%1&lt;/i>ប្រើខ្នាត​​មធ្យម​ដែល​ភ្ជាប់​ទៅ​កាន់&lt;i>%2&lt;/i>រួច​ហើយ​​​ ។</translation>
    </message>
    <message>
      <source>Add Controller</source>
      <translation>បន្ថែម​ឧបករណ៍​បញ្ជា​</translation>
    </message>
    <message>
      <source>Add IDE Controller</source>
      <translation>បន្ថែម​វត្ថុ​បញ្ជា IDE</translation>
    </message>
    <message>
      <source>Add SATA Controller</source>
      <translation>បន្ថែម​វត្ថុ​បញ្ជា SATA</translation>
    </message>
    <message>
      <source>Add SCSI Controller</source>
      <translation>បន្ថែម​វត្ថុ​បញ្ជា​ SCSI</translation>
    </message>
    <message>
      <source>Add Floppy Controller</source>
      <translation>បន្ថែម​ឧបករណ៍​បញ្ជា​ថាសទន់​</translation>
    </message>
    <message>
      <source>Remove Controller</source>
      <translation>យក​វត្ថុ​បញ្ជា​ចេញ​</translation>
    </message>
    <message>
      <source>Add Attachment</source>
      <translation>បន្ថែម​ឯកសារ​ភ្ជាប់​</translation>
    </message>
    <message>
      <source>Add Hard Disk</source>
      <translation>បន្ថែម​ថាស​រឹង​</translation>
    </message>
    <message>
      <source>Add CD/DVD Device</source>
      <translation>បន្ថែម​ឧបករណ៍​ស៊ីឌី/ឌីវីឌី</translation>
    </message>
    <message>
      <source>Add Floppy Device</source>
      <translation>បន្ថែម​ឧបករណ៍​ថាស​ទន់​</translation>
    </message>
    <message>
      <source>Remove Attachment</source>
      <translation>យក​ឯកសារ​ភ្ជាប់​ចេញ​</translation>
    </message>
    <message>
      <source>Adds a new controller to the end of the Storage Tree.</source>
      <translation>បន្ថែម​វត្ថុ​បញ្ជា​ថ្មី​ទៅ​ចុង​មែកធាង​ផ្ទុក ។</translation>
    </message>
    <message>
      <source>Removes the controller highlighted in the Storage Tree.</source>
      <translation>យក​ឧបករណ៍​បញ្ជា​ដែលបានបន្លិច​ចេញ​នៅ​ក្នុង​មែកធាង​ផ្ទុក ។</translation>
    </message>
    <message>
      <source>Adds a new attachment to the Storage Tree using currently selected controller as parent.</source>
      <translation>បន្ថែម​ឯកសារ​ភ្ជាប់ថ្មី​ទៅ​​មែក​ធាង​ផ្ទុក ដោយ​ប្រើ​​កម្មវិធីបញ្ជាដែលបានជ្រើស​ជា​មេ​ ។</translation>
    </message>
    <message>
      <source>Removes the attachment highlighted in the Storage Tree.</source>
      <translation>យក​ឯកសារ​ភ្ជាប់​ដែល​បាន​បន្លិច​ចេញ​នៅ​ក្នុង​មែកធាង​ផ្ទុក ។</translation>
    </message>
    <message>
      <source>IDE Controller</source>
      <translation>ឧបករណ៍​បញ្ជា​ IDE</translation>
    </message>
    <message>
      <source>SATA Controller</source>
      <translation>ឧបករណ៍​បញ្ជា​ SATA</translation>
    </message>
    <message>
      <source>SCSI Controller</source>
      <translation>ឧបករណ៍​​​បញ្ជា​ SCSI</translation>
    </message>
    <message>
      <source>Floppy Controller</source>
      <translation>ឧបករណ៍​បញ្ជា​ថាស​ទន់​</translation>
    </message>
    <message>
      <source>Hard &amp;Disk:</source>
      <translation>ថាស​រឹង​ ៖</translation>
    </message>
    <message>
      <source>&amp;CD/DVD Device:</source>
      <translation>ឧបករណ៍​​ស៊ី​ឌី​​/ឌីវីឌី ៖</translation>
    </message>
    <message>
      <source>&amp;Floppy Device:</source>
      <translation>ឧបករណ៍ថាសទន់ ៖</translation>
    </message>
    <message>
      <source>&amp;Storage Tree</source>
      <translation>មែក​ធាង​ផ្ទុក​</translation>
    </message>
    <message>
      <source>Contains all storage controllers for this machine and the virtual images and host drives attached to them.</source>
      <translation>មាន​ឧបករណ៍​បញ្ជា​ផ្ទុក​ទាំង​អស់​សម្រាប់​ម៉ាស៊ី​ននេះ​ និង​រូប​ភាព​និម្មិត​ និងដ្រាយ​​ម៉ាស៊ីន​ដែល​បាន​​ភ្ជាប់​​ទៅកាន់​ពួកវា ។</translation>
    </message>
    <message>
      <source>Information</source>
      <translation>​ព័ត៌​មាន​</translation>
    </message>
    <message>
      <source>The Storage Tree can contain several controllers of different types. This machine currently has no controllers.</source>
      <translation>មែក​ធាង​ផ្ទុក​អាច​មាន​ឧបករណ៍​បញ្ជា​​​បី​ បួន​​​​ប្រ​ភេទ​ខុសៗ​គ្នា​ ។ ម៉ាស៊ីន​បច្ចុប្បន្ន​នេះ មិន​មាន​ឧបករណ៍​បញ្ជា​ឡើយ​ ។</translation>
    </message>
    <message>
      <source>Attributes</source>
      <translation>គុណ​លក្ខណៈ</translation>
    </message>
    <message>
      <source>&amp;Name:</source>
      <translation>ឈ្មោះ ៖</translation>
    </message>
    <message>
      <source>Changes the name of the storage controller currently selected in the Storage Tree.</source>
      <translation>ប្តូរ​ឈ្មោះ​របស់​ឧបករណ៍​បញ្ជា​ផ្ទុក ​បច្ចុប្បន្ន​ដែល​បានជ្រើស​ក្នុ​ងមែក​ធាង​ផ្ទុក ។</translation>
    </message>
    <message>
      <source>&amp;Type:</source>
      <translation>ប្រភេទ ៖</translation>
    </message>
    <message>
      <source>Selects the sub-type of the storage controller currently selected in the Storage Tree.</source>
      <translation>​ជ្រើស​ប្រ​ភេទ​រង​​របស់​ឧបករណ៍​បញ្ជា​ផ្ទុក​បច្ចុប្បន្ន​ដែល​បាន​ជ្រើស​ក្នុ​ង​មែក​ធាង​ផ្ទុក ។</translation>
    </message>
    <message>
      <source>S&amp;lot:</source>
      <translation>រន្ធ ៖</translation>
    </message>
    <message>
      <source>Selects the slot on the storage controller used by this attachment. The available slots depend on the type of the controller and other attachments on it.</source>
      <translation>ជ្រើស​រន្ធ​លើ​ឧបករណ៍​បញ្ជា​ផ្ទុក ​ដែល​បាន​ប្រើ​ដោយ​ឯកសារ​ភ្ជាប់​នេះ​ ។​ ​រន្ធ​ដែលអាចរកបានអាស្រ័យ​​លើ​ប្រ​ភេទ​របស់​ឧបករណ៍បញ្ជា​ និង​ឯកសារ​ភ្ជាប់​​​​លើ​វាផ្សេង​ទៀត​​ ។</translation>
    </message>
    <message>
      <source>Selects the virtual disk image or the host drive used by this attachment.</source>
      <translation>ជ្រើស​រូប​ភាព​ថាស​​និម្មិត​ ឬ​ដ្រាយ​ម៉ាស៊ីន​ដែល​បាន​ប្រើ ​ដោយ​ឯក​សារ​ភ្ជាប់​នេះ​ ។</translation>
    </message>
    <message>
      <source>Opens the Virtual Media Manager to select a virtual image for this attachment.</source>
      <translation>បើក​កម្មវិធី​គ្រប់គ្រង​មេឌៀ​និម្មិត ដើម្បី​ជ្រើស​រូបភាព​​និម្មិត​សម្រាប់​ឯកសារ​ភ្ជាប់​នេះ ។</translation>
    </message>
    <message>
      <source>Open Virtual Media Manager</source>
      <translation>បើក​កម្មវិធី​គ្រប់គ្រង​មេឌៀ​និម្មិត​</translation>
    </message>
    <message>
      <source>D&amp;ifferencing Disks</source>
      <translation>ថាសផ្សេងៗ​គ្នា​</translation>
    </message>
    <message>
      <source>When checked, allows the guest to send ATAPI commands directly to the host-drive which makes it possible to use CD/DVD writers connected to the host inside the VM. Note that writing audio CD inside the VM is not yet supported.</source>
      <translation>នៅ​ពេល​ដែល​បាន​ធីក​ អនុញ្ញាត​ម៉ាស៊ីន​ភ្ញៀវ​ផ្ញើពាក្យ​បញ្ជា​​ ATAPI ដោយ​ផ្ទាល់​​ទៅ​ដ្រាយ​ម៉ាស៊ីន ដែល​​​ធ្វើ​ឲ្យ​វា​អាច​ប្រើ​​ឧបករណ៍​​​សរសេរ​ស៊ី​ឌី​​/​ឌី​វី​ឌី​ ​ដែល​បាន​តភ្ជាប់​ទៅ​ម៉ាស៊ីន​នៅ​​ក្នុង​ VM ។ ចំណាំថា ​ការ​សរសេរស៊ីឌីអូឌីយ៉ូ​ខាង​ក្នុង​ VM មិន​ត្រូវ​បាន​គាំ​ទ្រ​នៅ​ឡើយ​ទេ​ ។</translation>
    </message>
    <message>
      <source>&amp;Passthrough</source>
      <translation>ហុចតាម​</translation>
    </message>
    <message>
      <source>Virtual Size:</source>
      <translation>ទំហំនិម្មិត​ ៖</translation>
    </message>
    <message>
      <source>Actual Size:</source>
      <translation>ទំហំ​ពិត​ ៖</translation>
    </message>
    <message>
      <source>Size:</source>
      <translation>ទំហំ​ ៖</translation>
    </message>
    <message>
      <source>Location:</source>
      <translation>ទី​តាំង​ ៖</translation>
    </message>
    <message>
      <source>Type (Format):</source>
      <translation>ប្រ​ភេទ​​ (ទ្រង់​ទ្រាយ​) ៖</translation>
    </message>
    <message>
      <source>Attached To:</source>
      <translation>បាន​​ភ្ជាប់ទៅកាន់ ៖</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsNetwork</name>
    <message>
      <source>When checked, plugs this virtual network adapter into the virtual machine.</source>
      <translation>នៅពេល​គូស​ធីក ដោត​អាដាប់ទ័រ​បណ្ដាញ​និម្មិត​ទៅ​ក្នុងម៉ាស៊ីន​នកម្មិត ។</translation>
    </message>
    <message>
      <source>&amp;Enable Network Adapter</source>
      <translation>បើក​អាដាប់ទ័រ​បណ្ដាញ</translation>
    </message>
    <message>
      <source>Selects the type of the virtual network adapter. Depending on this value, VirtualBox will provide different network hardware to the virtual machine.</source>
      <translation>ជ្រើស​ប្រភេទ​អាដាប់ទ័រ​បណ្ដាញ​និម្មិត ។ អាស្រ័យ​លើ​តម្លៃ​នេះ VirtualBox នឹង​ផ្ដល់​នូវ​ផ្នែករឹង​​បណ្ដាញ​ផ្សេង​ទៅ​ឲ្យ​ម៉ាស៊ីន​និម្មិត ។</translation>
    </message>
    <message>
      <source>&amp;Attached to:</source>
      <translation>បាន​ភ្ជាប់​ទៅ ៖</translation>
    </message>
    <message>
      <source>Displays the MAC address of this adapter. It contains exactly 12 characters chosen from {0-9,A-F}. Note that the second character must be an even digit.</source>
      <translation>បង្ហាញ​អាសយដ្ឋាន MAC របស់​អាដាប់ទ័រ​នេះ ។ វា​មាន​តួអក្សរ​យ៉ាង​ជាក់លាក់​ចំនួន​ ១២ ដែល​បាន​ជ្រើស​ពី {0-9,A-F} ។ ចំណាំ​ថា​តួអក្សរ​ទីពីរ​ត្រូវ​តែ​ជា​​តួលេខ​គូ ។</translation>
    </message>
    <message>
      <source>Generates a new random MAC address.</source>
      <translation>បង្កើត​អាសយដ្ឋាន MAC ចៃដន្យ​ថ្មី ។</translation>
    </message>
    <message>
      <source>Indicates whether the virtual network cable is plugged in on machine startup or not.</source>
      <translation>បង្ហាញ​ថាតើ​ខ្សែ​បណ្ដាញ​និម្មិត​ត្រូ​វបានដោត​​នៅពេល​ម៉ាស៊ីន​ចាប់ផ្ដើម​ ឬ​ក៏អត់ ។</translation>
    </message>
    <message>
      <source>no bridged network adapter is selected</source>
      <translation>មិន​​​មាន​អាដាប់​ទ័រ​បណ្តាញប្រ៊ីដ្យ​ត្រូវ​បានជ្រើស​រើស​ទេ​</translation>
    </message>
    <message>
      <source>no internal network name is specified</source>
      <translation>មិនមាន​ឈ្មោះ​បណ្ដាញ​ខាង​ក្នុង​ត្រូវ​បានបញ្ជាក់ទេ</translation>
    </message>
    <message>
      <source>no host-only network adapter is selected</source>
      <translation>មិនមាន​អាដាប់ទំព័រ​ម៉ាស៊ីន​ត្រូវ​បាន​ជ្រើស​ទេ</translation>
    </message>
    <message>
      <source>Not selected</source>
      <comment>

network adapter name</comment>
      <translation>មិន​​បាន​ជ្រើស​ឡើយ​</translation>
    </message>
    <message>
      <source>Controls how this virtual adapter is attached to the real network of the Host OS.</source>
      <translation>ត្រួតពិនិត្យ​វិធី​ដែល​អាដាប់ទ័រ​និម្មិត​នេះ​ត្រូវ​បាន​ភ្ជាប់​ទៅ​បណ្ដាញ​ពិតប្រាកដ​​របស់​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន ។</translation>
    </message>
    <message>
      <source>&amp;Name:</source>
      <translation>ឈ្មោះ ៖</translation>
    </message>
    <message>
      <source>Selects the name of the network adapter for &lt;b>Bridged Adapter&lt;/b> or &lt;b>Host-only Adapter&lt;/b> attachments and the name of the network &lt;b>Internal Network&lt;/b> attachments.</source>
      <translation>ជ្រើស​ឈ្មោះ​អាដាប់ទ័រ​បណ្ដាញ​សម្រាប់ &lt;b>អាដាប់ទ័រ​ប្រ៊ីដ្យ&lt;/b> ឬ &lt;b>អាដាប់ទ័រម៉ាស៊ីន&lt;/b> ការ​ភ្ជាប់ និង​ឈ្មោះ​​របស់​បណ្ដាញ ការ​ភ្ជាប់&lt;b>បណ្ដាញ​ខាង​ក្នុង&lt;/b> ។</translation>
    </message>
    <message>
      <source>A&amp;dvanced</source>
      <translation>កម្រិត​ខ្ពស់​</translation>
    </message>
    <message>
      <source>Shows or hides additional network adapter options.</source>
      <translation>បង្ហាញ​ ឬ​លាក់​ជម្រើស​អាដាប់ទ័រ​បណ្ដាញ​បន្ថែម ។</translation>
    </message>
    <message>
      <source>Adapter &amp;Type:</source>
      <translation>ប្រភេទ​អាដាប់​ទ័រ​ ៖</translation>
    </message>
    <message>
      <source>&amp;Mac Address:</source>
      <translation>អាសយដ្ឋាន Mac ៖</translation>
    </message>
    <message>
      <source>&amp;Cable connected</source>
      <translation> បាន​តភ្ជាប់​ខ្សែ​</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsParallel</name>
    <message>
      <source>Port %1</source>
      <comment>parallel ports</comment>
      <translation>ច្រក %1</translation>
    </message>
    <message>
      <source>When checked, enables the given parallel port of the virtual machine.</source>
      <translation>នៅពេល​បាន​គូស​ធីក បើក​ច្រក​ប៉ារ៉ាឡែល​របស់​ម៉ាស៊ីន​និម្មិត ។</translation>
    </message>
    <message>
      <source>&amp;Enable Parallel Port</source>
      <translation>បើក​ច្រក​ស្រប</translation>
    </message>
    <message>
      <source>Port &amp;Number:</source>
      <translation>លេខ​ច្រក ៖</translation>
    </message>
    <message>
      <source>Displays the parallel port number. You can choose one of the standard parallel ports or select &lt;b>User-defined&lt;/b> and specify port parameters manually.</source>
      <translation>បង្ហាញលេចច្រក​ប៉ារ៉ាឡែល ។ អ្នកអាចជ្រើស​ច្រក​មួយ​ក្នុងចំណោម​ច្រក​ប៉ារ៉ាឡែល​ជា​ច្រើន ឬ​ជ្រើស &lt;b>កំណត់​ដោយ​អ្នក​ប្រើ&lt;/b> ហើយ​បញ្ញាក់​ប៉ារ៉ាម៉ែត្រ​ច្រក​ដោយ​ដៃ ។</translation>
    </message>
    <message>
      <source>&amp;IRQ:</source>
      <translation>IRQ ៖</translation>
    </message>
    <message>
      <source>Displays the IRQ number of this parallel port. This should be a whole number between &lt;tt>0&lt;/tt> and &lt;tt>255&lt;/tt>. Values greater than &lt;tt>15&lt;/tt> may only be used if the &lt;b>IO APIC&lt;/b> setting is enabled for this virtual machine.</source>
      <translation>បង្ហាញលេខ IRQ របស​ច្រក​ប៉ារ៉ាឡែល​នេះ ។ តម្លៃ​ត្រឹមត្រូវ​ជា​ចំនួន​គត់​ដែល​ស្ថិត​ក្នុងជួរពី &lt;tt>0&lt;/tt> ដល់ &lt;tt>២៥៥&lt;/tt> ។ តម្លៃ​ធំជាង &lt;tt>១៥&lt;/tt> អាច​ត្រូវ​បានប្រើ​តែ​ក្នុងករណី​ដែល &lt;b>IO APIC&lt;/b> ត្រូ​វបានបើក​សម្រាប់​ម៉ាស៊ីន​និម្មិត​នេះ​ប៉ុណ្ណោះ ។</translation>
    </message>
    <message>
      <source>I/O Po&amp;rt:</source>
      <translation>ច្រក I/O ៖</translation>
    </message>
    <message>
      <source>Displays the base I/O port address of this parallel port. Valid values are integer numbers in range from &lt;tt>0&lt;/tt> to &lt;tt>0xFFFF&lt;/tt>.</source>
      <translation>បង្ហាញអាសយដ្ឋាន​ច្រក​ I/O មូលដ្ឋាន ។ តម្លៃដែល​ត្រឹមត្រូវ​គឺ​ធំជាង​ចំនួន​ដែល​ស្ថិត​ក្នុងជួរ​ពី &lt;tt>0&lt;/tt>ដល់&lt;tt>0xFFFF&lt;/tt> ។</translation>
    </message>
    <message>
      <source>Port &amp;Path:</source>
      <translation>ផ្លូវ​ច្រក ៖</translation>
    </message>
    <message>
      <source>Displays the host parallel device name.</source>
      <translation>បង្ហាញ​ឈ្មោះ​ឧបករណ៍​ស្រប​គ្នា​​របស់​ម៉ាស៊ីន ។</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsParallelPage</name>
    <message>
      <source>Duplicate port number selected </source>
      <translation>បាន​ជ្រើស​លេខ​ច្រក​ស្ទួន</translation>
    </message>
    <message>
      <source>Port path not specified </source>
      <translation>មិន​បាន​បញ្ជាក់ផ្លូវ​ច្រក​ទេ</translation>
    </message>
    <message>
      <source>Duplicate port path entered </source>
      <translation>បាន​បញ្ចូល​ផ្លូវ​ច្រក​ស្ទួន</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsSF</name>
    <message>
      <source>Adds a new shared folder definition.</source>
      <translation>បន្ថែម​កា​រកំណត់​ថត​ដែលបានចែករំលែកថ្មី ។</translation>
    </message>
    <message>
      <source>Edits the selected shared folder definition.</source>
      <translation>កែសម្រួល​កា​រកំណត់​ថត​ដែល​បានចែករំលែក​ដែល​បានជ្រើស ។</translation>
    </message>
    <message>
      <source>Removes the selected shared folder definition.</source>
      <translation>យក​កា​រកំណត់​ថត​ដែលបានចែករំលែក​ដែល​បានជ្រើ​សចេញ</translation>
    </message>
    <message>
      <source> Machine Folders</source>
      <translation> ថត​ម៉ាស៊ីន</translation>
    </message>
    <message>
      <source> Transient Folders</source>
      <translation> ថត​បណ្ដោះអាសន្ន</translation>
    </message>
    <message>
      <source>Full</source>
      <translation>ពេញលេញ</translation>
    </message>
    <message>
      <source>Read-only</source>
      <translation>បាន​តែ​អាន</translation>
    </message>
    <message>
      <source>Lists all shared folders accessible to this machine. Use 'net use x: \\vboxsvr\share' to access a shared folder named &lt;i>share&lt;/i> from a DOS-like OS, or 'mount -t vboxsf share mount_point' to access it from a Linux OS. This feature requires Guest Additions.</source>
      <translation>រាយ​ថត​ដែល​បានចែករំលែក​ទាំង​អស់​ដែល​អាច​ចូលដំណើរការ​បានទៅ​ម៉ាស៊ីន​នេះ ។ ប្រើ​ 'net ប្រើ x: \\vboxsvr\share' ដើម្បី​ចូលដំណើរការ​ថត​ដែល​បានចែករំលែក​ដែលមាន​ឈ្មោះ &lt;i>ចែករំលែក&lt;/i> ពីប្រព័ន្ធ​ប្រតិបត្តិការ​ដូចនឹង DOS ដែរ ឬ​ 'mount -t vboxsf share mount_point' ដើម្បីចូលដំណើរការ​វា​ពី​ប្រព័ន្ធ​ប្រតិបត្តិការ​លីនុច ។ លក្ខណៈពិសេស​នេះ​ត្រូវការ​ផ្នែក​បន្ថែម​របស់ម៉ាស៊ីន​ភ្ញៀវ ។</translation>
    </message>
    <message>
      <source>Name</source>
      <translation>ឈ្មោះ</translation>
    </message>
    <message>
      <source>Path</source>
      <translation>ផ្លូវ</translation>
    </message>
    <message>
      <source>Access</source>
      <translation>ចូលដំណើរការ</translation>
    </message>
    <message>
      <source> Global Folders</source>
      <translation> ថត​សកល​</translation>
    </message>
    <message>
      <source>&amp;Add Shared Folder</source>
      <translation>បន្ថែម​ថត​ដែល​បាន​ចែករំលែក​</translation>
    </message>
    <message>
      <source>&amp;Edit Shared Folder</source>
      <translation>កែសម្រួល​ថត​ដែលបានចែករំលែក​</translation>
    </message>
    <message>
      <source>&amp;Remove Shared Folder</source>
      <translation>យក​ថត​ដែលបានចែករំលែក​ចេញ​</translation>
    </message>
    <message>
      <source>&amp;Folders List</source>
      <translation>បញ្ជី​ថត​</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsSFDetails</name>
    <message>
      <source>Add Share</source>
      <translation>បន្ថែម​ការ​ចែករំលែក</translation>
    </message>
    <message>
      <source>Edit Share</source>
      <translation>កែសម្រួល​ការ​ចែករំលែក</translation>
    </message>
    <message>
      <source>Dialog</source>
      <translation>ប្រអប់</translation>
    </message>
    <message>
      <source>Folder Path:</source>
      <translation>ផ្លូវ​ថត ៖</translation>
    </message>
    <message>
      <source>Folder Name:</source>
      <translation>ឈ្មោះ​ថត ៖</translation>
    </message>
    <message>
      <source>Displays the name of the shared folder (as it will be seen by the guest OS).</source>
      <translation>បង្ហាញ​ឈ្មោះ​របស់​ថត​ដែលបានចែករំលែក (ព្រោះថា​វា​នឹង​ត្រូវ​បានឃើញ​ដោយ​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ) ។</translation>
    </message>
    <message>
      <source>When checked, the guest OS will not be able to write to the specified shared folder.</source>
      <translation>នៅពេល​បានគូសធីក ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​មិនអាច​សរសេរ​ទៅកាន​ថត​ដែល​បានចែករំលែក​​ដែល​បាន​បញ្ជាក់​ទេ ។</translation>
    </message>
    <message>
      <source>&amp;Read-only</source>
      <translation>បានតែ​អាន</translation>
    </message>
    <message>
      <source>&amp;Make Permanent</source>
      <translation>ធ្វើ​ឲ្យ​អចិន្ត្រៃយ៍</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsSerial</name>
    <message>
      <source>Port %1</source>
      <comment>serial ports</comment>
      <translation>ច្រក %1</translation>
    </message>
    <message>
      <source>When checked, enables the given serial port of the virtual machine.</source>
      <translation>នៅពេល​គូសធីក បើក​ច្រក​ស៊េរី​ដែល​បានផ្ដល់​របស់ម៉ាស៊ីននិម្មិត ។</translation>
    </message>
    <message>
      <source>&amp;Enable Serial Port</source>
      <translation>បើក​ច្រក​ស៊េរី</translation>
    </message>
    <message>
      <source>Port &amp;Number:</source>
      <translation>លេខ​ច្រក ៖</translation>
    </message>
    <message>
      <source>Displays the serial port number. You can choose one of the standard serial ports or select &lt;b>User-defined&lt;/b> and specify port parameters manually.</source>
      <translation>បង្ហាញលេខ​ច្រក​ស៊េរៀល ។ អ្នកអាច​ជ្រើ​ស​ច្រក​ស៊េរៀល​ស្តង់ដារ​មួយ​ក្នុងចំណោម​ច្រកជា​ច្រើន ឬ​ជ្រើស &lt;b>កំណត់​ដោយ​អ្នក​ប្រើ&lt;/b> ហើយ​បញ្ជាក់​ប៉ារ៉ាម៉ែត្រ​ច្រក​ដោយដៃ ។</translation>
    </message>
    <message>
      <source>&amp;IRQ:</source>
      <translation>IRQ ៖</translation>
    </message>
    <message>
      <source>Displays the IRQ number of this serial port. This should be a whole number between &lt;tt>0&lt;/tt> and &lt;tt>255&lt;/tt>. Values greater than &lt;tt>15&lt;/tt> may only be used if the &lt;b>IO APIC&lt;/b> setting is enabled for this virtual machine.</source>
      <translation>បង្ហាញ​លេខ IRQ របស់​ច្រក​ស៊េរៀល​នេះ ។ តម្លៃ​ដែល​ត្រឹមត្រូវ​គឺ​ជា​ចំនួន​គត់​ដែល​ស្ថិត​នៅ​ក្នុង​ចន្លោះ​ពី &lt;tt>0&lt;/tt> ដល់ &lt;tt>២៥៥&lt;/tt> ។ តម្លៃ​ធំជាង​ &lt;tt>១៥&lt;/tt> អាច​ត្រូ​វបានប្រើ​តែ​ក្នុងករណី​ដែល &lt;b>IO APIC&lt;/b> ត្រូវ​បានបើក​សម្រាប់​ម៉ាស៊ីន​និម្មិត ។</translation>
    </message>
    <message>
      <source>I/O Po&amp;rt:</source>
      <translation>ច្រក​ I/O ៖</translation>
    </message>
    <message>
      <source>Displays the base I/O port address of this serial port. Valid values are integer numbers in range from &lt;tt>0&lt;/tt> to &lt;tt>0xFFFF&lt;/tt>.</source>
      <translation>បង្ហាញ​អាសយដ្ឋាន​ច្រក I/O មូលដ្ឋាន​របស់​ច្រក​ស៊េរៀលនេះ ។ តម្លៃ​ដែល​ត្រឹមត្រូវ​គឺ​ជា​ចំនួន​ដែល​ស្ថិត​ក្នុង​ចន្លោះពី &lt;tt>0&lt;/tt> ដល់&lt;tt>0xFFFF&lt;/tt> ។</translation>
    </message>
    <message>
      <source>Port &amp;Mode:</source>
      <translation>របៀប​ច្រក ៖</translation>
    </message>
    <message>
      <source>Controls the working mode of this serial port. If you select &lt;b>Disconnected&lt;/b>, the guest OS will detect the serial port but will not be able to operate it.</source>
      <translation>ពិនិត្យ​មើល​របៀប​ដំណើរការ​របស់​ច្រក​ស៊េរៀល ។ ប្រសិនបើ​អ្នក​ជ្រើស​ &lt;b>បានផ្ដាច់&lt;/b> ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់​ម៉ាស៊ីន​ភ្ញៀវ​រកឃើញ​ច្រក​ស៊េរៀល​ ប៉ុន្តែ​នឹង​មិន​អាច​ប្រតិបត្តិ​វា​បានទេ ។</translation>
    </message>
    <message>
      <source>If checked, the pipe specified in the &lt;b>Port Path&lt;/b> field will be created by the virtual machine when it starts. Otherwise, the virtual machine will assume that the pipe exists and try to use it.</source>
      <translation>ប្រសិន​បើ​បានគូស​ធីក បំពង់​បាន​បញ្ជាក់​នៅ​ក្នុងវាល &lt;b>ផ្លូវ​ច្រក&lt;/b> នឹង​ត្រូវ​បាន​បង្កើត​ដោយ​​ម៉ាស៊ីន​និម្មិត នៅពេល​វា​ចាប់ផ្ដើម ។ បើ​មិន​ដូច្នេះ​ទេ ម៉ាស៊ីន​និម្មិត​នឹង​ព្យាយាម​ប្រើ​បំពង់​ដែល​បានស្រាប់ ។</translation>
    </message>
    <message>
      <source>&amp;Create Pipe</source>
      <translation>បង្កើត​បំពង់</translation>
    </message>
    <message>
      <source>Displays the path to the serial port's pipe on the host when the port is working in &lt;b>Host Pipe&lt;/b> mode, or the host serial device name when the port is working in &lt;b>Host Device&lt;/b> mode.</source>
      <translation>បង្ហាញ​ផ្លូវ​ទៅកាន់​ច្រក​ស៊េរៀល​នៅ​លើ​ម៉ាស៊ីន នៅពេល​ច្រក​ដំណើការ​ក្នុង​របៀប &lt;b>បំពង់​ម៉ាស៊ីន&lt;/b> ឬ​ឈ្មោះ​ឧបករណ៍​ស៊េរៀល​នៅពេល​ច្រក​ដំណើរការ​ក្នុង​របៀប &lt;b>ឧបករណ៍​ម៉ាស៊ីន&lt;/b> ។</translation>
    </message>
    <message>
      <source>Port/File &amp;Path:</source>
      <translation>ផ្លូវ​ ច្រក​​/ឯកសារ​ ៖</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsSerialPage</name>
    <message>
      <source>Duplicate port number selected </source>
      <translation>បានជ្រើស​លេខ​ច្រក​ស្ទួន</translation>
    </message>
    <message>
      <source>Port path not specified </source>
      <translation>មិនបានបញ្ជាក់​ផ្លូវ​ច្រក​ស្ទួន​ទេ</translation>
    </message>
    <message>
      <source>Duplicate port path entered </source>
      <translation>បាន​បញ្ចូល​ផ្លូវ​ច្រក​ស្ទួន</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsSystem</name>
    <message>
      <source>you have assigned more than &lt;b>%1%&lt;/b> of your computer's memory (&lt;b>%2&lt;/b>) to the virtual machine. Not enough memory is left for your host operating system. Please select a smaller amount.</source>
      <translation>អ្នកបាន​ផ្ដល់សតិ​កុំព្យូទ័ររបស់​អ្នក​ច្រើនជាង &lt;b>%1%&lt;/b> (&lt;b>%2&lt;/b>) ដល់​ម៉ាស៊ីន​និម្មិត ។ មិនមាន​សតិ​គ្រប់គ្រាន់​នៅ​សល់​សម្រាប់​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​របស់​អ្នក​ទេ ។ សូម​ជ្រើស​​ចំនួន​សតិ​តូច​ជាង​​នេះ ។</translation>
    </message>
    <message>
      <source>you have assigned more than &lt;b>%1%&lt;/b> of your computer's memory (&lt;b>%2&lt;/b>) to the virtual machine. There might not be enough memory left for your host operating system. Continue at your own risk.</source>
      <translation>អ្នក​បាន​ផ្ដល់​សតិ​កុំព្យូទ័រ​ច្រើន &lt;b>%1%&lt;/b> (&lt;b>%2&lt;/b>) ដល់​ម៉ាស៊ីន​និម្មិត ។ វា​ប្រហែលជា​មិន​គ្រប់គ្រាន់​​សម្រាប់​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​របស់​អ្នក​ទេ ។ បន្ត​ដោយ​មានគ្រោះថ្នាក់ ។</translation>
    </message>
    <message>
      <source>for performance reasons, the number of virtual CPUs attached to the virtual machine may not be more than twice the number of physical CPUs on the host (&lt;b>%1&lt;/b>). Please reduce the number of virtual CPUs.</source>
      <translation>សម្រាប់​អនុវត្ត ចំនួន​ស៊ីភីយូ​ដែល​បាន​ភ្ជាប់​ទៅ​ម៉ាស៊ីន​មិន​អាច​លើស​ពី​​ចំនួន​ពីរ​នៃ​ស៊ីភីយូ​ហ្វីស៊ីខល​នៅ​លើ​ម៉ាស៊ីន (&lt;b>%1&lt;/b>) ។ សូម​កាត់​បន្ថយ​ចំនួន​ស៊ីភីយូ​និម្មិត ។</translation>
    </message>
    <message>
      <source>you have assigned more virtual CPUs to the virtual machine than the number of physical CPUs on your host system (&lt;b>%1&lt;/b>). This is likely to degrade the performance of your virtual machine. Please consider reducing the number of virtual CPUs.</source>
      <translation>អ្នក​បានផ្ដល់​ស៊ីភីយូ​និម្មិត​ច្រើន​ដល់​ម៉ាស៊ីន​និម្មិត​ជាង​ចំនួន​ស៊ីភីយូ​ហ្វីស៊ីខល​នៅ​ក្នុង​ប្រព័ន្ធ​ម៉ាស៊ីន​របស់​អ្នក (&lt;b>%1&lt;/b>) ។ វា​ទំនង​ជា​បន្ថយ​ការ​អនុវត្ត​ម៉ាស៊ីននិម្មិត​របស់​អ្នក ។ សូម​ពិចារណា​លើការ​កាត់បន្ថយ​ចំនួន​ស៊ីភីយូ​និម្មិត ។</translation>
    </message>
    <message>
      <source>you have assigned more than one virtual CPU to this VM. This will not work unless the IO-APIC feature is also enabled. This will be done automatically when you accept the VM Settings by pressing the OK button.</source>
      <translation>អ្នកបានផ្ដល់​ស៊ីភីយូ​និម្មិត​ច្រើនជាង​ដល់ VM នេះ ។ វា​នឹង​មិន​ដំណើរការ​ទេ លុះត្រា​តែ​លក្ខណៈពិសេស​របស់ IO-APIC ត្រូវ​បានបើក ។ វា​នឹង​ត្រូវ​បាន​ធ្វើ​ដោយ​ស្វ័យ​ប្រវត្តិ នៅពេល​អ្នក​ទទួល​យក​ការ​កំណត់ VM ដោយ​ចុច​ប៊ូតុង​យល់ព្រម ។</translation>
    </message>
    <message>
      <source>you have assigned more than one virtual CPU to this VM. This will not work unless hardware virtualization (VT-x/AMD-V) is also enabled. This will be done automatically when you accept the VM Settings by pressing the OK button.</source>
      <translation>អ្នក​បានផ្ដល់​ស៊ីភីយូ​និម្មិត​ច្រើនជាង​មួយ​ដល់ VM ។ វា​នឹង​មិនដំណើរការ​ទេ លុះត្រា​តែ​ផ្នែក​រឹង​និម្មិត (VT-x/AMD-V) ត្រូវ​បានបើក​ដែរ ។ វា​នឹង​ត្រូវ​បានធ្វើ​ដោយ​ស្វ័យ​ប្រវត្តិ នៅពេល​អ្នក​ទទួល​យក​ការ​កំណត់ VM ដោយ​ចុច​ប៊ូតុងយល់ព្រម ។</translation>
    </message>
    <message>
      <source>&lt;qt>%1&amp;nbsp;MB&lt;/qt></source>
      <translation>&lt;qt>%1&amp;nbsp;មេកាបៃ&lt;/qt></translation>
    </message>
    <message>
      <source>&lt;qt>%1&amp;nbsp;CPU&lt;/qt></source>
      <comment>

%1 is 1 for now</comment>
      <translation>&lt;qt>%1&amp;nbsp; ស៊ី​ភី​យូ&lt;/qt></translation>
    </message>
    <message>
      <source>&lt;qt>%1&amp;nbsp;CPUs&lt;/qt></source>
      <comment>

%1 is host cpu count * 2 for now</comment>
      <translation>&lt;qt>%1&amp;nbsp; ស៊ី​ភី​យូ​&lt;/qt></translation>
    </message>
    <message>
      <source>&amp;Motherboard</source>
      <translation>បន្ទះ​មេ​</translation>
    </message>
    <message>
      <source>Base &amp;Memory:</source>
      <translation>សតិ​គោល​ ៖</translation>
    </message>
    <message>
      <source>Controls the amount of memory provided to the virtual machine. If you assign too much, the machine might not start.</source>
      <translation>ត្រួតពិនិត្យ​ចំនួន​សតិ​ដែលបានផ្ដល់​ទៅ​ម៉ាស៊ីន​និម្មិត ។ ប្រសិនបើ​អ្នក​ផ្ដល់​ច្រើនពេក ម៉ាស៊ីន​មិនអាច​ចាប់ផ្ដើម​បានទេ ។</translation>
    </message>
    <message>
      <source>MB</source>
      <translation>មេកាបៃ</translation>
    </message>
    <message>
      <source>&amp;Boot Order:</source>
      <translation>លំដាប់​ចាប់ផ្ដើម ៖</translation>
    </message>
    <message>
      <source>Defines the boot device order. Use the checkboxes on the left to enable or disable individual boot devices. Move items up and down to change the device order.</source>
      <translation>កំណត់​លំដាប់​ឧបករណ៍​ចាប់ផ្ដើម​ឡើង ។ ប្រើ​ប្រអប់​ធីក​នៅ​ខាង​ឆ្វេង ដើម្បី​បើក ឬ​បិទ​ឧបករណ៍​ចាប់ផ្ដើមឡើង ។ ផ្លាស់ទី​វា​ឡើង​លើ និង​ចុះក្រោម​ ដើម្បី​ផ្លាស់ប្ដូរ​លំដាប់​ឧបករណ៍ ។</translation>
    </message>
    <message>
      <source>Move Up (Ctrl-Up)</source>
      <translation>ផ្លាស់ទី​ឡើង​លើ (Ctrl-Up)</translation>
    </message>
    <message>
      <source>Moves the selected boot device up.</source>
      <translation>ផ្លាស់ទី​ឧបករណ៍​ចាប់ផ្ដើម​ដែលបាន​ជ្រើស​ឡើង​លើ ។</translation>
    </message>
    <message>
      <source>Move Down (Ctrl-Down)</source>
      <translation>ផ្លាស់ទី​ចុះក្រោម (Ctrl-Down)</translation>
    </message>
    <message>
      <source>Moves the selected boot device down.</source>
      <translation>ផ្លាស់ទី​ឧបករណ៍​ចាប់ផ្ដើម​ដែល​បាន​ជ្រើស​ចុះក្រោម</translation>
    </message>
    <message>
      <source>Extended Features:</source>
      <translation>លក្ខណៈ​ពិសេស​ដែល​បានពង្រីក ៖</translation>
    </message>
    <message>
      <source>When checked, the virtual machine will support the Input Output APIC (IO APIC), which may slightly decrease performance. &lt;b>Note:&lt;/b> don't disable this feature after having installed a Windows guest operating system!</source>
      <translation>នៅ​ពេល​បានគូស​ធីក ម៉ាស៊ីន​និម្មិត​នឹង​គាំទ្រ​ Input Output APIC (IO APIC) ដែលអាច​បន្ថយ​ការ​អនុវត្ត​បន្តិចបន្តួច ។ &lt;b>ចំណាំ ៖&lt;/b> កុំ​បិទ​លក្ខណៈពិសេស​នេះ បន្ទាប់​ពី​ដំឡើង​ប្រព័ន្ធ​ប្រតិបត្តិការ​ម៉ាស៊ីន​ភ្ញៀវ​វីនដូ !</translation>
    </message>
    <message>
      <source>Enable &amp;IO APIC</source>
      <translation>បើក IO APIC</translation>
    </message>
    <message>
      <source>When checked, the guest will support the Extended Firmware Interface (EFI), which is required to boot certain guest OSes. Non-EFI aware OSes will not be able to boot if this option is activated.</source>
      <translation>​នៅ​ពេល​​ដែលបាន​គូស​ធីក​ ម៉ាស៊ីន​ភ្ញៀវ​នឹង​គាំ​ទ្រ Extended Firmware Interface (EFI) ដែល​ត្រូវ​បាន​ទាម​ទារ​ដើម្បី​ចាប់ផ្ដើម​ប្រ​ព័ន្ធ​ប្រតិបត្តិ​​ការ​​ម៉ាស៊ីន​ភ្ញៀវពិត​ប្រាកដ ។​ ប្រ​ព័ន្ធ​ប្រតិបត្តិ​ការ​​ដែល​មិនមែន​ ​EFI ​​នឹង​មិនអាច​ទៅ​ចាប់ផ្តើម​​បាន​ទេ ​​ប្រ​សិន​បើ​ជម្រើស​ត្រូវ​បាន​ធ្វើ​ឲ្យ​សកម្ម​ ។</translation>
    </message>
    <message>
      <source>Enable &amp;EFI (special OSes only)</source>
      <translation>បើក EFI (តែ OSes ប៉ុណ្ណោះ)</translation>
    </message>
    <message>
      <source>&amp;Processor</source>
      <translation>ប្រ​ព័ន្ធ​ដំណើរ​ការ​</translation>
    </message>
    <message>
      <source>&amp;Processor(s):</source>
      <translation>ប្រ​ព័ន្ធ​ដំណើរ​ការ​​ ៖</translation>
    </message>
    <message>
      <source>Controls the number of virtual CPUs in the virtual machine.</source>
      <translation>ត្រួត​ពិនិត្យចំនួន​ស៊ីភីយូ​និម្មិត​នៅ​ក្នុង​ម៉ាស៊ីន​និម្មិត ។</translation>
    </message>
    <message>
      <source>When checked, the Physical Address Extension (PAE) feature of the host CPU will be exposed to the virtual machine.</source>
      <translation>នៅពេល​បាន​គូស​ធីក លក្ខណៈពិសេស Physical Address Extension (PAE) របស់​ស៊ីភីយូ​​ម៉ាស៊ីន​នឹង​ត្រូវ​បានបង្ហាញ​​ម៉ាស៊ីន​និម្មិត ។</translation>
    </message>
    <message>
      <source>Enable PA&amp;E/NX</source>
      <translation>បើក PAE/NX</translation>
    </message>
    <message>
      <source>Acce&amp;leration</source>
      <translation>ការ​បង្កើន​ល្បឿន​</translation>
    </message>
    <message>
      <source>Hardware Virtualization:</source>
      <translation>ការ​ធ្វើ​ផ្នែក​រឹង​និម្មិត ៖</translation>
    </message>
    <message>
      <source>When checked, the virtual machine will try to make use of the host CPU's hardware virtualization extensions such as Intel VT-x and AMD-V.</source>
      <translation>នៅ​ពេលបាន​គូស​ធីក ម៉ាស៊ីននិម្មិត​នឹង​ព្យាយាម​ប្រាកដថា​ ការ​ប្រើ​ប្រាស់​ផ្នែកបន្ថែម​និម្មិត​ផ្នែក​រឹង​របស់​ស៊ីភីយូ ដូ​ចជា Intel VT-x និង AMD-V ។</translation>
    </message>
    <message>
      <source>Enable &amp;VT-x/AMD-V</source>
      <translation>បើក VT-x/AMD-V</translation>
    </message>
    <message>
      <source>When checked, the virtual machine will try to make use of the nested paging extension of Intel VT-x and AMD-V.</source>
      <translation>នៅពេល​ដែលបានគូស​ធីក ម៉ាស៊ីន​និម្មិត​នឹង​ព្យាយាម​ប្រាកដ​ថា​ផ្នែក​បន្ថែម paging ខាង​ក្នុង​របស់ Intel VT-x និង AMD-V ។</translation>
    </message>
    <message>
      <source>Enable Nested Pa&amp;ging</source>
      <translation>បើកការ​ដាក់​ទំព័រ​ក្នុង​</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsUSB</name>
    <message>
      <source>&amp;Add Empty Filter</source>
      <translation>បន្ថែម​តម្រង​ទទេ</translation>
    </message>
    <message>
      <source>A&amp;dd Filter From Device</source>
      <translation>បន្ថែម​តម្រង​ពី​ឧបករណ៍</translation>
    </message>
    <message>
      <source>&amp;Edit Filter</source>
      <translation>កែសម្រួល​តម្រង</translation>
    </message>
    <message>
      <source>&amp;Remove Filter</source>
      <translation>យក​តម្រង​ចេញ</translation>
    </message>
    <message>
      <source>&amp;Move Filter Up</source>
      <translation>ផ្លាស់ទី​តម្រង​ឡើង​លើ</translation>
    </message>
    <message>
      <source>M&amp;ove Filter Down</source>
      <translation>ផ្លាស់ទី​តម្រង​ចុះក្រោម</translation>
    </message>
    <message>
      <source>Adds a new USB filter with all fields initially set to empty strings. Note that such a filter will match any attached USB device.</source>
      <translation>បន្ថែម​តម្រង USB ថ្មី​ដែលមាន​វាល​ទាំងអស់​ ដោយ​ដំបូង​កំណត់​ខ្សែអក្សរ​ទៅ​ទទេ ។ ចំណាំ​ថា តម្រង​បែប​នេះ​នឹង​ផ្គូផ្គង​ឧបករណ៍ USB ដែល​បាន​ភ្ជាប់ ។</translation>
    </message>
    <message>
      <source>Adds a new USB filter with all fields set to the values of the selected USB device attached to the host PC.</source>
      <translation>បន្ថែម​តម្រង USB ថ្មី​ដែល​មាន​វាល​ទាំងអស់​កំណត់​ទៅ​តម្លៃ​របស់​ឧបករណ៍​ USB ដែល​ជ្រើស​ដែលបានភ្ជាប់​ទៅ នឹង​ម៉ាស៊ីន ។</translation>
    </message>
    <message>
      <source>Edits the selected USB filter.</source>
      <translation>កែសម្រួល​តម្រង USB ដែល​បាន​ជ្រើស ។</translation>
    </message>
    <message>
      <source>Removes the selected USB filter.</source>
      <translation>យក​តម្រង USB ដែលបាន​ជ្រើស​ចេញ ។</translation>
    </message>
    <message>
      <source>Moves the selected USB filter up.</source>
      <translation>ផ្លាស់ទី​តម្រង​ USB ដែលបាន​ជ្រើស​ឡើង​វលើ ។</translation>
    </message>
    <message>
      <source>Moves the selected USB filter down.</source>
      <translation>ផ្លាស់ទី​តម្រង​ USB ដែលបាន​ជ្រើស​ចុះក្រោម ។</translation>
    </message>
    <message>
      <source>New Filter %1</source>
      <comment>usb</comment>
      <translation>តម្រង​ថ្មី %1</translation>
    </message>
    <message>
      <source>When checked, enables the virtual USB controller of this machine.</source>
      <translation>នៅពេល​បានគូសធីក បើក​ឧបករណ៍​ត្រួតពិនិត្យ​ USB និម្មិត​របស់ម៉ាស៊ីន​នេះ ។</translation>
    </message>
    <message>
      <source>Enable &amp;USB Controller</source>
      <translation>បើក​វត្ថុ​បញ្ជា USB</translation>
    </message>
    <message>
      <source>When checked, enables the virtual USB EHCI controller of this machine. The USB EHCI controller provides USB 2.0 support.</source>
      <translation>នៅពេល​បានគូស​ធីក បើក​ឧបករណ៍​ត្រួតពិនិត្យ​ USB EHCI និម្មិត​របស់​ម៉ាស៊ីន​នេះ ។ ឧបករណ៍​បញ្ជា USB EHCI ផ្ដល់​នូវ​ការ​គាំទ្រ USB 2.0 ។</translation>
    </message>
    <message>
      <source>Enable USB 2.0 (E&amp;HCI) Controller</source>
      <translation>បើក​វត្ថុ​បញ្ជា USB 2.0 (E&amp;HCI)</translation>
    </message>
    <message>
      <source>USB Device &amp;Filters</source>
      <translation>តម្រង​ឧបករណ៍ USB</translation>
    </message>
    <message>
      <source>Lists all USB filters of this machine. The checkbox to the left defines whether the particular filter is enabled or not. Use the context menu or buttons to the right to add or remove USB filters.</source>
      <translation>រាយ​តម្រង USB ទាំង​អស់​របស់ម៉ាស៊ីន​នេះ ។ ប្រអប់​ធីក​នៅ​ខាង​ឆ្វេ​ងកំណត់​ថាតើ​តម្រង​ជាក់លាក់​ត្រូ​វបើក​ ឬ​ក៏​អត់ ។ ប្រើម៉ឺនុយ​បរិបទ​ ឬ​ប៊ូតុង​នៅ​ខាង​ស្ដាំ​ដើម្បី​បន្ថែម ឬ​យក​តម្រង  USB ចេញ ។</translation>
    </message>
    <message>
      <source>[filter]</source>
      <translation>[តម្រង]</translation>
    </message>
  </context>
  <context>
    <name>VBoxVMSettingsUSBFilterDetails</name>
    <message>
      <source>Any</source>
      <comment>remote</comment>
      <translation>ណាមួយ</translation>
    </message>
    <message>
      <source>Yes</source>
      <comment>remote</comment>
      <translation>បាទ/ចាស</translation>
    </message>
    <message>
      <source>No</source>
      <comment>remote</comment>
      <translation>ទេ</translation>
    </message>
    <message>
      <source>&amp;Name:</source>
      <translation>ឈ្មោះ ៖</translation>
    </message>
    <message>
      <source>Displays the filter name.</source>
      <translation>បង្ហាញឈ្មោះ​តម្រង ។</translation>
    </message>
    <message>
      <source>&amp;Vendor ID:</source>
      <translation>លេខ​សម្គាល់​អ្នក​លក់ ៖</translation>
    </message>
    <message>
      <source>Defines the vendor ID filter. The &lt;i>exact match&lt;/i> string format is &lt;tt>XXXX&lt;/tt> where &lt;tt>X&lt;/tt> is a hexadecimal digit. An empty string will match any value.</source>
      <translation>កំណត់​តម្រង​លេខ​សម្គាល់​អ្នក​លក់ ។ ទ្រង់ទ្រាយ​ខ្សែអក្សរ &lt;i>ផ្គូផ្គងជាក់លាក់&lt;/i> គឺ &lt;tt>XXXX&lt;/tt> ដែល &lt;tt>X&lt;/tt> ជា​គោល​ដប់ប្រាំមួយ ។ ខ្សែអក្សរ​ទទេ​នឹង​ផ្គូផ្គង​តម្លៃ​ណាមួយ ។</translation>
    </message>
    <message>
      <source>&amp;Product ID:</source>
      <translation>លេខ​សម្គាល់​ផលិតផល ៖</translation>
    </message>
    <message>
      <source>Defines the product ID filter. The &lt;i>exact match&lt;/i> string format is &lt;tt>XXXX&lt;/tt> where &lt;tt>X&lt;/tt> is a hexadecimal digit. An empty string will match any value.</source>
      <translation>កំណត់​តម្រង​លេខ​សម្គាល់​ផលិតផល ។ ទ្រង់ទ្រាយ​ខ្សែអក្សរ&lt;i>ផ្គូផ្គង​ជាក់លាក់&lt;/i> គឺ&lt;tt>XXXX&lt;/tt> ដែល &lt;tt>X&lt;/tt> គឺជា​គោល​ដប់​ប្រាំមួយ ។ ខ្សែអក្សរ​ទទេ​នឹង​ផ្គូផ្គង​តម្លៃ​ណាមួយ ។</translation>
    </message>
    <message>
      <source>&amp;Revision:</source>
      <translation>ការ​ពិនិត្យ​ឡើង​វិញ ៖</translation>
    </message>
    <message>
      <source>Defines the revision number filter. The &lt;i>exact match&lt;/i> string format is &lt;tt>IIFF&lt;/tt> where &lt;tt>I&lt;/tt> is a decimal digit of the integer part and &lt;tt>F&lt;/tt> is a decimal digit of the fractional part. An empty string will match any value.</source>
      <translation>កំណត់​ការ​ពិនិត្យ​តម្រង​លេខ​ឡើង​វិញ ។ ទ្រង់ទ្រាយ​ខ្សែអក្សរ &lt;i>ផ្គូផ្គង​ជាក់លាក់&lt;/i> គឺ​&lt;tt>IIFF&lt;/tt> ដែល &lt;tt>I&lt;/tt> ជា​ចំនួន​គោល​ដប់​របស់​ផ្នែក​ចំនួន​គត់​ ហើយ &lt;tt>F&lt;/tt> គឺជា​ចំនួន​គោលដប់​របស់​ផ្នែក​ប្រភាគ ។ ខ្សែអក្សរ​ទទេ​នឹង​ផ្គូផ្គង​តម្លៃ​ណាមួយ ។</translation>
    </message>
    <message>
      <source>&amp;Manufacturer:</source>
      <translation>អ្នក​បង្កើត ៖</translation>
    </message>
    <message>
      <source>Defines the manufacturer filter as an &lt;i>exact match&lt;/i> string. An empty string will match any value.</source>
      <translation>កំណត់​តម្រង​អ្នក​ផលិត​ជាខ្សែអក្សរ &lt;i>ផ្គូផ្គង​ជាក់លាក់&lt;/i> ។ ខ្សែអក្សរ​ទទេ​នឹង​ផ្គូផ្គង​តម្លៃ​ណាមួយ ។</translation>
    </message>
    <message>
      <source>Pro&amp;duct:</source>
      <translation>ផលិតផល ៖</translation>
    </message>
    <message>
      <source>Defines the product name filter as an &lt;i>exact match&lt;/i> string. An empty string will match any value.</source>
      <translation>កំណត់​តម្រង​ឈ្មោះផលិតផល​ជាខ្សែអក្សរ &lt;i>ផ្គូផ្គងជាក់លាក់&lt;/i> ។ ខ្សែអក្សរទទេ​នឹង​ផ្គូផ្គង​តម្លៃ​ណាមួយ ។</translation>
    </message>
    <message>
      <source>&amp;Serial No.:</source>
      <translation>លេខ​ស៊េរី ៖</translation>
    </message>
    <message>
      <source>Defines the serial number filter as an &lt;i>exact match&lt;/i> string. An empty string will match any value.</source>
      <translation>កំណត់​តម្រង​លេខ​ស៊េរី​ជា​ខ្សែអក្សរ &lt;i>កា​រផ្គូផ្គង​ជាក់លាក់&lt;/i> ។ ខ្សែអក្សរ​ទទេ​នឹង​ផ្គូផ្គង​តម្លៃ​ណាមួយ ។</translation>
    </message>
    <message>
      <source>Por&amp;t:</source>
      <translation>ច្រក ៖</translation>
    </message>
    <message>
      <source>Defines the host USB port filter as an &lt;i>exact match&lt;/i> string. An empty string will match any value.</source>
      <translation>កំណត់​តម្រង​ច្រក USB ម៉ាស៊ីណ​ជា​ខ្សែអក្សរ &lt;i>ផ្គូផ្គង​ពិតប្រាកដ&lt;/i> ។ ខ្សែអក្សរ​ទទេ​នឹង​ផ្គូផ្គង​តម្លៃ​ណាមួយ ។</translation>
    </message>
    <message>
      <source>R&amp;emote:</source>
      <translation>ពីចម្ងាយ ៖</translation>
    </message>
    <message>
      <source>Defines whether this filter applies to USB devices attached locally to the host computer (&lt;i>No&lt;/i>), to a VRDP client's computer (&lt;i>Yes&lt;/i>), or both (&lt;i>Any&lt;/i>).</source>
      <translation>កំណត់​ថាតើ​តម្រង​នេះ​អនុវត្ត​ទៅកាន់​ឧបករណ៍ USB ដែលបាន​ភ្ជាប់​ទៅ​​កុំព្យូទ័រ​ដែរឬទេ (&lt;i>គ្មានទេ&lt;/i>) ទៅកាន់​កុំព្យូទ័រ​ភ្ញៀវ​របស់ VRDP (&lt;i>បាទ/ចាស&lt;/i>) ឬ​ទាំង​ពីរ (&lt;i>ណាមួយ&lt;/i>) ។</translation>
    </message>
    <message>
      <source>&amp;Action:</source>
      <translation>សកម្មភាព ៖</translation>
    </message>
    <message>
      <source>Defines an action performed by the host computer when a matching device is attached: give it up to the host OS (&lt;i>Ignore&lt;/i>) or grab it for later usage by virtual machines (&lt;i>Hold&lt;/i>).</source>
      <translation>កំណត់​សកម្មភាព​ដែល​បាន​អនុវត្ត​ដោយ​កុំព្យូទ័រ នៅពេល​ផ្គូផ្គង​ឧបករណ៍​ដែល​ត្រូវ​បាន​ភ្ជាប់ ៖ ផ្ដល់​វា​ទៅ​ប្រព័ន្ធ​ប្រតិបត្តិការ​របស់ម៉ាស៊ីន (&lt;i>មិនអើពើ&lt;/i>) ឬ​ចាប់យក​វា​សម្រាប់​ប្រើ​ពេលក្រោយ ដោយ​ម៉ាស៊ីន​និម្មិត &lt;i>រង់ចាំ&lt;/i>) ។</translation>
    </message>
    <message>
      <source>USB Filter Details</source>
      <translation>សេចក្ដី​លម្អិត​តម្រង USB</translation>
    </message>
  </context>
</TS>
