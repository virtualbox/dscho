<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ko">
<context>
    <name>AudioOutput</name>
    <message>
        <source>&lt;html&gt;The audio playback device &lt;b&gt;%1&lt;/b&gt; does not work.&lt;br/&gt;Falling back to &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which just became available and has higher preference.&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Revert back to device &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Phonon::</name>
    <message>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Communication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Games</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Accessibility</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::Backend</name>
    <message>
        <source>Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.
          Some video features have been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning: You do not seem to have the base GStreamer plugins installed.
          All audio and video support has been disabled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::MediaObject</name>
    <message>
        <source>Cannot start playback. 

Check your Gstreamer installation and make sure you 
have libgstreamer-plugins-base installed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>A required codec is missing. You need to install the following codec(s) to play this content: %0</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Could not open media source.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid source type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not locate media source.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not open audio device. The device is already in use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not decode media source.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Phonon::VolumeSlider</name>
    <message>
        <source>Volume: %1%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use this slider to adjust the volume. The leftmost position is 0%, the rightmost is %1%</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <source>%1, %2 not defined</source>
        <translation>%1, %2이(가) 정의되지 않았습니다</translation>
    </message>
    <message>
        <source>Ambiguous %1 not handled</source>
        <translation>모호한 %1이(가) 처리되지 않았습니다</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <source>True</source>
        <translation>참</translation>
    </message>
    <message>
        <source>False</source>
        <translation>거짓</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation>삽입</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>삭제</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>업데이트</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <source>Copy or Move a File</source>
        <translation>파일 복사 또는 이동</translation>
    </message>
    <message>
        <source>Read: %1</source>
        <translation>읽기: %1</translation>
    </message>
    <message>
        <source>Write: %1</source>
        <translation>쓰기: %1</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation>모든 파일 (*)</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>크기</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>종류</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>날짜</translation>
    </message>
    <message>
        <source>Attributes</source>
        <translation>속성</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>확인(&amp;O)</translation>
    </message>
    <message>
        <source>Look &amp;in:</source>
        <translation>다음에서 찾기(&amp;I):</translation>
    </message>
    <message>
        <source>File &amp;name:</source>
        <translation>파일 이름(&amp;N):</translation>
    </message>
    <message>
        <source>File &amp;type:</source>
        <translation>파일 형식(&amp;T):</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>뒤로</translation>
    </message>
    <message>
        <source>One directory up</source>
        <translation>한 단계 위로</translation>
    </message>
    <message>
        <source>Create New Folder</source>
        <translation>새 폴더 만들기</translation>
    </message>
    <message>
        <source>List View</source>
        <translation>목록으로 보기</translation>
    </message>
    <message>
        <source>Detail View</source>
        <translation>자세히 보기</translation>
    </message>
    <message>
        <source>Preview File Info</source>
        <translation>파일 정보 미리 보기</translation>
    </message>
    <message>
        <source>Preview File Contents</source>
        <translation>파일 내용 미리 보기</translation>
    </message>
    <message>
        <source>Read-write</source>
        <translation>읽기-쓰기</translation>
    </message>
    <message>
        <source>Read-only</source>
        <translation>읽기 전용</translation>
    </message>
    <message>
        <source>Write-only</source>
        <translation>쓰기 전용</translation>
    </message>
    <message>
        <source>Inaccessible</source>
        <translation>접근할 수 없음</translation>
    </message>
    <message>
        <source>Symlink to File</source>
        <translation>파일로 향한 링크</translation>
    </message>
    <message>
        <source>Symlink to Directory</source>
        <translation>디렉터리로 향한 링크</translation>
    </message>
    <message>
        <source>Symlink to Special</source>
        <translation>특수 파일로 향한 링크</translation>
    </message>
    <message>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <source>Dir</source>
        <translation>디렉터리</translation>
    </message>
    <message>
        <source>Special</source>
        <translation>특수 파일</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>열기</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>다른 이름으로 저장</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>열기(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>저장(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation>이름 바꾸기(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>삭제(&amp;D)</translation>
    </message>
    <message>
        <source>R&amp;eload</source>
        <translation>새로 고침(&amp;E)</translation>
    </message>
    <message>
        <source>Sort by &amp;Name</source>
        <translation>이름으로 정렬(&amp;N)</translation>
    </message>
    <message>
        <source>Sort by &amp;Size</source>
        <translation>크기로 정렬(&amp;S)</translation>
    </message>
    <message>
        <source>Sort by &amp;Date</source>
        <translation>날짜로 정렬(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Unsorted</source>
        <translation>정렬하지 않음(&amp;U)</translation>
    </message>
    <message>
        <source>Sort</source>
        <translation>정렬</translation>
    </message>
    <message>
        <source>Show &amp;hidden files</source>
        <translation>숨김 파일 보이기(&amp;H)</translation>
    </message>
    <message>
        <source>the file</source>
        <translation>파일</translation>
    </message>
    <message>
        <source>the directory</source>
        <translation>디렉터리</translation>
    </message>
    <message>
        <source>the symlink</source>
        <translation>링크</translation>
    </message>
    <message>
        <source>Delete %1</source>
        <translation>%1 삭제</translation>
    </message>
    <message>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;%1 &quot;%2&quot;을(를) 삭제하시겠습까?&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>예(&amp;Y)</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>아니오(&amp;N)</translation>
    </message>
    <message>
        <source>New Folder 1</source>
        <translation>새 폴더 1</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation>새 폴더</translation>
    </message>
    <message>
        <source>New Folder %1</source>
        <translation>새 폴더 %1</translation>
    </message>
    <message>
        <source>Find Directory</source>
        <translation>디렉터리 찾기</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation>디렉터리</translation>
    </message>
    <message>
        <source>Directory:</source>
        <translation>디렉터리:</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <source>%1
File not found.
Check path and filename.</source>
        <translation>%1
파일을 찾을 수 없습니다.
경로와 파일 이름을 확인하십시오.</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <source>Could not read directory
%1</source>
        <translation>다음 디렉터리를 읽을 수 없음
%1</translation>
    </message>
    <message>
        <source>Could not create directory
%1</source>
        <translation>다음 디렉터리를 만들 수 없음
%1</translation>
    </message>
    <message>
        <source>Could not remove file or directory
%1</source>
        <translation>파일이나 디렉터리를 삭제할 수 없음
%1</translation>
    </message>
    <message>
        <source>Could not rename
%1
to
%2</source>
        <translation>%1
의 이름을
%2
(으)로 바꿀 수 없음</translation>
    </message>
    <message>
        <source>Could not open
%1</source>
        <translation>다음을 열 수 없음
%1</translation>
    </message>
    <message>
        <source>Could not write
%1</source>
        <translation>다음에 쓸 수 없음
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <source>Line up</source>
        <translation>정렬하기</translation>
    </message>
    <message>
        <source>Customize...</source>
        <translation>사용자 정의...</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <source>Operation stopped by the user</source>
        <translation>사용자가 동작을 중지함</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <source>OK</source>
        <translation>확인</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>적용</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation>기본값 복원</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <source>&amp;Undo</source>
        <translation>실행 취소(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>다시 실행(&amp;R)</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>잘라내기(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>복사(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>붙여넣기(&amp;P)</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>지우기</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>모두 선택</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <source>System</source>
        <translation>시스템</translation>
    </message>
    <message>
        <source>Restore up</source>
        <translation>복원</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>최소화</translation>
    </message>
    <message>
        <source>Restore down</source>
        <translation>복원</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>최대화</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <source>Contains commands to manipulate the window</source>
        <translation>창을 조작하는 명령을 포함합니다</translation>
    </message>
    <message>
        <source>Puts a minimized back to normal</source>
        <translation>최소화된 창을 되돌립니다</translation>
    </message>
    <message>
        <source>Moves the window out of the way</source>
        <translation>창을 이동시킵니다</translation>
    </message>
    <message>
        <source>Puts a maximized window back to normal</source>
        <translation>최대화된 창을 되돌립니다</translation>
    </message>
    <message>
        <source>Makes the window full screen</source>
        <translation>창을 전체 화면으로 만듭니다</translation>
    </message>
    <message>
        <source>Closes the window</source>
        <translation>창을 닫습니다</translation>
    </message>
    <message>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation>창의 이름을 보여주고 조작하기 위한 컨트롤을 포함합니다</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <source>More...</source>
        <translation>더 보기...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <source>The protocol `%1&apos; is not supported</source>
        <translation>프로토콜 `%1&apos;은(는) 지원하지 않습니다</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation>프로토콜 `%1&apos;에서 디렉터리 목록을 볼 수 없습니다</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation>프로토콜 `%1&apos;에서 새 디렉터리를 만들 수 없습니다</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation>프로토콜 `%1&apos;에서 파일이나 디렉터리를 삭제할 수 없습니다</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation>프로토콜 `%1&apos;에서 파일이나 디렉터리의 이름을 바꿀 수 없습니다</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation>프로토콜 `%1&apos;에서 파일을 가져올 수 없습니다</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation>프로토콜 `%1&apos;에서 파일을 올릴 수 없습니다</translation>
    </message>
    <message>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation>프로토콜 `%1&apos;에서 파일이나 디렉터리를 복사하거나 이동할 수 없습니다</translation>
    </message>
    <message>
        <source>(unknown)</source>
        <translation>(알 수 없음)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <source>&amp;Cancel</source>
        <translation>취소(&amp;C)</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 이전(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation>다음 (&amp;N) &gt;</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>완료(&amp;F)</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>도움말(&amp;H)</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <source>Host not found</source>
        <translation>호스트를 찾을 수 없음</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>연결이 거부됨</translation>
    </message>
    <message>
        <source>Socket operation timed out</source>
        <translation>소켓 작업 시간 초과</translation>
    </message>
    <message>
        <source>Socket is not connected</source>
        <translation>소켓이 연결되지 않음</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <source>&amp;Step up</source>
        <translation>한 단계 위로(&amp;S)</translation>
    </message>
    <message>
        <source>Step &amp;down</source>
        <translation>한 단계 아래로(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Select All</source>
        <translation>모두 선택(&amp;S)</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Activate</source>
        <translation>활성화</translation>
    </message>
    <message>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>실행 파일 &apos;%1&apos;은(는) Qt %2을(를) 필요로 하지만 현재 Qt %3이(가) 설치되어 있습니다.</translation>
    </message>
    <message>
        <source>Incompatible Qt Library Error</source>
        <translation>Qt 라이브러리 호환성 오류</translation>
    </message>
    <message>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>LTR</translation>
    </message>
    <message>
        <source>Activates the program&apos;s main window</source>
        <translation>프로그램의 주 창 활성화</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <source>Uncheck</source>
        <translation>선택 해제</translation>
    </message>
    <message>
        <source>Check</source>
        <translation>선택</translation>
    </message>
    <message>
        <source>Toggle</source>
        <translation>선택 반전</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <source>Hu&amp;e:</source>
        <translation>색상(&amp;E):</translation>
    </message>
    <message>
        <source>&amp;Sat:</source>
        <translation>선명도(&amp;S):</translation>
    </message>
    <message>
        <source>&amp;Val:</source>
        <translation>휘도(&amp;V):</translation>
    </message>
    <message>
        <source>&amp;Red:</source>
        <translation>빨강(&amp;R):</translation>
    </message>
    <message>
        <source>&amp;Green:</source>
        <translation>녹색(&amp;G):</translation>
    </message>
    <message>
        <source>Bl&amp;ue:</source>
        <translation>파랑(&amp;U):</translation>
    </message>
    <message>
        <source>A&amp;lpha channel:</source>
        <translation>투명도(&amp;L):</translation>
    </message>
    <message>
        <source>&amp;Basic colors</source>
        <translation>기본 색상(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Custom colors</source>
        <translation>사용자 정의 색상(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Add to Custom Colors</source>
        <translation>사용자 정의 색상에 추가(&amp;A)</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation>색 선택</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <source>Open</source>
        <translation>열기</translation>
    </message>
    <message>
        <source>False</source>
        <translation>거짓</translation>
    </message>
    <message>
        <source>True</source>
        <translation>참</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>%1: permission denied</source>
        <comment>QSystemSemaphore</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <comment>QSystemSemaphore</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <comment>QSystemSemaphore</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <source>Unable to connect</source>
        <translation>연결할 수 없음</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation>트랜잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation>트랜잭션을 되돌릴 수 없음</translation>
    </message>
    <message>
        <source>Unable to set autocommit</source>
        <translation>자동 커밋을 설정할 수 없음</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <source>Unable to execute statement</source>
        <translation>구문을 실행할 수 없음</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation>구문을 준비할 수 없음</translation>
    </message>
    <message>
        <source>Unable to bind variable</source>
        <translation>변수를 바인딩할 수 없음</translation>
    </message>
    <message>
        <source>Unable to fetch record %1</source>
        <translation>레코드 %1을(를) 가져올 수 없음</translation>
    </message>
    <message>
        <source>Unable to fetch next</source>
        <translation>다음 항목을 가져올 수 없음</translation>
    </message>
    <message>
        <source>Unable to fetch first</source>
        <translation>이전 항목을 가져올 수 없음</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <source>AM</source>
        <translation>오전</translation>
    </message>
    <message>
        <source>am</source>
        <translation>오전</translation>
    </message>
    <message>
        <source>PM</source>
        <translation>오후</translation>
    </message>
    <message>
        <source>pm</source>
        <translation>오후</translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <source>QDial</source>
        <translation>QDial</translation>
    </message>
    <message>
        <source>SpeedoMeter</source>
        <translation>속도계</translation>
    </message>
    <message>
        <source>SliderHandle</source>
        <translation>슬라이더 핸들</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <source>What&apos;s This?</source>
        <translation>이것에 대한 설명</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="unfinished">완료</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <source>OK</source>
        <translation>확인</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>저장</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>열기</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>적용</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>초기화</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation>저장하지 않음</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>무시</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>예(&amp;Y)</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation>모두 예(&amp;A)</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>아니오(&amp;N)</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation>모두 아니오(&amp;O)</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation>모두 저장</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>중단</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>다시 시도</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>무시</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>기본값 복원</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation>저장하지 않고 닫기</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished">확인(&amp;O)</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>크기</translation>
    </message>
    <message>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>종류</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>종류</translation>
    </message>
    <message>
        <source>Date Modified</source>
        <translation>수정한 날짜</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <source>Dock</source>
        <translation>붙이기</translation>
    </message>
    <message>
        <source>Float</source>
        <translation>띄우기</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <source>More</source>
        <translation>더 보기</translation>
    </message>
    <message>
        <source>Less</source>
        <translation>덜 보기</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <source>Debug Message:</source>
        <translation>디버그 메시지:</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation>경고:</translation>
    </message>
    <message>
        <source>Fatal Error:</source>
        <translation>치명적 오류:</translation>
    </message>
    <message>
        <source>&amp;Show this message again</source>
        <translation>이 메시지를 다시 보이기(&amp;S)</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>확인(&amp;O)</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <source>All Files (*)</source>
        <translation>모든 파일 (*)</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation>디렉터리</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>열기(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>저장(&amp;S)</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>열기</translation>
    </message>
    <message>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1이(가) 이미 존재합니다.
바꾸시겠습니까?</translation>
    </message>
    <message>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>%1
파일을 찾을 수 없습니다.
경로와 파일 이름을 확인하십시오.</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation>내 컴퓨터</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation>이름 바꾸기(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>삭제(&amp;D)</translation>
    </message>
    <message>
        <source>Show &amp;hidden files</source>
        <translation>숨김 파일 보이기(&amp;H)</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>뒤로</translation>
    </message>
    <message>
        <source>Parent Directory</source>
        <translation>부모 디렉터리</translation>
    </message>
    <message>
        <source>List View</source>
        <translation>목록으로 보기</translation>
    </message>
    <message>
        <source>Detail View</source>
        <translation>자세히 보기</translation>
    </message>
    <message>
        <source>Files of type:</source>
        <translation>파일 형식:</translation>
    </message>
    <message>
        <source>Directory:</source>
        <translation>디렉터리:</translation>
    </message>
    <message>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>%1
디렉터리를 찾을 수 없습니다.
경로와 파일 이름을 확인하십시오.</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>&apos;%1&apos;이(가) 쓰기 금지되어 있습니다.
그래도 삭제하시겠습니까?</translation>
    </message>
    <message>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation>&apos;%1&apos;을(를) 삭제하시겠습니까?</translation>
    </message>
    <message>
        <source>Could not delete directory.</source>
        <translation>디렉터리를 삭제할 수 없습니다.</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>다른 이름으로 저장</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>드라이브</translation>
    </message>
    <message>
        <source>File</source>
        <translation>파일</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>알 수 없음</translation>
    </message>
    <message>
        <source>Find Directory</source>
        <translation>디렉터리 찾기</translation>
    </message>
    <message>
        <source>Show </source>
        <translation>보이기</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>앞으로</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation>새 폴더</translation>
    </message>
    <message>
        <source>&amp;New Folder</source>
        <translation>새 폴더(&amp;N)</translation>
    </message>
    <message>
        <source>&amp;Choose</source>
        <translation>선택(&amp;C)</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>삭제</translation>
    </message>
    <message>
        <source>File &amp;name:</source>
        <translation>파일 이름(&amp;N):</translation>
    </message>
    <message>
        <source>Look in:</source>
        <translation>다음에서 찾기:</translation>
    </message>
    <message>
        <source>Create New Folder</source>
        <translation>새 폴더 만들기</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <source>Invalid filename</source>
        <translation>잘못된 파일 이름</translation>
    </message>
    <message>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation>&lt;b&gt;&quot;%1&quot; 이름을 사용할 수 없습니다.&lt;/b&gt;&lt;p&gt;다른 이름을 사용하거나, 글자 수를 줄이거나, 구두점을 사용하지 마십시오.</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>이름</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>크기</translation>
    </message>
    <message>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>종류</translation>
    </message>
    <message>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>종류</translation>
    </message>
    <message>
        <source>Date Modified</source>
        <translation>수정한 날짜</translation>
    </message>
    <message>
        <source>My Computer</source>
        <translation>내 컴퓨터</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation>컴퓨터</translation>
    </message>
    <message>
        <source>%1 TB</source>
        <translation>%1 TB</translation>
    </message>
    <message>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <source>%1 bytes</source>
        <translation>%1바이트</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Demi Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Demi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Oblique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Any</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Latin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Greek</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cyrillic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Armenian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hebrew</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Arabic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Syriac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thaana</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Devanagari</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bengali</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gurmukhi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gujarati</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Oriya</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tamil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Telugu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Kannada</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Malayalam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sinhala</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thai</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lao</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tibetan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Myanmar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Georgian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Khmer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Traditional Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Korean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vietnamese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ogham</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Runic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <source>&amp;Font</source>
        <translation>글꼴(&amp;F)</translation>
    </message>
    <message>
        <source>Font st&amp;yle</source>
        <translation>글꼴 스타일(&amp;Y)</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation>크기(&amp;S)</translation>
    </message>
    <message>
        <source>Effects</source>
        <translation>효과</translation>
    </message>
    <message>
        <source>Stri&amp;keout</source>
        <translation>취소선(&amp;K)</translation>
    </message>
    <message>
        <source>&amp;Underline</source>
        <translation>밑줄(&amp;U)</translation>
    </message>
    <message>
        <source>Sample</source>
        <translation>미리 보기</translation>
    </message>
    <message>
        <source>Wr&amp;iting System</source>
        <translation>문자 체계(&amp;I)</translation>
    </message>
    <message>
        <source>Select Font</source>
        <translation>글꼴 선택</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <source>Not connected</source>
        <translation>연결되지 않음</translation>
    </message>
    <message>
        <source>Host %1 not found</source>
        <translation>호스트 %1을(를) 찾을 수 없음</translation>
    </message>
    <message>
        <source>Connection refused to host %1</source>
        <translation>호스트 %1와(과)의 연결이 거부됨</translation>
    </message>
    <message>
        <source>Connected to host %1</source>
        <translation>호스트 %1에 연결됨</translation>
    </message>
    <message>
        <source>Connection refused for data connection</source>
        <translation>데이터 연결이 거부됨</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <source>Connecting to host failed:
%1</source>
        <translation>호스트 연결 실패:
%1</translation>
    </message>
    <message>
        <source>Login failed:
%1</source>
        <translation>로그인 실패:
%1</translation>
    </message>
    <message>
        <source>Listing directory failed:
%1</source>
        <translation>디렉터리 목록 표시 실패:
%1</translation>
    </message>
    <message>
        <source>Changing directory failed:
%1</source>
        <translation>디렉터리 변경 실패:
%1</translation>
    </message>
    <message>
        <source>Downloading file failed:
%1</source>
        <translation>파일 다운로드 실패:
%1</translation>
    </message>
    <message>
        <source>Uploading file failed:
%1</source>
        <translation>파일 업로드 실패:
%1</translation>
    </message>
    <message>
        <source>Removing file failed:
%1</source>
        <translation>파일 삭제 실패:
%1</translation>
    </message>
    <message>
        <source>Creating directory failed:
%1</source>
        <translation>디렉터리 생성 실패:
%1</translation>
    </message>
    <message>
        <source>Removing directory failed:
%1</source>
        <translation>디렉터리 삭제 실패:
%1</translation>
    </message>
    <message>
        <source>Connection closed</source>
        <translation>연결이 종료됨</translation>
    </message>
    <message>
        <source>Host %1 found</source>
        <translation>호스트 %1을(를) 찾았음</translation>
    </message>
    <message>
        <source>Connection to %1 closed</source>
        <translation>%1와(과)의 연결이 종료됨</translation>
    </message>
    <message>
        <source>Host found</source>
        <translation>호스트를 찾았음</translation>
    </message>
    <message>
        <source>Connected to host</source>
        <translation>호스트에 연결됨</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <source>Host not found</source>
        <translation>호스트를 찾을 수 없음</translation>
    </message>
    <message>
        <source>Unknown address type</source>
        <translation>알 수 없는 주소 종류</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <source>Request aborted</source>
        <translation>요청이 중단됨</translation>
    </message>
    <message>
        <source>No server set to connect to</source>
        <translation>연결할 서버가 설정되지 않음</translation>
    </message>
    <message>
        <source>Wrong content length</source>
        <translation>내용 길이가 잘못됨</translation>
    </message>
    <message>
        <source>Server closed connection unexpectedly</source>
        <translation>서버에서 예상하지 못하게 연결을 종료함</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>연결이 거부됨</translation>
    </message>
    <message>
        <source>Host %1 not found</source>
        <translation>호스트 %1을(를) 찾을 수 없음</translation>
    </message>
    <message>
        <source>HTTP request failed</source>
        <translation>HTTP 요청이 실패함</translation>
    </message>
    <message>
        <source>Invalid HTTP response header</source>
        <translation>HTTP 응답 헤더가 잘못됨</translation>
    </message>
    <message>
        <source>Invalid HTTP chunked body</source>
        <translation>HTTP 조각난 본문이 잘못됨</translation>
    </message>
    <message>
        <source>Host %1 found</source>
        <translation>호스트 %1을(를) 찾았음</translation>
    </message>
    <message>
        <source>Connected to host %1</source>
        <translation>호스트 %1에 연결됨</translation>
    </message>
    <message>
        <source>Connection to %1 closed</source>
        <translation>%1와(과)의 연결이 종료됨</translation>
    </message>
    <message>
        <source>Host found</source>
        <translation>호스트를 찾았음</translation>
    </message>
    <message>
        <source>Connected to host</source>
        <translation>호스트에 연결됨</translation>
    </message>
    <message>
        <source>Connection closed</source>
        <translation>연결이 종료됨</translation>
    </message>
    <message>
        <source>Proxy authentication required</source>
        <translation>프록시 인증이 필요함</translation>
    </message>
    <message>
        <source>Authentication required</source>
        <translation>인증이 필요함</translation>
    </message>
    <message>
        <source>HTTPS connection requested but SSL support not compiled in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connection refused (or timed out)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Proxy requires authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host requires authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data corrupted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown protocol specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SSL handshake failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <source>Authentication required</source>
        <translation>인증이 필요함</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <source>Error opening database</source>
        <translation>데이터베이스를 여는 중 오류 발생</translation>
    </message>
    <message>
        <source>Could not start transaction</source>
        <translation>트랙잭션을 시작할 수 없음</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation>트랜잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation>트랜잭션을 되돌릴 수 없음</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <source>Unable to create BLOB</source>
        <translation>BLOB을 만들 수 없음</translation>
    </message>
    <message>
        <source>Unable to write BLOB</source>
        <translation>BLOB에 쓸 수 없음</translation>
    </message>
    <message>
        <source>Unable to open BLOB</source>
        <translation>BLOB을 열 수 없음</translation>
    </message>
    <message>
        <source>Unable to read BLOB</source>
        <translation>BLOB에서 읽을 수 없음</translation>
    </message>
    <message>
        <source>Could not find array</source>
        <translation>배열을 찾을 수 없음</translation>
    </message>
    <message>
        <source>Could not get array data</source>
        <translation>배열 데이터를 가져올 수 없음</translation>
    </message>
    <message>
        <source>Could not get query info</source>
        <translation>쿼리 정보를 가져올 수 없음</translation>
    </message>
    <message>
        <source>Could not start transaction</source>
        <translation>트랜잭션을 시작할 수 없음</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation>트랜잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Could not allocate statement</source>
        <translation>구문을 할당할 수 없음</translation>
    </message>
    <message>
        <source>Could not prepare statement</source>
        <translation>구문을 준비할 수 없음</translation>
    </message>
    <message>
        <source>Could not describe input statement</source>
        <translation>입력 구문을 설명할 수 없음</translation>
    </message>
    <message>
        <source>Could not describe statement</source>
        <translation>구문을 설명할 수 없음</translation>
    </message>
    <message>
        <source>Unable to close statement</source>
        <translation>구문을 닫을 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute query</source>
        <translation>쿼리를 실행할 수 없음</translation>
    </message>
    <message>
        <source>Could not fetch next item</source>
        <translation>다음 항목을 가져올 수 없음</translation>
    </message>
    <message>
        <source>Could not get statement info</source>
        <translation>구문 정보를 가져올 수 없음</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <source>Permission denied</source>
        <translation>권한이 거부됨</translation>
    </message>
    <message>
        <source>Too many open files</source>
        <translation>너무 많은 파일이 열렸음</translation>
    </message>
    <message>
        <source>No such file or directory</source>
        <translation>그러한 파일이나 디렉터리가 없음</translation>
    </message>
    <message>
        <source>No space left on device</source>
        <translation>장치에 공간이 부족함</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <source>XIM</source>
        <translation>XIM</translation>
    </message>
    <message>
        <source>XIM input method</source>
        <translation>XIM 입력기</translation>
    </message>
    <message>
        <source>Windows input method</source>
        <translation>윈도 입력기</translation>
    </message>
    <message>
        <source>Mac OS X input method</source>
        <translation>Mac OS X 입력기</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation>QLibrary::load_sys: %1을(를) 불러올 수 없음 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation>QLibrary::unload_sys: %1을(를) 닫을 수 없음 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation>QLibrary::resolve_sys: 심볼 &quot;%1&quot;이(가) %2에 정의되지 않음 (%3)</translation>
    </message>
    <message>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation>&apos;%1&apos;에서 mmap을 실행할 수 없음: %2</translation>
    </message>
    <message>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation>&apos;%1&apos;의 플러그인 확인 데이터가 일치하지 않음</translation>
    </message>
    <message>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation>&apos;%1&apos;의 매핑을 해제할 수 없음: %2</translation>
    </message>
    <message>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation>플러그인 &apos;%1&apos;은(는) 호환되지 않는 Qt 라이브러리를 사용합니다. (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation>플러그인 &apos;%1&apos;은(는) 호환되지 않는 Qt 라이브러리를 사용합니다. 빌드 키 &quot;%2&quot;을(를) 예상했지만 &quot;%3&quot;이 돌아왔습니다</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <source>The shared library was not found.</source>
        <translation>공유 라이브러리를 찾을 수 없습니다.</translation>
    </message>
    <message>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>파일 &apos;%1&apos;은(는) 올바른 Qt 플러그인이 아닙니다.</translation>
    </message>
    <message>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation>플러그인 &apos;%1&apos;은(는) 호환되지 않는 Qt 라이브러리를 사용합니다. (디버그와 릴리즈 라이브러리를 섞을 수 없습니다.)</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <source>&amp;Undo</source>
        <translation>실행 취소(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>다시 실행(&amp;R)</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>잘라내기(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>복사(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>붙여넣기(&amp;P)</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>삭제</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>모두 선택</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <source>%1: Name error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Permission denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Address in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Unknown error %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <source>%1: Connection refused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Remote closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Invalid name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Socket access error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Socket resource error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Socket operation timed out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Datagram too large</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Connection error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: The socket operation is not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: Unknown error %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <source>Unable to open database &apos;</source>
        <translation>다음 데이터베이스를 열 수 없음: &apos;</translation>
    </message>
    <message>
        <source>Unable to connect</source>
        <translation>연결할 수 없음</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation>트랜잭션을 시작할 수 없음</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation>트랜잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation>트랜잭션을 되돌릴 수 없음</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <source>Unable to fetch data</source>
        <translation>데이터를 가져올 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute query</source>
        <translation>쿼리를 실행할 수 없음</translation>
    </message>
    <message>
        <source>Unable to store result</source>
        <translation>결과를 저장할 수 없음</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation>구문을 준비할 수 없음</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation>구문을 초기화할 수 없음</translation>
    </message>
    <message>
        <source>Unable to bind value</source>
        <translation>값을 바인딩할 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation>구문을 실행할 수 없음</translation>
    </message>
    <message>
        <source>Unable to bind outvalues</source>
        <translation>outvalue를 바인딩할 수 없음</translation>
    </message>
    <message>
        <source>Unable to store statement results</source>
        <translation>구문 실행 결과를 저장할 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute next query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to store next result</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <source>(Untitled)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>최소화</translation>
    </message>
    <message>
        <source>Restore Down</source>
        <translation>복원</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation>복원(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation>이동(&amp;M)</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation>크기(&amp;S)</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation>최소화(&amp;N)</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation>최대화(&amp;N)</translation>
    </message>
    <message>
        <source>Stay on &amp;Top</source>
        <translation>항상 위(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>닫기(&amp;C)</translation>
    </message>
    <message>
        <source>- [%1]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="unfinished">최대화</translation>
    </message>
    <message>
        <source>Unshade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="unfinished">도움말</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="unfinished">메뉴</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>열기</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation>실행</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>확인</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>Qt 정보</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation>&lt;p&gt;이 프로그램은 Qt 버전 %1을(를) 사용합니다.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Show Details...</source>
        <translation>자세한 정보 보기...</translation>
    </message>
    <message>
        <source>Hide Details...</source>
        <translation>자세한 정보 숨기기...</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://www.trolltech.com/company/model/&quot;&gt;www.trolltech.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation>&lt;p&gt;이 프로그램은 Qt 오픈소스 에디션 버전 %1을(를) 사용합니다.&lt;/p&gt;&lt;p&gt;Qt 오픈소스 에디션은 오픈소스 프로그램의 개발을 위한 것입니다. 상용(비공개 소스) 프로그램을 개발하고 싶다면 상용 Qt 라이선스를 구입하십시오.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://www.trolltech.com/company/model&quot;&gt;www.trolltech.com/company/model/&lt;/a&gt; 페이지를 방문하셔서 Qt 라이선스 정책에 대해서 알아보십시오.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://www.trolltech.com/qt/&quot;&gt;www.trolltech.com/qt/&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <source>Select IM</source>
        <translation>입력기 선택</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <source>Multiple input method switcher</source>
        <translation>다중 입력기 전환기</translation>
    </message>
    <message>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation>텍스트 위젯의 컨텍스트 메뉴를 사용하는 다중 입력기 전환기</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <source>The remote host closed the connection</source>
        <translation>원격 호스트에서 연결을 닫음</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation>네트워크 작업 시간 초과</translation>
    </message>
    <message>
        <source>Out of resources</source>
        <translation>자원 부족</translation>
    </message>
    <message>
        <source>Unsupported socket operation</source>
        <translation>지원하지 않는 소켓 작업</translation>
    </message>
    <message>
        <source>Protocol type not supported</source>
        <translation>지원하지 않는 프로토콜 종류</translation>
    </message>
    <message>
        <source>Invalid socket descriptor</source>
        <translation>잘못된 소켓 설명자</translation>
    </message>
    <message>
        <source>Network unreachable</source>
        <translation>네트워크에 접근할 수 없음</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation>권한이 거부됨</translation>
    </message>
    <message>
        <source>Connection timed out</source>
        <translation>연결 시간 초과됨</translation>
    </message>
    <message>
        <source>Connection refused</source>
        <translation>연결이 거부됨</translation>
    </message>
    <message>
        <source>The bound address is already in use</source>
        <translation>지정한 주소가 이미 사용 중</translation>
    </message>
    <message>
        <source>The address is not available</source>
        <translation>주소를 사용할 수 없음</translation>
    </message>
    <message>
        <source>The address is protected</source>
        <translation>주소가 보호되어 있음</translation>
    </message>
    <message>
        <source>Unable to send a message</source>
        <translation>메시지를 보낼 수 없음</translation>
    </message>
    <message>
        <source>Unable to receive a message</source>
        <translation>메시지를 받을 수 없음</translation>
    </message>
    <message>
        <source>Unable to write</source>
        <translation>쓸 수 없음</translation>
    </message>
    <message>
        <source>Network error</source>
        <translation>네트워크 오류</translation>
    </message>
    <message>
        <source>Another socket is already listening on the same port</source>
        <translation>다른 소켓이 지정한 포트에서 듣고 있음</translation>
    </message>
    <message>
        <source>Unable to initialize non-blocking socket</source>
        <translation>논블러킹 소켓을 초기화할 수 없음</translation>
    </message>
    <message>
        <source>Unable to initialize broadcast socket</source>
        <translation>브로드캐스트 소켓을 초기화할 수 없음</translation>
    </message>
    <message>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>IPv6을 지원하지 않는 플랫폼에서 IPv6 소켓을 사용하려고 시도함</translation>
    </message>
    <message>
        <source>Host unreachable</source>
        <translation>호스트에 접근할 수 없음</translation>
    </message>
    <message>
        <source>Datagram was too large to send</source>
        <translation>한 번에 보낼 데이터그램이 너무 큼</translation>
    </message>
    <message>
        <source>Operation on non-socket</source>
        <translation>비 소켓에서 작업 실행됨</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <source>The proxy type is invalid for this operation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <source>Request for opening non-local file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error opening %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Write error writing to %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot open %1: Path is a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Read error reading from %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <source>Cannot open %1: is a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logging in to %1 failed: authentication required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error while downloading %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error while uploading %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <source>Error downloading %1 - server replied: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <source>Operation canceled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <source>Unable to logon</source>
        <translation>로그온할 수 없음</translation>
    </message>
    <message>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation>초기화할 수 없음</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation type="unfinished">트랜잭션을 시작할 수 없음</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation type="unfinished">트랜잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="unfinished">트랜잭션을 되돌릴 수 없음</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <source>Unable to bind column for batch execute</source>
        <translation>배치 실행의 열을 바인딩할 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute batch statement</source>
        <translation>배치 구문을 실행할 수 없음</translation>
    </message>
    <message>
        <source>Unable to goto next</source>
        <translation>다음으로 이동할 수 없음</translation>
    </message>
    <message>
        <source>Unable to alloc statement</source>
        <translation>구문을 할당할 수 없음</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation>구문을 준비할 수 없음</translation>
    </message>
    <message>
        <source>Unable to bind value</source>
        <translation>값을 바인딩할 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation>SELECT 구문을 실행할 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation>구문을 실행할 수 없음</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <source>Unable to connect</source>
        <translation>연결할 수 없음</translation>
    </message>
    <message>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation>연결할 수 없음 - 드라이버가 모든 필요한 기능을 제공하지 않습니다</translation>
    </message>
    <message>
        <source>Unable to disable autocommit</source>
        <translation>자동 커밋을 해제할 수 없음</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation>트랜잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation>트랜잭션을 되돌릴 수 없음</translation>
    </message>
    <message>
        <source>Unable to enable autocommit</source>
        <translation>자동 커밋을 설정할 수 없음</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation>QODBCResult::reset: &apos;SQL_CURSOR_STATIC&apos;을 구문 속성으로 설정할 수 없음. ODBC 드라이버의 설정을 확인하십시오</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation>구문을 실행할 수 없음</translation>
    </message>
    <message>
        <source>Unable to fetch next</source>
        <translation>다음 항목을 가져올 수 없음</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation>구문을 준비할 수 없음</translation>
    </message>
    <message>
        <source>Unable to bind variable</source>
        <translation>변수를 바인딩할 수 없음</translation>
    </message>
    <message>
        <source>Unable to fetch last</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to fetch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to fetch first</source>
        <translation type="unfinished">이전 항목을 가져올 수 없음</translation>
    </message>
    <message>
        <source>Unable to fetch previous</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Operation not supported on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid URI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Write error writing to %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Read error reading from %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Socket error on %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Protocol error: packet of size 0 received</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <source>Name</source>
        <translation type="unfinished">이름</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="unfinished">값</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <source>Unable to connect</source>
        <translation>연결할 수 없음</translation>
    </message>
    <message>
        <source>Could not begin transaction</source>
        <translation>트랙잭션을 시작할 수 없음</translation>
    </message>
    <message>
        <source>Could not commit transaction</source>
        <translation>트랙잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Could not rollback transaction</source>
        <translation>트랙잭션을 되돌릴 수 없음</translation>
    </message>
    <message>
        <source>Unable to subscribe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to unsubscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <source>Unable to create query</source>
        <translation>쿼리를 만들 수 없음</translation>
    </message>
    <message>
        <source>Unable to prepare statement</source>
        <translation type="unfinished">구문을 준비할 수 없음</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <source>Centimeters (cm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Millimeters (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inches (in)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Points (pt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Page size:</source>
        <translation type="unfinished">쪽 크기:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paper source:</source>
        <translation type="unfinished">종이 공급:</translation>
    </message>
    <message>
        <source>Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Portrait</source>
        <translation type="unfinished">세로</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="unfinished">가로</translation>
    </message>
    <message>
        <source>Reverse landscape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reverse portrait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Margins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>top margin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>left margin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>right margin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bottom margin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <source>Unknown error</source>
        <translation>알 수 없는 오류</translation>
    </message>
    <message>
        <source>The plugin was not loaded.</source>
        <translation>플러그인을 불러오지 못했습니다.</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>locally connected</source>
        <translation>지역적으로 연결됨</translation>
    </message>
    <message>
        <source>Aliases: %1</source>
        <translation>별명: %1</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation>알 수 없음</translation>
    </message>
    <message>
        <source>Print To File ...</source>
        <translation>파일로 인쇄...</translation>
    </message>
    <message>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>파일 %1에 쓸 수 없습니다.
다른 파일 이름을 선택하십시오.</translation>
    </message>
    <message>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>%1이(가) 이미 존재합니다.
덮어쓰시겠습니까?</translation>
    </message>
    <message>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>%1은(는) 디렉터리입니다.
다른 파일 이름을 선택하십시오.</translation>
    </message>
    <message>
        <source>A0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C5E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Executive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Folio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ledger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Legal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tabloid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>US Common #10 Envelope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Options &gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Options &lt;&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print to File (PDF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print to File (Postscript)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Local file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Write %1 file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <source>Page Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>First page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fit width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fit page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Portrait</source>
        <translation type="unfinished">세로</translation>
    </message>
    <message>
        <source>Landscape</source>
        <translation type="unfinished">가로</translation>
    </message>
    <message>
        <source>Show single page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show facing pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show overview of all pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="unfinished">Print</translation>
    </message>
    <message>
        <source>Page setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished">닫기</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copies</source>
        <translation type="unfinished">여러 부 인쇄</translation>
    </message>
    <message>
        <source>Print range</source>
        <translation type="unfinished">인쇄 범위</translation>
    </message>
    <message>
        <source>Print all</source>
        <translation type="unfinished">모두 인쇄</translation>
    </message>
    <message>
        <source>Pages from</source>
        <translation type="unfinished">시작 쪽</translation>
    </message>
    <message>
        <source>to</source>
        <translation type="unfinished">끝 쪽</translation>
    </message>
    <message>
        <source>Selection</source>
        <translation type="unfinished">선택</translation>
    </message>
    <message>
        <source>Output Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copies:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Collate</source>
        <translation type="unfinished">한 부씩 인쇄</translation>
    </message>
    <message>
        <source>Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished">옵션</translation>
    </message>
    <message>
        <source>Color Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Grayscale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Duplex Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Long side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Short side</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printer</source>
        <translation type="unfinished">프린터</translation>
    </message>
    <message>
        <source>&amp;Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Output &amp;file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <source>Open</source>
        <translation>열기</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <source>Check</source>
        <translation>선택</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <source>no error occurred</source>
        <translation>오류 없음</translation>
    </message>
    <message>
        <source>disabled feature used</source>
        <translation>비활성화된 기능 사용됨</translation>
    </message>
    <message>
        <source>bad char class syntax</source>
        <translation>잘못된 문자열 클래스 문법</translation>
    </message>
    <message>
        <source>bad lookahead syntax</source>
        <translation>잘못된 룩어헤드 문법</translation>
    </message>
    <message>
        <source>bad repetition syntax</source>
        <translation>잘못된 반복 문법</translation>
    </message>
    <message>
        <source>invalid octal value</source>
        <translation>잘못된 8진 값</translation>
    </message>
    <message>
        <source>missing left delim</source>
        <translation>왼쪽 구분자 없음</translation>
    </message>
    <message>
        <source>unexpected end</source>
        <translation>예상하지 못한 끝</translation>
    </message>
    <message>
        <source>met internal limit</source>
        <translation>내부 한계에 도달함</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <source>Error to open database</source>
        <translation>데이터베이스를 여는 중 오류 발생</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation>트랜잭션을 시작할 수 없음</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation>트랜잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Unable to rollback Transaction</source>
        <translation>트랜잭션을 되돌릴 수 없음</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <source>Unable to fetch results</source>
        <translation>결과를 가져올 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation>구문을 실행할 수 없음</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <source>Error opening database</source>
        <translation>데이터베이스를 여는 중 오류 발생</translation>
    </message>
    <message>
        <source>Error closing database</source>
        <translation>데이터베이스를 닫는 중 오류 발생</translation>
    </message>
    <message>
        <source>Unable to begin transaction</source>
        <translation>트랜잭션을 시작할 수 없음</translation>
    </message>
    <message>
        <source>Unable to commit transaction</source>
        <translation>트랜잭션을 커밋할 수 없음</translation>
    </message>
    <message>
        <source>Unable to rollback transaction</source>
        <translation type="unfinished">트랜잭션을 되돌릴 수 없음</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <source>Unable to fetch row</source>
        <translation>열을 가져올 수 없음</translation>
    </message>
    <message>
        <source>Unable to execute statement</source>
        <translation>구문을 실행할 수 없음</translation>
    </message>
    <message>
        <source>Unable to reset statement</source>
        <translation>구문을 초기화할 수 없음</translation>
    </message>
    <message>
        <source>Unable to bind parameters</source>
        <translation>인자를 바인딩할 수 없음</translation>
    </message>
    <message>
        <source>Parameter count mismatch</source>
        <translation>인자 수가 일치하지 않음</translation>
    </message>
    <message>
        <source>No query</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <source>Scroll here</source>
        <translation>여기로 스크롤</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation>왼쪽 경계</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>맨 위</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation>오른쪽 경계</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>맨 아래</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation>왼쪽 페이지</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation>위쪽 페이지</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation>오른쪽 페이지</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation>아래쪽 페이지</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation>왼쪽으로 스크롤</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation>위로 스크롤</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation>오른쪽으로 스크롤</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation>아래로 스크롤</translation>
    </message>
    <message>
        <source>Line up</source>
        <translation>한 줄 위로</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>위치</translation>
    </message>
    <message>
        <source>Line down</source>
        <translation>한 줄 아래로</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <source>%1: unable to set key on lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: create size is less then 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: unable to lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: unable to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: permission denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: key is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: unix key file doesn&apos;t exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: ftok failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: unable to make key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: system-imposed size restrictions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1: not attached</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <source>Space</source>
        <translation>Space</translation>
    </message>
    <message>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <source>Backtab</source>
        <translation>Backtab</translation>
    </message>
    <message>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <source>Return</source>
        <translation>Return</translation>
    </message>
    <message>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <source>SysReq</source>
        <translation>SysReq</translation>
    </message>
    <message>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>왼쪽</translation>
    </message>
    <message>
        <source>Up</source>
        <translation>위</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>오른쪽</translation>
    </message>
    <message>
        <source>Down</source>
        <translation>아래</translation>
    </message>
    <message>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
    <message>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>메뉴</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>뒤로</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>앞으로</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>정지</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>새로 고침</translation>
    </message>
    <message>
        <source>Volume Down</source>
        <translation>음량 감소</translation>
    </message>
    <message>
        <source>Volume Mute</source>
        <translation>음소거</translation>
    </message>
    <message>
        <source>Volume Up</source>
        <translation>음량 증가</translation>
    </message>
    <message>
        <source>Bass Boost</source>
        <translation>저음 강조</translation>
    </message>
    <message>
        <source>Bass Up</source>
        <translation>저음 증가</translation>
    </message>
    <message>
        <source>Bass Down</source>
        <translation>저음 감소</translation>
    </message>
    <message>
        <source>Treble Up</source>
        <translation>고음 증가</translation>
    </message>
    <message>
        <source>Treble Down</source>
        <translation>고음 감소</translation>
    </message>
    <message>
        <source>Media Play</source>
        <translation>미디어 재생</translation>
    </message>
    <message>
        <source>Media Stop</source>
        <translation>미디어 정지</translation>
    </message>
    <message>
        <source>Media Previous</source>
        <translation>미디어 이전</translation>
    </message>
    <message>
        <source>Media Next</source>
        <translation>미디어 다음</translation>
    </message>
    <message>
        <source>Media Record</source>
        <translation>미디어 녹음</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>즐겨찾기</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>검색</translation>
    </message>
    <message>
        <source>Standby</source>
        <translation>대기 모드</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation>URL 열기</translation>
    </message>
    <message>
        <source>Launch Mail</source>
        <translation>메일 실행</translation>
    </message>
    <message>
        <source>Launch Media</source>
        <translation>미디어 실행</translation>
    </message>
    <message>
        <source>Launch (0)</source>
        <translation>(0) 실행</translation>
    </message>
    <message>
        <source>Launch (1)</source>
        <translation>(1) 실행</translation>
    </message>
    <message>
        <source>Launch (2)</source>
        <translation>(2) 실행</translation>
    </message>
    <message>
        <source>Launch (3)</source>
        <translation>(3) 실행</translation>
    </message>
    <message>
        <source>Launch (4)</source>
        <translation>(4) 실행</translation>
    </message>
    <message>
        <source>Launch (5)</source>
        <translation>(5) 실행</translation>
    </message>
    <message>
        <source>Launch (6)</source>
        <translation>(6) 실행</translation>
    </message>
    <message>
        <source>Launch (7)</source>
        <translation>(7) 실행</translation>
    </message>
    <message>
        <source>Launch (8)</source>
        <translation>(8) 실행</translation>
    </message>
    <message>
        <source>Launch (9)</source>
        <translation>(9) 실행</translation>
    </message>
    <message>
        <source>Launch (A)</source>
        <translation>(A) 실행</translation>
    </message>
    <message>
        <source>Launch (B)</source>
        <translation>(B) 실행</translation>
    </message>
    <message>
        <source>Launch (C)</source>
        <translation>(C) 실행</translation>
    </message>
    <message>
        <source>Launch (D)</source>
        <translation>(D) 실행</translation>
    </message>
    <message>
        <source>Launch (E)</source>
        <translation>(E) 실행</translation>
    </message>
    <message>
        <source>Launch (F)</source>
        <translation>(F) 실행</translation>
    </message>
    <message>
        <source>Print Screen</source>
        <translation>Print Screen</translation>
    </message>
    <message>
        <source>Page Up</source>
        <translation>Page Up</translation>
    </message>
    <message>
        <source>Page Down</source>
        <translation>Page Down</translation>
    </message>
    <message>
        <source>Caps Lock</source>
        <translation>Caps Lock</translation>
    </message>
    <message>
        <source>Num Lock</source>
        <translation>Num Lock</translation>
    </message>
    <message>
        <source>Number Lock</source>
        <translation>Number Lock</translation>
    </message>
    <message>
        <source>Scroll Lock</source>
        <translation>Scroll Lock</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation>Insert</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <source>Escape</source>
        <translation>Escape</translation>
    </message>
    <message>
        <source>System Request</source>
        <translation>System Request</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>선택</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>예</translation>
    </message>
    <message>
        <source>No</source>
        <translation>아니오</translation>
    </message>
    <message>
        <source>Context1</source>
        <translation>Context1</translation>
    </message>
    <message>
        <source>Context2</source>
        <translation>Context2</translation>
    </message>
    <message>
        <source>Context3</source>
        <translation>Context3</translation>
    </message>
    <message>
        <source>Context4</source>
        <translation>Context4</translation>
    </message>
    <message>
        <source>Call</source>
        <translation>호출</translation>
    </message>
    <message>
        <source>Hangup</source>
        <translation>끊기</translation>
    </message>
    <message>
        <source>Flip</source>
        <translation>뒤집기</translation>
    </message>
    <message>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <source>Meta</source>
        <translation>Meta</translation>
    </message>
    <message>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <source>F%1</source>
        <translation>F%1</translation>
    </message>
    <message>
        <source>Home Page</source>
        <translation>홈 페이지</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <source>Page left</source>
        <translation>왼쪽 페이지</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation>위쪽 페이지</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>위치</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation>오른쪽 페이지</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation>아래쪽 페이지</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation>SOCKS 서버에 연결하는 중 SOCKS5 시간 초과 오류</translation>
    </message>
    <message>
        <source>Network operation timed out</source>
        <translation type="unfinished">네트워크 작업 시간 초과</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <source>More</source>
        <translation>더 보기</translation>
    </message>
    <message>
        <source>Less</source>
        <translation>덜 보기</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <source>Delete</source>
        <translation>삭제</translation>
    </message>
    <message>
        <source>Delete this record?</source>
        <translation>이 레코드를 삭제하시겠습니까?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>예</translation>
    </message>
    <message>
        <source>No</source>
        <translation>아니오</translation>
    </message>
    <message>
        <source>Insert</source>
        <translation>삽입</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>업데이트</translation>
    </message>
    <message>
        <source>Save edits?</source>
        <translation>편집을 저장하시겠습니까?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>확인</translation>
    </message>
    <message>
        <source>Cancel your edits?</source>
        <translation>편집을 취소하시겠습니까?</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <source>Unable to write data: %1</source>
        <translation>데이터를 쓸 수 없음: %1</translation>
    </message>
    <message>
        <source>Error while reading: %1</source>
        <translation>읽는 중 오류 발생: %1</translation>
    </message>
    <message>
        <source>Error during SSL handshake: %1</source>
        <translation>SSL 악수 중 오류 발생: %1</translation>
    </message>
    <message>
        <source>Error creating SSL context (%1)</source>
        <translation>SSL 컨텍스트를 만드는 중 오류 발생(%1)</translation>
    </message>
    <message>
        <source>Invalid or empty cipher list (%1)</source>
        <translation>잘못되거나 비어 있는 암호화 키 목록 (%1)</translation>
    </message>
    <message>
        <source>Error creating SSL session, %1</source>
        <translation>SSL 세션을 만드는 중 오류 발생, %1</translation>
    </message>
    <message>
        <source>Error creating SSL session: %1</source>
        <translation>SSL 세션을 만드는 중 오류 발생: %1</translation>
    </message>
    <message>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation>키가 없는 인증서를 제공할 수 없음, %1</translation>
    </message>
    <message>
        <source>Error loading local certificate, %1</source>
        <translation>로컬 인증서를 불러올 수 없음, %1</translation>
    </message>
    <message>
        <source>Error loading private key, %1</source>
        <translation>개인 키를 불러올 수 없음, %1</translation>
    </message>
    <message>
        <source>Private key does not certificate public key, %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <source>Unable to open connection</source>
        <translation>연결을 열 수 없음</translation>
    </message>
    <message>
        <source>Unable to use database</source>
        <translation>데이터베이스를 사용할 수 없음</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <source>Scroll Left</source>
        <translation>왼쪽으로 스크롤</translation>
    </message>
    <message>
        <source>Scroll Right</source>
        <translation>오른쪽으로 스크롤</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation>실행 취소(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>다시 실행(&amp;R)</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>잘라내기(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>복사(&amp;C)</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation>링크 주소 복사(&amp;L)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>붙여넣기(&amp;P)</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>삭제</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>모두 선택</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <source>Press</source>
        <translation>누름</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>열기</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <source>This platform does not support IPv6</source>
        <translation>이 플랫폼에서는 IPv6을 지원하지 않습니다</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <source>Undo</source>
        <translation>실행 취소</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>다시 실행</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <source>&lt;empty&gt;</source>
        <translation>&lt;비어 있음&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <source>Undo</source>
        <translation>실행 취소</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>다시 실행</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <source>LRM Left-to-right mark</source>
        <translation>LRM 왼쪽에서 오른쪽 기호</translation>
    </message>
    <message>
        <source>RLM Right-to-left mark</source>
        <translation>RLM 오른쪽에서 왼쪽 기호</translation>
    </message>
    <message>
        <source>ZWJ Zero width joiner</source>
        <translation>ZWJ 폭이 0인 결합자</translation>
    </message>
    <message>
        <source>ZWNJ Zero width non-joiner</source>
        <translation>ZWNJ 폭이 0인 비결합자</translation>
    </message>
    <message>
        <source>ZWSP Zero width space</source>
        <translation>ZWSP 폭이 0인 공백</translation>
    </message>
    <message>
        <source>LRE Start of left-to-right embedding</source>
        <translation>LRE 왼쪽에서 오른쪽 임베딩 시작</translation>
    </message>
    <message>
        <source>RLE Start of right-to-left embedding</source>
        <translation>RLE 오른쪽에서 왼쪽 임베딩 시작</translation>
    </message>
    <message>
        <source>LRO Start of left-to-right override</source>
        <translation>LRO 왼쪽에서 오른쪽 재정의 시작</translation>
    </message>
    <message>
        <source>RLO Start of right-to-left override</source>
        <translation>RLO 오른쪽에서 왼쪽 재정의 시작</translation>
    </message>
    <message>
        <source>PDF Pop directional formatting</source>
        <translation>PDF Pop 방향 포매팅</translation>
    </message>
    <message>
        <source>Insert Unicode control character</source>
        <translation>유니코드 제어 문자 삽입</translation>
    </message>
</context>
<context>
    <name>QWebFrame</name>
    <message>
        <source>Request cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Request blocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot show URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame load interruped by policy change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot show mimetype</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File does not exist</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <source>Bad HTTP request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Submit</source>
        <comment>default label for Submit buttons in forms on web pages</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Submit</source>
        <comment>Submit (input element) alt text for &lt;input&gt; elements with no alt, title, or value</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation type="unfinished">초기화</translation>
    </message>
    <message>
        <source>This is a searchable index. Enter search keywords: </source>
        <comment>text that appears at the start of nearly-obsolete web pages in the form of a &apos;searchable index&apos;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose File</source>
        <comment>title for file button used in HTML forms</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No file selected</source>
        <comment>text to display in file button used in HTML forms when no file is selected</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open in New Window</source>
        <comment>Open in New Window context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Link...</source>
        <comment>Download Linked File context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy Link</source>
        <comment>Copy Link context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Image</source>
        <comment>Open Image in New Window context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Image</source>
        <comment>Download Image context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy Image</source>
        <comment>Copy Link context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Frame</source>
        <comment>Open Frame in New Window context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy</source>
        <comment>Copy context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Go Back</source>
        <comment>Back context menu item</comment>
        <translation type="unfinished">뒤로 가기</translation>
    </message>
    <message>
        <source>Go Forward</source>
        <comment>Forward context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation type="unfinished">정지</translation>
    </message>
    <message>
        <source>Reload</source>
        <comment>Reload context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cut</source>
        <comment>Cut context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste</source>
        <comment>Paste context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Guesses Found</source>
        <comment>No Guesses Found context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation type="unfinished">무시</translation>
    </message>
    <message>
        <source>Add To Dictionary</source>
        <comment>Learn Spelling context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search The Web</source>
        <comment>Search The Web context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Look Up In Dictionary</source>
        <comment>Look Up in Dictionary context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Link</source>
        <comment>Open Link context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation type="unfinished">무시</translation>
    </message>
    <message>
        <source>Spelling</source>
        <comment>Spelling and Grammar context sub-menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check Spelling</source>
        <comment>Check spelling context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check Spelling While Typing</source>
        <comment>Check spelling while typing context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check Grammar With Spelling</source>
        <comment>Check grammar with spelling context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fonts</source>
        <comment>Font context sub-menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bold</source>
        <comment>Bold context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italic</source>
        <comment>Italic context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Underline</source>
        <comment>Underline context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outline</source>
        <comment>Outline context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Direction</source>
        <comment>Writing direction context sub-menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <comment>Default writing direction context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>LTR</source>
        <comment>Left to Right context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>RTL</source>
        <comment>Right to Left context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inspect</source>
        <comment>Inspect Element context menu item</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No recent searches</source>
        <comment>Label for only item in menu that appears when clicking on the search field image, when no searches have been performed</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recent searches</source>
        <comment>label for first item in the menu that appears when clicking on the search field image, used as embedded menu title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear recent searches</source>
        <comment>menu item in Recent Searches menu that empties menu&apos;s contents</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation type="unfinished">알 수 없음</translation>
    </message>
    <message>
        <source>%1 (%2x%3 pixels)</source>
        <comment>Title string for images</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Web Inspector - %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <source>What&apos;s This?</source>
        <translation>이것에 대한 설명</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <source>Go Back</source>
        <translation>뒤로 가기</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>계속</translation>
    </message>
    <message>
        <source>Commit</source>
        <translation>커밋</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>완료</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>종료</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>도움말</translation>
    </message>
    <message>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 이전(&amp;B)</translation>
    </message>
    <message>
        <source>&amp;Finish</source>
        <translation>완료(&amp;F)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>취소</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>도움말(&amp;H)</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Next &gt;</source>
        <translation type="unfinished">다음 (&amp;N) &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <source>&amp;Restore</source>
        <translation>복원(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation>이동(&amp;M)</translation>
    </message>
    <message>
        <source>&amp;Size</source>
        <translation>크기 조정(&amp;S)</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation>최소화(&amp;N)</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation>최대화(&amp;N)</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>닫기(&amp;C)</translation>
    </message>
    <message>
        <source>Stay on &amp;Top</source>
        <translation>항상 위(&amp;T)</translation>
    </message>
    <message>
        <source>Sh&amp;ade</source>
        <translation>말아 올리기(&amp;A)</translation>
    </message>
    <message>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>최소화</translation>
    </message>
    <message>
        <source>Restore Down</source>
        <translation>복원</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>닫기</translation>
    </message>
    <message>
        <source>&amp;Unshade</source>
        <translation>풀어 내리기(&amp;U)</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <source>no error occurred</source>
        <translation>오류 없음</translation>
    </message>
    <message>
        <source>error triggered by consumer</source>
        <translation>사용자가 오류를 발생시킴</translation>
    </message>
    <message>
        <source>unexpected end of file</source>
        <translation>예상하지 못한 파일의 끝</translation>
    </message>
    <message>
        <source>more than one document type definition</source>
        <translation>하나 이상의 문서 종류 정의가 있음</translation>
    </message>
    <message>
        <source>error occurred while parsing element</source>
        <translation>원소를 처리하는 중 오류 발생</translation>
    </message>
    <message>
        <source>tag mismatch</source>
        <translation>태그가 일치하지 않음</translation>
    </message>
    <message>
        <source>error occurred while parsing content</source>
        <translation>내용을 처리하는 중 오류 발생</translation>
    </message>
    <message>
        <source>unexpected character</source>
        <translation>예상하지 못한 글자</translation>
    </message>
    <message>
        <source>invalid name for processing instruction</source>
        <translation>잘못된 이름이나 처리 방법</translation>
    </message>
    <message>
        <source>version expected while reading the XML declaration</source>
        <translation>XML 선언을 읽는 중 버전이 필요함</translation>
    </message>
    <message>
        <source>wrong value for standalone declaration</source>
        <translation>standalone 선언의 값이 잘못됨</translation>
    </message>
    <message>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation>XML 선언을 읽는 중 인코딩이나 standard 선언이 필요함</translation>
    </message>
    <message>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation>XML 선언을 읽는 중 standara 선언이 필요함</translation>
    </message>
    <message>
        <source>error occurred while parsing document type definition</source>
        <translation>문서 종류 정의를 처리하는 중 오류 발생</translation>
    </message>
    <message>
        <source>letter is expected</source>
        <translation>글자가 필요함</translation>
    </message>
    <message>
        <source>error occurred while parsing comment</source>
        <translation>주석을 처리하는 중 오류 발생</translation>
    </message>
    <message>
        <source>error occurred while parsing reference</source>
        <translation>참조를 처리하는 중 오류 발생</translation>
    </message>
    <message>
        <source>internal general entity reference not allowed in DTD</source>
        <translation>DTD에서 내부 일반 엔티티 참조를 사용할 수 없음</translation>
    </message>
    <message>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation>속성 값에는 외부에서 처리한 일반 엔티티 참조를 사용할 수 없음</translation>
    </message>
    <message>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation>DTD에서 외부에서 처리한 일반 엔티티 참조를 사용할 수 없음</translation>
    </message>
    <message>
        <source>unparsed entity reference in wrong context</source>
        <translation>잘못된 컨텍스트에 처리되지 않은 엔티티 참조가 있음</translation>
    </message>
    <message>
        <source>recursive entities</source>
        <translation>재귀적 엔티티</translation>
    </message>
    <message>
        <source>error in the text declaration of an external entity</source>
        <translation>외부 엔티티 텍스트 선언에 오류가 있음</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <source>Extra content at end of document.</source>
        <translation>문서의 끝에 내용이 더 있습니다.</translation>
    </message>
    <message>
        <source>Invalid entity value.</source>
        <translation>엔티티 값이 잘못되었습니다.</translation>
    </message>
    <message>
        <source>Invalid XML character.</source>
        <translation>XML 글자가 잘못되었습니다.</translation>
    </message>
    <message>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>내용에 문자열 &apos;]]&gt;&apos;가 올 수 없습니다.</translation>
    </message>
    <message>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>네임스페이스 접두사 &apos;%1&apos;이(가) 선언되지 않았음</translation>
    </message>
    <message>
        <source>Attribute redefined.</source>
        <translation>속성이 재정의되었습니다.</translation>
    </message>
    <message>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>공개 ID 리터럴에 예상하지 못한 문자 &apos;%1&apos;이(가) 있습니다.</translation>
    </message>
    <message>
        <source>Invalid XML version string.</source>
        <translation>XML 버전 문자열이 잘못되었습니다.</translation>
    </message>
    <message>
        <source>Unsupported XML version.</source>
        <translation>지원하지 않는 XML 버전입니다.</translation>
    </message>
    <message>
        <source>%1 is an invalid encoding name.</source>
        <translation>인코딩 이름 %1은(는) 잘못되었습니다.</translation>
    </message>
    <message>
        <source>Encoding %1 is unsupported</source>
        <translation>인코딩 %1은(는) 지원되지 않습니다</translation>
    </message>
    <message>
        <source>Standalone accepts only yes or no.</source>
        <translation>Standalone은 예나 아니오만 지원합니다.</translation>
    </message>
    <message>
        <source>Invalid attribute in XML declaration.</source>
        <translation>XML 선언에서 속성이 잘못되었습니다.</translation>
    </message>
    <message>
        <source>Premature end of document.</source>
        <translation>문서가 완전하지 못하게 끝났습니다.</translation>
    </message>
    <message>
        <source>Invalid document.</source>
        <translation>문서가 잘못되었습니다.</translation>
    </message>
    <message>
        <source>Expected </source>
        <translation>다음을 예상했지만</translation>
    </message>
    <message>
        <source>, but got &apos;</source>
        <translation>, 돌아온 것은 &apos;</translation>
    </message>
    <message>
        <source>Unexpected &apos;</source>
        <translation>예상하지 못한 &apos;</translation>
    </message>
    <message>
        <source>Expected character data.</source>
        <translation>예상하지 못한 문자열 데이터입니다.</translation>
    </message>
    <message>
        <source>Recursive entity detected.</source>
        <translation>재귀적 엔트리가 감지되었습니다.</translation>
    </message>
    <message>
        <source>Start tag expected.</source>
        <translation>시작 태그가 필요합니다.</translation>
    </message>
    <message>
        <source>XML declaration not at start of document.</source>
        <translation>XML 선언이 문서 시작에 없습니다.</translation>
    </message>
    <message>
        <source>NDATA in parameter entity declaration.</source>
        <translation>인자 엔티티 선언에 NDATA가 있습니다.</translation>
    </message>
    <message>
        <source>%1 is an invalid processing instruction name.</source>
        <translation>%1은(는) 잘못된 처리 방법 이름입니다.</translation>
    </message>
    <message>
        <source>Invalid processing instruction name.</source>
        <translation>잘못된 처리 방법 이름입니다.</translation>
    </message>
    <message>
        <source>Illegal namespace declaration.</source>
        <translation>네임스페이스 선언이 잘못되었습니다.</translation>
    </message>
    <message>
        <source>Invalid XML name.</source>
        <translation>XML 이름이 잘못되었습니다.</translation>
    </message>
    <message>
        <source>Opening and ending tag mismatch.</source>
        <translation>여는 태그와 닫는 태그가 일치하지 않습니다.</translation>
    </message>
    <message>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation>처리되지 않은 엔티티 &apos;%1&apos;을(를) 참고합니다.</translation>
    </message>
    <message>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>엔티티 &apos;%1&apos;이(가) 선언되지 않았습니다.</translation>
    </message>
    <message>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation>속성 값에서 외부 엔티티 &apos;%1&apos;을(를) 참조하고 있습니다.</translation>
    </message>
    <message>
        <source>Invalid character reference.</source>
        <translation>잘못된 문자 참조입니다.</translation>
    </message>
    <message>
        <source>Encountered incorrectly encoded content.</source>
        <translation>잘못 인코딩된 내용을 만났습니다.</translation>
    </message>
    <message>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation>standalone 의사 속성은 인코딩 다음에 와야 합니다.</translation>
    </message>
    <message>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation>%1은(는) 잘못된 PUBLIC 식별자입니다.</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <source>An %1-attribute with value %2 has already been declared.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An %1-attribute must have a valid %2 as value, which %3 isn&apos;t.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Network timeout.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Day %1 is invalid for month %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>At least one component must be present.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No operand in an integer division, %1, can be %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The first operand in an integer division, %1, cannot be infinity (%2).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The second operand in a division, %1, cannot be zero (%2).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not a valid value of type %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not valid as a value of type %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No casting is possible with %1 as the target type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It is not possible to cast from %1 to %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Casting to %1 is not possible because it is an abstract type, and can therefore never be instantiated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It&apos;s not possible to cast the value %1 of type %2 to %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failure when casting from %1 to %2: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A comment cannot contain %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A comment cannot end with a %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No comparisons can be done involving the type %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Operator %1 is not available between atomic values of type %2 and %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, is %2 invalid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No namespace binding exists for the prefix %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is an invalid %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The first argument to %1 cannot be of type %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 was called.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 matches newline characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Matches are case insensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It will not be possible to retrieve %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The default collection is undefined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 cannot be retrieved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not a whole number of minutes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The item %1 did not match the required type %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is an unknown schema type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The initialization of variable %1 depends on itself</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No variable by name %1 exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The variable %1 is unused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No function with signature %1 is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only the prefix %1 can be declared to bind the namespace %2. By default, it is already bound to the prefix %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The module import feature is not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A variable by name %1 has already been declared in the prolog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No value is available for the external variable by name %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1 which exists for cases like this)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A function already exists with the signature %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An argument by name %1 has already been declared. Every argument name must be unique.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not a valid numeric literal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No function by name %1 is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is an invalid namespace URI.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It is not possible to bind to the prefix %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An attribute by name %1 has already appeared on this element.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The name %1 does not refer to any schema type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not a valid name for a processing-instruction. Therefore this name test will never match.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>zero or one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>exactly one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>one or more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>zero or more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Required type is %1, but %2 was found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The focus is undefined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An attribute by name %1 has already been created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <source>Muted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WebCore::PlatformScrollbar</name>
    <message>
        <source>Scroll here</source>
        <translation type="unfinished">여기로 스크롤</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="unfinished">왼쪽 경계</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="unfinished">맨 위</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="unfinished">오른쪽 경계</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="unfinished">맨 아래</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="unfinished">왼쪽 페이지</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="unfinished">위쪽 페이지</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="unfinished">오른쪽 페이지</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="unfinished">아래쪽 페이지</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="unfinished">왼쪽으로 스크롤</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="unfinished">위로 스크롤</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="unfinished">오른쪽으로 스크롤</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="unfinished">아래로 스크롤</translation>
    </message>
</context>
</TS>
