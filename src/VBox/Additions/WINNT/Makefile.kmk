# $Id$
## @file
# Sub-Makefile for the Windows NT++ Guest Additions.
#

# Copyright (C) 2006-2007 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

SUB_DEPTH = ../../../..
include $(KBUILD_PATH)/subheader.kmk

# Include Sub-Makefiles.
include $(PATH_SUB_CURRENT)/Graphics/Makefile.kmk
include $(PATH_SUB_CURRENT)/i8042prt/Makefile.kmk
include $(PATH_SUB_CURRENT)/MouseFilter/Makefile.kmk
include $(PATH_SUB_CURRENT)/VBoxGuest/Makefile.kmk
include $(PATH_SUB_CURRENT)/VBoxTray/Makefile.kmk
include $(PATH_SUB_CURRENT)/VBoxGINA/Makefile.kmk
include $(PATH_SUB_CURRENT)/VBoxCredProv/Makefile.kmk
include $(PATH_SUB_CURRENT)/VBoxHook/Makefile.kmk
ifndef VBOX_OSE
 include $(PATH_SUB_CURRENT)/Network/Makefile.kmk
endif
ifdef VBOX_WITH_WIN32_ADDITIONS_SHAREDFOLDERS
 include $(PATH_SUB_CURRENT)/SharedFolders/Makefile.kmk
endif
ifneq ($(wildcard $(PATH_SUB_CURRENT)/Installer/Makefile.kmk),)
 include $(PATH_SUB_CURRENT)/Installer/Makefile.kmk
endif

#
# Inf2Cat requires all the files referenced in the .inf file
# to be present in the directory, so we have to do this from here.
#
INSTALLS += VBoxGuest-inf
VBoxGuest-inf_INST = $(INST_ADDITIONS)
VBoxGuest-inf_MODE = a+r,u+w
VBoxGuest-inf_SOURCES = \
	$(PATH_TARGET)/VBoxGuestCat.dir/VBoxGuest.inf
ifdef VBOX_SIGN_ADDITIONS
VBoxGuest-inf_SOURCES += \
	$(PATH_TARGET)/VBoxGuestCat.dir/VBoxGuest.cat \
	$(PATH_TARGET)/VBoxGuestCat.dir/VBoxGuest.sys \
	$(PATH_TARGET)/VBoxGuestCat.dir/VBoxControl.exe \
	$(PATH_TARGET)/VBoxGuestCat.dir/VBoxTray.exe \
	$(PATH_TARGET)/VBoxGuestCat.dir/VBCoInst.dll
endif # signing
VBoxGuest-inf_CLEAN = $(VBoxGuest-inf_SOURCES)
VBoxGuest-inf_BLDDIRS = \
	$(PATH_TARGET)/VBoxGuestCat.dir

$(PATH_TARGET)/VBoxGuestCat.dir/VBoxGuest.inf: $(PATH_SUB_CURRENT)/VBoxGuest/VBoxGuest.inf $(MAKEFILE_CURRENT) | $$(call DIRDEP,$$(@D))
	$(call MSG_GENERATE,VBoxGuest-inf,$@,$<)
	$(call VBOX_EDIT_INF_FN,$<,$@)

$(PATH_TARGET)/VBoxGuestCat.dir/VBoxGuest.sys:   $$(TARGET_VBoxGuest) | $$(call DIRDEP,$$(@D))
	$(INSTALL) -m 644 $< $(@D)

$(PATH_TARGET)/VBoxGuestCat.dir/VBoxControl.exe: $$(TARGET_VBoxControl) | $$(call DIRDEP,$$(@D))
	$(INSTALL) -m 755 $< $(@D)

$(PATH_TARGET)/VBoxGuestCat.dir/VBoxTray.exe:    $$(TARGET_VBoxTray) | $$(call DIRDEP,$$(@D))
	$(INSTALL) -m 755 $< $(@D)

$(PATH_TARGET)/VBoxGuestCat.dir/VBCoInst.dll:    $$(TARGET_VBCoInst) | $$(call DIRDEP,$$(@D))
	$(INSTALL) -m 644 $< $(@D)

$(PATH_TARGET)/VBoxGuestCat.dir/VBoxGuest.cat: \
		$(PATH_TARGET)/VBoxGuestCat.dir/VBoxGuest.inf \
		$(PATH_TARGET)/VBoxGuestCat.dir/VBoxGuest.sys \
		$(PATH_TARGET)/VBoxGuestCat.dir/VBoxControl.exe \
		$(PATH_TARGET)/VBoxGuestCat.dir/VBoxTray.exe \
		$(PATH_TARGET)/VBoxGuestCat.dir/VBCoInst.dll
	$(call MSG_TOOL,Inf2Cat,VBoxGuest-inf,$@,$<)
	$(call VBOX_MAKE_CAT_FN, $(@D),$@)

include $(KBUILD_PATH)/subfooter.kmk

