# $Id$
## @file
# Makefile for the Wine D3D related dlls for VirtualBox.
#

#
# Copyright (C) 2009 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#


SUB_DEPTH = ../../../../../..
include $(KBUILD_PATH)/subheader.kmk

DLLS += libWine

libWine_TEMPLATE     = VBOXGUESTR3NPDLL
libWine_DEFS         = __WINESRC__ WINE_UNICODE_API="" _REENTRANT WINE_NOWINSOCK USE_WIN32_OPENGL __i386__ \
    DLLDIR=\"\" \
    BINDIR=\"\" \
    LIB_TO_BINDIR=\"\" \
    LIB_TO_DLLDIR=\"\" \
    BIN_TO_DLLDIR=\"\" \
    LIB_TO_DATADIR=\"\" \
    BIN_TO_DATADIR=\"\"
libWine_INCS         := $(PATH_SUB_CURRENT)/include
libWine_SOURCES      := \
    libWine/wine.def \
    libWine/casemap.c \
    libWine/collation.c \
    libWine/compose.c \
    libWine/config.c \
    libWine/cptable.c \
    libWine/debug.c \
    libWine/fold.c \
    libWine/ldt.c \
    libWine/loader.c \
    libWine/mbtowc.c \
    libWine/mmap.c \
    libWine/port.c \
    libWine/sortkey.c \
    libWine/string.c \
    libWine/utf8.c \
    libWine/wctomb.c \
    libWine/wctype.c \
    libWine/c_037.c \
    libWine/c_424.c \
    libWine/c_437.c \
    libWine/c_500.c \
    libWine/c_737.c \
    libWine/c_775.c \
    libWine/c_850.c \
    libWine/c_852.c \
    libWine/c_855.c \
    libWine/c_856.c \
    libWine/c_857.c \
    libWine/c_860.c \
    libWine/c_861.c \
    libWine/c_862.c \
    libWine/c_863.c \
    libWine/c_864.c \
    libWine/c_865.c \
    libWine/c_866.c \
    libWine/c_869.c \
    libWine/c_874.c \
    libWine/c_875.c \
    libWine/c_878.c \
    libWine/c_932.c \
    libWine/c_936.c \
    libWine/c_949.c \
    libWine/c_950.c \
    libWine/c_1006.c \
    libWine/c_1026.c \
    libWine/c_1250.c \
    libWine/c_1251.c \
    libWine/c_1252.c \
    libWine/c_1253.c \
    libWine/c_1254.c \
    libWine/c_1255.c \
    libWine/c_1256.c \
    libWine/c_1257.c \
    libWine/c_1258.c \
    libWine/c_1361.c \
    libWine/c_10000.c \
    libWine/c_10006.c \
    libWine/c_10007.c \
    libWine/c_10029.c \
    libWine/c_10079.c \
    libWine/c_10081.c \
    libWine/c_20127.c \
    libWine/c_20866.c \
    libWine/c_20932.c \
    libWine/c_21866.c \
    libWine/c_28591.c \
    libWine/c_28592.c \
    libWine/c_28593.c \
    libWine/c_28594.c \
    libWine/c_28595.c \
    libWine/c_28596.c \
    libWine/c_28597.c \
    libWine/c_28598.c \
    libWine/c_28599.c \
    libWine/c_28600.c \
    libWine/c_28603.c \
    libWine/c_28604.c \
    libWine/c_28605.c \
    libWine/c_28606.c \
    libWine/version.c

DLLS += wined3d

wined3d_TEMPLATE     = VBOXGUESTR3NPDLL
wined3d_DEFS         = __WINESRC__ WINE_UNICODE_API="" _REENTRANT WINE_NOWINSOCK USE_WIN32_OPENGL __i386__ _USE_MATH_DEFINES\
    DLLDIR=\"\" \
    BINDIR=\"\" \
    LIB_TO_BINDIR=\"\" \
    LIB_TO_DLLDIR=\"\" \
    BIN_TO_DLLDIR=\"\" \
    LIB_TO_DATADIR=\"\" \
    BIN_TO_DATADIR=\"\"
ifneq ($(KBUILD_TYPE),debug)
 wined3d_DEFS        += WINE_NO_DEBUG_MSGS
endif
wined3d_INCS         := $(PATH_SUB_CURRENT)/include
wined3d_SOURCES      := \
    wined3d/arb_program_shader.c \
    wined3d/ati_fragment_shader.c \
    wined3d/baseshader.c \
	wined3d/basetexture.c \
	wined3d/buffer.c \
    wined3d/clipper.c \
    wined3d/context.c \
    wined3d/cubetexture.c \
    wined3d/device.c \
    wined3d/directx.c \
    wined3d/drawprim.c \
	wined3d/gl_compat.c \
    wined3d/glsl_shader.c \
    wined3d/nvidia_texture_shader.c \
    wined3d/palette.c \
    wined3d/query.c \
    wined3d/resource.c \
	wined3d/shader.c \
	wined3d/shader_sm1.c \
	wined3d/shader_sm4.c \
    wined3d/state.c \
    wined3d/stateblock.c \
    wined3d/surface_base.c \
    wined3d/surface.c \
    wined3d/surface_gdi.c \
    wined3d/swapchain.c \
    wined3d/swapchain_gdi.c \
    wined3d/swapchain_base.c \
    wined3d/texture.c \
    wined3d/utils.c \
    wined3d/vertexdeclaration.c \
	wined3d/view.c \
    wined3d/volume.c \
    wined3d/volumetexture.c \
    wined3d/wined3d_main.c \
    wined3d/wined3d.def
wined3d_LIBS = \
    $(PATH_LIB)/libWine$(VBOX_SUFF_LIB)

DLLS += VBoxD3D8

VBoxD3D8_TEMPLATE     = VBOXGUESTR3NPDLL
VBoxD3D8_DEFS         = __WINESRC__ WINE_UNICODE_API="" _REENTRANT WINE_NOWINSOCK USE_WIN32_OPENGL __i386__ _USE_MATH_DEFINES\
    DLLDIR=\"\" \
    BINDIR=\"\" \
    LIB_TO_BINDIR=\"\" \
    LIB_TO_DLLDIR=\"\" \
    BIN_TO_DLLDIR=\"\" \
    LIB_TO_DATADIR=\"\" \
    BIN_TO_DATADIR=\"\"
ifneq ($(KBUILD_TYPE),debug)
 VBoxD3D8_DEFS       += WINE_NO_DEBUG_MSGS
endif
VBoxD3D8_INCS         := $(PATH_SUB_CURRENT)/include
VBoxD3D8_SOURCES      := \
    d3d8/cubetexture.c \
    d3d8/d3d8_main.c \
    d3d8/device.c \
    d3d8/directx.c \
    d3d8/indexbuffer.c \
    d3d8/pixelshader.c \
    d3d8/surface.c \
    d3d8/swapchain.c \
    d3d8/texture.c \
    d3d8/vertexbuffer.c \
    d3d8/vertexdeclaration.c \
    d3d8/vertexshader.c \
    d3d8/volume.c \
    d3d8/volumetexture.c \
    d3d8/d3d8.def
VBoxD3D8_LIBS = \
    $(PATH_LIB)/libWine$(VBOX_SUFF_LIB) \
    $(PATH_LIB)/wined3d$(VBOX_SUFF_LIB)

DLLS += VBoxD3D9

VBoxD3D9_TEMPLATE     = VBOXGUESTR3NPDLL
VBoxD3D9_DEFS         = __WINESRC__ WINE_UNICODE_API="" _REENTRANT WINE_NOWINSOCK USE_WIN32_OPENGL __i386__ _USE_MATH_DEFINES\
    DLLDIR=\"\" \
    BINDIR=\"\" \
    LIB_TO_BINDIR=\"\" \
    LIB_TO_DLLDIR=\"\" \
    BIN_TO_DLLDIR=\"\" \
    LIB_TO_DATADIR=\"\" \
    BIN_TO_DATADIR=\"\"
ifneq ($(KBUILD_TYPE),debug)
 VBoxD3D9_DEFS       += WINE_NO_DEBUG_MSGS
endif
VBoxD3D9_INCS         := $(PATH_SUB_CURRENT)/include
VBoxD3D9_SOURCES      := \
    d3d9/cubetexture.c \
    d3d9/d3d9_main.c \
    d3d9/device.c \
    d3d9/directx.c \
    d3d9/indexbuffer.c \
    d3d9/pixelshader.c \
    d3d9/query.c \
    d3d9/stateblock.c \
    d3d9/surface.c \
    d3d9/swapchain.c \
    d3d9/texture.c \
    d3d9/vertexbuffer.c \
    d3d9/vertexdeclaration.c \
    d3d9/vertexshader.c \
    d3d9/volume.c \
    d3d9/volumetexture.c \
    d3d9/d3d9.def
VBoxD3D9_LIBS = \
    $(PATH_LIB)/libWine$(VBOX_SUFF_LIB) \
    $(PATH_LIB)/wined3d$(VBOX_SUFF_LIB)

DLLS += d3d8
d3d8_TEMPLATE      = VBOXGUESTR3NPDLL
d3d8_DEFS          = __i386__
d3d8_INCS         := $(PATH_SUB_CURRENT)/include
d3d8_SOURCES       =  \
    switcher/d3d8_main.c \
    switcher/sw_common.c \
    switcher/sw_d3d8.def \
    switcher/d3d8.rc

DLLS += d3d9
d3d9_TEMPLATE      = VBOXGUESTR3NPDLL
d3d9_DEFS          = __i386__
d3d9_INCS         := $(PATH_SUB_CURRENT)/include
d3d9_SOURCES       =  \
    switcher/d3d9_main.c \
    switcher/sw_common.c \
    switcher/sw_d3d9.def \
    switcher/d3d9.rc

include $(KBUILD_PATH)/subfooter.kmk
