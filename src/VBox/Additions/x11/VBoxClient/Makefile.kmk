# $Id$
## @file
# Sub-Makefile for the VirtualBox Guest Addition X11 Client.
#

#
# Copyright (C) 2006-2007 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

SUB_DEPTH = ../../../../..
include	$(KBUILD_PATH)/subheader.kmk

#
# VBoxClient - clipboard and seamless.
#
PROGRAMS += VBoxClient

VBoxClient_TEMPLATE = VBOXGUESTR3CPPEXE
VBoxClient_DEFS += VBOX_X11_CLIPBOARD VBOX_WITH_HGCM
ifdef VBOX_WITH_DBUS
 VBoxClient_DEFS += VBOX_WITH_DBUS
endif
VBoxClient_DEFS.linux += _GNU_SOURCE
VBoxClient_SOURCES = \
	main.cpp
VBoxClient_SOURCES += \
  	$(PATH_ROOT)/src/VBox/GuestHost/SharedClipboard/clipboard-helper.cpp \
  	$(PATH_ROOT)/src/VBox/GuestHost/SharedClipboard/x11-clipboard.cpp \
	clipboard.cpp
VBoxClient_LIBPATH = \
	$(VBOX_LIBPATH32_X11)
VBoxClient_LIBS.freebsd = \
	iconv
VBoxClient_LIBS.linux = \
	dl
VBoxClient_LIBS.solaris = \
	dl
VBoxClient_LIBS = \
	$(VBOX_LIB_IPRT_GUEST_R3) \
	$(VBOX_LIB_VBGL_R3) \
	$(VBOX_LIB_IPRT_GUEST_R3) \
	X11 \
	Xrandr \
	Xt
# These are static replacements for gcc-specific parts of libstdc++
VBoxClient_LIBS += \
	supc++ \
	gcc_eh
ifdef VBOX_X11_SEAMLESS_GUEST
 VBoxClient_DEFS += SEAMLESS_GUEST DYNAMIC_RESIZE
 VBoxClient_SOURCES += \
	seamless.cpp \
	seamless-host.cpp \
	seamless-x11.cpp \
	thread.cpp \
	display.cpp \
	hostversion.cpp
 VBoxClient_LIBS += \
	Xext Xmu
endif
ifdef VBOX_WITH_GUEST_PROPS
 VBoxClient_DEFS += VBOX_WITH_GUEST_PROPS
endif
#
# Link aginst libstdc++.a. (http://www.trilithium.com/johan/2005/06/static-libstdc/).
# (It would've been preferred to avoid features depending on libstdc++, of course...)
#
# Actually, this is darn annoying and will *NOT* be tolerated for any new code!
#
VBoxClient_LIBPATH  += $(PATH_VBoxClient)
VBoxClient_ORDERDEPS = $(PATH_VBoxClient)/libstdc++.a
VBoxClient_CLEAN     = $(PATH_VBoxClient)/libstdc++.a
$$(PATH_VBoxClient)/libstdc++.a:
	$(call MSG_L1,Forcing static libstdc++)
	$(QUIET)$(MKDIR) -p $(@D)
	$(QUIET)$(LN_EXT) -sf `$(TOOL_$(VBOX_GCC_TOOL)_CXX) $(TEMPLATE_VBOXGUESTR3CPPEXE_CXXFLAGS.$(KBUILD_TARGET_ARCH)) -print-file-name=libstdc++.a` $@ \
	     || $(CP_EXT) -f  `$(TOOL_$(VBOX_GCC_TOOL)_CXX) $(TEMPLATE_VBOXGUESTR3CPPEXE_CXXFLAGS.$(KBUILD_TARGET_ARCH)) -print-file-name=libstdc++.a` $@

ifdef VBOX_X11_SEAMLESS_GUEST
 if defined(VBOX_WITH_TESTCASES) && !defined(VBOX_ONLY_ADDITIONS) && !defined(VBOX_ONLY_SDK)
  if1of ($(KBUILD_TARGET), freebsd linux netbsd openbsd solaris)

# Set this in LocalConfig.kmk if you are working on the X11 clipboard service
# to automatically run the unit test at build time.
#       OTHERS += $(PATH_tstSeamlessX11-auto)/tstSeamlessX11-auto.run

   PROGRAMS += tstSeamlessX11-auto
   tstSeamlessX11-auto_TEMPLATE = VBOXR3TSTEXE
   tstSeamlessX11-auto_SOURCES = \
           testcase/tstSeamlessX11-auto.cpp \
           seamless-x11.cpp
   tstSeamlessX11-auto_DEFS = TESTCASE
   tstSeamlessX11-auto_LIBS = \
           $(LIB_RUNTIME)

   TESTING  += $(PATH_tstSeamlessX11-auto)/tstSeamlessX11-auto
$$(PATH_tstSeamlessX11-auto)/tstSeamlessX11-auto.run: \
        $$(INSTARGET_tstSeamlessX11-auto)
	export VBOX_LOG_DEST=nofile; $(INSTARGET_tstSeamlessX11-auto) quiet
	$(QUIET)$(APPEND) -t "$@" "done"

   #
   # Additional testcase designed to be run manually, which initiates and starts the Linux
   # guest client part of the seamless additions in the host, faking seamless events from
   # the host and writing information about guest events to standard output.
   #
   PROGRAMS += tstSeamlessX11
   tstSeamlessX11_TEMPLATE = VBOXR3TSTEXE
   tstSeamlessX11_SOURCES = \
           testcase/tstSeamlessX11.cpp \
           seamless-host.cpp \
           seamless-x11.cpp \
           thread.cpp
   tstSeamlessX11_LIBPATH = \
           $(VBOX_LIBPATH_X11)
   tstSeamlessX11_LIBS = \
           $(LIB_RUNTIME) \
           Xext \
           Xmu \
           X11
  endif
 endif
endif

include	$(KBUILD_PATH)/subfooter.kmk

