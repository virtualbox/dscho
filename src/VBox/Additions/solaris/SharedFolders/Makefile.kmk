# $Id$
## @file
# Sub-Makefile for the Solaris Shared folder kernel module.
#

#
# Copyright (C) 2008 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

SUB_DEPTH = ../../../../..
include	$(KBUILD_PATH)/subheader.kmk

ifneq ($(KBUILD_HOST),solaris)
$(error "The Solaris guest additions can only be built on Solaris!")
endif

#
# vboxfs - The Shared Folder Driver
#
SYSMODS.solaris      += vboxfs
vboxfs_TEMPLATE      = VBOXGUESTR0
vboxfs_DEFS          = VBOX_WITH_HGCM VBOX_SVN_REV=$(VBOX_SVN_REV)
vboxfs_DEPS         += $(VBOX_SVN_REV_KMK)
vboxfs_INCS         := \
	.
vboxfs_SOURCES       = \
	vboxfs_vfs.c \
	vboxfs_vnode.c \
	vboxfs_prov.c
vboxfs_LIBS          = \
	$(VBOX_LIB_VBGL_R0) \
	$(VBOX_LIB_IPRT_GUEST_R0)
vboxfs_LDFLAGS      += -N drv/vboxguest


ifndef VBOX_OSE
#
# vboxfs_s10 - The Shared Folder Driver for Solaris 10
#
SYSMODS.solaris      += vboxfs_s10
vboxfs_s10_TEMPLATE      = VBOXGUESTR0
vboxfs_s10_DEFS          = VBOX_WITH_HGCM VBOX_VFS_SOLARIS_10U6 VBOX_SVN_REV=$(VBOX_SVN_REV)
vboxfs_s10_DEPS         += $(VBOX_SVN_REV_KMK)
vboxfs_s10_INCS         := \
	solaris10/ \
	.
vboxfs_s10_SOURCES       = \
	vboxfs_vfs.c \
	vboxfs_vnode.c \
	vboxfs_prov.c
vboxfs_s10_LIBS          = \
	$(VBOX_LIB_VBGL_R0) \
	$(VBOX_LIB_IPRT_GUEST_R0)
vboxfs_s10_LDFLAGS      += -N drv/vboxguest
endif # VBOX_OSE


#
# mount - Userland mount wrapper for vboxfs
#
PROGRAMS                += vboxfsmount
vboxfsmount_TEMPLATE    = VBOXGUESTR3EXE
vboxfsmount_SOURCES     = vboxfs_mount.c

include	$(KBUILD_PATH)/subfooter.kmk

