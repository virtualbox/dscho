# $Id$
## @file
# Sub-Makefile for the device testcases.
#

#
# Copyright (C) 2006-2010 Sun Microsystems, Inc.
#
# This file is part of VirtualBox Open Source Edition (OSE), as
# available from http://www.virtualbox.org. This file is free software;
# you can redistribute it and/or modify it under the terms of the GNU
# General Public License (GPL) as published by the Free Software
# Foundation, in version 2 as it comes in the "COPYING" file of the
# VirtualBox OSE distribution. VirtualBox OSE is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY of any kind.
#
# Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa
# Clara, CA 95054 USA or visit http://www.sun.com if you need
# additional information or have any questions.
#

SUB_DEPTH = ../../../..
include	$(KBUILD_PATH)/subheader.kmk

#
# Globals
#
VBOX_PATH_DEVICES_SRC ?= $(PATH_ROOT)/src/VBox/Devices
VBOX_DEVICES_TEST_OUT_DIR := $(PATH_TARGET)/Devices/testcase
BLDDIRS += $(VBOX_DEVICES_TEST_OUT_DIR)

#
# We setup one 'other' target for executing the structure & alignment
# validation testcases. Perhaps a bit hackish, but extremely useful.
#
ifeq ($(KBUILD_TARGET),$(KBUILD_HOST))
 ifeq ($(filter-out x86.x86 amd64.amd64 x86.amd64, $(KBUILD_TARGET_ARCH).$(KBUILD_HOST_ARCH)),)
  OTHERS += \
	$(VBOX_DEVICES_TEST_OUT_DIR)/tstDeviceStructSize.run
 endif
endif

# The normal testing pass.
TESTING += \
	$(VBOX_DEVICES_TEST_OUT_DIR)/tstDeviceStructSize.run


ifdef VBOX_WITH_RAW_MODE
 #
 # The testcase generator.
 #
 PROGRAMS += tstDeviceStructSizeRC
 tstDeviceStructSizeRC_TEMPLATE  = VBoxRcExe
 tstDeviceStructSizeRC_DEFS      = VBOX_WITH_RAW_MODE
 ifdef VBOX_WITH_USB
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_USB
 endif
 ifdef VBOX_WITH_AHCI
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_AHCI
 endif
 ifdef VBOX_WITH_E1000
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_E1000
 endif
 ifdef VBOX_WITH_VIRTIO
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_VIRTIO
 endif
 ifdef VBOX_WITH_BUSLOGIC
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_BUSLOGIC
 endif
 ifdef VBOX_WITH_SCSI
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_SCSI
 endif
 ifdef VBOX_WITH_LSILOGIC
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_LSILOGIC
 endif
 ifdef VBOX_WITH_HPET
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_HPET
 endif
 ifdef VBOX_WITH_HGSMI
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_HGSMI
 endif
 ifdef VBOX_WITH_VIDEOHWACCEL
  tstDeviceStructSizeRC_DEFS    += VBOX_WITH_VIDEOHWACCEL
 endif
 tstDeviceStructSizeRC_SOURCES   = tstDeviceStructSizeRC.cpp
 tstDeviceStructSizeRC_INCS      = \
 	$(VBOX_PATH_DEVICES_SRC) \
 	$(VBOX_PATH_DEVICES_SRC)/Bus \
 	$(VBOX_DEVICES_TEST_OUT_DIR)
endif # VBOX_WITH_RAW_MODE

#
# The testcase it self.
#
PROGRAMS += tstDeviceStructSize
tstDeviceStructSize_TEMPLATE = VBOXR3AUTOTST
tstDeviceStructSize_DEFS     =
ifdef VBOX_WITH_USB
 tstDeviceStructSize_DEFS   += VBOX_WITH_USB
endif
ifdef VBOX_WITH_AHCI
 tstDeviceStructSize_DEFS   += VBOX_WITH_AHCI
endif
ifdef VBOX_WITH_E1000
 tstDeviceStructSize_DEFS   += VBOX_WITH_E1000
endif
ifdef VBOX_WITH_VIRTIO
 tstDeviceStructSize_DEFS   += VBOX_WITH_VIRTIO
endif
ifdef VBOX_WITH_BUSLOGIC
 tstDeviceStructSize_DEFS   += VBOX_WITH_BUSLOGIC
endif
ifdef VBOX_WITH_LSILOGIC
 tstDeviceStructSize_DEFS   += VBOX_WITH_LSILOGIC
endif
ifdef VBOX_WITH_HPET
 tstDeviceStructSize_DEFS   += VBOX_WITH_HPET
endif
ifdef VBOX_WITH_SMC
 tstDeviceStructSize_DEFS   += VBOX_WITH_SMC
endif
ifdef VBOX_WITH_LPC
 tstDeviceStructSize_DEFS   += VBOX_WITH_LPC
endif
ifdef VBOX_WITH_HGSMI
 tstDeviceStructSize_DEFS   += VBOX_WITH_HGSMI
endif
ifdef VBOX_WITH_VIDEOHWACCEL
 tstDeviceStructSize_DEFS   += VBOX_WITH_VIDEOHWACCEL
endif
ifdef VBOX_WITH_RAW_MODE
 tstDeviceStructSize_DEFS   += VBOX_WITH_RAW_MODE
endif
tstDeviceStructSize_INCS     = \
	$(VBOX_PATH_DEVICES_SRC) \
	$(VBOX_PATH_DEVICES_SRC)/Bus \
	$(VBOX_DEVICES_TEST_OUT_DIR)
tstDeviceStructSize_SOURCES  = tstDeviceStructSize.cpp
tstDeviceStructSize_CLEAN    = \
	$(VBOX_DEVICES_TEST_OUT_DIR)/tstDeviceStructSizeRC.h \
	$(VBOX_DEVICES_TEST_OUT_DIR)/tstDeviceStructSize.run
ifdef VBOX_WITH_RAW_MODE
 tstDeviceStructSize.cpp_DEPS = $(VBOX_DEVICES_TEST_OUT_DIR)/tstDeviceStructSizeRC.h
endif

#
# Run rule for tstDeviceStructSize.
#

# 1. Dump selected structure in the VMMGC.gc debug info.
# 2. Generate a testcase from the dump
## future

ifdef VBOX_WITH_RAW_MODE
# 1&2. Manually dump selected structures and members.
$(VBOX_DEVICES_TEST_OUT_DIR)/tstDeviceStructSizeRC.h: $$(INSTARGET_tstDeviceStructSizeRC) | $$(dir $$@)
	$(call MSG_GENERATE,,$@)
	$(QUIET)$(REDIRECT) -wo $@ -- $^
endif

# 3. run it.
$(VBOX_DEVICES_TEST_OUT_DIR)/tstDeviceStructSize.run: $$(INSTARGET_tstDeviceStructSize) | $$(dir $$@)
	$(QUIET)$(RM) -f $@
	$^
	$(QUIET)$(APPEND) "$@" "done"

# alias for the struct test.
run-struct-tests: $(VBOX_DEVICES_TEST_OUT_DIR)/tstDeviceStructSize.run


include	$(KBUILD_PATH)/subfooter.kmk

